<?php defined('BASEPATH') or exit('No direct script access allowed');
class Aplicaciones extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('usuarios_model');
        //$this->load->model('login_model');		
        $this->load->database('default');
		$this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form'));
		
		/*if(!$this->session->userdata('id_usuario') || $this->session->userdata('id_usuario') == ''){
			$this->session->sess_destroy();
			redirect(base_url());
		}*/
    }

     public function index()
    {
    	$this->load->model("aplicaciones_model");
		$this->session->set_userdata('controller','Expendedor droga','controller');
        $data["controller"]=$this->session->userdata('controller');
        $data['id_usuario']=$this->session->userdata('id_persona');	
        
        $data['info'] = $this->aplicaciones_model->mis_aplicaciones();


        $data['contenido'] = 'cargar_formulario'; 
        //$data["menu"] = "adminmenu";
       
        
        $this->load->view('templates/layout',$data);
    	
		
	}

	public function save_apliaciones(){
		header('Content-Type: application/json');
        $this->load->model("aplicaciones_model");
        $data = array();
               	
       	$msj = "El registro se insertó exitosamente.";
        
        $aplicacion = $this->input->post('aplicacion');

        $registro=$this->aplicaciones_model->saveAplicaciones();
        if(!$registro){
        	 $data["result"] = true;
            $this->session->set_flashdata('retornoExito', $msj);
        }else{
        	$data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
        }

        /*if ($idActividad = $this->aplicaciones_model->saveAplicaciones()) {
            //$this->capitulo1->insertarControl();
            echo "Aqui";
            $data["result"] = true;
            $this->session->set_flashdata('retornoExito', $msj);
        } else {
        	echo "Nooo";
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
        }*/
        

        echo json_encode($data);
	}

    public function ver_registros()
    {
    	$this->load->model("aplicaciones_model");
		$this->session->set_userdata('controller','Expendedor droga','controller');
        $data["controller"]=$this->session->userdata('controller');
        $data['id_usuario']=$this->session->userdata('id_persona');	
        
        $data['info'] = $this->aplicaciones_model->mis_aplicaciones();


        $data['contenido'] = 'aplicaciones'; 
        //$data["menu"] = "adminmenu";
       
        
        $this->load->view('templates/layout',$data);
    	
		
	}

	//Método para cargar el formuario de registro
    public function cargar_formulario(){
    	//header("Content-Type: text/plain; charset=utf-8"); //Para evitar problemas de acentos
       
        $this->load->model("aplicaciones_model");
       
        $data['id_usuario']=$this->session->userdata('id_persona');	
        $data['id_persona']=$this->session->userdata('idpersona'); 
        $data['information'] = FALSE;
        if($this->input->post('idRow')&&$this->input->post('idRow')=='persona'){
            $usuario= $data['id_persona'];
        }else{
             $usuario= $data['id_usuario'];
        }
       	$data['information'] = $this->expendedor->consulta_persona($usuario);
       	$data['nivel_educativo'] = $this->expendedor->consulta_nivel();
        $this->load->view("editar_personas", $data);
        //$this->load->view('templates/layout_expendedor',$data);
    }

	public function logout_ci()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

	
}