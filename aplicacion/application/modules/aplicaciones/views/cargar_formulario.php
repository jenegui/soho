<script type="text/javascript" src="<?php echo base_url("assets/js/expendedor/validate/expendedor.js"); ?>"></script>
<div class="modal-header">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4 class="list-group-item-heading">
				<i class="fa fa-gear fa-fw"></i> &nbsp;  REGISTRAR APLICACIONES
			</h4>
		</div>
	</div>
</div>
<br>
	<?php
	$retornoExito = $this->session->flashdata('retornoExito');
	if ($retornoExito) {
		?>
		<div class="col-lg-12">	
			<div class="alert alert-success ">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<?php echo $retornoExito ?>		
			</div>
		</div>
		<?php
	}

	$retornoError = $this->session->flashdata('retornoError');
	if ($retornoError) {
		?>
		<div class="col-lg-12">	
			<div class="alert alert-danger ">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<?php echo $retornoError ?>
			</div>
		</div>
		<?php
	}
	?>

<div class="modal-body">
	<p class="text-danger text-left">Los campos con * son obligatorios.</p>
	<form name="form" id="form" role="form" method="post">
		<input type="hidden" id="hddId" name="hddId" value="x">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group text-left">
					<label class="control-label" for="aplicacion">Nombre de la aplicación *</label>
					<input type="text" id="aplicacion" name="aplicacion" class="form-control" value="" placeholder="Digite el nombre de la aplicación" required>
				</div>
			</div>
			
		</div>
		
		<div class="form-group">
			<div class="row" align="center">
				<div style="width:50%;" align="center">
					<input type="button" id="btnSubmit" name="btnSubmit" value="Guardar" class="btn btn-primary"/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div id="div_load" style="display:none">
				<div class="progress progress-striped active">
					<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
						<span class="sr-only">45% completado</span>
					</div>
				</div>
			</div>
			<div id="div_error" style="display:none">
				<div class="alert alert-danger"><span class="glyphicon glyphicon-remove" id="span_msj">Ya existe un registro con los datos ingresados.</span></div>
			</div>
		</div>

	</form>
</div>