<div id="page-wrapper">
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="list-group-item-heading">
						<i class="fa fa-bookmark fa-fw"></i> APLICACIONES DESTACADAS
					</h4>
				</div>
			</div>
		</div>
		<!-- /.col-lg-12 -->				
	</div>
	
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-folder-open"></i> Lista de aplicaciones destacadas
				</div>
				<div class="panel-body">
					<!--<button type="button" class="btn btn-success btn-block registrar" data-toggle="modal" data-target="#modal">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Registrar aplicaciones
					</button><br>-->
					<br>
						<?php
						$retornoExito = $this->session->flashdata('retornoExito');
						if ($retornoExito) {
							?>
							<div class="col-lg-12">	
								<div class="alert alert-success ">
									<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									<?php echo $retornoExito ?>		
								</div>
							</div>
							<?php
						}

						$retornoError = $this->session->flashdata('retornoError');
						if ($retornoError) {
							?>
							<div class="col-lg-12">	
								<div class="alert alert-danger ">
									<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
									<?php echo $retornoError ?>
								</div>
							</div>
							<?php
						}

						if($info){	
						?> 
						
						<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
							<thead>
								<tr>
									<th class="text-center">Id. aplicación</th>
									<th class="text-center">Nombres aplicación</th>
									<th class="text-center">Fecha</th>
								</tr>
							</thead>
							<tbody>							
								<?php
								for($i=0; $i<=count($info)-1; $i++){
									//echo "MMM".$info ."as". $lista;
									echo "<tr>";
										echo "<td>&nbsp;" . $info[$i]['id_proyecto'] . "</td>";
										echo "<td class='text-center'>" . $info[$i]['nombre_proyecto'] . "</td>";
										echo "<td class='text-center'>" . $info[$i]['fecha'] . "</td>";
									echo "</tr>";
								}
								?>
							</tbody>
						</table>

					<?php } ?>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	
</div>
	<!-- /#page-wrapper -->


<!--INICIO Modal para adicionar HAZARDS -->
<div class="modal fade text-center" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">    
	<div class="modal-dialog" role="document">
		<div class="modal-content" id="tablaDatos">

		</div>
	</div>
</div>                       
	<!--FIN Modal para adicionar HAZARDS -->

	