<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 */
class aplicaciones_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function aceptarTerminos($id_persona) {

        $data = array(
            'fecha_terminos' => date('Y-m-d')
        );
        $this->db->where('id_persona', $id_persona);
        return $this->db->update('usuarios', $data);
    }

    /**
     * Verifica trámite
     * @since  21/01/2020
     */
    public function verifyTramite($arrData) 
    {
        $this->db->where($arrData["column"], $arrData["value"]);
        $query = $this->db->get("expdrogas_tramite");

        if ($query->num_rows() >= 1) {
            return true;
        } else{ 
            return false; 
        }
        $this->db->close();
    }

    //Rgistra el támite de expendedor de droga
    public function registrarTramite($param) {
        $this->db->trans_start();
        $this->db->insert('expdrogas_tramite', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function insertarArchivo($param) {
        $this->db->trans_start();
        $this->db->insert('archivos', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function consulta_tramite($id_usuario){
        $cadena_sql = " SELECT
                        id_expdrogas_tramite,
                        id_usuario,
                        id_estado,
                        fecha_registro,
                        observaciones
                    FROM expdrogas_tramite
                    WHERE id_usuario = ".$id_usuario."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

    public function consulta_persona($id_usuario){
        $cadena_sql = " SELECT
                        id_persona,
                        nume_identificacion,
                        p_nombre,
                        s_nombre,
                        p_apellido,
                        s_apellido,
                        email,
                        telefono_fijo,
                        telefono_celular,
                        nacionalidad,
                        departamento,
                        ciudad_nacimiento,
                        ciudad_resi,
                        dire_resi,
                        fecha_nacimiento,
                        edad,
                        sexo,
                        etnia,
                        estado_civil,
                        nivel_educativo
                    FROM persona
                    WHERE id_persona = ".$id_usuario."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }               

    public function registrarSeguimiento($param) {
        $this->db->trans_start();
        $this->db->insert('expdrogas_historico_tramite', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
        /*$str = $this->db->last_query();

        echo "<pre>";
        print_r($str);
        exit;*/
        $this->db->close();
    }

     /**
     * Obtiene la lista del nivel educativo.
     * @author sjneirag
     * @since  06/2020
     */
    function mis_aplicaciones(){
        $nivel = array();
        $sql = "SELECT id_proyecto, nombre_proyecto, fecha
        FROM proyectos
        ORDER BY id_proyecto DESC ";
         
        $query = $this->db->query($sql);
        
        if ($query->num_rows()>0){
            $i=0;
            foreach($query->result() as $row){
                $nivel[$i]["id_proyecto"] = $row->id_proyecto;
                $nivel[$i]["nombre_proyecto"] = $row->nombre_proyecto;
                $nivel[$i]["fecha"] = $row->fecha;
         $i++;
            }
        }
        $this->db->close();
        return $nivel;
    }

    public function saveAplicaciones()
    {
        
        $data = array(
            'nombre_proyecto' => $this->input->post('aplicacion'),
            'fecha' =>  $fecha_registro = date('Y-m-d')
        );
       
        $query = $this->db->insert('proyectos', $data);
                
        /*$str = $this->db->last_query();
        echo "<pre>";
        print_r($str);
        exit;*/
        
        $this->db->close();
    }

      	

}
