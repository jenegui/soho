<?php defined('BASEPATH') or exit('No direct script access allowed');
class Expendedor_droga extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('usuarios_model');
        $this->load->model('login_model');		
        $this->load->database('default');
		$this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form'));
		
		if(!$this->session->userdata('id_usuario') || $this->session->userdata('id_usuario') == ''){
			$this->session->sess_destroy();
			redirect(base_url());
		}
    }

    public function index()
    {
    	$this->load->model("expendedor");
		$this->session->set_userdata('controller','Expendedor droga','controller');
        $data["controller"]=$this->session->userdata('controller');
        $data['id_usuario']=$this->session->userdata('id_persona');	
        $data["nom_usuario"] = $nom_usuario;
        
        $data['contenido'] = 'cargar_documentos'; 
        //$data["menu"] = "adminmenu";
        //Valida la existencia de registro de trámite
        $arrParam = array(
            "column" => "id_usuario",
            "value" => $data['id_usuario']
        );
        $result_tramite = $this->expendedor->verifyTramite($arrParam);	
        if ($result_tramite) {
        	
        	/*$data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong></strong> El ciudadano ya tiene registrado un trámite para licencia de expendedor de droga.');*/
            redirect(base_url('expendedor_droga/verTramite/'), 'refresh');
        } 
        else {
        	$this->load->view('templates/layout_expendedor',$data);
    	}
		
	}

	//Método para cargar los ducumentos para témite de expendedor de drogas
	public function registrar_documentos(){
		$this->load->model("expendedor");

		$data['id_usuario']=$this->session->userdata('id_persona');	
		$data['id_estado']=1;	
		$data['fecha_registro']=date('Y-m-d H:i:s');

		$arrParam = array(
            "column" => "id_usuario",
            "value" => $data['id_usuario']
        );
        //Valida la existencia de registro de trámite
        $result_tramite = $this->expendedor->verifyTramite($arrParam);	
        if ($result_tramite) {
        	
        	$data["result"] = "error";
            $this->session->set_flashdata('retornoError', 'El ciudadano ya tiene registrado un trámite para licencia de expendedor de droga.'); 
            redirect(base_url('expendedor_droga/verTramite/'), 'refresh');
        } 
        else {
        	//Regista el trámite
			$registrarTramite = $this->expendedor->registrarTramite($data);
			//Consulta trámite
			$idtramite = $this->expendedor->consulta_tramite($data['id_usuario']);
			$dataTramite['id_tramite'] = $idtramite->id_expdrogas_tramite;
			$dataTramite['id_usuario']=$this->session->userdata('id_persona');	
			$dataTramite['id_estado']=1;
			$dataTramite['observaciones']=$this->input->get_post('observaciones');
			//Regista el seguimiento hostórico del trámite
			$registrarSeguimiento = $this->expendedor->registrarSeguimiento($dataTramite);

			/**
			Cargar archivos del equipo
			*/
								
			$bandera = 1;
			$documentos = array("cedula","registro_civil", "tarjeta_reservista", "certificado_salud", "antecedentes_judiciales","certificado_vecindad", "manisfestacion_expresa", "certificado_minsalud", "certificado_estudios");
			
			//Bucle para el conteo de los documento cargados
            for($i=0;$i<=count($documentos);$i++){
                //Vallida la rcecpción de archivos.
				if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {
                    
					$nombre_archivo = $data['id_usuario']."-".$documentos[$i]."-".date('YmdHis');
					
					$config['upload_path'] = "uploads/expendedor/";
					$config['allowed_types'] = 'pdf';
					$config['max_size'] = '35000';
					$config['file_name'] = $nombre_archivo;
					/* Fin Configuracion parametros para carga de archivos */
					
					// Cargue libreria
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					//var_dump($config);
					if ($this->upload->do_upload($documentos[$i])) {
						$upload_data = $this->upload->data();
						$rutaFinal = array('rutaFinal' => $this->upload->data());
					} else {
						$bandera = 0;
					}

					$datosAr['ruta'] = 'uploads/expendedor/';
					$datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
					$datosAr['fecha'] = date('Y-m-d');
					$datosAr['tags'] = "";
					$datosAr['es_publico'] = 1;
                    $datosAr['id_persona'] = $data['id_usuario'];
                    $datosAr['condicion'] = 0;
                    $datosAr['estado'] = 'AC';

					$resultadoIDDocumentoArc = $this->expendedor->insertarArchivo($datosAr);
									
				}
			}

			$this->session->set_flashdata('retornoExito', 'El trámite se registró exitosamente.</b>');
			redirect(base_url('expendedor_droga/verTramite/'), 'refresh');
			exit;
		}
	}

	//Método para mostrar los trámites registrados, en este caso uno
	public function verTramite(){
		$this->load->model("expendedor");
		$data['id_usuario']=$this->session->userdata('id_persona');	
        $data['info'] = $this->expendedor->mis_tramites($data['id_usuario']);
        $data['archivos'] = $this->expendedor->consulta_archivos($data['id_usuario']);
		//echo "MMM".$data['archivos'][0]['id_archivo'];     
        $data['titulo'] = 'Licencias Expendedor de drogas';
        
        $data['contenido'] = 'mi_tramite';
        $this->load->view('templates/layout_expendedor',$data);


    }

    //Método para editar los datos personales del ciudadano
    public function editar_tramite(){
    	//header("Content-Type: text/plain; charset=utf-8"); //Para evitar problemas de acentos
        $this->load->model("expendedor");

        $data['id_usuario']=$this->session->userdata('id_persona');	
        $data['id_persona']=$this->session->userdata('idpersona'); 
        $data['information'] = FALSE;
        if($this->input->post('idRow')&&$this->input->post('idRow')=='persona'){
            $usuario= $data['id_persona'];
        }else{
             $usuario= $data['id_usuario'];
        }
       	$data['information'] = $this->expendedor->consulta_persona($usuario);
       	$data['nivel_educativo'] = $this->expendedor->consulta_nivel();
        $this->load->view("editar_personas", $data);
        //$this->load->view('templates/layout_expendedor',$data);
    }

    //Método para guardar los camibos de los datos personales
    public function save_persona(){
    	header('Content-Type: application/json');
        $this->load->model("expendedor");
        $data = array();
        $idusuario = $this->session->userdata('id_persona');
       	
       	$msj = "Se actualiz&oacute; el registro exitosamente.";
        
        $documento = $this->input->post('documento');

        if ($idActividad = $this->expendedor->savePersona($idusuario)) {
            //$this->capitulo1->insertarControl();
            $data["result"] = true;
            $this->session->set_flashdata('retornoExito', $msj);
        } else {
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
        }
        

        echo json_encode($data);
    }

    //Método para ver los trámites pendientes
    public function tramites_pendientes(){
       $this->load->model("expendedor");
        $data['id_usuario']=$this->session->userdata('id_persona'); 
        $data['info'] = $this->expendedor->get_tramites();

        $data['archivos'] = $this->expendedor->consulta_archivos($data['id_usuario']);
        //echo "MMM".$data['archivos'][0]['id_archivo'];     
        $data['titulo'] = 'Licencias Expendedor de drogas';
        
        $data['contenido'] = 'lista_tramites';
        $this->load->view('templates/layout_expendedor',$data);
    }

    //Método para ver los trámites seleccinados de la lista
    public function aprobar_tramite($id_persona){
        $this->load->model("expendedor");
        $data['id_usuario']=$this->session->userdata('id_persona');  
        $data['info'] = $this->expendedor->mis_tramites($id_persona);
        $data['estados'] = $this->expendedor->consulat_estados();
        $data['archivos'] = $this->expendedor->consulta_archivos($id_persona);
        $this->session->set_userdata('idpersona',$id_persona);
       
        if($this->session->userdata('perfil')==5){
            $data['boton']="Aprobar y Firmar";
        }else{
            $data['boton']="Aprobar";
        }
        $data['titulo'] = 'Licencias Expendedor de drogas';
        
        $data['contenido'] = 'aprobar_tramite_view';
        $this->load->view('templates/layout_expendedor',$data);
    }

    //Método para aprobar validador
    public function save_aprobar_validador(){
        header('Content-Type: application/json');
        $this->load->model("expendedor");
        $data = array();
        $idpersona = $this->session->userdata('idpersona');
       
        $msj = "Se aprobó el tramite satisfactoriamente.";
        //Registra en el histódiro de trámite
        if ($idTramite = $this->expendedor->save_historico($idpersona)) {
            //Actualiza el trpamite
           $this->expendedor->actualizar_tramite($idpersona);
           //Consulta los archivos registardos por el ciudadano
           $dataarchivos['archivos']=$this->expendedor->consulta_archivos($idpersona);
           //Actualiza el estado de los archivos
            for($i=0; $i<=count( $dataarchivos['archivos'])-1; $i++){
               $this->expendedor->actualizar_archivo($dataarchivos['archivos'][$i]['id_archivo']);
            }
            //Si el pefil es validador
            if($this->session->userdata('perfil')==3){
                if($this->input->post('estado')==5 || $this->input->post('estado')==13){
                    $info = $this->expendedor->mis_tramites($idpersona);
                    //Asigno la información a un arreglo $datos_tramite
                    $datos_tramite['p_nombre']=$info[0]->p_nombre;
                    $datos_tramite['p_apellido']=$info[0]->p_apellido;
                    $datos_tramite['email']=$info[0]->email;
                    $datos_tramite['estado']=$info[0]->id_estado;
                    $datos_tramite['observaciones']=$info[0]->observaciones;
                    $this->enviarCorreo($datos_tramite);
                }
            }
            //Si el perfil es director
            if($this->session->userdata('perfil')==5){
                //si el trámite es probado
                if($this->input->post('estado')==4){
                    //Verifica la existencia de una resolución
                    $result_resolucion = $this->expendedor->verifyResolucion($idpersona,$this->input->post('estado'));  
                    if ($result_resolucion) {
                        $data["result"] = "error";
                        $this->session->set_flashdata('retornoError', '<strong></strong> El ciudadano ya tiene registrada una resolución de licencia de expendedor de droga.');
                        //redirect(base_url('expendedor_droga/editar_tramite/'), 'refresh');
                    }else{
                        //Genera código de verificación 
                        $codigo_verificacion = $this->genera_codigo();
                        //Registra la resolución en la bd
                        $result_save_resolucion = $this->expendedor->save_resolucion($idpersona, $codigo_verificacion,$this->input->post('estado'));
                        //Envía correo de notificación
                        if($result_save_resolucion){
                            //Consulta la información del ciudadano para el envío de correo
                            $info = $this->expendedor->mis_tramites($idpersona);
                            //Asigno la información a un arreglo $datos_tramite
                            $datos_tramite['p_nombre']=$info[0]->p_nombre;
                            $datos_tramite['p_apellido']=$info[0]->p_apellido;
                            $datos_tramite['email']=$info[0]->email;
                            $datos_tramite['estado']=$info[0]->id_estado;
                            //$datos_tramite['email']='sjneira@saludcapital.gov.co';
                            $this->enviarCorreo($datos_tramite);
                        }
                    }
                }elseif($this->input->post('estado')==7){
                    //Verifica la existencia de una resolución
                    $result_resolucion = $this->expendedor->verifyResolucion($idpersona,$this->input->post('estado'));  
                    if ($result_resolucion) {
                        $data["result"] = "error";
                        $this->session->set_flashdata('retornoError', '<strong></strong> El ciudadano ya tiene registrada una resolución de licencia de expendedor de droga.');
                        //redirect(base_url('expendedor_droga/editar_tramite/'), 'refresh');
                    }else{
                        //Genera código de verificación 
                        $codigo_verificacion = $this->genera_codigo();
                        //Registra la resolución en la bd
                        $result_save_resolucion = $this->expendedor->save_resolucion($idpersona, $codigo_verificacion,$this->input->post('estado'));
                        //Envía correo de notificación
                        if($result_save_resolucion){
                            //Consulta la información del ciudadano para el envío de correo
                            $info = $this->expendedor->mis_tramites($idpersona);
                            //Asigno la información a un arreglo $datos_tramite
                            $datos_tramite['p_nombre']=$info[0]->p_nombre;
                            $datos_tramite['p_apellido']=$info[0]->p_apellido;
                            $datos_tramite['email']=$info[0]->email;
                            $datos_tramite['estado']=$info[0]->id_estado;
                            $datos_tramite['observaciones']=$info[0]->observaciones;
                            $this->enviarCorreo($datos_tramite);
                        }
                    }
                }
            }

            $data["result"] = true;
            $this->session->set_flashdata('retornoExito', $msj);
        } else {
            $data["result"] = "error";
            $this->session->set_flashdata('retornoError', '<strong>Error!!!</strong> Contactarse con el administrador.');
        }
        
        echo json_encode($data);
    }

    //Método para descargar los PDF con las resoluciones de la credenciales o resolción de solicitud rechazada
    public function resoluciones(){
        $this->load->model("expendedor");
        $data['id_usuario']=$this->session->userdata('id_persona');
        $idpersona = $this->session->userdata('idpersona');
        $datos['info'] = $this->expendedor->mis_tramites($data['id_usuario']);
        $data['exp'] = $this->expendedor->consulta_resolucion($data['id_usuario'],4);
        $datos['nume_resolucion'] =$data['exp']->id_resolucion;
        $datos['codigo_veridicacion'] =$data['exp']->codigo_verificacion;
        //$datos['nume_resolucion'] =$data['info']->tipo_identificacion;
        $fechaExp=explode('-',$data['exp']->fecha_resolucion);
        $datos['anio']=$fechaExp[0];
        $datos['dia']=$fechaExp[2];
        $mes=$fechaExp[1];
        //Invoco la función meese para rescatar el nombre del mes
        $datos['mes']=$this->meses($mes);
        
        $ruta_archivos = "uploads/expendedor/";
        $nombre_archivo = "Credencial-".$idpersona."-".date('YmdHis').".pdf";

        //load mPDF library para linux
        $this->load->library('M_pdf2');
        //$mpdf = new mPDF('c', 'Letter', '', '', 10, 10, 30, 1, 5, 5 , 'P');
        $mpdf = new \Mpdf\Mpdf(['orientation' => 'P','margin_left' => 10,'margin_right' => 10,'margin_top' => 30,'margin_bottom' => 1,'margin_header' => 5,'margin_footer' => 5]);

        //load mPDF library para windows
        //require_once APPPATH . 'libraries/vendor/autoload.php';
        //$mpdf = new \Mpdf\Mpdf();
        $mpdf->debug = true;
        $mpdf->allow_output_buffering= true;
        $mpdf->showImageErrors = true;
        $mpdf->showWatermarkText = true;
        $imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
        $imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
        $mpdf->SetHTMLHeader("<table class='sinborde centro' width='100%' border='0'>
                                    <tr class='centro'> 
                                        <td width='100%'>
                                            <img src='".$imagenHeader."' width='250px'>
                                        </td>
                                    </tr>
                                </table>","O");
        $mpdf->SetHTMLFooter("<table class='sinborde centro' border='0' width='100%'>
                                    <tr class='centro'> 
                                        <td width='100%'>
                                            <img src='".$imagenFooter."' width='550px'>
                                        </td>
                                    </tr>
                                </table>","O"); 
        $mpdf->SetWatermarkText('Aprobado');
        $mpdf->AddPage();
        //echo "MMM";
       //echo $this->load->view('resolucion_aprobacion', $datos, true);
        $mpdf->WriteHTML($this->load->view('resolucion_aprobacion', $datos, true)); 
        $mpdf->Output();
        //$mpdf->Output($ruta_archivos . $nombre_archivo, "F");
    }

    //Método para cambiar el mes de número a string
    public function meses($mes){
        switch($mes){
            case '01': $nombremes = 'Enero'; break;
            case '02': $nombremes = 'Febrero'; break;
            case '03': $nombremes = 'Marzo'; break;
            case '04': $nombremes = 'Abril'; break;
            case '05': $nombremes = 'Mayo'; break;
            case '06': $nombremes = 'Junio'; break;
            case '07': $nombremes = 'Julio'; break;
            case '08': $nombremes = 'Agosto'; break;
            case '09': $nombremes = 'Septiembre'; break;
            case '10': $nombremes = 'Octubre'; break;
            case '11': $nombremes = 'Noviembre'; break;
            case '12': $nombremes = 'Diciembre'; break;
        }
        return $nombremes;
    }

    //Genera el código de verificación
    public function genera_codigo(){
        //Se define una cadena de caractares. Te recomiendo que uses esta.
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        //Obtenemos la longitud de la cadena de caracteres
        $longitudCadena = strlen($cadena);

        //Se define la variable que va a contener la contrase&ntilde;a
        $pass = "";
        //Se define la longitud de la contrase&ntilde;a, en mi caso 10, pero puedes poner la longitud que quieras
        $longitudPass = 10;

        //Creamos la contrase&ntilde;a
        for ($i = 1; $i <= $longitudPass; $i++) {
            //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
            $pos = rand(0, $longitudCadena - 1);

            //Vamos formando la contrase&ntilde;a en cada iteraccion del bucle, a&ntilde;adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
            $pass .= substr($cadena, $pos, 1);
        }
        return $pass;
    }

    //Método para envío de correo
    public function enviarCorreo($datos_tramite){
       
        require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
        $mail = new PHPMailer(true);

        try {
                $mail->IsSMTP(); // set mailer to use SMTP
                $mail->Host = "172.16.0.238"; // specif smtp server
                $mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
                $mail->Port = 25; // Used instead of 587 when only POP mail is selected
                $mail->SMTPAuth = false;
                $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
                $mail->Password = "Colombia2018"; // SMTP password
                $mail->FromName = "Secretaría Distrital de Salud";
                $mail->From = "contactenos@saludcapital.gov.co";
                //$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
                $mail->AddAddress($datos_tramite['email'], $datos_tramite['email']);
                //$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
                $mail->WordWrap = 50;
                $mail->CharSet = 'UTF-8';
                $mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
                $mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
                $mail->IsHTML(true); // set email format to HTML
                $mail->Subject = 'Solicitud de trámite licencia expendedor de droga';
                if($datos_tramite['estado']==4){
                    $html = '
                    <p>Señor(a)</p>
                    <p><b>' . $datos_tramite['p_nombre'] . ' ' . $datos_tramite['p_apellido'] . ',</b>
                    </p>
                    <p>Una vez realizado el proceso de validación de documentos del Trámite de Licencia de Expendedor de Droga, nos complace informar que su licencia de expendedor de droga fue APROBADA, favor ingrese a la plataforma <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> y  "Descargue allí su credencial" haga click en la imagen PDF para descargar la licencia.
                    </p>
                    
                    <p>Ante cualquier inquietud o novedad no dude consultar primeramente la documentación dispuesta en el portal de la Ventanilla Única de Trámites y Servicios o por medio del correo: contactenos@saludcapital.gov.co
                    </p>
                    <p><b>Secretaría Distrital de Salud.<br>
                        Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
                        <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Licencia de Exhumación<br>
                        Cra 32 #12-81 Bogotá D.C, Colombia<br>
                        Teléfono: (571) 3649090
                    </p>';
                }elseif($datos_tramite['estado']==7 || $datos_tramite['estado']==5 || $datos_tramite['estado']==13){
                    $html = '
                    <p>Señor(a)</p>
                    <p><b>' . $datos_tramite['p_nombre'] . ' ' . $datos_tramite['p_apellido'] . ',</b>
                    </p>
                    <p>Una vez realizado el proceso de validación de documentos del Trámite de licencia de expenedor de drogas, se encontró la siguiente inconsistencia por favor ingrese a la plataforma <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> y realice los ajustes correspondientes para continuar con su trámite:
                   </p>
                      <p>
                        '.$datos_tramite['observaciones'].'
                      </p>
                    
                    <p>Ante cualquier inquietud o novedad no dude consultar primeramente la documentación dispuesta en el portal de la Ventanilla Única de Trámites y Servicios o por medio del correo: contactenos@saludcapital.gov.co
                    </p>
                    <p><b>Secretaría Distrital de Salud.<br>
                        Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
                        <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Licencia de Exhumación<br>
                        Cra 32 #12-81 Bogotá D.C, Colombia<br>
                        Teléfono: (571) 3649090
                    </p>';
                }
                $mail->Body = nl2br ($html,false);

                $mail->Send();

        }catch (Exception $e){
            print_r($e->getMessage());
            exit;
        }
    }
}