<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 */
class expendedor extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function aceptarTerminos($id_persona) {

        $data = array(
            'fecha_terminos' => date('Y-m-d')
        );
        $this->db->where('id_persona', $id_persona);
        return $this->db->update('usuarios', $data);
    }

    /**
     * Verifica trámite
     * @since  21/01/2020
     */
    public function verifyTramite($arrData) 
    {
        $this->db->where($arrData["column"], $arrData["value"]);
        $query = $this->db->get("expdrogas_tramite");

        if ($query->num_rows() >= 1) {
            return true;
        } else{ 
            return false; 
        }
        $this->db->close();
    }

    //Rgistra el támite de expendedor de droga
    public function registrarTramite($param) {
        $this->db->trans_start();
        $this->db->insert('expdrogas_tramite', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function insertarArchivo($param) {
        $this->db->trans_start();
        $this->db->insert('archivos', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function consulta_tramite($id_usuario){
        $cadena_sql = " SELECT
                        id_expdrogas_tramite,
                        id_usuario,
                        id_estado,
                        fecha_registro,
                        observaciones
                    FROM expdrogas_tramite
                    WHERE id_usuario = ".$id_usuario."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

    public function consulta_persona($id_usuario){
        $cadena_sql = " SELECT
                        id_persona,
                        nume_identificacion,
                        p_nombre,
                        s_nombre,
                        p_apellido,
                        s_apellido,
                        email,
                        telefono_fijo,
                        telefono_celular,
                        nacionalidad,
                        departamento,
                        ciudad_nacimiento,
                        ciudad_resi,
                        dire_resi,
                        fecha_nacimiento,
                        edad,
                        sexo,
                        etnia,
                        estado_civil,
                        nivel_educativo
                    FROM persona
                    WHERE id_persona = ".$id_usuario."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }               

    public function registrarSeguimiento($param) {
        $this->db->trans_start();
        $this->db->insert('expdrogas_historico_tramite', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
        /*$str = $this->db->last_query();

        echo "<pre>";
        print_r($str);
        exit;*/
        $this->db->close();
    }

     /**
     * Obtiene la lista del nivel educativo.
     * @author sjneirag
     * @since  06/2020
     */
    function consulta_nivel(){
        $nivel = array();
        $sql = "SELECT IdNivelEducativo, Nombre
        FROM pr_niveleducativo
        ORDER BY IdNivelEducativo ASC ";
         
        $query = $this->db->query($sql);
        
        if ($query->num_rows()>0){
            $i=0;
            foreach($query->result() as $row){
                $nivel[$i]["IdNivelEducativo"] = $row->IdNivelEducativo;
                $nivel[$i]["Nombre"] = $row->Nombre;
         $i++;
            }
        }
        $this->db->close();
        return $nivel;
    }

     /**
     * Obtiene la lista de los estados de trámite.
     * @author sjneirag
     * @since  06/2020
     */
    function consulat_estados(){
       
        $estados = array();
        $sql = "SELECT id_estado, descripcion
        FROM pr_estado_tramite ";
        if($this->session->userdata('perfil')==3){
            $sql.= "WHERE id_estado IN (1,2,5,12,13) ";
        }else if($this->session->userdata('perfil')==4){
            $sql.= "WHERE id_estado IN (3,6,10,15,18) ";
        }else if($this->session->userdata('perfil')==5){
            $sql.= "WHERE id_estado IN (4,7) ";
        }
        $sql.="ORDER BY id_estado ASC ";
         
        $query = $this->db->query($sql);
        
        if ($query->num_rows()>0){
            $i=0;
            foreach($query->result() as $row){
                $estados[$i]["id_estado"] = $row->id_estado;
                $estados[$i]["descripcion"] = $row->descripcion;
         $i++;
            }
        }
        $this->db->close();
        return $estados;
    }

    function formatoFecha($fecha,$formato){
        $fecha = substr($fecha,0,10);
        switch($formato){
            case '-':  //Guardar en la Base de Datos
                       $arrayFecha = explode("-",$fecha);
                       $string = $arrayFecha[2]."/".$arrayFecha[1]."/".$arrayFecha[0];
                       break;
                      
            case '/':  //Recoger de la base de datos
                       $arrayFecha = explode('/',$fecha);
                       $string = $arrayFecha[2].'-'.$arrayFecha[1].'-'.$arrayFecha[0];
                       break;
                      
            case '\\': //Recoger de la base de datos
                       $arrayFecha = explode('\\',$fecha);
                       $string = $arrayFecha[2].'-'.$arrayFecha[1].'-'.$arrayFecha[0];
                       break;         
        }
        return $string;         
    }   

    /**
     * Actualiza datos personales 
     * @since 06/2020
     */
    public function savePersona($idusuario)
    {
        $idusuario = $this->session->userdata('id_persona');
        $fecha_nacimiento=$this->input->post('fecha_nacimiento');
        $newFecha_nacimiento=$this->expendedor->formatoFecha($fecha_nacimiento,'-');
        $data = array(
            'nume_identificacion' => $this->input->post('numiden'),
            'p_nombre' => $this->input->post('p_nombre'),
            's_nombre' => $this->input->post("s_nombre"),
            'p_apellido ' => $this->input->post('p_apellido'),
            's_apellido' => $this->input->post('s_apellido'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
            'email' => $this->input->post('email'),
            'telefono_celular' => $this->input->post('telefono_celular'),
            'telefono_fijo' => $this->input->post('telefono_fijo'),
            'dire_resi' => $this->input->post('dire_resi'),
            'nivel_educativo' => $this->input->post('nivel_educativo')
            
        );
        $this->db->where('id_persona', $idusuario);
        $query = $this->db->update('persona', $data);
        
        $this->db->close();
        $str = $this->db->last_query();
        /*echo "<pre>";
        print_r($str);
        exit;*/
        if ($query) {
            return $idActividad;
        } else {
            return false;
        }
    }

     /**
     * Obtiene la lista de archivos cargados.
     * @author sjneirag
     * @since  06/2020
     */
    function consulta_archivos($id_usuario){
        $archivos = array();
        $sql = "SELECT id_archivo, ruta, nombre, id_persona, condicion
        FROM archivos
        WHERE id_persona = ".$id_usuario."
        ORDER BY id_archivo ASC ";
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows()>0){
            $i=0;
            foreach($query->result() as $row){
                $archivos[$i]["id_archivo"] = $row->id_archivo;
                $archivos[$i]["ruta"] = $row->ruta;
                $archivos[$i]["nombre"] = $row->nombre;
                $archivos[$i]["id_persona"] = $row->id_persona;
                $archivos[$i]["condicion"] = $row->condicion;
            $i++;
            }
        }
        
        return $archivos;
        $this->db->close();
    }

    public function get_tramites(){
        $tramites = array();
        $sql = "SELECT  
                TR.id_expdrogas_tramite,
                TR.id_usuario,
                TR.id_estado,
                ET.descripcion,
                TR.fecha_registro,
                PER.nume_identificacion,
                PER.p_nombre,
                PER.s_nombre,
                PER.p_apellido,
                PER.s_apellido,
                PER.email,
                PER.telefono_fijo,
                PER.telefono_celular,
                TR.observaciones
        FROM expdrogas_tramite TR
        JOIN persona PER ON PER.id_persona = TR.id_usuario
        JOIN pr_estado_tramite ET ON ET.id_estado = TR.id_estado ";
       
        if($this->session->userdata('perfil')==3){
            $sql.= "WHERE TR.id_estado IN (1,5,12,13) ";
        }else if($this->session->userdata('perfil')==4){
             $sql.= "WHERE TR.id_estado IN (2,9) ";
        }else if($this->session->userdata('perfil')==5){
             $sql.= "WHERE TR.id_estado IN (3,10) ";
        }
             
        $sql.= "ORDER BY 1 DESC ";
        
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            $i = 0;
            foreach($query->result() as $row){
                $tramites[$i]["id_expdrogas"] = $row->id_expdrogas_tramite;
                $tramites[$i]["idusuario"] = $row->id_usuario;
                $tramites[$i]["id_estado"] = $row->id_estado;
                $tramites[$i]["descripcion"] = $row->descripcion;
                $tramites[$i]["fecha_registro"] = $row->fecha_registro;
                $tramites[$i]["numidentificacion"] = $row->nume_identificacion;
                $tramites[$i]["p_nombre"] = $row->p_nombre;
                $tramites[$i]["s_nombre"] = $row->s_nombre;
                $tramites[$i]["p_apellido"] = $row->p_apellido;
                $tramites[$i]["s_apellido"] = $row->s_apellido;
                $tramites[$i]["email"] = $row->email;
                $tramites[$i]["telefono_fijo"] = $row->telefono_fijo;
                $tramites[$i]["telefono_celular"] = $row->telefono_celular;
                $tramites[$i]["observaciones"] = $row->observaciones;
                $i++;
            }
        }
        $this->db->close();
        return $tramites;
        $this->db->close();
    }

    //
    public function mis_tramites($id_usuario){
        $cadena_sql = " SELECT
                        TR.id_expdrogas_tramite,
                        TR.id_estado,
                        ET.descripcion,
                        TR.fecha_registro,
                        PER.nume_identificacion,
                        PER.tipo_identificacion,
                        PER.p_nombre,
                        PER.s_nombre,
                        PER.p_apellido,
                        PER.s_apellido,
                        PER.email,
                        PER.telefono_fijo,
                        PER.telefono_celular,
                        TR.observaciones
                    FROM expdrogas_tramite TR
                    JOIN persona PER ON PER.id_persona = TR.id_usuario
                    JOIN pr_estado_tramite ET ON ET.id_estado = TR.id_estado
                    WHERE TR.id_usuario = ".$id_usuario."
                    ORDER BY 1";
                 
        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        
        return $result;
        $this->db->close();
    }

	// Creación validacion no duplicados en mysql.
    public function validartramitemsql($datosAr){

		$data = array(
            'institucion_educativa' => $datosAr['institucion_educativa'],
			'titulo_equivalente' => $datosAr['titulo_equivalente'],
            'profesion' => $datosAr['profesion'],
            'id_persona' => $datosAr['id_persona'],

        );
        $cadena_sql = " SELECT DISTINCT
                        RT.id_titulo,
                        RT.id_persona,
                        RT.fecha_tramite,
                        RT.tipo_titulo,
                        RT.institucion_educativa,
                        INS.nombre_institucion,
                        RT.profesion,
                        PU.nombre_programa,
                        RT.tarjeta,
                        RT.diploma,
                        RT.acta,
                        RT.libro,
                        RT.folio,
                        RT.anio,
                        RT.cod_universidad,
                        RT.resolucion,
                        RT.fecha_resolucion,
                        RT.entidad,
                        RT.titulo_equivalente,
                        RT.pdf_documento,
                        RT.pdf_titulo,
                        RT.pdf_acta,
                        RT.pdf_tarjeta,
                        RT.pdf_resolucion,
                        RT.estado,
                        ET.descripcion
                    FROM registro_titulo RT
                    JOIN pr_estado_tramite ET ON ET.id_estado = RT.estado
                    LEFT JOIN pr_institucion INS ON INS.id_institucion = RT.institucion_educativa
                    LEFT JOIN pr_programas_univ PU ON PU.id_programa = RT.profesion AND PU.id_institucion = RT.institucion_educativa
					LEFT JOIN pr_programa_equivalente PEQ ON PEQ.id_programaequi = RT.titulo_equivalente";


					if($data['institucion_educativa'] !="" && $data['profesion'] !=""){
					  $cadena_sql.=" WHERE RT.id_persona = ".$data['id_persona']." AND RT.institucion_educativa =".$data['institucion_educativa']." AND RT.profesion ='".$data['profesion']."' AND RT.estado <> 16 ORDER BY 1";
					}
					elseif ($data['titulo_equivalente'] !="" ) {
					  $cadena_sql.=" WHERE RT.id_persona = ".$data['id_persona']." AND PEQ.id_programaequi ='".$data['titulo_equivalente']."' AND RT.estado <> 16 ORDER BY 1";
					}
					else{
					  $cadena_sql.=" WHERE RT.id_persona = ".$data['id_persona']." AND RT.estado = 0 ORDER BY 1";
					}

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
        $this->db->close();
    }

    public function consulta_resolucion($id_persona,$estado){
        $cadena_sql = " SELECT
                        id_resolucion,
                        fecha_resolucion,
                        id_tramite,
                        codigo_verificacion,
                        id_estado_tramite,
                        estado_resolucion
                    FROM expdrogas_resoluciones
                    WHERE id_persona = ".$id_persona." 
                    AND id_estado_tramite= ".$estado.";";
                   
        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }


    public function tramites_seguimientociudadanano_id($id_titulo){
	    $cadena_sql = " SELECT SEG.fecha_registro, USR.perfil, PER.p_apellido, PER.s_apellido, PER.p_nombre, PER.s_nombre, EST.descripcion, SEG.observaciones, SEG.tipomotivoaclaracion
        FROM seguimiento_tramite SEG 
		JOIN usuarios USR ON USR.id = SEG.id_usuario
		LEFT JOIN persona PER ON PER.id_persona = USR.id_persona
		JOIN pr_estado_tramite EST ON EST.id_estado = SEG.estado
        WHERE SEG.estado in (13,16) AND SEG.id_titulo = ".$id_titulo;

	    $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
        $this->db->close();
    }

    public function consulta_resolucionreposicion($id_titulo){
        $cadena_sql = " SELECT
                        id_resolucion,
                        id_titulo,
                        id_archivo,
                        codigo_verificacion,
                        estado
                    FROM resoluciones_titulo
                    WHERE id_titulo = ".$id_titulo.";";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

    public function consulta_seguimientoresolucion($id_titulo){
        $cadena_sql = " SELECT
                        *
                    FROM seguimiento_tramite
                    WHERE id_titulo = ".$id_titulo.";";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }
	
    public function consultar_archivo_resolucion($id_archivo){
        $cadena_sql = " SELECT
                            id_archivo,
                            ruta,
                            nombre,
                            fecha,
                            tags,
                            es_publico,
                            estado
                        FROM
                            archivos
                        WHERE id_archivo = ".$id_archivo." ";
        //echo $cadena_sql;
        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

	//Author: Mario BeltrA�n mebeltran@saludcapital.gov.co Since: 17062019
	//Ajuste campo fecha term error al editar formulario.

    public function datos_tramite($id_titulo){
        $cadena_sql = " SELECT DISTINCT
                        RT.id_titulo,
                        RT.id_persona,
                        RT.fecha_tramite,
                        RT.tipo_titulo,
                        RT.institucion_educativa ins_titulo,
                        INS.nombre_institucion,
                        RT.profesion,
                        PU.nombre_programa,
						RT.fecha_term,
						RT.fecha_term_ext,
                        RT.tarjeta,
                        RT.diploma,
                        RT.acta,
                        RT.libro,
                        RT.folio,
                        RT.anio,
                        RT.cod_universidad,
                        RT.resolucion,
                        RT.fecha_resolucion,
                        RT.entidad,
                        RT.titulo_equivalente,
						PEQ.nombre_programa as programaequivalente,
						RT.pais_tituloequi,
                        RT.pdf_documento,
                        RT.pdf_titulo,
                        RT.pdf_acta,
                        RT.pdf_tarjeta,
                        RT.pdf_resolucion,
                        RT.estado,
                        ET.descripcion descEstado,
                        TI.Descripcion descTipoIden,
                        PE.*
                    FROM registro_titulo RT
                    JOIN persona PE ON PE.id_persona = RT.id_persona
                    JOIN pr_estado_tramite ET ON ET.id_estado = RT.estado
                    LEFT JOIN pr_institucion INS ON INS.id_institucion = RT.institucion_educativa
                    LEFT JOIN pr_programas_univ PU ON PU.id_programa = RT.profesion AND PU.id_institucion = RT.institucion_educativa
					LEFT JOIN pr_programa_equivalente PEQ ON PEQ.id_programaequi = RT.titulo_equivalente
                    JOIN pr_tipoidentificacion TI ON TI.IdTipoIdentificacion = PE.tipo_identificacion
                    WHERE RT.id_titulo = ".$id_titulo."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
        $this->db->close();
    }

     /**
     * Guuarda el histórico del trámite 
     * @since 06/2020
     */
    public function save_historico($idpersona)
    {
        $id_usuario = $this->session->userdata('id_persona');
        $fecha_registro = date('Y-m-d');
        $newFecha_nacimiento=$this->expendedor->formatoFecha($fecha_nacimiento,'-');
        $data = array(
            'id_tramite' => $this->input->post('idtramite'),
            'id_usuario' => $id_usuario,
            'fecha_registro' => $fecha_registro,
            'id_estado' => $this->input->post('estado'),
            'observaciones' => $this->input->post('observaciones')
        );
       
        $query = $this->db->insert('expdrogas_historico_tramite', $data);

        $idTramite = $this->db->insert_id();
       
       if ($query) {
            return $idTramite;
        } else {
            return false;
        }

        
    }

    //Actualiza el trámite cuando cambian el estado
    public function actualizar_tramite($idpersona){
        $id_usuario = $this->session->userdata('id_persona');
        $fecha_revision = date('Y-m-d');
        if($this->session->userdata('perfil')==3){
            $data = array(
                'id_estado' => $this->input->post('estado'),
                'id_usuario_revisa' =>  $id_usuario,
                'fecha_revision' => $fecha_revision,
                'observaciones' => $this->input->post('observaciones')
            );
        }else if($this->session->userdata('perfil')==4){
            $data = array(
                'id_estado' => $this->input->post('estado'),
                'id_usuario_coordinador' =>  $id_usuario,
                'fecha_revisa_coordinador' => $fecha_revision,
                'observaciones' => $this->input->post('observaciones')
            );
        }else if($this->session->userdata('perfil')==5){
            $data = array(
                'id_estado' => $this->input->post('estado'),
                'id_usuario_aprueba' =>  $id_usuario,
                'fecha_aprobacion' => $fecha_revision,
                'observaciones' => $this->input->post('observaciones')
            );
        }
        $this->db->where('id_usuario', $idpersona);
        return $this->db->update('expdrogas_tramite', $data);
        /*$str = $this->db->last_query();
        echo "<pre>";
        print_r($str);*/
        $this->db->close();
    }

    //Actualiza los estados del archivo
    public function actualizar_archivo ($idarchivo){
        $data = array(
            'condicion' => $this->input->post($idarchivo)
        );
        $this->db->where('id_archivo', $idarchivo);
        return $this->db->update('archivos', $data);
        $str = $this->db->last_query();
        echo "<pre>";
        print_r($str);
        $this->db->close();
    }

    public function programasInstitucion($id_institucion){
        $cadena_sql = " SELECT  id_programa, id_institucion, nombre_programa, tipo_prog, sede FROM pr_programas_univ WHERE id_institucion = ".$id_institucion." ORDER BY 3 ";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

    

    public function consulta_consecutivo($id_titulo){
        $cadena_sql = " SELECT
                            max(id_consecutivo) max_cons
                        FROM
                            seguimiento_tramite
                        WHERE id_titulo = ".$id_titulo." ";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
    }

    public function consulta_existe_documento($id_persona){
        $cadena_sql = " SELECT
                            pdf_documento
                        FROM
                            registro_titulo
                        WHERE id_persona = ".$id_persona." ";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
    }
	
	public function actualizarEstadoReposicion($datos) {

        $data = array(
            'estado' => $datos['estado'],
			'fecha_reposicion' => $datos['fecha_reposicion']
        );

        $this->db->where('id_titulo', $datos['id_titulo']);
        return $this->db->update('registro_titulo', $data);
    }

	public function actualizarEstadoAclaracion($datos) {

        $data = array(
            'estado' => $datos['estado'],
			'fecha_aclaracion' => $datos['fecha_aclaracion']
        );

        $this->db->where('id_titulo', $datos['id_titulo']);
        return $this->db->update('registro_titulo', $data);
    }

	// Creación validacion no duplicados en mysql Exh Mario Beltran 30012020.
    	public function validartramitemsqlExh($datosAr){
	    $data = array(
            	'numero_licencia' => $datosAr['numero_licencia'],
		'fechaInh' => $datosAr['fechaInh'],
	        'id_persona' => $datosAr['id_persona'],

	    );
	
        $cadena_sql = "SELECT DISTINCT
                       l.numero_licencia 
	    	       FROM licencia_exhuma as l, persona as p,estado_tramite_licenciaExh as e";

	if($data['numero_licencia'] !="" && $data['fechaInh'] !=""){
		  $cadena_sql.=" WHERE l.numero_licencia = '".$data['numero_licencia']."' AND l.fecha_inhumacion = '".$data['fechaInh']."' AND 			l.id_persona ='".$data['id_persona']."' AND l.estado <> 5 ORDER BY 1";
	}
	else{
		  $cadena_sql.=" WHERE l.id_persona = ".$data['id_persona']." AND l.estado = 0 ORDER BY 1";
	}
        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

    /**
     * Verifica resolución
     * @since  06/2020
     */
    public function verifyResolucion($idpersona,$estadoTramite) 
    {
        $this->db->where('id_persona', $idpersona);
        $this->db->where('id_estado_tramite', $estadoTramite);
        $query = $this->db->get("expdrogas_resoluciones");

        if ($query->num_rows() >= 1) {
            return true;
        } else{ 
            return false; 
        }
        $this->db->close();
    }

    /**
     * Guuarda el histórico del trámite 
     * @since 06/2020
     */
    public function save_resolucion($idpersona, $codigo_verificacion, $estadoTramite)
    {
        $idpersona = $this->session->userdata('idpersona');
        $fecha_resolucion = date('Y-m-d');
        $newFecha_nacimiento=$this->expendedor->formatoFecha($fecha_nacimiento,'-');
        $data = array(
            'fecha_resolucion' => $fecha_resolucion,
            'id_tramite' => $this->input->post('idtramite'),
            'id_persona' => $idpersona,
            'codigo_verificacion' => $codigo_verificacion,
            'id_estado_tramite' => $estadoTramite,
            'estado_resolucion' => 1
        );
       
        $query = $this->db->insert('expdrogas_resoluciones', $data);

        $idResolucion= $this->db->insert_id();
       
       if ($query) {
            return $idResolucion;
        } else {
            return false;
        }
        echo "<pre>";
        print_r($str);
        exit;
        $this->db->close();
        
    }    	

}
