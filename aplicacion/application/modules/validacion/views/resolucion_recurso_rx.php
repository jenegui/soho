<html>
<head>
  <title>Certificación de Contratistas - Tramites en Linea . SDS</title>
  <meta charset="utf-8">
  <style type="text/css">

    @page { margin: 180px 50px; }
    #header { position: fixed; left: 0px; top: -150px; right: 0px; height: 0px; text-align: center; }
    #footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; text-align: left; font-size:10px;}

    body,td,th {
      font-family: Arial, Helvetica, sans-serif;
    }
    p {
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
    }
    li {
      margin-bottom: 1em;
      color: #000000;
      line-height: 1.5em;
      font-size:13px;
      letter-spacing: 0px;
    }
    body {
      margin-left: 0cm;
      margin-top: 0cm;
      margin-right: 0cm;
      margin-bottom: 0cm;
    }
    i {
      color: #000000;
      padding-right: 1em;
    }
    h1{
      font-size: 36px;
      color: #000000;
    }
    h3{
      font-size: 24px;
      color: #000000;
    }
    span {
      color: #000000;
    }
  </style>
</head>
<body>
<div id="header">
  <!--<img src="./assets/img/memologo.jpg" />-->
</div>
  <div style="width:100%;">
      <div style="margin-left:45px;margin-right:45px;margin-top:-50px;">
        <p align="center">
            <br><b><em>Dirección de Calidad de Servicios de Salud<br>
              Subdirección Inspección, Vigilancia y Control de Servicios de Salud</em>
                </b>
        </p>
        <p align="center">
            <b><em>Resolución No. <?php echo $nume_resolucion?> de <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
              "Por medio de la cual se Concede una Licencia de Practica Medica"
              </em></b>
        </p>
        <p align="justify">En uso de sus facultades
          legales y en especial las que confiere el Decreto Distrital 507 de 2013 expedido por el Alcalde
          Mayor de Bogotá y la Resolución 482 de 2018 del Ministerio de Salud y Protección Social y</p>
        <p align="justify"></p>
        <p align="justify">Que mediante Resolución No. 482 del 22 de febrero de 2018, el Ministerio de
          Salud y Protección Social reglamentó el uso de equipos generadores de radiación ionizante, su control
          de calidad, la prestación de servicios de protección radiológica y se dictan otras disposiciones
          regulando las autorizaciones y licencias de funcionamiento y establece:</p>
        <p align="justify">Que el Artículo 19 de la Resolución 482 de 2018 estable: Licencia de prácticas médicas.
          Los prestadores de servicios de salud interesados en realizar una práctica médica que haga uso de equipos
          generadores de radiación ionizante, móviles o fijos, deberán solicitar licencia de práctica médica ante
          la entidad territorial de salud de carácter departamental o distrital de la jurisdicción en la que se
          encuentre la respectiva instalación. </p>
        <p align="justify">Que mediante el Artículo 20 de la Resolución 482 de 2018 se categorizaron las prácticas
          médicas así:</p>
        <p align="justify">
          20.1. Categoría I:<br>
          20.1.1. Radiología odontológica periapical. <br>
          20.1.2. Densitometría ósea.<br><br>
          20.2. Categoría II: <br>
          20.2.1. Radioterapia. <br>
          20.2.2. Radiodiagnóstico de alta complejidad. <br>
          20.2.3. Radiodiagnóstico de media complejidad. <br>
          20.2.4. Radiodiagnóstico de baja complejidad. <br>
          20.2.5. Radiografías odontológicas panorámicas y tomografías orales
        </p>
        <p align="justify">Parágrafo. Las prácticas médicas que no se encuentren expresamente señaladas en el presente
          artículo, se considerarán como categoría II.”</p>
        <p align="justify">Que de conformidad con los artículos 21 y 23 de la resolución 482 de 2018 corresponde a las
          entidades territoriales de salud otorgar las licencias de prácticas médicas,a solicitud de los prestadores de
          servicios de salud.</p>
        <p align="justify">Que el (la) señor(a) <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>, 
            identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?> en su
          calidad de Representante Legal, mediante radicado No <?php echo $datos_tramite->id?> de fecha <?php echo $datos_tramite->created_at?> ha solicitado licencia de
          práctica médica de categoría <?php echo $datos_tramite->categoria?>.
        </p>
        <p align="justify">COMPLEJIDAD para <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?> 
            identificado (a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>, ubicado (a) en la
          <?php echo $rayosxDireccion->dire_entidad?> de la nomenclatura urbana de Bogotá, de acuerdo a los preceptos de la Resolución 482 de 2018.</p>
        <p align="justify">Que examinada la documentación allegada se encontró que se cumple con los requisitos exigidos por
          la Resolución 482 de 2018 y demás normas vigentes relacionadas con la protección de las personas expuestas a las
          Radiaciones Ionizantes, expedidas por el Ministerio de Salud y Protección Social.</p>
        <p align="justify">Que realizada la visita de verificación con enfoque de riesgo a las instalaciones del prestador
          de servicios de salud se evidenció que cumple requisitos. </p>
        <p align="justify">En mérito a lo expuesto, este Despacho</p>
        <p align="center"><b>RESUELVE:</b></p>
        <p align="justify"><b>ARTÍCULO PRIMERO:</b> Conceder Licencia de Práctica Médica de categoría <?php echo $datos_tramite->categoria?> a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
          ubicada en la $rayosxDireccion->dire_entidad representado legalmente por el señor (a) <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?> identificado(a) con
           <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?> con código de habilitación No. XXXXXXXXXXXXX</p>
        <p align="justify"><b>ARTÍCULO SEGUNDO:</b> El Nombre del Oficial o Encargado de Protección Radiológica es:</p>
        <p align="justify">Apellidos y Nombres: <?php echo $rayosxOficialToe->encargado_papellido." ".$rayosxOficialToe->encargado_sapellido." ".$rayosxOficialToe->encargado_pnombre." ".$rayosxOficialToe->encargado_snombre?><br>
          Identificado con <?php echo $rayosxOficialToe->encargado_tdocumento?> No: <?php echo $rayosxOficialToe->encargado_ndocumento?><br>
          Profesión: <?php echo $rayosxOficialToe->encargado_profesion?></p>
        <p align="justify"><b>ARTÍCULO TERCERO:</b> El (los) equipo (s) que allí funciona (n) es (son):</p>
          <?php
          
            for($i=0;$i<count($rayosxEquipo);$i++){
                ?>
                <p align="justify">
                  Clase de Equipo: <?php echo $datos_tramite->categoria?><br>
                  Marca: <?php echo $rayosxEquipo[$i]->marca_equipo?><br>
                  Modelo: <?php echo $rayosxEquipo[$i]->modelo_equipo?><br>
                  Serie: <?php echo $rayosxEquipo[$i]->serie_equipo?><br>
                  Tubo: <?php echo $rayosxEquipo[$i]->marca_tubo_rx?><br>
                  Modelo Tubo: <?php echo $rayosxEquipo[$i]->modelo_tubo_rx?><br>
                  Serie Tubo: <?php echo $rayosxEquipo[$i]->serie_tubo_rx?><br>
                  Tensión Max. Miliamperaje: <?php echo $rayosxEquipo[$i]->tension_tubo_rx?><br>
                  Contenido Max. Kilovoltaje: <?php echo $rayosxEquipo[$i]->contiene_tubo_rx?><br>
                  Energía Fotones: <?php echo $rayosxEquipo[$i]->energia_fotones?><br>
                  Energía Electrones. Kilovoltaje: <?php echo $rayosxEquipo[$i]->energia_electrones?><br>
                  Contenido Max. Kilovoltaje: <?php echo $rayosxEquipo[$i]->carga_trabajo?><br>
                </p>
                <?php
            }
          
          ?>
          
          
        
        <p align="justify"><b>ARTÍCULO CUARTO:</b> La presente licencia se concede por el término de CUATRO (04)años, contados a partir
          de la fecha de expedición del presente acto administrativo, y podrá ser renovada por un término igual mediante solicitud
          presentada con sesenta (60) días de antelación de su vencimiento, de conformidad con los artículos 22 y 25 de la
          Resolución 482 de 2018.</p>
        <p align="justify"><b>ARTÍCULO QUINTO:</b> Conforme al Artículo 29 de la Resolución 482 de 2018 el titular de la licencia de
          práctica médica categoría I o II podrá solicitar la modificación de algunas de las condiciones que se señalan en el
          literal 29.1 y literal 29.2.</p>
        <p align="justify"><b>ARTÍCULO SEXTO:</b> Notificar el contenido de esta providencia al representante legal, o a un tercero
          debidamente autorizado, de conformidad con lo dispuesto en los artículos 67 y 69 del Código  de Procedimiento
          Administrativo y de lo Contencioso Administrativo, haciéndole (s) saber que contra la presente proceden los recursos
          de reposición ante este Despacho y de apelación ante el Secretario Distrital de Salud, dentro de los cinco (5) días
          siguientes a la notificación del presente acto administrativo, de conformidad con lo establecido en el Artículo 74
          del Código  de Procedimiento Administrativo y de lo Contencioso Administrativo.</p>
        <p align="center"><b>NOTIFÍQUESE Y CÚMPLASE</b></p>
        <p align="center">Dado en Bogotá, D.C. a los <?php echo date('d')." días del mes de ".$mes." del a&ntilde;o ".date('Y');?></p>
        <p align="justify"><b>YOLIMA AGUDELO SEDANO</b></p>
        <p align="justify">Director de Calidad de Servicios de Salud</p>
        <p align="justify"></p>
        <p align="justify"></p>
        <p align="justify"></p>
        <p align="justify"></p>
        <p align="justify"></p>


      </div>
  </div>
  <div id="footer" style="margin-left:45px;margin-right:45px">



  </div>

</body>
</html>
