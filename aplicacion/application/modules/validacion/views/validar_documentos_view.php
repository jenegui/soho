﻿<script type="text/javascript">
		
    $(document).ready(function(){
        $(".actualizarVisorAction").click(function() {                   
            $("#visorPdf").attr('src', $(this).attr('title'));            
        });
    });
                        
</script>
<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
	<br>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
	<br>
	<div class="alert alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
		<?php echo $retornoExito ?>
	</div>
<?php
}
?>


<form class="form-horizontal" id="form_tramite" name="form_tramite" action="<?php echo base_url('validacion/guardarEstado/'.$tramites_pendientes->id_titulo)?>" method="post">
<input type="hidden" name="id_persona" value="<?php echo $tramites_pendientes->id_persona?>">
<input type="hidden" name="id_titulo" value="<?php echo $tramites_pendientes->id_titulo?>">
<fieldset class="header-form">
<div class="row block right" style="width:100%;">
			<div class="col-12 col-md-12 pl-4">
				<div class="subtitle">
					<h2><b>Tr&aacute;mite ID.<?php echo $tramites_pendientes->id_titulo?></b></h2>
					<h4><b>Estado Trámite:</b> <?php echo $tramites_pendientes->descEstado?>
						<br>
						<b>Fecha Trámite:</b> <?php echo $tramites_pendientes->fecha_tramite?>
					</h4>
				</div>
            </div>

			<div class="col-12 col-md-12 text-center">
			
				<h3>
					<table style="width:100%;">
					<tr>
					<td align="left"><b>Datos Personales</b> <i class="fas fa-user"></i></td>
					<td align="right"><font color="#3D5DA6"><i id="mindatospersonal" class="fas fa-angle-up"></i><i id="moredatospersonal" class="fas fa-angle-down"></i></font></td>
					</tr>
					</table>
				</h3>
            </div>		
            <div class="col-12 col-md-6 pl-4" id="divdatopertipoiden">
                <div class="paragraph">
					<br><br>
                    <label for=""><b>Tipo Identificaci&oacute;n:</b></label>
					<select id="tipo_identificacion" name="tipo_identificacion" class="form-control validate[required]">
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($tipo_identificacion);$i++){
							
							if($tramites_pendientes->tipo_identificacion == $tipo_identificacion[$i]->IdTipoIdentificacion){
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."' selected>".$tipo_identificacion[$i]->Descripcion."</option>";
							}else{
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."'>".$tipo_identificacion[$i]->Descripcion."</option>";
							}                                    
						}
						?>
					</select>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatopernumiden">
                <div class="paragraph">
					<br><br>
                    <label for="num_doc"><b>N&uacute;mero Identificaci&oacute;n:</b></label>
                    <input id="nume_documento" name="nume_documento" placeholder="Número de documento de identidad" class="form-control input-md validate[required, maxSize[11], custom[number]]" required="" type="text" value="<?php echo $tramites_pendientes->nume_identificacion?>" disabled style="width:100%;">                    
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatoperpnombre">
                <div class="paragraph">
					<label for=""><b>Primer nombre:</b></label>
                    <input id="p_nombre" name="p_nombre" placeholder="Primer Nombre" class="form-control validate[required, maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->p_nombre?>">
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatopersnombre">
                <div class="paragraph">
					<label for=""><b>Segundo nombre:</b></label>
                    <input id="s_nombre" name="s_nombre" placeholder="Segundo Nombre" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->s_nombre?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatoperpapellido">
                <div class="paragraph">
					<label for=""><b>Primer apellido:</b></label>
                    <input id="p_apellido" name="p_apellido" placeholder="Primer apellido" class="form-control validate[required, maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->p_apellido?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatopersapellido">
                <div class="paragraph">
					<label for=""><b>Segundo apellido:</b></label>
					<input id="s_apellido" name="s_apellido" placeholder="Segundo apellido" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->s_apellido?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatopertelcelu">
                <div class="paragraph">
					<label for=""><b>Teléfono celular:</b></label>
					<input id="telefono_celular" name="telefono_celular" placeholder="Teléfono celular" class="form-control input-md validate[required, maxSize[10], custom[number]]" type="text" value="<?php echo $tramites_pendientes->telefono_celular?>">
				</div>
            </div>				
			<div class="col-12 col-md-6 pl-4" id="divdatopertelfijo">
                <div class="paragraph">
					<label for=""><b>Teléfono fijo:</b></label>
					<input id="telefono_fijo" name="telefono_fijo" placeholder="Teléfono fijo" class="form-control input-md validate[required, maxSize[10], custom[number]]" type="text" value="<?php echo $tramites_pendientes->telefono_fijo?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatoperemail">
                <div class="paragraph">
					<label for=""><b>Correo electr&oacute;nico:</b></label>
                    <input id="email" name="email" placeholder="Correo eletrónico " class="form-control input-md validate[required, custom[email]]" required="" type="text" value="<?php echo $tramites_pendientes->email?>">
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatoperfecnac">
                <div class="paragraph">
					<label for=""><b>Fecha Nacimiento:</b></label>
					<input id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha terminaci&oacute;n" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_nacimiento?>" autocomplete="off" readonly="readonly">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="divdatopersexo">
                <div class="paragraph">
					<label for=""><b>Sexo:</b></label>
						<select id="sexo" name="sexo" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<option value="1"  <?php if($tramites_pendientes->sexo == 1){ echo "selected";}?>>Hombre</option>
							<option value="2"  <?php if($tramites_pendientes->sexo == 2){ echo "selected";}?>>Mujer</option>
							<option value="3"  <?php if($tramites_pendientes->sexo == 3){ echo "selected";}?>>Intersexual</option>
						</select>
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatopergenero">
                <div class="paragraph">
					<label for=""><b>Genero:</b></label>
						<select id="genero" name="genero" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<option value="1" <?php if($tramites_pendientes->genero == 1){ echo "selected";}?>>Masculino</option>
							<option value="2" <?php if($tramites_pendientes->genero == 2){ echo "selected";}?>>Femenino</option>
							<option value="3" <?php if($tramites_pendientes->genero == 3){ echo "selected";}?>>Transgenero</option>
							<option value="4" <?php if($tramites_pendientes->genero == 4){ echo "selected";}?>>No responde</option>
						</select>
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatoperorienta">
                <div class="paragraph">
					<label for=""><b>Orientaci&oacute;n:</b></label>
						<select id="orientacion" name="orientacion" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<option value="1" <?php if($tramites_pendientes->orientacion == 1){ echo "selected";}?>>Heterosexual</option>
							<option value="2" <?php if($tramites_pendientes->orientacion == 2){ echo "selected";}?>>Homosexual</option>
							<option value="3" <?php if($tramites_pendientes->orientacion == 3){ echo "selected";}?>>Bisexual</option>
						</select>
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatoperetnia">
                <div class="paragraph">
					<label for=""><b>Etnia:</b></label>
						<select id="etnia" name="etnia" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<option value="1" <?php if($tramites_pendientes->etnia == 1){ echo "selected";}?>>Indigena</option>
							<option value="2" <?php if($tramites_pendientes->etnia == 2){ echo "selected";}?>>Rom-Gitano</option>
							<option value="3" <?php if($tramites_pendientes->etnia == 3){ echo "selected";}?>>Raizal</option>
							<option value="4" <?php if($tramites_pendientes->etnia == 4){ echo "selected";}?>>Palenquero</option>
							<option value="5" <?php if($tramites_pendientes->etnia == 5){ echo "selected";}?>>Afrocolombiano</option>
							<option value="6" <?php if($tramites_pendientes->etnia == 6){ echo "selected";}?>>Ninguna</option>
						</select>
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatoperestciv">
                <div class="paragraph">
					<label for=""><b>Estado Civil:</b></label>
						<select id="estado_civil" name="estado_civil" class="form-control">
							<option value="">Seleccione...</option>
							<option value="1" <?php if($tramites_pendientes->estado_civil == 1){ echo "selected";}?>>Soltero</option>
							<option value="2" <?php if($tramites_pendientes->estado_civil == 2){ echo "selected";}?>>Casado</option>
							<option value="3" <?php if($tramites_pendientes->estado_civil == 3){ echo "selected";}?>>Union marital de hecho</option>
							<option value="4" <?php if($tramites_pendientes->estado_civil == 4){ echo "selected";}?>>Divorciado</option>
							<option value="5" <?php if($tramites_pendientes->estado_civil == 5){ echo "selected";}?>>Viudo</option>
						</select>
				</div>
            </div>			
			<div class="col-12 col-md-6 pl-4" id="divdatopernivedu">
                <div class="paragraph">
					<label for=""><b>Nivel Educativo:</b></label>
						<select id="tipo" name="nivel_educativo" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<option value="1" <?php if($tramites_pendientes->nivel_educativo == 1){ echo "selected";}?>>Primaria</option>
							<option value="2" <?php if($tramites_pendientes->nivel_educativo == 2){ echo "selected";}?>>Secundaria</option>
							<option value="3" <?php if($tramites_pendientes->nivel_educativo == 3){ echo "selected";}?>>T&eacute;cnico</option>
							<option value="4" <?php if($tramites_pendientes->nivel_educativo == 4){ echo "selected";}?>>Tecn&oacute;logo</option>
							<option value="5" <?php if($tramites_pendientes->nivel_educativo == 5){ echo "selected";}?>>Profesional</option>
							<option value="6" <?php if($tramites_pendientes->nivel_educativo == 6){ echo "selected";}?>>Especialista</option>
							<option value="7" <?php if($tramites_pendientes->nivel_educativo == 7){ echo "selected";}?>>Maestria</option>
							<option value="8" <?php if($tramites_pendientes->nivel_educativo == 8){ echo "selected";}?>>Doctorado</option>
							<option value="9" <?php if($tramites_pendientes->nivel_educativo == 9){ echo "selected";}?>>Post-Doctorado</option>
							<option value="10" <?php if($tramites_pendientes->nivel_educativo == 10){ echo "selected";}?>>Ninguno</option>
						</select>
				</div>
            </div>

			<div class="col-12 col-md-12 text-center ">
			<br><br>	
				<h3>
					<table style="width:100%;">
					<tr>
					<td align="left"><b>Datos Geograficos </b> <i class="fas fa-globe-americas"></i></td>
					<td align="right"><font color="#3D5DA6"><i id="mindatosgeo" class="fas fa-angle-up"></i><i id="moredatosgeo" class="fas fa-angle-down"></i></font></td>
					</tr>
					</table>
				</h3>
            </div>				
			<div class="col-12 col-md-12 text-center " style="max-width:96%;" id="divdatogeonac">
				<label for=""><b>Pais Nacionalidad:</b></label>
					<select id="nacionalidad" name="nacionalidad" class="form-control validate[required]" required>
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($paises);$i++){
							 if($tramites_pendientes->nacionalidad == $paises[$i]->IdPais){
								echo "<option value='".$paises[$i]->IdPais."' selected>".$paises[$i]->Nombre."</option>";
							 }
							 else{
								echo "<option value='".$paises[$i]->IdPais."'>".$paises[$i]->Nombre."</option>";
							 }
						}
						?>
					</select> 	
            </div>
			<?php
			if($tramites_pendientes->nacionalidad == 170){
			?>
			<div class="col-12 col-md-6 pl-4" id="divdatogeodepanac">
                <div class="paragraph">
					<label for=""><b>Departamento de nacimiento:</b></label>
						<select id="departamento" name="departamento" class="form-control">
							<option value="">Seleccione...</option>
							<?php
								for($i=0;$i<count($departamentos_col);$i++){
									
									if($tramites_pendientes->departamento == $departamentos_col[$i]->IdDepartamento){
										echo "<option value='".$departamentos_col[$i]->IdDepartamento."' selected>".$departamentos_col[$i]->Descripcion."</option>";
									}
									else{
										echo "<option value='".$departamentos_col[$i]->IdDepartamento."'>".$departamentos_col[$i]->Descripcion."</option>";
									}

								}
							?>
						</select>					
				</div>
            </div>	

			<?php
			if(!empty($tramites_pendientes->departamento)){
				$ciudadesdepa = $this->login_model->municipios_col($tramites_pendientes->departamento);
			}
			?>

			<div class="col-12 col-md-6 pl-4" id="divdatogeociudnac">
                <div class="paragraph">
					<label for=""><b>Ciudad de nacimiento:</b></label>
						<select id="ciudad_nacimiento" name="ciudad_nacimiento" class="form-control">
							<option value="">Seleccione...</option>
							<?php
								for($i=0;$i<count($ciudadesdepa);$i++){
									if($tramites_pendientes->ciudad_nacimiento == $ciudadesdepa[$i]->IdMunicipio){
										echo "<option value='".$ciudadesdepa[$i]->IdMunicipio."' selected>".$ciudadesdepa[$i]->Descripcion."</option>";    
									}else{
										echo "<option value='".$ciudadesdepa[$i]->IdMunicipio."'>".$ciudadesdepa[$i]->Descripcion."</option>";    
									}
								}
							?>
						</select>					
				</div>
            </div>				
			<?php
			}
			?>
			<div class="col-12 col-md-6 pl-4" id="divdatogeodeparesi">
                <div class="paragraph">
					<label for=""><b>Departamento de residencia:</b></label>
						<select id="depa_resi" name="depa_resi" class="form-control validate[required]" required>
							<option value="">Seleccione...</option>
							<?php
								for($i=0;$i<count($departamentos_col);$i++){
									if($tramites_pendientes->depa_resi == $departamentos_col[$i]->IdDepartamento){
										echo "<option value='".$departamentos_col[$i]->IdDepartamento."' selected>".$departamentos_col[$i]->Descripcion."</option>";
									}
									else{
										echo "<option value='".$departamentos_col[$i]->IdDepartamento."'>".$departamentos_col[$i]->Descripcion."</option>";
									}										
								}
							?>
						</select>					
				</div>
            </div>				

			<?php
			$ciudadesdepa2 = $this->login_model->municipios_col($tramites_pendientes->depa_resi);
			?>
			
			<div class="col-12 col-md-6 pl-4" id="divdatogeociudresi">
                <div class="paragraph">
					<label for=""><b>Ciudad de residencia:</b></label>
						<select id="ciudad_resi" name="ciudad_resi" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<?php
								for($i=0;$i<count($ciudadesdepa2);$i++){
									if($tramites_pendientes->ciudad_resi == $ciudadesdepa2[$i]->IdMunicipio){
										echo "<option value='".$ciudadesdepa2[$i]->IdMunicipio."' selected>".$ciudadesdepa2[$i]->Descripcion."</option>";    
									}else{
										echo "<option value='".$ciudadesdepa2[$i]->IdMunicipio."'>".$ciudadesdepa2[$i]->Descripcion."</option>";    
									}
								}
							?>
						</select>
				</div>
            </div>		

			<div class="col-12 col-md-12 text-center ">
			<br><br>
				<h3>
					<table style="width:100%;">
					<tr>
					<td align="left"><b>Informaci&oacute;n del t&iacute;tulo</b> <i class="fas fa-graduation-cap"></i></td>
					<td align="right">
					<font color="#3D5DA6"><i id="mindatosaca" class="fas fa-angle-up"></i><i id="moredatosaca" class="fas fa-angle-down"></i></font>
					<input type="hidden" id="tipotitulovalue" value="<?php echo $tramites_pendientes->tipo_titulo?>">
					</td>
					</tr>
					</table>
				</h3>
            </div>	

			<?php
				if($tramites_pendientes->tipo_titulo == '1'){
					//echo "Nacional";
					$div_nacional = "style='display:block'";
					$div_extranjero = "style='display:none'";
				}else{
					//echo "Extranjero";
					$div_nacional = "style='display:none'";
					$div_extranjero = "style='display:block'";
				}
			?>

			<div class="col-12 col-md-6 pl-4" id="div_nacional" <?php echo $div_nacional;?>>
                <div class="paragraph">
					<label for=""><b>Instituci&oacute;n educativa:</b></label>
						<select id="institucion_educativa" name="institucion_educativa" class="form-control select2 validate[required]">
							<option value="">Seleccione...</option>
							<?php
							for($i=0;$i<count($instituciones);$i++){
								if($tramites_pendientes->institucion_educativa2 == $instituciones[$i]->id_institucion){
									echo "<option value='".$instituciones[$i]->id_institucion."' selected>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";    
								}else{
									echo "<option value='".$instituciones[$i]->id_institucion."'>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
								}
							}
							?>
						</select>   
				</div>
				<?php
				//var_dump($tramites_pendientes);
					$programas_result = $this->login_model->programasInstitucion($tramites_pendientes->institucion_educativa2);
				?>
                <div class="paragraph">
					<label for=""><b>Profesi&oacute;n:</b></label>
						<select id="profesion" name="profesion" class="form-control select2 validate[required]">
							<option value="">Seleccione...</option>
							<?php
							for($i=0;$i<count($programas_result);$i++){
								if($tramites_pendientes->profesion == $programas_result[$i]->id_programa){
									echo "<option value='".$programas_result[$i]->id_programa."' selected>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";    
								}else{
									echo "<option value='".$programas_result[$i]->id_programa."'>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";    
								}
							}
							?>
						</select>
				</div>

				<?php
				if($tramites_pendientes->tarjeta != ''){
				?>

                <div class="paragraph">
					<label for=""><b>Tarjeta Profesional:</b></label>
					<input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md" type="text" value="<?php echo $tramites_pendientes->tarjeta;?>">
				</div>	

				<?php
				}
				?>

                <div class="paragraph">
					<label for=""><b>Fecha Terminación:</b></label>
					<input id="fecha_term" name="fecha_term" placeholder="Fecha Terminación" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_term;?>">
				</div>	
			</div>
			
			<div class="col-12 col-md-6 pl-4" id="div_nacional2" <?php echo $div_nacional;?>>
                <div class="paragraph">
					<label for=""><b>Diploma:</b></label>
					<input id="diploma" name="diploma" placeholder="Diploma No." class="form-control input-md validate[minSize[1], maxSize[30]]" type="text" value="<?php echo $tramites_pendientes->diploma;?>">
				</div>	
				
                <div class="paragraph">
					<label for=""><b>Acta:</b></label>
					<input id="acta" name="acta" placeholder="Acta de grado" class="form-control input-md validate[minSize[1], maxSize[30]]" type="text" value="<?php echo $tramites_pendientes->acta;?>">
				</div>	

                <div class="paragraph">
					<label for=""><b>Libro:</b></label>
					<input id="libro" name="libro" placeholder="Libro" class="form-control input-md validate[minSize[1], maxSize[6]]" type="text" value="<?php echo $tramites_pendientes->libro;?>">
				</div>	

                <div class="paragraph">
					<label for=""><b>Folio:</b></label>
					<input id="folio" name="folio" placeholder="Folio" class="form-control input-md validate[minSize[1], maxSize[6]]" type="text" value="<?php echo $tramites_pendientes->folio;?>">
				</div>	

                <div class="paragraph">
					<label for=""><b>A&ntilde;o Título:</b></label>
					<input id="anio" name="anio" placeholder="A&ntilde;o" class="form-control input-md validate[required, minSize[4], maxSize[4]]" type="text" value="<?php echo $tramites_pendientes->anio;?>">
				</div>					
            </div>				

			<div class="col-12 col-md-6 pl-4" id="div_extranjero" <?php echo $div_extranjero;?>>
                <div class="paragraph">
					<label for=""><b>C&oacute;digo Universidad:</b></label>
					<input id="cod_universidad" name="cod_universidad" placeholder="C&oacute;digo Universidad internacional" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->cod_universidad;?>">  
				</div>

				<?php
				$profesionesequi = $this->login_model->profesionesequi();
				?>
				
                <div class="paragraph">
					<label for=""><b>Titulo Equivalente:</b></label>
						<select id="titulo_equivalente" name="titulo_equivalente" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<?php
							for($i=0;$i<count($profesionesequi);$i++){
								if($tramites_pendientes->titulo_equivalente == $profesionesequi[$i]->id_programaequi){
									echo "<option value='".$profesionesequi[$i]->id_programaequi."' selected>".$profesionesequi[$i]->nombre_programa." - ".$profesionesequi[$i]->tipo_prog."</option>";    
								}else{
									echo "<option value='".$profesionesequi[$i]->id_programaequi."'>".$profesionesequi[$i]->nombre_programa." - ".$profesionesequi[$i]->tipo_prog."</option>";    
								}
							}
							?>
						</select>
				</div>

				<?php
				//var_dump($tramites_pendientes);
				$paises = $this->login_model->paises();
				?>

                <div class="paragraph">
					<label for=""><b>Pais Origen T&iacute;tulo:</b></label>
						<select id="pais_tituloequi" name="pais_tituloequi" class="form-control validate[required]">
							<option value="">Seleccione...</option>
							<?php
							for($i=0;$i<count($paises);$i++){
								if($tramites_pendientes->pais_tituloequi == $paises[$i]->IdPais){
									echo "<option value='".$paises[$i]->IdPais."' selected>".$paises[$i]->Nombre."</option>";    
								}else{
									echo "<option value='".$paises[$i]->IdPais."'>".$paises[$i]->Nombre."</option>";    
								}
							}
							?>
						</select>
				</div>			

				<?php
				if($tramites_pendientes->tarjeta != ''){
				?>

                <div class="paragraph">
					<label for=""><b>Tarjeta Profesional:</b></label>
					<input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md" type="text" value="<?php echo $tramites_pendientes->tarjeta;?>">
				</div>	

				<?php
				}
				?>

                <div class="paragraph">
					<label for=""><b>Fecha Terminación:</b></label>
					<input id="fecha_term_ext" name="fecha_term_ext" placeholder="Fecha Terminación" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_term_ext;?>">
				</div>
			</div>
			
			<div class="col-12 col-md-6 pl-4" id="div_extranjero2" <?php echo $div_extranjero;?>>
                <div class="paragraph">
					<label for=""><b>Fecha Resoluci&oacute;n:</b></label>
					<input id="fecha_resolucion" name="fecha_resolucion" placeholder="Fecha de resoluci&oacute;n" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_resolucion;?>">
				</div>

                <div class="paragraph">
					<label for=""><b>A&ntilde;o Título:</b></label>
					<input id="anio2" name="anio2" placeholder="A&ntilde;o" class="form-control input-md validate[required, minSize[4], maxSize[4]]" type="text" value="<?php echo $tramites_pendientes->anio;?>">
				</div>		
				
				<div class="paragraph">
					<label for=""><b>Resoluci&oacute;n de convalidaci&oacute;n:</b></label>
					<input id="resolucion" name="resolucion" placeholder="Numero de resoluci&oacute;n de convalidaci&oacute;n" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->resolucion;?>">
				</div>
            </div>

			<div class="col-12 col-md-12 text-center ">
			<br><br>
				<h3>
					<table style="width:100%;">
					<tr>
					<td align="left"><b>Visualizador Archivos Adjuntos</b> <i class="far fa-file-pdf"></i></td>
					<td align="right"><font color="#3D5DA6"><i id="mindatosarch" class="fas fa-angle-up"></i><i id="moredatosarch" class="fas fa-angle-down"></i></font></td>
					</tr>
					</table>				
				</h3>
            </div>

			<div class="col-4 col-md-4 " id="divdatoarch">
				<table class="table">
					<tr>
						<th>Descripci&oacute;n</th>
						<th>Ver</th>
					</tr>

				<?php
				if($tramites_pendientes->pdf_documento != 0){
					$resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_documento);
					$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
					?>
					<tr>
						<td>Documento Identidad </td>
						<td>
							<a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />                                        
							
						</td>
					</tr>                              
					<?php
				}

				if($tramites_pendientes->pdf_titulo != 0){
					$resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_titulo);
					$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
					?>
						<tr>
							<td>T&iacute;tulo</td>
							<td>
								<a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							</td>
						</tr>
						<?php
				}

				if($tramites_pendientes->pdf_acta != 0){
					$resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_acta);
					$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
					?>
							<tr class="inp-form">
								<td>Acta de grado</td>
								<td>
									<a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								</td>
							</tr>
							<?php
				}

				if($tramites_pendientes->pdf_tarjeta != 0){
					$resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_tarjeta);
					$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
					?>
								<tr class="inp-form">
									<td>Tarjeta Profesional </td>
									<td>
										<a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
									</td>
								</tr>
								<?php
				}

				if($tramites_pendientes->pdf_resolucion != 0){
					$resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_resolucion);
					$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
					?>
									<tr class="inp-form">
										<td>Resoluci&oacute;n</td>
										<td>
											<a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
												<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
											</a>
											<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
										</td>
									</tr>
									<?php
				}
				?>
				</table>			
            </div>
			
			<div class="col-8 col-md-8 " style="height: 450px;" id="divdatoarchvisor">
				<iframe id="visorPdf" style="height: 100%; width: 100%;" src=""></iframe>
			</div>
</div>

<div class="row block right" style="width:100%;">
	<div class="col-12 col-md-12 pl-4">
		<div class="subtitle">
			<h2><b>Resultado de la validaci&oacute;n</b> <i class="fas fa-check-double"></i></h2>
		</div>
	</div>
	
	<div class="col-10 col-md-10 pl-4">
	<select id="resultado_validacion" name="resultado_validacion" class="form-control validate[required]">
		<option value="">Seleccione...</option>
		<?php
		for($i=0;$i<count($resultado_validacion);$i++){
			echo "<option value='".$resultado_validacion[$i]->id_estado."'>".$resultado_validacion[$i]->descripcion."</option>";
		}
		?>
	</select>
	
	<div class="col-12 col-md-12 pl-4">
			<!--Author: Mario Beltrán mebeltran@saludcapital.gov.co Since: 12062019
		Validación Oracle lado Ciudadano existencia de Resoluciones-->
		<?php
		if(count($tramitesoracle)>0){
		?>
		<br>
		<div class="alert alert-danger">
		  <b>Apreciado Validador(a)!</b>
			<p>El sistema ha identificado que el numero de identificación del trámite actual ya cuenta con profesiones registradas en nuestras bases de datos existentes Oracle.<br>
			Agradecemos validar la siguiente información, si la profesión a realizar el trámite se encuentra a continuación. Favor abstenerse de continuar con la gestión del trámite.
			</p>
			<br>
			<?php
					for($i=0;$i<1;$i++){
					  echo "<center><b>Numero de Identificación:</b> ".$tramitesoracle[$i]->NROIDENT." - <b>Nombres y Apellidos:</b> ".$tramitesoracle[$i]->NOMBRES." ".$tramitesoracle[$i]->APELLIDOS."</center>";
					}
			?>
			<table style="width:100%;border: 1px solid #a94442;" border="1">
			  <thead>
				<tr>
				  <th><font color="#a94442"><b>Nombre Profesión</b></font></th>
				  <th><font color="#a94442"><b>Nombre Institución</b></font></th>
				  <th><font color="#a94442"><b>Fecha y No Resolución</b></font></th>
				</tr>
			  </thead>
			  <tbody>
				  <?php
					for($i=0;$i<count($tramitesoracle);$i++){
					  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->NOMBRE_PROFESION."</font></td>";
					  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->NOMBRE_INSTIT."</font></td>";
					  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->FECHA_RESOLUCION." - No Resolución: ".$tramitesoracle[$i]->NUMERO_RESOLUCION."</font></td></tr>";
					}
				  ?>
			  </tbody>
			</table>
		</div>
		<?php
		}
		?>

	
		<div id="div_resuelve" style="display:none">
			<div id="div_negacion" style="display:none">
				<div>
					<label><b>Causales de negaci&oacute;n:</b></label>
					<select id="causales_negacion" name="causales_negacion[]" class="form-control" multiple="multiple">
						<?php
						for($i=0;$i<count($causales_negacion);$i++){
							echo "<option value='".$causales_negacion[$i]->id_causal."'>".$causales_negacion[$i]->desc_causal."</option>";
						}
						?>
					</select>				
				</div>
				<div>
					<label><b>Otras causales de negaci&oacute;n:</b></label>
					<textarea name="otras_causales_negacion" id="otras_causales_negacion" class="form-control" style="width:100%;height:80px"></textarea>				
				</div>				
			</div>
			<div id="div_recurso" style="display:none">
				<div>
					<label><b>Argumentos del recurrente:</b></label>
					<textarea name="argumentos_recurrente" id="argumentos_recurrente" class="form-control" style="width:100%;height:80px"></textarea>				
				</div>
				<div>
					<label><b>Consideraciones de la Direcci&oacute;n de calidad de servicios de salud:</b></label>
					<textarea name="consideraciones" id="consideraciones" class="form-control" style="width:100%;height:80px"></textarea>				
				</div>
				<div>
					<label><b>Meritos Expuestos:</b></label>
					<textarea name="consideraciones2" id="consideraciones2" class="form-control" style="width:100%;height:80px"></textarea>
				</div>				
				<div>
					<label><b>Articulos:</b></label>
					<textarea name="articulos" id="articulos" class="form-control" style="width:100%;height:80px">ARTICULO PRIMERO:
					ARTICULO SEGUNDO:
                    </textarea>			
				</div>
			</div>

			<div id="div_masinformacion" style="display:none">
				<label><b>Digite la informaci&oacute;n adicional que desea que adjunte o ajuste el usuario:</b></label>
				<textarea name="mensaje" id="mensaje" class="form-control validate[required]" style="width:100%;height:80px"></textarea>

			</div>
			<div id="div_aclaracion" style="display:none">
			<br>
			<div class="alert alert-danger">
			  <b>Apreciado Validador(a)!</b>
				<p align="justify">El sistema ha identificado que se esta iniciando la gestión de una resolución de aclaración.<br>
				Por favor actualizar en la parte superior el dato a corregir quedando sobre la ficha del trámite los datos correctos segun corresponda. A continuación, seleccione el tipo motivo de aclaración para ingresar los datos errados y asi poder generar la proyección de la resolución de aclaración correctamente.
				<br> Hacerlo detenidamente para evitar nuevas novedades sobre el trámite. Si tiene dudas de como diligenciar estos items favor leer el manual de usuario.
				</p>
			</div>

			<?php
			if(count($tramites_seguimientos)>0){
				for($i=0;$i<count($tramites_seguimientos);$i++){
					if($tramites_seguimientos[$i]->tipomotivoaclaracion != NULL){
			?>	

				<div>
					<label><b>Tipo Motivo Aclaración Ciudadano:</b>
						<?php 
							if($tramites_seguimientos[$i]->tipomotivoaclaracion == 1){ echo "Error digitación Nombres y/o Apellidos";}
							if($tramites_seguimientos[$i]->tipomotivoaclaracion == 2){ echo "Error selección Titulación Profesión";}
							if($tramites_seguimientos[$i]->tipomotivoaclaracion == 3){ echo "Error selección Institución Educativa";}
							if($tramites_seguimientos[$i]->tipomotivoaclaracion == 4){ echo "Error selección Tipo de Documento";}
							if($tramites_seguimientos[$i]->tipomotivoaclaracion == 5){ echo "Error digitación Fecha de Grado";}
						?>
					</label>
					<input type="hidden" id="valtipomotivo" value="<?php echo $tramites_seguimientos[$i]->tipomotivoaclaracion; ?>">
				</div>

				<div>
					<label><b>Observaciones Motivo Aclaración Ciudadano:</b></label>
					<textarea name="observacion3aclaracion" id="observacion3aclaracion" class="form-control" style="width:100%;height:80px" readonly="true"><?php echo $tramites_seguimientos[$i]->observaciones; ?></textarea>
				</div>
			<?php
					}
				}
			}
			?>
				<div>
					<label><b>Tipo Motivo Aclaración:</b></label><br>
					<input type="checkbox" name="cboxmotivo1" id="cboxmotivo1" value="1"> Error digitación Nombres y/o Apellidos<br>
					<input type="checkbox" name="cboxmotivo2" id="cboxmotivo2" value="2"> Error selección Titulación Profesión<br>
					<input type="checkbox" name="cboxmotivo3" id="cboxmotivo3" value="3"> Error selección Institución Educativa<br>
					<input type="checkbox" name="cboxmotivo4" id="cboxmotivo4" value="4"> Error selección Tipo de Documento<br>
					<input type="checkbox" name="cboxmotivo5" id="cboxmotivo5" value="5"> Error digitación Fecha de Grado<br>
				</div>

				<div id="divmotivo1" style="display:none;">
					<label><b>Ingresar Nombres y Apellidos Errados:</b></label>
					<input type="text" name="nombresapellidos_errados" id="nombresapellidos_errados" class="form-control" value="<?php echo $tramites_pendientes->p_nombre . " ". $tramites_pendientes->s_nombre . " ". $tramites_pendientes->p_apellido . " ". $tramites_pendientes->s_apellido;?>" style="width:100%;">
				</div>
				
				
				<div id="divmotivo2" style="display:none;">
					<label><b>Ingresar Nombre Título Errado:</b></label>
					<select id="nombre_profesionnerrado" name="nombre_profesionnerrado" class="form-control select2 validate[required]">
						<?php
						for($i=0;$i<count($programas_result);$i++){
							if($tramites_pendientes->profesion == $programas_result[$i]->id_programa){
								echo "<option value='".$programas_result[$i]->nombre_programa."' selected>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";    
							}else{
								echo "<option value='".$programas_result[$i]->nombre_programa."'>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";    
							}
						}
						?>
					</select>
				</div>

				<div id="divmotivo3" style="display:none;">
					<label><b>Ingresar Nombre Institución Errado:</b></label>
					<select id="nombre_institucionerrado" name="nombre_institucionerrado" class="form-control select2 validate[required]">
						<?php
						for($i=0;$i<count($instituciones);$i++){
							if($tramites_pendientes->institucion_educativa2 == $instituciones[$i]->id_institucion){
								echo "<option value='".$instituciones[$i]->id_institucion."' selected>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";    
							}else{
								echo "<option value='".$instituciones[$i]->id_institucion."'>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
							}
						}
						?>
					</select>					
				</div>
				
				<div id="divmotivo4" style="display:none;">
					<label><b>Ingresar Nombre Tipo de Documento Errado:</b></label>
					<select id="tipo_identificacionerrada" name="tipo_identificacionerrada" class="form-control">
						<?php
						for($i=0;$i<count($tipo_identificacion);$i++){
							
							if($tramites_pendientes->tipo_identificacion == $tipo_identificacion[$i]->IdTipoIdentificacion){
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."' selected>".$tipo_identificacion[$i]->Descripcion."</option>";
							}else{
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."'>".$tipo_identificacion[$i]->Descripcion."</option>";
							}                                    
						}
						?>
					</select>
				</div>

				<div id="divmotivo5" style="display:none;">
					<label><b>Ingresar Fecha de Grado Errada (AAAA-MM-DD):</b></label>
					<input id="fecha_termerrada" name="fecha_termerrada" placeholder="Fecha Terminación" class="form-control input-md" type="text" value="<?php echo $tramites_pendientes->fecha_term;?>" autocomplete="off" readonly="readonly">
				</div>
				
				<div>
					<label><b>Parrafo Motivos Aclaración:</b></label>
					<textarea name="observacion1aclaracion" id="observacion1aclaracion" class="form-control" required style="width:100%;height:80px"></textarea>
				</div>
				<div>
					<label><b>Parrafo Justificación Motivos Aclaración:</b></label>
					<textarea name="observacion2aclaracion" id="observacion2aclaracion" class="form-control" style="width:100%;height:80px"></textarea>
				</div>
				<div>
					<label><b>Parrafo Artículo 1 Motivos Aclaración:</b></label>
					<textarea name="observacion3aclaracion" id="observacion3aclaracion" class="form-control" style="width:100%;height:80px"></textarea>
				</div>
			</div>			
			<div>
				<label><b>Observaciones</b></label>
				<textarea name="observaciones" id="observaciones" class="form-control" style="width:100%;height:80px"></textarea>
			</div>
			
			<div id="div_preliminar" style="display:none">
				<br><br>
				<button name="preliminar" id="preliminar" class="btn blue w-100 py-2" type="submit" class="btn btn-primary" role="button" formtarget="_blank">Preliminar</button>
			</div>
			<div>
				<br><br>
				<button name="guardar" id="guardar" class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button" onclick="this.disabled">Guardar</button>
			</div>
		</div>

	</div>
	</div>

	<div class="col-2 col-md-2 pl-4" align="center">
		<a id="btn_seguimiento" href="#" data-target="#modalseguimiento" data-toggle="modal" class="modalseguimientolink"><img src="<?php echo base_url('assets/imgs/audit.png')?>"><br>Seguimiento Auditoría</a>
		<br><br>
	</div>	
</div>
</fieldset>
</form>


<div class="modal fade" id="modalseguimiento" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog modal-lg" role="dialog">
    <div class="modal-content">
        <div class="modal-header" style="background:#3D5DA6;">
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Ventana de Seguimiento y Auditoría</h4>		
        </div>
        <div class="modal-body">
		<legend>Tabla de Seguimiento</legend>
			<table width="100%" border="1" id="customers">
				<thead>
					<tr>
						<th width="12%"><b>Fecha Seguimiento</b></th>
						<th width="25%"><b>Usuario</b></th>
						<th width="15%"><b>Estado</b></th>
						<th width="48%"><b>Observación</b></th>
					</tr>
				</thead>
				<tbody>
				<?php
				if(count($tramites_seguimientos)>0){
                for($i=0;$i<count($tramites_seguimientos);$i++){
                ?>
				<tr>
					<td style="height:55px;">
						<?php echo date("Y-m-d",strtotime($tramites_seguimientos[$i]->fecha_registro));?>
                    </td>
					<td style="height:55px;">
                        <?php echo $tramites_seguimientos[$i]->p_apellido." ".$tramites_seguimientos[$i]->p_apellido." ".$tramites_seguimientos[$i]->p_nombre." ".$tramites_seguimientos[$i]->s_nombre;?> 
                    </td>
                    <td style="height:55px;">
						<?php echo $tramites_seguimientos[$i]->descripcion?>
                    </td>
                    <td style="height:55px;">
						<?php if(strlen($tramites_seguimientos[$i]->observaciones)>165){ echo $tramites_seguimientos[$i]->observaciones; } else{ echo $tramites_seguimientos[$i]->observaciones;}?>
                    </td>
				</tr>
				<?php									
				}
				}
				?>
				</tbody>
			</table>			
        </div>
        <div class="modal-footer" align="center">
		<table width="100%">
		<tr>
			<td align="center">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</td>
		</tr>
		</table>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">

</script>
