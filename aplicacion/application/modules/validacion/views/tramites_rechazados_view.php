<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>

<form action="<?php echo base_url('validacion/rechazados/') ?>" method="post" autocomplete="off" class="form-horizontal">
	<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-">
                <div class="subtitle">
                    <h2><b>Filtro Consulta y Descarga Excel Tr&aacute;mites Rechazados</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Inicial:</b></label>
                    <input id="fecha_i" name="fecha_i" class="form-control" placeholder="Fecha Seguimiento Inicio" style="width:100%;">
                </div>
            </div>
			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Final:</b></label>
                    <input id="fecha_f" name="fecha_f" class="form-control" placeholder="Fecha Seguimiento Fin"  style="width:100%;">
                </div>
            </div>

			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input type="submit" class="btn btn-info" value="Consultar" style="width:100%;">
                </div>
            </div>
			<?php
				if($this->session->userdata('perfil')== 7){
			?>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="ExcelAC" value="Descargar Excel AC" style="width:100%;">
                </div>
            </div>			
			<?php
			}
			else if($this->session->userdata('perfil')== 3 OR $this->session->userdata('perfil')== 4 OR $this->session->userdata('perfil')== 5){
			?>			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="Excel" value="Descargar Excel VC" style="width:100%;">
                </div>
            </div>		
			<?php										
			}
			?> 			
</form>
		<div class="col-12 col-md-12">
		<br>
			<div class="alert alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			<b>Apreciado Usuario!</b>
			<p>La consulta y el reporte de tr&aacute;mites en estado de Rechazo, es por rango de fecha de acuerdo a la fecha del seguimiento generada por el sistema.<br>
			Los Tr&aacute;mites visualizados corresponden a los últimos 30 d&iacute;as desde la fecha actual, a excepci&oacute;n de los tr&aacute;mites que ya cuenten con resoluci&oacute;n. Sobre este reporte se puede presentar duplicidad en registros ya que el sistema permite rechazar el tr&aacute;mite varias veces.
			</p>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
		<br>
			<table class="table" id="tabla_tramites"  style="font-size:small;">
				<thead>
                    <tr>
						<th>ID Tr&aacute;mite</th>
                        <th>Identificaci&oacute;n</th>
                        <th>Nombres y Apellidos</th>
						<th>Fecha Radicaci&oacute;n</th>
                        <th>Fecha Seguimiento</th>
                        <th>Estado</th>
						<th>Observaci&oacute;n</th>
                        <th>PDF</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(count($tramites_rechazados)>0){
                        for($i=0;$i<count($tramites_rechazados);$i++){
                        ?>
                    <tr>
                        <td>
                            <?php echo $tramites_rechazados[$i]->id_titulo;?>
                        </td>
                        <td>
                            <?php echo $tramites_rechazados[$i]->descTipoIden." - ".$tramites_rechazados[$i]->nume_identificacion?>
                        </td>
                        <td>
                            <?php echo $tramites_rechazados[$i]->p_nombre." ".$tramites_rechazados[$i]->s_nombre." ".$tramites_rechazados[$i]->p_apellido." ".$tramites_rechazados[$i]->s_apellido?>
                        </td>
						<td>
                            <?php echo $tramites_rechazados[$i]->fecha_tramite?>
                        </td>
                        <td>
                            <?php echo $tramites_rechazados[$i]->fecha_seguimiento?>
                        </td>
						<td>
                           <?php
                                    if($tramites_rechazados[$i]->estado == 13){
                                        ?>
										Solicitar mas Informaci&oacute;n
                                        <?php
                                    }
									else{
                                        ?>
										Solicitar mas Informaci&oacute;n
                                        <?php										
									}
                            ?>
						</td>
						<td>
							<?php echo substr($tramites_rechazados[$i]->observaciones,0,25)?>...
                        </td>		  
                        <td>
                           <?php
                                    if($tramites_rechazados[$i]->estado == 13){
                                        ?>
                                        <center>
                                          <a href="<?php echo base_url('validacion/visualizar_documentos/'.$tramites_rechazados[$i]->id_titulo)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="20px">
                                            <br>Visualizar Informaci&oacute;n
                                            </a>
                                        </center>
                                        <?php
                                    }
                            ?>
                        </td>
                    </tr>
                <?php
						}
				}
				?>
                </tbody>
            </table>
		</div>
	</div>          
		
<!--Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 17062019
//Script Generar Excel-->		       
		<script type="text/javascript">
           $("#Excel").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelrechazados?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>
		
		<script type="text/javascript">
           $("#ExcelAC").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelrechazadosAC?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>	
		<script>
		// Get the modal
		var modal = document.getElementById("myModal");

		// Get the button that opens the modal
		var btn = document.getElementById("moreinfo");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks the button, open the modal 
		btn.onclick = function() {
		  modal.style.display = "block";
		}

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		  modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		  if (event.target == modal) {
			modal.style.display = "none";
		  }
		}
		</script>		
