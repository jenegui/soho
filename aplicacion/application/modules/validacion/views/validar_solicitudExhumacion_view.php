﻿<script type="text/javascript">
		
    $(document).ready(function(){
        $(".actualizarVisorAction").click(function() {                   
            $("#visorPdf").attr('src', $(this).attr('title'));            
        });
    });
                        
</script>

<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Documento Preliminar</h4> 
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <?php if(isset($documento_preliminar->nombre)){ ?>
                    <embed src="<?php echo base_url("uploads/exhumacion/" . $documento_preliminar->nombre)?>" width="100%" height="600" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">	
                   <?php }?>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background:#3D5DA6;">
                <h4 class="modal-title" id="myModalLabel" style="color:white;">Documento Preliminar</h4> 
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <?php $nombre_archivo = "validacion-".$listadosolicitudesExh_vali->idlicencia_exhumacion."-3-".date('Ymd').".pdf"; 
                    if(isset($nombre_archivo)){ ?>
                    <embed src="<?php echo base_url("uploads/exhumacion/" . $nombre_archivo)?>" id="idASG" width="100%" height="600" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">	
                    <?php } ?>
                </div>
            </div>
			<div class="modal-footer" align="center">
			<table width="100%">
			<tr>
				<td align="center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</td>
			</tr>
			</table>
			</div>
		</div>
    </div>
</div>

<form class="form-horizontal" id="form_tramite" name="form_tramite" action="<?php echo base_url('validacion/guardarAprobacionLicenciaExh/' . $listadosolicitudesExh_vali->idlicencia_exhumacion) ?>" method="post">
<div class="row block right" style="width:100%;">
			<div class="col-12 col-md-12 pl-4">
				<div class="subtitle">
					<h2><b>Tr&aacute;mite ID.<?php echo $listadosolicitudesExh_vali->idlicencia_exhumacion ?></b></h2>
					<h4><b>Estado Trámite:</b> <?php echo $listadosolicitudesExh_vali->des_estado ?>
					<br> <?php if (isset($documento_preliminar->nombre)) { ?>
					  <!--  <button id="verLic" type="button" class="btn btn-primary btn-md">
							Ver Documento Preliminar
						</button> -->
					  <!--  <a href="<?php //echo base_url("uploads/exhumacion/" . $documento_preliminar->nombre) ?>" target="_blank">Ver Documento</a> -->
					<?php }?>
						<b>Fecha Trámite:</b> <?php echo $listadosolicitudesExh_vali->fecha_solicitud?>
					</h4>
				</div>
            </div>
			<div class="col-12 col-md-12 text-center ">
				<h3><b>Datos Personales</b> <i class="fas fa-user"></i></h3>
            </div>
            <div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
                    <label for=""><b>Tipo Identificaci&oacute;n:</b></label>
					<input type="hidden" name="id_persona" value="<?php echo $listadosolicitudesExh_vali->id_persona?>">
					<input type="hidden" name="idlicencia_exhumacion" value="<?php echo $listadosolicitudesExh_vali->idlicencia_exhumacion?>">
					<select id="tipo_identificacion" name="tipo_identificacion" class="form-control validate[required]" required="">
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($tipo_identificacion);$i++){
							
							if($listadosolicitudesExh_vali->tipo_identificacion == $tipo_identificacion[$i]->IdTipoIdentificacion){
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."' selected>".$tipo_identificacion[$i]->Descripcion."</option>";
							}else{
								echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."'>".$tipo_identificacion[$i]->Descripcion."</option>";
							}                                    
						}
						?>
					</select>
                </div>
            </div>			
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
                    <label for="num_doc"><b>N&uacute;mero Identificaci&oacute;n:</b></label>
                    <input readonly="true" id="nume_documento" name="nume_documento" placeholder="Número de documento de identidad" class="form-control input-md validate[required, maxSize[11], custom[number]]" required="" type="text" value="<?php echo $listadosolicitudesExh_vali->nume_identificacion ?>" disabled style="width:100%;">
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Primer nombre:</b></label>
                    <input  id="p_nombre" name="p_nombre" placeholder="Primer Nombre" class="form-control validate[required, maxSize[80]]" required="" type="text" value="<?php echo $listadosolicitudesExh_vali->p_nombre?>">
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Segundo nombre:</b></label>
                    <input id="s_nombre" name="s_nombre" placeholder="Segundo Nombre" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $listadosolicitudesExh_vali->s_nombre?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Primer apellido:</b></label>
                    <input id="p_apellido" name="p_apellido" placeholder="Primer apellido" class="form-control validate[required, maxSize[80]]" type="text" value="<?php echo $listadosolicitudesExh_vali->p_apellido?>" required="">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Segundo apellido:</b></label>
					<input id="s_apellido" name="s_apellido" placeholder="Segundo apellido" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $listadosolicitudesExh_vali->s_apellido?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Correo:</b></label>
					<input id="email" name="email" placeholder="Correo eletrónico " class="form-control input-md validate[required, custom[email]]" required="" type="text" value="<?php echo $listadosolicitudesExh_vali->email?>" required="">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Tel&eacute;fono fijo:</b></label>
					<input id="telefono_fijo" name="telefono_fijo" placeholder="Teléfono Fijo" class="form-control input-md validate[maxSize[10], custom[number]]" type="text" value="<?php echo $listadosolicitudesExh_vali->telefono_fijo?>">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<label for=""><b>Teléfono celular:</b></label>
					<input id="telefono_celular" name="telefono_celular" placeholder="Teléfono celular" class="form-control input-md validate[required, maxSize[10], custom[number]]" type="text" value="<?php echo $listadosolicitudesExh_vali->telefono_celular?>" required="">
				</div>
            </div>			

			<div class="col-12 col-md-12 text-center ">
			<br><br>
				<h3><b>Informaci&oacute;n de la solicitud y Archivos Adjuntos<i class="fas fa-address-card"></i></b></h3>
            </div>
			<div class="col-12 col-md-6 pl-4">
			
			<div>
                <div class="paragraph">
					<label for=""><b>Número Licencia de Inhumación:</b></label>
					<input id="numero_licencia" name="numero_licencia" placeholder="Licencia de Inhumación" class="form-control validate[required,maxSize[11], custom[number]]" required="" type="text" value="<?php echo $listadosolicitudesExh_vali->numero_licencia?>" autocomplete="off" disabled>
				</div>
            </div>
			</div>
			<!--
			<div>
                <div class="paragraph">
					<label for=""><b>Registro Defunción:</b></label>
					<input id="registro_defuncion" name="registro_defuncion" placeholder="Registro Defunción" class="form-control validate[required, maxSize[80]]" required="" type="text" value="<?php //echo $listadosolicitudesExh_vali->numero_regdefuncion?>">
				</div>
            </div>	
			-->
			<div class="col-12 col-md-6 pl-4">
			<div>
                <div class="paragraph">
					<label for=""><b>Fecha de Inhumación:</b></label>
					<input id="fecha_inhumacion" name="fecha_inhumacion" placeholder="Fecha de Inhumación" class="form-control input-md validate[required]" type="text" value="<?php echo $listadosolicitudesExh_vali->fecha_inhumacion?>" autocomplete="off" required="" disabled>
				</div>
            </div>
			</div>

			<div class="col-12 col-md-6 pl-4">
			<!--
			<div>
                <div class="paragraph">
					<label for=""><b>Número Documento Fallecido:</b></label>
					<input id="numero_docfallecido" name="numero_docfallecido" placeholder="Número Documento Fallecido" class="form-control input-md validate[required, maxSize[11], custom[number]]" required="" type="text" value="<?php //echo $listadosolicitudesExh_vali->numero_docfallecido?>">
				</div>
            </div>-->			
			<div>
                <div class="paragraph">
					<label for=""><b>Indicación de intervención de Medicina Legal:</b></label>
					<select id="intervienemedlegal" name="intervienemedlegal" class="validate[required] form-control">
					  <option value="" selected="true" disabled>Seleccione una opción</option>
					  <option value="0" <?php if($listadosolicitudesExh_vali->interviene_medlegal==0) echo "selected"?>>No</option>
					  <option value="1" <?php if($listadosolicitudesExh_vali->interviene_medlegal==1) echo "selected"?>>SI</option>
					</select>
				</div>
            </div>		
			</div>

			<div class="col-12 col-md-6 pl-4">
			<div>
                <div class="paragraph">
					<label for=""><b>Parentezco con el Difunto:</b></label>			
					<select name="parentesco"  class="validate[required] form-control" id="parentesco" required>
						<option value="<?php print $listadosolicitudesExh_vali->parentesco?>"><?php print $listadosolicitudesExh_vali->parentesco ?></option>
						<option value="Conyugue">Conyugue/compañero(a) permanente</option>
						<option value="Padre/Madre">Padre/Madre</option>
						<option value="Hermano(a)">Hermano(a)</option>
						<option value="Hijo(a)">Hijo(a)</option>
						<option value="Abuelo(a)">Abuelo(a)</option>
						<option value="Nieto(a)">Nieto(a)</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
            </div>	
			</div>			

			<div class="col-12 col-md-12 pl-4" id="divparentesco" style="display:none">
			<div>
                <div class="paragraph">
					<label for=""><b>Digite el parentesco evidenciado:</b></label>			
					<input id="parentescoOTRO" name="parentescoOTRO" placeholder="Otro Parentesco" class="form-control validate[maxSize[80]]" type="text" value="">
				</div>
            </div>	
			</div>

			<div class="col-12 col-md-12 ">
			<br><br>
			</div>
			<div class="col-4 col-md-4 " id="divdatoarch">
				<table class="table">
					<tr>
						<th>Descripci&oacute;n</th>
						<th colspan="2">Ver</th>
					</tr>


                    <?php
                    if ($listadosolicitudesExh_vali->pdf_cedulasolicitante != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_cedulasolicitante);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento Solicitante</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }
                    if ($listadosolicitudesExh_vali->pdf_otro != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_otro);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento Acreditar Parentesco</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>	
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }

                    if ($listadosolicitudesExh_vali->pdf_certificadocementerio != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_certificadocementerio);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Certificado cementerio</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }

                    if ($listadosolicitudesExh_vali->pdf_ordenjudicial != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_ordenjudicial);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento Orden Judicial</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>	
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }

                    if ($listadosolicitudesExh_vali->pdf_autorizacionfiscal != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_autorizacionfiscal);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento Fiscalía</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }
					
                    if ($listadosolicitudesExh_vali->pdf_certificado_per4 != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_certificado_per4);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr class="inp-form">
                            <td>Certificado permanencia mínimo 4 años</td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>	
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }

                    if ($listadosolicitudesExh_vali->pdf_certificado_per3 != 0) {
                        $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh_vali->pdf_certificado_per3);
						$visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/exhumacion/'.$resultado_archivo->nombre);
                        ?>
                        <tr class="inp-form">
                            <td>Certificado permanencia mínimo 3 años </td>
                            <td>
                                <a href="<?php echo base_url('uploads/exhumacion/' . $resultado_archivo->nombre) ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="40px">
                                </a>
							</td>
							<td>	
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
			
			<div class="col-8 col-md-8 " style="height: 450px;" id="divdatoarchvisor">
				<iframe id="visorPdf" style="height: 100%; width: 100%;" src=""></iframe>
			</div>
		</fieldset>
</div>

<div class="row block right" style="width:100%;">
		<div class="col-12 col-md-12 pl-4">
			<div class="subtitle">
				<h2><b>Resultado de la validaci&oacute;n</b> <i class="fas fa-check-double"></i></h2>
			</div>
		</div>
	
	
	<div class="col-10 col-md-10 pl-4">
		<div class="col-12 col-md-12 pl-4">
		<input type="hidden" value="<?php echo $listadosolicitudesExh_vali->estado ?>" name="idestado">
		<input type="hidden" value="<?php echo $listadosolicitudesExh_vali->idlicencia_exhumacion ?>" id="idlicec">
		<select id="resultado_validacion" required="true" name="resultado_validacion" class="form-control validate[required]">
			<option value="0">Seleccione...</option>
			<option value="2">Solicitar más información</option>
			<option value="3">Aprobado Validador</option>
			<option value="5">Rechazado</option>
		</select>
		</div>
		
		<div class="col-12 col-md-12 pl-4">
		<br><br>
			<?php if(isset($info_aprobacionL->num_licencia_inhumacion)){?>
			<div class="alert alert-danger">
						<?php echo "<b><center> Nota: ya existe una licencia de exhumación en Mysql, asociada a la licencia de inhumación: ";
						echo $info_aprobacionL->num_licencia_inhumacion . ' con fecha de inhumación: '. $info_aprobacionL->fecha_inhumacion;
						echo '<br>El número de licencia de Exhumación es: '.$info_aprobacionL->numero_lic_exhu . " con fecha: ".$info_aprobacionL->fecha_aprob;
						echo '</center></b>';
						?>
			</div><br><br>
			<?php } 
			 if(isset($informacionLEXH_oracle->EXH_NUM_LICENCIA)){?>
			<div class="alert alert-danger">
						<?php echo "<b><center> Nota: ya existe una licencia de exhumación en Oracle, asociada a la licencia de inhumación: ";
						echo $listadosolicitudesExh_vali->numero_licencia . ' con fecha de inhumación: '. $listadosolicitudesExh_vali->fecha_inhumacion;
						echo '<br> El número de licencia de Exhumación es: '.$informacionLEXH_oracle->EXH_NUM_LICENCIA. " con fecha: ".$informacionLEXH_oracle->EXH_FEC_LICENCIA;
						echo '</center></b>';
						?>
			</div>
			<?php
			}
			?>
		</div>
        <?php  
		
       
		$aprobacion_preliminar = $this->mlicencia_exhumacion->info_aprobacionlicencia($listadosolicitudesExh_vali->idlicencia_exhumacion);
		//var_dump($aprobacion_preliminar);
		//var_dump($datos_oracle)
		
        ?>
	</div>

	<div class="col-2 col-md-2 pl-4" align="center">
		<a id="btn_seguimiento" href="#" data-target="#modalseguimiento" data-toggle="modal" class="modalseguimientolink"><img src="<?php echo base_url('assets/imgs/audit.png')?>"><br>Seguimiento Auditoría</a>
		<br><br>
	</div>		

		<div id="div_resuelve">
			<!-- -->

			<div id="aprobado">
			
			<div class="alert alert-danger">
			  <b>Apreciado Validador(a)!</b>
				<p align="justify">El sistema ha consultado información sobre la licencia de Inhumación, contenida en el aplicativo ORACLE.<br>
				Por favor revisar la siguiente información, si esta no coincide o no aparece; con los datos de Inhumación de la solicitud del trámite, realice la consulta en ORACLE para validar la existencia previa y correcta de la Licencia de Inhumación.
				<br>A continuación, podrá ajustar los datos de Número Licencia y Fecha de Inhumación del trámite, para que se actualice la consulta de ORACLE con los datos correctos que serán proyectados en la Licencia de Exhumación. Cualquier inquietud remitirse al manual o al referente del sistema.
				</p>
			</div>
			
			
				<div class="col-12 col-md-12 pl-4">
					<table style="width:100%;">
					<tr>
					<td>
					<label for="cementerio"><b>Tipo de muerte:</b> <?php if (isset($datos_oracle->TIPO_MUERTE) && $datos_oracle->TIPO_MUERTE != '')   echo trim($datos_oracle->TIPO_MUERTE)?> </label>
					</td>
					<td>
					<label for="cementerio"><b>Código Medicina Legal:</b>
						<?php if (isset($datos_oracle->COD_INST) && $datos_oracle->COD_INST != '') echo trim($datos_oracle->COD_INST).'  '?>
						<?php if (isset($datos_oracle->RAZON_INST) && $datos_oracle->RAZON_INST != '') echo trim($datos_oracle->RAZON_INST)?>					   
					</label>
					</td>
					</table>
				</div>
				
				<div class="col-12 col-md-12 pl-4" >
					<label for="cementerio"><b>Número de Documento Difunto:</b></label>
					<input  type="text"  name="num_docdifunto" id="num_docdifunto" title=" Dato Oracle: <?php if(isset($datos_oracle)){ echo $datos_oracle->NROIDENT; } ?>" class="form-control validate[maxSize[60]]" 
						value="<?php if (isset($aprobacion_preliminar->num_docdifunto) && $aprobacion_preliminar->num_docdifunto != '') {
								echo $aprobacion_preliminar->num_docdifunto;
							}else{
								echo $datos_oracle->NROIDENT;
							  }?>"	required="" readonly="true">
				</div>
				<div class="col-12 col-md-12 pl-4" >
				
					<label for="nombre_difunto"><b>Nombre completo difunto:</b></label>
						<?php
						$fetal = isset($datos_oracle->FETAL_Y_NO_FETAL) ? $datos_oracle->FETAL_Y_NO_FETAL : '';
						$fetales = trim($fetal);
						$nombre = isset($datos_oracle->PRIMER_NOMBRE) ? $datos_oracle->PRIMER_NOMBRE : '';
						$nombre .= isset($datos_oracle->SEGUNDO_NOMBRE) ? ' ' . $datos_oracle->SEGUNDO_NOMBRE : '';
						$nombre .= isset($datos_oracle->PRIMER_APELLIDO) ? ' ' . $datos_oracle->PRIMER_APELLIDO : '';
						$nombre .= isset($datos_oracle->SEGUNDO_APELLIDO) ? ' ' . $datos_oracle->SEGUNDO_APELLIDO : '';
						if(isset($aprobacion_preliminar->nombre_difunto)){ 
						?>
						<input type="text"  name="nombre_difunto" id="nombre_difunto" title=" Dato Oracle: <?php if(isset($nombre)){ echo $nombre; } ?>" class="form-control validate[required, maxSize[90]]" value="<?php echo $aprobacion_preliminar->nombre_difunto; ?>">
						<?php
						}else if ($fetales == "FETAL"){
						?>
						<input type="text"  name="nombre_difunto" id="nombre_difunto" title=" Dato Oracle: <?php if(isset($nombre)){ echo $nombre; } ?>" class="form-control validate[required, maxSize[90]]" value=" HIJO DE <?php echo $nombre; ?>">
						<?php
						} else {
						?>
						<input type="text"  name="nombre_difunto" id="nombre_difunto" title=" Dato Oracle: <?php if(isset($nombre)){ echo $nombre; } ?>" class="form-control validate[required, maxSize[90]]" value="<?php echo $nombre; ?>">
						<?php
						}
						//}
						?>
				</div>
				<div class="col-12 col-md-12 pl-4" >
					<label for="cementerio"><b>Número Certificado de Defunción:</b></label>
					<input  type="text"  name="num_certdefunsion" id="num_certdefunsion" title=" Dato Oracle: <?php if(isset($datos_oracle)){ echo $datos_oracle->NUM_CERTIFICADO_DEFUNCION; } ?>"  class="form-control validate[required, maxSize[60]]" 
						value="<?php if (isset($aprobacion_preliminar->num_certdefunsion) && $aprobacion_preliminar->num_certdefunsion != '') {
								echo $aprobacion_preliminar->num_certdefunsion;
							}else{
								echo $datos_oracle->NUM_CERTIFICADO_DEFUNCION;
							}
						?>"	required="" readonly="true">

				</div>
				<div class="col-12 col-md-12 pl-4" >
					<label for="cementerio"><b>Cementerio:</b></label>
					<input  type="text"  name="cementerio" id="cementerio" title=" Dato Oracle: <?php if(isset($datos_oracle)){ echo $datos_oracle->CEMENTERIO; } ?>" class="form-control validate[required, maxSize[60]]" 
						value="<?php if (isset($aprobacion_preliminar->cementerio) && $aprobacion_preliminar->cementerio != '') {
								echo $aprobacion_preliminar->cementerio;
							}else{
								echo  trim($datos_oracle->CEMENTERIO);} ?> " required=""> 
				</div>				
				<div class="col-12 col-md-12 pl-4" >
					<label for=""><b>Número Lic. Inhumación:</b> Oracle</label>
					<input type="number"  name="num_licencia_inhumacion" id="num_licencia_inhumacion" title=" Dato Oracle: <?php if(isset($datos_oracle)){ echo $datos_oracle->INH_NUM_LICENCIA; } ?>" class="form-control validate[required,maxSize[11], custom[number]]" required="" value="<?php if (isset($datos_oracle->INH_NUM_LICENCIA)){ echo $datos_oracle->INH_NUM_LICENCIA;} ?>" required="" readonly="true">
				</div>
				<div class="col-12 col-md-12 pl-4" >
					<label for=""><b>Fecha Inhumación:</b> Oracle</label>
					<input name="fecha_inh"  id="fecha_inh" class="form-control input-md validate[required]" title=" Dato Oracle: <?php if(isset($datos_oracle)){ echo $datos_oracle->INH_FEC_LICENCIA; } ?>" value="<?php if (isset($datos_oracle->INH_FEC_LICENCIA)){ echo $datos_oracle->INH_FEC_LICENCIA;} ?>" required="" readonly="true">
				</div>
				<div class="col-12 col-md-12 pl-4" >
					<label for="observaciones"><b>Observaciones Importantes:</b></label>
					<textarea name="observaciones" style="text-transform: uppercase;" id="observaciones" class="form-control" style="width:100%;height:80px"><?php if(isset($aprobacion_preliminar->observaciones)){ echo $aprobacion_preliminar->observaciones; } ?></textarea>
				</div>
				<div class="col-12 col-md-12 pl-4" >
					<label for="observaciones"><b>Observaciones Internas:</b></label>
					<textarea name="observacionesint" id="observacionesint" class="form-control" style="width:100%;height:80px"></textarea>
				</div>
				<!--
				<div class="col-12 text-center">
					<br>
					<table style="width:100%;">
					<tr>
					<td style="width:50%;">
					<input class="btn blue w-100 py-2" type="button" name="VPDF" id="VPDF" class="btn btn-primary" value="Generar PDF">
					</td>
					<td style="width:50%;">if(isset($datos_oracle)){
					<input class="btn blue w-100 py-2" type="button" id="BtnVerPDF" style="display:none" data-toggle="modal" data-target="#myModalPDF" value="VER PDF">
					</td>
					</table>
				</div>
				-->
			</div><!-- -->
			<!-- -->
			<div id="div_masinformacion" style="display:none">
				<div class="col-12 col-md-12 pl-4">
					<label for="mensaje" style="width:100%;"><b>Digite la informaci&oacute;n  que desea que actualice el Ciudadano(a), esta información le llegara al correo electrónico y estara disponible en la sección de observaciones - Mis Trámites con la opción de editar el trámite:</b></label>
					<textarea name="mensaje" id="mensaje" class="form-control validate[required]" style="width:100%;height:80px"></textarea>
				</div>
			</div>
			<div id="div_rechazado" style="display:none">
				<div class="col-12 col-md-12 pl-4">
					<label for="mensajeR" style="width:100%;"><b>Digite la informaci&oacute;n por la cual se rechaza la solicitud, esta información le llegara al Ciudadano(a) por correo electrónico y estara disponible en la sección de observaciones - Mis Trámites::</b></label>
					<textarea name="mensajeR" id="mensajeR" class="form-control validate[required]" style="width:100%;height:80px"></textarea>
				</div>
			</div>
			

			<div id="div_preliminar" style="display:none">
				<br><br>
				<button name="preliminar" id="preliminar" class="btn blue w-100 py-2" type="submit" class="btn btn-primary" role="button" formtarget="_blank">Preliminar</button>
			</div>
			<div id="div_guardar" style="display:none">
				<br><br>
				<button name="guardar" id="guardar" class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button" onclick="this.disabled">Guardar</button>
			</div>
	</div>
</div>
</form>				

<form class="form-horizontal" id="form_tramite2" name="form_tramite2" action="<?php echo base_url('validacion/actualizarLicenciaInh/')?>" method="post">
<div id="div_actualizarInh">
<div class="row block right" style="width:100%;">
		<div class="col-12 col-md-12 pl-4">
			<div class="subtitle">
				<h2><b>Ajustar Datos Inhumación Consulta ORACLE</b> <i class="fas fa-check-double"></i></h2>
			</div>
		</div>

				<div class="col-12 col-md-6 pl-4" >
					<label for=""><b>Número Licencia de Inhumación:</b> Registro Trámite</label>
					<input type="hidden" name="idlicencia_exhumacion" value="<?php echo $listadosolicitudesExh_vali->idlicencia_exhumacion?>">
					<input id="numero_licencia" name="numero_licencia" placeholder="Licencia de Inhumación" class="form-control validate[required,maxSize[11], custom[number]]" required="" type="text" value="<?php echo $listadosolicitudesExh_vali->numero_licencia?>" autocomplete="off" required="">
				</div>
		
				<div class="col-12 col-md-6 pl-4" >
					<label for=""><b>Fecha de Inhumación:</b> Registro Trámite</label>
					<input id="fecha_inhumacion2" name="fecha_inhumacion" placeholder="Fecha de Inhumación" class="form-control input-md validate[required]" type="text" value="<?php echo $listadosolicitudesExh_vali->fecha_inhumacion?>" autocomplete="off" required="" readonly="">
				</div>
				<div class="col-12 col-md-12 pl-4">
					<br><br>
					<button name="actualizarInhuma" id="actualizarInhuma" class="btn yellow w-100 py-2" type="submit" class="btn btn-primary" role="button" onclick="this.disabled">Actualizar Inhumación</button>
				</div>				

</div>
</div>
</form>				




<div class="modal fade" id="modalseguimiento" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog modal-lg" role="dialog">
    <div class="modal-content">
        <div class="modal-header" style="background:#3D5DA6;">
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Ventana de Seguimiento y Auditoría</h4>		
        </div>
        <div class="modal-body">
		<legend>Tabla de Seguimiento</legend>
			<table width="100%" border="1" id="customers">
				<thead>
					<tr>
						<th width="12%"><b>Fecha Seguimiento</b></th>
						<th width="25%"><b>Usuario</b></th>
						<th width="15%"><b>Estado</b></th>
						<th width="48%"><b>Observación</b></th>
					</tr>
				</thead>
				<tbody>
				<?php
				if(count($tramites_seguimientos)>0){
                for($i=0;$i<count($tramites_seguimientos);$i++){
                ?>
				<tr>
					<td style="height:55px;">
						<?php echo date("Y-m-d",strtotime($tramites_seguimientos[$i]->fecha_registro));?>
                    </td>
					<td style="height:55px;">
						<?php echo $tramites_seguimientos[$i]->p_apellido." ".$tramites_seguimientos[$i]->p_apellido." ".$tramites_seguimientos[$i]->p_nombre." ".$tramites_seguimientos[$i]->s_nombre;?> 
                    </td>
                    <td style="height:55px;">
						<?php echo $tramites_seguimientos[$i]->des_estado?>
                    </td>
                    <td style="height:55px;">
						<?php if(strlen($tramites_seguimientos[$i]->observaciones)>165){ echo $tramites_seguimientos[$i]->observaciones; } else{ echo $tramites_seguimientos[$i]->observaciones;}?>
                    </td>
				</tr>
				<?php
				}
				}
				?>
				</tbody>
			</table>			
        </div>
        <div class="modal-footer" align="center">
		<table width="100%">
		<tr>
			<td align="center">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</td>
		</tr>
		</table>
        </div>
    </div>
</div>
</div>


<script>
</script>