<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resoluci&oacute;n Aclaraci&oacute;n</title>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 10px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #4F5155;
        }

        table {
            border: 1px solid #fff;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        .encabezados {
            color: #000000;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        h2 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 14px;
            font-weight: bold;
            margin: 5px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        code {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        .centro {
            text-align: center;
        }
        
        .justificado {
            text-align: justify;
        }

        .derecha {
            text-align: right;
        }

        .total {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #000000;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
            color: #002166;
        }

        .marca-de-agua {
            padding: 0;
            width: 100%;
            height: auto;
            opacity: 0.7;
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 25px;
        }

    </style>
</head>

<body>
    <table width="100%">
        <tr align="left">
            <td width="100%">                
                <img src="<?php echo FCPATH.'assets/imgs/logo_pdf_alcaldia.png'?>" width="150px">
            </td>
        </tr>
    </table>
    <table class="table centro" width="100%">
        <tr class="encabezados">
            <td width="100%">
                <h1>Resoluci&oacute;n No A<?php echo $nume_resolucion;?> del d&iacute;a <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
                Secretaría Distrital de Salud de Bogot&aacute; D.C</h1>
            </td>
        </tr>
        <tr>
            <td width="100%">
                Por la cual se aclara la resolución No A<?php if(!empty($resolucion)){echo $resolucion->id_resolucion;} ?> del 
				<?php
					if(!empty($resolucion)){
						$dia = date("d", strtotime($resolucionarchivo->fecha)); 

						echo $dia;
						
						switch(date("M", strtotime($resolucionarchivo->fecha))){
							case 'Jan':
								$mes2 = 'Enero';
								break;
							case 'Feb':
								$mes2 = 'Febrero';
								break;
							case 'Mar':
								$mes2 = 'Marzo';
								break;
							case 'Apr':
								$mes2 = 'Abril';
								break;
							case 'May':
								$mes2 = 'Mayo';
								break;
							case 'Jun':
								$mes2 = 'Junio';
								break;
							case 'Jul':
								$mes2 = 'Julio';
								break;
							case 'Aug':
								$mes2 = 'Agosto';
								break;
							case 'Sep':
								$mes2 = 'Septiembre';
								break;
							case 'Oct':
								$mes2 = 'Octubre';
								break;
							case 'Nov':
								$mes2 = 'Noviembre';
								break;
							case 'Dec':
								$mes2 = 'Diciembre';
							break;
						}
						echo " de ".$mes2;
						
						$año = date("Y", strtotime($resolucionarchivo->fecha));
						
						echo " de ".$año;
					}
					?>.<br>
                <br><b>LA SUBDIRECCIÓN INSPECCIÓN, VIGILANCIA Y CONTROL DE SERVICIOS DE SALUD</b>					

            </td>
        </tr>
    </table>
    <p class="justificado">
        En uso de sus facultades legales y en especial las conferidas por el Decreto 780 de 2016, Ley 1164 de 2007 y Resolución 3030 de 2014  del Ministerio de Salud y Protección Social y,
    </p>
    <p class="centro">
        <b>CONSIDERANDO</b>
    </p>
    <p class="justificado">
		Que mediante resolución No A<?php if(!empty($resolucion)){echo $resolucion->id_resolucion; } ?> del <?php if(!empty($resolucion)){ echo $dia; echo " de ".$mes2; echo " de ".$año; } ?> la Secretaría Distrital de Salud de Bogotá D.C, 
		otorgó a el(la) señor(a) 
		<?php 	
			if(!empty($nombresapellidos_errados)) {
				echo $nombresapellidos_errados; 
			}
			else{
				echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "."";
			}
		?>
        Identificado(a) con 
		<?php 
			if(!empty($tipo_identificacionerrada)){
				echo $tipo_identificacionerrada;
			}
			else{
				echo $datos_tramite->descTipoIden;
			}
		?>
						   número <?php echo $datos_tramite->nume_identificacion?>, la autorización del ejercicio de su profesión/ocupación 
		
		
				<?php 	
				if(!empty($nombre_profesionnerrado)){
					if($datos_tramite->tipo_titulo == 1){
						echo $nombre_profesionnerrado; ?> conferido por 
						<?php
						if(!empty($nombre_institucionerrado)){
							echo $nombre_institucionerrado; ?>,
						<?php
						}else{
							echo $datos_tramite->nombre_institucion?>,
						<?php
						}
						?>
				<?php
					}else{ 
						echo strtoupper($nombre_profesionnerrado);?> conferido por 
						<?php
						if(!empty($nombre_institucionerrado)){
							echo $nombre_institucionerrado; ?>,
						<?php
						}else{
							echo strtoupper($datos_tramite->nombre_institucion);?>,
						<?php
						}
						?>
						
				<?php 
					}
				}else{
					if($datos_tramite->tipo_titulo == 1){
						echo $datos_tramite->nombre_programa?> conferido por 
						<?php
						if(!empty($nombre_institucionerrado)){
							echo $nombre_institucionerrado; ?>,
						<?php
						}else{
							echo $datos_tramite->nombre_institucion?>,
						<?php
						}
						?>
						
				<?php
					}else{ 
						echo strtoupper($datos_tramite->nombreprogramaequivalente);?> conferido por 
						<?php
						if(!empty($nombre_institucionerrado)){
							echo $nombre_institucionerrado; ?>,
						<?php
						}else{
							echo strtoupper($datos_tramite->cod_universidad);?>,
						<?php
						}
					}
				}
				?>
		en el territorio nacional.<br><br>
        Que el(la) señor(a) <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
		mediante comunicación con el No <?php echo $datos_tramite->id_titulo; ?>, solicita que se aclare:
        <?php echo $observacion1aclaracion; ?>
        
        <br> 
		<?php echo $observacion2aclaracion; ?>
		<br>
    </p>
	    
		

    <p class="justificado">
        En virtud de lo expuesto este Despacho,
    </p>
    <p class="centro">
        <b>RESUELVE:</b>
    </p>
    <p class="justificado">
        <b>ARTICULO PRIMERO</b>: Aclarar la Resolución No A<?php if(!empty($resolucion)){echo $resolucion->id_resolucion; } ?> del <?php if(!empty($resolucion)){ echo $dia; echo " de ".$mes2; echo " de ".$año; } ?>,
		expedida por la Secretaría Distrital de Salud de Bogotá D.C en el sentido de: <?php echo $observacion3aclaracion; ?>
    </p>
    <p class="justificado">
        <b>ARTICULO SEGUNDO:</b> Las demás partes de la resolución No A<?php if(!empty($resolucion)){echo $resolucion->id_resolucion; } ?> del <?php if(!empty($resolucion)){ echo $dia; echo " de ".$mes2; echo " de ".$año; } ?>  expedida por la Secretaría Distrital de Salud de Bogotá D.C se mantienen en firme.
    </p>
    <p class="justificado">
        <b>NOTIFIQUESE, Y CÚMPLASE</b><br>
        Dada en  Bogotá, D.C. a los <?php echo date('d')." d&iacute;as del mes de ".$mes." del a&ntilde;o ".date('Y')?>
    </p>
    <br><br><br><br>
    <p class="justificado">
        <?php
            if($firma == true){
                ?>
                <img src="<?php echo FCPATH.'assets/imgs/firma_docrosmira.JPG'?>" width="300px"><br>
                Subdirector (a) Inspección Vigilancia y Control de Servicios de Salud (E).<br>
                C&oacute;digo de verificaci&oacute;n: <?php echo $codigo_verificacion;?>
                <?php
            }
        ?>
    </p>

</body>

</html>
