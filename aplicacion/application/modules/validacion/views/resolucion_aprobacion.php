<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resoluci&oacute;n Aprobaci&oacute;n</title>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 10px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #4F5155;
        }

        table {
            border: 1px solid #fff;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        .encabezados {
            color: #000000;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        h2 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 14px;
            font-weight: bold;
            margin: 5px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        code {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        .centro {
            text-align: center;
        }
        
        .justificado {
            text-align: justify;
        }

        .derecha {
            text-align: right;
        }

        .total {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #000000;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
            color: #002166;
        }

        .marca-de-agua {
            padding: 0;
            width: 100%;
            height: auto;
            opacity: 0.7;
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 25px;
        }

    </style>
</head>

<body>
    <table width="100%">
        <tr align="left">
            <td width="100%">
                <!--<img src="<?php echo FCPATH.'assets/imgs/logo_pdf_alcaldia.png'?>" width="200px">-->
                <img src="<?php echo FCPATH.'assets/imgs/logo_pdf_alcaldia.png'?>" width="150px">
            </td>
        </tr>
    </table>
    <table class="table centro" width="100%">
        <tr class="encabezados">
            <td width="100%">
                <h1>Resoluci&oacute;n No A<?php echo $nume_resolucion;?> del d&iacute;a <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
                Secretaría Distrital de Salud de Bogot&aacute; D.C</h1>
            </td>
        </tr>
        <tr>
            <td width="100%">
                Por la cual se autoriza el ejercicio de una profesión/ocupación en el Territorio Nacional.
                <br><b>LA SUBDIRECCIÓN INSPECCIÓN, VIGILANCIA Y CONTROL DE SERVICIOS DE SALUD</b>

            </td>
        </tr>
    </table>
    <p class="justificado">
        En uso de sus facultades legales y en especial las conferidas por el Decreto 780 de 2016, Ley 1164 de 2007 y Resolución 3030 de 2014  del Ministerio de Salud y Protección Social y,
    </p>
    <p class="centro">
        <b>CONSIDERANDO</b>
    </p>
    <p class="justificado">
        Que el(la) señor(a) <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        Identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
		<?php if($datos_tramite->tipo_titulo == 1){	?>
        solicitó ante esta Secretaría la autorización del ejercicio de su profesión/ocupación <?php echo $datos_tramite->nombre_programa?> otorgado por <?php echo $datos_tramite->nombre_institucion?>, el día <?php echo $datos_tramite->fecha_term?>, 
		<?php 
			if($datos_tramite->acta != ''){
            echo "con el acta ".$datos_tramite->acta.", ";
			}
			
			if($datos_tramite->libro != ''){
				echo "registrado en el libro ".$datos_tramite->libro.", ";
			}
			
			if($datos_tramite->folio != ''){
				echo "Folio ".$datos_tramite->folio.", ";
			}
			echo 'año '.$datos_tramite->anio.'.';
			
		}else{ ?>
        solicitó ante esta Secretaría la autorización del ejercicio de su profesión/ocupación <?php echo strtoupper($datos_tramite->nombreprogramaequivalente);?> otorgado por <?php echo strtoupper($datos_tramite->cod_universidad);?>, el día <?php echo $datos_tramite->fecha_term_ext?>, 
        <?php
        }
        ?>
    </p>
	<?php
		$dateTime = date_create_from_format('Y-m-d', $datos_tramite->fecha_resolucion);		
		
		switch (date_format($dateTime, 'm')) {
			case 1: $strMes= 'Enero'; break;
			case 2: $strMes= 'Febrero'; break;
			case 3: $strMes= 'Marzo'; break;
			case 4: $strMes= 'Abril'; break;
			case 5: $strMes= 'Mayo'; break;
			case 6: $strMes= 'Junio'; break;
			case 7: $strMes= 'Julio'; break;
			case 8: $strMes= 'Agosto'; break;
			case 9: $strMes= 'Septiembre'; break;
			case 10: $strMes= 'Octubre'; break;
			case 11: $strMes= 'Noviembre'; break;
			case 12: $strMes= 'Diciembre'; break;
		default:'';
		}
		
		if($datos_tramite->tipo_titulo == 2){
	
	?>
	<p class="justificado">
	
	Que dicho TÍTULO fue convalidado y reconocido para todos los efectos académicos y legales por el Ministerio de 
	Educación Nacional según resolución No <?php echo $datos_tramite->resolucion?> del <?php echo date_format($dateTime, 'd')?> de <?php echo $strMes//date_format($dateTime, 'm')?> de <?php echo date_format($dateTime, 'Y')?> como <?php echo $datos_tramite->nombreprogramaequivalente?>.
	</p>
	<?php } ?>
	<p class="justificado">
        Que estudiada la documentación presentada por el solicitante esta cumple con los requisitos establecidos en las normas legales vigentes;
    </p>
    <p class="justificado">
        En virtud de lo expuesto este Despacho,
    </p>
    <p class="centro">
        <b>RESUELVE:</b>
    </p>
	<?php
			if($datos_tramite->tipo_titulo == 2){
	
	?>
	<p class="justificado">
        <b>ARTICULO PRIMERO</b>: Autorizar a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        para ejercer la profesión/ocupación de  <?php echo strtoupper($datos_tramite->nombreprogramaequivalente);?> en el Territorio Nacional.
    </p>
	<?php } ?>
	
	<?php
			if($datos_tramite->aplicarethusequivalente == 1){
	?>	
	
	<p class="justificado">
        <b>ARTICULO SEGUNDO: </b><?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        quedará inscrito en el Registro Único Nacional del Talento Humano en Salud (RETHUS) dentro de los primeros (5) cinco días hábiles del mes inmediatamente siguiente al de expedición de este Acto Administrativo.
    </p>
    <p class="justificado">
        <b>ARTÍCULO TERCERO: </b>Notifíquese electrónicamente el contenido de la presente Resolución a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        o a quien haga sus veces, haciéndole saber que, contra la misma sólo procede el recurso de reposición ante esta Dirección, el cual deberá interponerse dentro de los diez (10) días siguientes a la notificación electrónica, por medio de la plataforma virtual mediante la cual se llevó a cabo el trámite inicial o por escrito.
    </p>
	<?php } ?>
	
	<?php if($datos_tramite->tipo_titulo == 1){ ?>
    <p class="justificado">
        <b>ARTICULO PRIMERO</b>: Autorizar a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        para ejercer la profesión/ocupación de  <?php echo $datos_tramite->nombre_programa?> en el Territorio Nacional.
    </p>
	<?php } ?>
    <?php
        if($datos_tramite->aplica_rethus == 1){
    ?>
    <p class="justificado">
        <b>ARTICULO SEGUNDO: </b><?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        quedará inscrito en el Registro Único Nacional del Talento Humano en Salud (RETHUS) dentro de los primeros (5) cinco días hábiles del mes inmediatamente siguiente al de expedición de este Acto Administrativo.
    </p>
    <p class="justificado">
        <b>ARTÍCULO TERCERO: </b>Notifíquese electrónicamente el contenido de la presente Resolución a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
        identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        o a quien haga sus veces, haciéndole saber que, contra la misma sólo procede el recurso de reposición ante esta Dirección, el cual deberá interponerse dentro de los diez (10) días siguientes a la notificación electrónica, por medio de la plataforma virtual mediante la cual se llevó a cabo el trámite inicial o por escrito.
    </p>
    <?php } ?>
    
    
    <p class="justificado">
        <b>NOTIFIQUESE, Y CÚMPLASE</b>
        Dada en  Bogotá, D.C. a los <?php echo date('d')." d&iacute;as del mes de ".$mes." del a&ntilde;o ".date('Y')?>
    </p>
    <br><br><br><br>
    <p class="justificado">
       <?php
            if($firma == true){
                ?>
                <img src="<?php echo FCPATH.'assets/imgs/firma_docrosmira.JPG'?>" width="300px"><br>
                Subdirector (a) Inspección Vigilancia y Control de Servicios de Salud (E).<br>
                C&oacute;digo de verificaci&oacute;n: <?php echo $codigo_verificacion;?>
                <?php
            }
        ?>

    </p>

</body>

</html>
