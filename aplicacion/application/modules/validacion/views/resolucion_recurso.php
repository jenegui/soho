<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resoluci&oacute;n Recurso de reposici&oacute;n</title>


    <style type="text/css">
        body {
            background-color: #fff;
            margin: 10px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #4F5155;
        }

        table {
            border: 1px solid #fff;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        .encabezados {
            color: #000000;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        h2 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 14px;
            font-weight: bold;
            margin: 5px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        code {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        .centro {
            text-align: center;
        }
        
        .justificado {
            text-align: justify;
        }

        .derecha {
            text-align: right;
        }

        .total {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #000000;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
            color: #002166;
        }

        .marca-de-agua {
            padding: 0;
            width: 100%;
            height: auto;
            opacity: 0.7;
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 25px;
        }

    </style>
</head>

<body>
    <table width="100%">
        <tr align="left">
            <td width="100%">                
                <img src="<?php echo FCPATH.'assets/imgs/logo_pdf_alcaldia.png'?>" width="150px">
            </td>
        </tr>
    </table>
    <table class="table centro" width="100%">
        <tr class="encabezados">
            <td width="100%">
                <h1>Resoluci&oacute;n No A<?php echo $nume_resolucion;?> del d&iacute;a <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
                Secretaría Distrital de Salud de Bogot&aacute; D.C</h1>
            </td>
        </tr>
        <tr>
            <td width="100%">
                Por la cual se resuelve recurso de reposición interpuesto en contra de la Resolución No….. de …….,
                mediante la cual se negó  autorización a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
                identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
                para ejercer la ocupación de  <?php echo $datos_tramite->nombre_programa?> en el Territorio Nacional.
                <br><br><b>LA SUBDIRECCIÓN INSPECCIÓN, VIGILANCIA Y CONTROL DE SERVICIOS DE SALUD</b>

            </td>
        </tr>
    </table>
    <p class="justificado">
        En uso de sus facultades legales y en especial las conferidas por el Decreto 780 de 2016, Ley 1164 de 2007 y Resolución 3030 de 2014  del Ministerio de Salud y Protección Social y,
    </p>
    <p class="centro">
        <b>CONSIDERANDO</b>
    </p>
    <p class="justificado">
        Que mediante Resolución No PENDIENTE de fecha PENDIENTE la Dirección de calidad de Servicios de Salud  negó  autorización a
        <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>
        Identificado(a) con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        para ejercer la ocupación de  <?php echo $datos_tramite->nombre_programa?> en el Territorio Nacional.
    </p>
    <p class="justificado">
        Que dentro del término de ley, el señor <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
        identificado con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        interpuso recurso de reposición contra la citada Resolución.
    </p>
    <p class="justificado">
        <?php
        echo $argumentos_recurrente;
        ?>
    </p>
    <p class="justificado">
        <?php
        echo $consideraciones;
        ?>
    </p>
    <p class="centro">
        <b>RESUELVE:</b>
    </p>
    <p class="justificado">
        <?php
        echo $articulos;
        ?>
    </p>

    <p class="justificado">
        <b>ARTÍCULO TERCERO: </b>Notifíquese electrónicamente el contenido de la presente Resolución a <?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido." "?>,
        identificado con <?php echo $datos_tramite->descTipoIden?> número <?php echo $datos_tramite->nume_identificacion?>,
        o a quien haga sus veces, haciéndole saber que, contra la misma no procede recurso alguno.
    </p>
    <p class="justificado">
        <b>NOTIFIQUESE, Y CÚMPLASE</b>
        Dada en  Bogotá, D.C. a los <?php echo date('d')." d&iacute;as del mes de ".$mes." del a&ntilde;o ".date('Y')?>
    </p>
    <br><br><br>
    <p class="justificado">
        <?php
            if($firma == true){
                ?>
                <img src="<?php echo FCPATH.'assets/imgs/firma_docrosmira.JPG'?>" width="300px"><br>
                Subdirector (a) Inspección Vigilancia y Control de Servicios de Salud (E).<br>
                C&oacute;digo de verificaci&oacute;n: <?php echo $codigo_verificacion;?>
                <?php
            }
        ?>
    </p>

</body>

</html>
