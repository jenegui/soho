﻿<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}


?>

		<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Trámites pendientes de validar.</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12">
					<table class="table" id="tabla_tramites" style="font-size:small;">
                        <thead>
                            <tr>
                                <th>ID Trámite</th>
                                <th>Identificaci&oacute;n</th>
                                <th>Nombre</th>
                                <th>Tipo de título</th>
                                <th>Fecha radicaci&oacute;n</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th>Tiempo de respuesta</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($tramites_pendientes)>0){
                                for($i=0;$i<count($tramites_pendientes);$i++){                                    
                                ?>
                            <tr>
                                <td>
                                    <?php 
										echo $tramites_pendientes[$i]->id_titulo;
									if(!empty($tramites_pendientes[$i]->fecha_editado)){ 
										echo '<i class="fas fa-exclamation-circle" alt="Trámite Editado"></i><font color="black" font-family="sans-serif;">Ajustado</font> ';
									} 										
									?>
                                </td>
                                <td>
                                    <?php echo $tramites_pendientes[$i]->descTipoIden." - ".$tramites_pendientes[$i]->nume_identificacion?>
                                </td>
                                <td>
                                    <?php echo $tramites_pendientes[$i]->p_nombre." ".$tramites_pendientes[$i]->s_nombre." ".$tramites_pendientes[$i]->p_apellido." ".$tramites_pendientes[$i]->s_apellido?>
                                </td>                                
                                <td>
                                    <?php
                                        if($tramites_pendientes[$i]->tipo_titulo == '1'){
                                            echo "Nacional";
                                        }else{
                                            echo "Extranjero";
                                        }
                                        ?>
                                </td>
                                <td>
                                    <?php echo $tramites_pendientes[$i]->fecha_tramite?>
                                </td>
                                <td>
                                    <?php echo $tramites_pendientes[$i]->descEstado?>
                                </td>
                                <td>
                                   <?php

                                            if($tramites_pendientes[$i]->estado == 13){
                                                echo "Pendiente de ajuste por parte del usuario";
                                            }else{
                                                ?>
                                                <center>
                                                  <a href="<?php echo base_url('validacion/validar_documentos/'.$tramites_pendientes[$i]->id_titulo)?>">
                                                    <img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="40px">
                                                    <br>Validar Informaci&oacute;n
                                                    </a>
                                                </center>
                                                <?php
                                            }

                                    ?>

                                </td>
                                <td>
                                    <?php
									if($tramites_pendientes[$i]->estado == 1 or $tramites_pendientes[$i]->estado == 12){
										$from = $tramites_pendientes[$i]->fecha_tramite;
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+1 day');
										$interval = new DateInterval('P1D');
										$periods = new DatePeriod($from, $interval, $to);

										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}

										$tiempo_validacion = 30;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 27 && $days <=30){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 21 && $days <27){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 20){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}
									elseif($tramites_pendientes[$i]->estado == 8){
										$from = $tramites_pendientes[$i]->fecha_reposicion;
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+1 day');
										$interval = new DateInterval('P1D');
										$periods = new DatePeriod($from, $interval, $to);

										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}

										$tiempo_validacion = 5;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days > 3 && $days <=5){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days > 2 && $days <=3){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}else if($days <= 2){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}
									}

																	
                                    ?>
                                </td>
                            </tr>
                            <?php
        }
        }
		

        ?>
                        </tbody>
                    </table>

            </div>
		</div>
		
