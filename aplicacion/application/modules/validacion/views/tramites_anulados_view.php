<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>
<form action="<?php echo base_url('validacion/anulados/') ?>" method="post" class="form-horizontal">	
	<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-">
                <div class="subtitle">
                    <h2><b>Filtro Consulta y Descarga Excel Tr&aacute;mites Anulados</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Inicial:</b></label>
                    <input id="fecha_i" name="fecha_i" class="form-control" placeholder="Fecha Seguimiento Inicio" style="width:100%;">
                </div>
            </div>
			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Final:</b></label>
                    <input id="fecha_f" name="fecha_f" class="form-control" placeholder="Fecha Seguimiento Fin"  style="width:100%;">
                </div>
            </div>

			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input type="submit" class="btn btn-info" value="Consultar" style="width:100%;">
                </div>
            </div>
			<?php
				if($this->session->userdata('perfil')== 7){
			?>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="ExcelAC" value="Descargar Excel AC" style="width:100%;">
                </div>
            </div>			
			<?php
			}
			else if($this->session->userdata('perfil')== 3 OR $this->session->userdata('perfil')== 4 OR $this->session->userdata('perfil')== 5){
			?>			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="Excel" value="Descargar Excel VC" style="width:100%;">
                </div>
            </div>		
			<?php										
			}
			?> 			
</form>
		<div class="col-12 col-md-12">
		<br>
			<div class="alert alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			<b>Apreciado Usuario!</b>
			<p>La consulta y el reporte de tr&aacute;mites en estado de Anulaci&oacute;n, es por rango de fecha de acuerdo a la fecha del seguimiento generada por el sistema.<br>
			Los Tr&aacute;mites visualizados corresponden a los &uacute;ltimos 30 d&iacute;as desde la fecha actual.
			</p>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
		<br>
			<table class="table" id="tabla_tramites"  style="font-size:small;">
					<thead>
						<tr>
							<th>ID Tr&aacute;mite</th>
							<th>Identificaci&oacute;n</th>
							<th>Nombres y Apellidos</th>
							<th>Fecha Radicaci&oacute;n</th>
							<th>Fecha Seguimiento</th>
							<th>Ver M&aacute;s</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($tramites_anulados)>0){
							for($i=0;$i<count($tramites_anulados);$i++){
							?>
						<tr>
							<td>
								<?php echo $tramites_anulados[$i]->id_titulo;?>
							</td>
							<td>
								<?php echo $tramites_anulados[$i]->descTipoIden." - ".$tramites_anulados[$i]->nume_identificacion?>
							</td>
							<td>
								<?php echo $tramites_anulados[$i]->p_nombre." ".$tramites_anulados[$i]->s_nombre." ".$tramites_anulados[$i]->p_apellido." ".$tramites_anulados[$i]->s_apellido?>
							</td>
							<td>
								<?php echo $tramites_anulados[$i]->fecha_tramite?>
							</td>
							<td>
								<?php echo $tramites_anulados[$i]->fecha_seguimiento?>
							</td>
							<td>
								<?php echo $tramites_anulados[$i]->tipo_titulo?>
							</td>
							<td>
								<center>
									<a href="<?php echo base_url('validacion/visualizar_documentos/'.$tramites_anulados[$i]->id_titulo)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="20px">
									<br>Visualizar Informaci&oacute;n
									</a>
								</center>
							</td>
						</tr>
					<?php
						}
					}
					?>
					</tbody>
			</table>
		</div>
	</div>

<!--Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 17062019
//Script Generar Excel-->		
		<script type="text/javascript">
           $("#Excel").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelanulados?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>
		<script type="text/javascript">
           $("#ExcelAC").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelanuladosAC?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>