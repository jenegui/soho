<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>

<form action="<?php echo base_url('validacion/buscarporcedula/') ?>" method="post" autocomplete="off" class="form-horizontal">
	<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-">
                <div class="subtitle">
                    <h2><b>Filtro Consulta de Tr&aacute;mites por Número de Documento</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>No. Documento Solicitante:</b></label>
                    <input id="num_doc" name="num_doc" class="form-control" placeholder="Ingresar el Número de Documento" style="width:100%;">
                </div>
            </div>
			
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input type="submit" class="btn btn-info " value="Consultar" style="width:100%;">
                </div>
            </div>

			
</form>
		<div class="col-12 col-md-12">
		<br>
			<div class="alert alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			<b>Apreciado Usuario!</b>
			<p>La siguiente consulta tiene como filtro de busqueda el Número de Documento del Solicitante, quien esta registrado como persona natural en el sistema Ventanilla Única de Trámites y Servicios de la Secretaría Distrital de Salud. Favor ingresar el número de documento sin separadores, espacios y/o puntos. 
			</p>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
		<br>
			<table class="table" id="tabla_tramites"  style="font-size:small;">
				<thead>
                    <tr>
                        <th>ID Tr&aacute;mite</th>
                        <th>Identificaci&oacute;n</th>
                        <th>Nombres y Apellidos</th>
						<th>Fecha Radicaci&oacute;n</th>
                        <th>Tipo de t&iacute;tulo</th>
						<th>Estado Trámite</th>
                        <th>Ver M&aacute;s</th>

                    </tr>
                </thead>
                <tbody>

                <?php
                //Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 28052019
                //Listado de tramites Aprobados por Validador
                if(count($tramites_aprobados)>0){
                        for($i=0;$i<count($tramites_aprobados);$i++){
                        ?>
                    <tr>
                        <td>
                            <?php echo $tramites_aprobados[$i]->id_titulo;?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->descTipoIden." - ".$tramites_aprobados[$i]->nume_identificacion?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->p_nombre." ".$tramites_aprobados[$i]->s_nombre." ".$tramites_aprobados[$i]->p_apellido." ".$tramites_aprobados[$i]->s_apellido?>
                        </td>
						<td>
                            <?php echo $tramites_aprobados[$i]->fecha_tramite?>
                        </td>
                        <td>
                            <?php if($tramites_aprobados[$i]->tipo_titulo=='1'){echo "Nacional"; } else { echo "Extranjero"; }?>
                        </td>
						<td>
                            <?php echo $tramites_aprobados[$i]->descEstado?>
                        </td>
                        <td>
							<center>
								<a href="<?php echo base_url('validacion/visualizar_documentos/'.$tramites_aprobados[$i]->id_titulo) ?>"  target="_blank">
								<img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="20px">
								<br>Visualizar Informaci&oacute;n
								</a>
							</center>
                        </td>
                    </tr>
                <?php
						}
				}
				?>
                </tbody>
            </table>
		</div>
	</div>

<!--Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 17062019
//Script Generar Excel-->
        <script type="text/javascript">
           $("#Excel").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelaprobados?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

         </script>
		<script type="text/javascript">
           $("#ExcelAC").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelaprobadosAC?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>			 
