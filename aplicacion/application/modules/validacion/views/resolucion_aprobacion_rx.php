<html>
<head>
  <title>Aprobación - Tramites en Linea . SDS</title>
  <meta charset="utf-8">
  <style type="text/css">

    body {
		background-color: #fff;
		font-family: Lucida Grande, Verdana, Sans-serif;
		font-size: 12px;
		color: #4F5155;
	}

	table {
		border: 1px solid #fff;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		font-weight: bold;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}
	
	h4 {
		color: #444;
		font-size: 12px;
		font-weight: bold;
	}

	.encabezados {
		color: #000000;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	h2 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 14px;
		font-weight: bold;
		margin: 5px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	code {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	.centro {
		text-align: center;
	}
	
	.justificado {
		text-align: justify;
	}

	.derecha {
		text-align: right;
	}

	.total {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #000000;
		border-bottom: 1px solid #000000;
		border-top: 1px solid #000000;
		color: #002166;
	}

	.marca-de-agua {
		padding: 0;
		width: 100%;
		height: auto;
		opacity: 0.7;
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 25px;
	}

	.contenido{
		margin-top: 60px;
		margin-bottom: 55px;
	} 

	.cursiva{
		font-style: italic;
	}	
  </style>
</head>
<body>
<div id="header">
  <!--<img src="./assets/img/memologo.jpg" />-->
  <!--<img src="<?php echo FCPATH.'assets/imgs/logo_pdf_alcaldia.png'?>" width="70px">-->
</div>
  <div style="width:100%;">
      <div style="margin-left:45px;margin-right:45px;margin-top:-50px;">
		<?php
		
		if($rayosxEquipo[0]->categoria1 != 0 || $rayosxEquipo[0]->categoria1 != NULL){
				if($rayosxEquipo[0]->categoria1 == 1){
					$categoria1 = "Radiolog&iacute;a odontol&oacute;gica periapical";
					$desc_categoria = $categoria1;
					
				}else if($rayosxEquipo[0]->categoria1 == 2){
					$categoria1 = "Equipo de RX";					
					$desc_categoria = $categoria1;
				}				
			}
	   
			if($rayosxEquipo[0]->categoria2 != 0 || $rayosxEquipo[0]->categoria2 != NULL){
				if($rayosxEquipo[0]->categoria2 == 1){
					$categoria2 = "Radioterapia";		
					$desc_categoria = $categoria2;	
				}else if($rayosxEquipo[0]->categoria2 == 2){
					$categoria2 = "Radio diagn&oacute;stico de alta complejida";
					$desc_categoria = $categoria2;	
				}else if($rayosxEquipo[0]->categoria2 == 3){
					$categoria2 = "Radio diagn&oacute;stico de media complejidad";					
					$desc_categoria = $categoria2;	
				}else if($rayosxEquipo[0]->categoria2 == 4){
					$categoria2 = "Radio diagn&oacute;stico de baja complejidad";										
					$desc_categoria = $categoria2;	
				}else if($rayosxEquipo[0]->categoria2 == 5){
					$categoria2 = "Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales";										
					$desc_categoria = $categoria2;	
				}
			}

			if($rayosxEquipo[0]->categoria1_1 != 0 || $rayosxEquipo[0]->categoria1_1 != NULL){
				if($rayosxEquipo[0]->categoria1_1 == 1){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical";
					$equipo_doc = $categoria1_1;
				}else if($rayosxEquipo[0]->categoria1_1 == 2){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical portat&iacute;l";
					$equipo_doc = $categoria1_1;
				}				
			}

			if($rayosxEquipo[0]->categoria1_2 != 0 || $rayosxEquipo[0]->categoria1_2 != NULL){
				if($rayosxEquipo[0]->categoria1_2 == 1){
					$categoria1_2 = "Densit&oacute;metro &oacute;seo";
					$equipo_doc = $categoria1_2;
				}
			}
			
			if($rayosxEquipo[0]->categoria2_1 != 0 || $rayosxEquipo[0]->categoria2_1 != NULL){
				if($rayosxEquipo[0]->categoria2_1 == 1){
					$categoria2_1 = "Equipo de RX convencional";					
				}else if($rayosxEquipo[0]->categoria2_1 == 2){
					$categoria2_1 = "Tomógrafo Odontológico";
				}else if($rayosxEquipo[0]->categoria2_1 == 3){
					$categoria2_1 = "Tomógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 4){
					$categoria2_1 = "Equipo de RX Portátil";
				}else if($rayosxEquipo[0]->categoria2_1 == 5){
					$categoria2_1 = "Equipo de RX Odontológico";
				}else if($rayosxEquipo[0]->categoria2_1 == 6){
					$categoria2_1 = "Panorámico Cefálico";
				}else if($rayosxEquipo[0]->categoria2_1 == 7){
					$categoria2_1 = "Fluoroscopio";
				}else if($rayosxEquipo[0]->categoria2_1 == 8){
					$categoria2_1 = "SPECT-CT";
				}else if($rayosxEquipo[0]->categoria2_1 == 9){
					$categoria2_1 = "Arco en C";
				}else if($rayosxEquipo[0]->categoria2_1 == 10){
					$categoria2_1 = "Mamógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 11){
					$categoria2_1 = "Litotriptor";
				}else if($rayosxEquipo[0]->categoria2_1 == 12){
					$categoria2_1 = "Angiógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 13){
					$categoria2_1 = "PET-CT";					
				}else if($rayosxEquipo[0]->categoria2_1 == 14){
					$categoria2_1 = "Acelerador lineal";					
				}else if($rayosxEquipo[0]->categoria2_1 == 15){
					$categoria2_1 = "Sistema de radiocirugia robótica";					
				}else if($rayosxEquipo[0]->categoria2_1 == 16){
					$categoria2_1 = $rayosxEquipo[0]->otro_equipo;					
				}else{
					$categoria2_1 = "";					
				}
				
				if($categoria2_1 != ""){
					$equipo_doc = $categoria2_1;	
				}				
			}	
		
		?>
        <p align="center">
            <br><b><em>Dirección de Calidad de Servicios de Salud<br>
              Subdirección Inspección, Vigilancia y Control de Servicios de Salud</em>
                </b>
        </p>
        <p align="center">
            <b><em>Resolución No. <?php echo $nume_resolucion?> de <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
              "Por medio de la cual se Concede una Licencia de Practica Medica"
              </em></b>
        </p>
        <p align="justify">En uso de sus facultades
          legales y en especial las que confiere el Decreto Distrital 507 de 2013 expedido por el Alcalde
          Mayor de Bogotá y la Resolución 482 de 2018 del Ministerio de Salud y Protección Social y</p>
        <p align="justify"></p>
        <p align="justify">Que mediante Resolución No. 482 del 22 de febrero de 2018, el Ministerio de
          Salud y Protección Social reglamentó el uso de equipos generadores de radiación ionizante, su control
          de calidad, la prestación de servicios de protección radiológica y se dictan otras disposiciones
          regulando las autorizaciones y licencias de funcionamiento y establece:</p>
        <p align="justify">Que el Artículo 19 de la Resolución 482 de 2018 estable: Licencia de prácticas médicas.
          Los prestadores de servicios de salud interesados en realizar una práctica médica que haga uso de equipos
          generadores de radiación ionizante, móviles o fijos, deberán solicitar licencia de práctica médica ante
          la entidad territorial de salud de carácter departamental o distrital de la jurisdicción en la que se
          encuentre la respectiva instalación. </p>
        <p align="justify">Que mediante el Artículo 20 de la Resolución 482 de 2018 se categorizaron las prácticas
          médicas así:</p>
        <p align="justify">
          20.1. Categoría I:<br>
          20.1.1. Radiología odontológica periapical. <br>
          20.1.2. Densitometría ósea.<br><br>
          20.2. Categoría II: <br>
          20.2.1. Radioterapia. <br>
          20.2.2. Radiodiagnóstico de alta complejidad. <br>
          20.2.3. Radiodiagnóstico de media complejidad. <br>
          20.2.4. Radiodiagnóstico de baja complejidad. <br>
          20.2.5. Radiografías odontológicas panorámicas y tomografías orales
        </p>
        <p align="justify">Parágrafo. Las prácticas médicas que no se encuentren expresamente señaladas en el presente
          artículo, se considerarán como categoría II.”</p>
        <p align="justify">Que de conformidad con los artículos 21 y 23 de la resolución 482 de 2018 corresponde a las
          entidades territoriales de salud otorgar las licencias de prácticas médicas,a solicitud de los prestadores de
          servicios de salud.</p>
		<?php
		if($datos_tramite->tipo_identificacion == 5){
				$nombre_rs = $datos_tramite->nombre_rs;
				$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
				switch ($datos_tramite->tipo_iden_rl) {
					case 1:
						$tipo_iden_rl = "Cédula de ciudadanía";
						break;
					case 2:
						$tipo_iden_rl = "Cédula de extranjería";
						break;
					case 3:
						$tipo_iden_rl = "Tarjeta de identidad";
						break;
					case 4:
						$tipo_iden_rl = "Permiso especial de permanencia";
						break;
					case 5:
						$tipo_iden_rl = "NIT";
						break;
				}				
				$nume_iden_rl = $datos_tramite->nume_iden_rl;
			}else{
				$nombre_rs = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
				$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
				
				$tipo_iden_rl = $datos_tramite->descTipoIden;
				$nume_iden_rl = $datos_tramite->nume_identificacion;
			}
			
			
			
		?>
        <p align="justify">Que el (la) señor(a) <?php echo $nombre_rl?>, 
            identificado(a) con <?php echo $tipo_iden_rl?> número <?php echo $nume_iden_rl?> en su
          calidad de Representante Legal, mediante radicado No <?php echo $datos_tramite->id?> de fecha <?php echo $datos_tramite->created_at?> ha solicitado licencia de
          práctica médica de categoría <?php echo $desc_categoria?>, para 
        </p>
        <p align="justify"><?php echo $nombre_rl?> 
            identificado (a) con <?php echo $tipo_iden_rl?> número <?php echo $nume_iden_rl?>, ubicado (a) en la
          <?php echo $rayosxDireccion->dire_entidad?> de la nomenclatura urbana de Bogotá, de acuerdo a los preceptos de la Resolución 482 de 2018.</p>
		  
		  
        <p align="justify">Que examinada la documentación allegada se encontró que se cumple con los requisitos exigidos por
          la Resolución 482 de 2018 y demás normas vigentes relacionadas con la protección de las personas expuestas a las
          Radiaciones Ionizantes, expedidas por el Ministerio de Salud y Protección Social.</p>
		  
		  <?php 
			if($datos_tramite->categoria == 2){
				?>
					<p align="justify">Que realizada la visita de verificación con enfoque de riesgo a las instalaciones del prestador de servicios de salud se evidenció que cumple requisitos. </p>	
				<?php
			}
		  
		  ?>
        
        <p align="justify">En mérito a lo expuesto, este Despacho</p>
        <p align="center"><b>RESUELVE:</b></p>
        <p align="justify"><b>ARTÍCULO PRIMERO:</b> Conceder Licencia de Práctica Médica de categoría <?php echo $datos_tramite->categoria?> a <?php echo $nombre_rs?>
          ubicada en la <?php echo $rayosxDireccion->dire_entidad?> representado legalmente por el señor (a) <?php echo $nombre_rl ?> identificado(a) con
           <?php echo $tipo_iden_rl?> número <?php echo $nume_iden_rl?></p>
        <p align="justify"><b>ARTÍCULO SEGUNDO:</b> El Nombre del Oficial o Encargado de Protección Radiológica es:</p>
		
		<table style="border-collapse: collapse;" width="100%">
			<tr>
				<td style="border:1px solid black;">NOMBRE</td>
				<td style="border:1px solid black;">IDENTIFICACIÓN</td>
				<td style="border:1px solid black;">PROFESIÓN</td>
			</tr>
			<tr>
				<td style="border:1px solid black;"><?php echo $rayosxOficialToe->encargado_papellido." ".$rayosxOficialToe->encargado_sapellido." ".$rayosxOficialToe->encargado_pnombre." ".$rayosxOficialToe->encargado_snombre?></td>
				<td style="border:1px solid black;"><?php echo $rayosxOficialToe->encargado_ndocumento?></td>
				<td style="border:1px solid black;"><?php echo $rayosxOficialToe->encargado_profesion?></td>
			</tr>
		</table>        
        <p align="justify"><b>ARTÍCULO TERCERO:</b> El (los) equipo (s) que allí funciona (n) es (son):</p>
          <?php
          
            for($i=0;$i<count($rayosxEquipo);$i++){
				
				if($rayosxEquipo[$i]->categoria1 != 0 || $rayosxEquipo[$i]->categoria1 != NULL){
				if($rayosxEquipo[$i]->categoria1 == 1){
					$categoria1 = "Radiolog&iacute;a odontol&oacute;gica periapical";
					
				}else if($rayosxEquipo[$i]->categoria1 == 2){
					$categoria1 = "Equipo de RX";					
				}				
			}
	   
			if($rayosxEquipo[$i]->categoria2 != 0 || $rayosxEquipo[$i]->categoria2 != NULL){
				if($rayosxEquipo[$i]->categoria2 == 1){
					$categoria2 = "Radioterapia";					
				}else if($rayosxEquipo[$i]->categoria2 == 2){
					$categoria2 = "Radio diagn&oacute;stico de alta complejida";
				}else if($rayosxEquipo[$i]->categoria2 == 3){
					$categoria2 = "Radio diagn&oacute;stico de media complejidad";					
				}else if($rayosxEquipo[$i]->categoria2 == 4){
					$categoria2 = "Radio diagn&oacute;stico de baja complejidad";										
				}else if($rayosxEquipo[$i]->categoria2 == 5){
					$categoria2 = "Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales";										
				}
			}

			if($rayosxEquipo[$i]->categoria1_1 != 0 || $rayosxEquipo[$i]->categoria1_1 != NULL){
				if($rayosxEquipo[$i]->categoria1_1 == 1){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical";
					$equipo_doc = $categoria1_1;
				}else if($rayosxEquipo[$i]->categoria1_1 == 2){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical portat&iacute;l";
					$equipo_doc = $categoria1_1;
				}				
			}

			if($rayosxEquipo[$i]->categoria1_2 != 0 || $rayosxEquipo[$i]->categoria1_2 != NULL){
				if($rayosxEquipo[$i]->categoria1_2 == 1){
					$categoria1_2 = "Densit&oacute;metro &oacute;seo";
					$equipo_doc = $categoria1_2;
				}
			}
			
			if($rayosxEquipo[$i]->categoria2_1 != 0 || $rayosxEquipo[$i]->categoria2_1 != NULL){
				if($rayosxEquipo[$i]->categoria2_1 == 1){
					$categoria2_1 = "Equipo de RX convencional";					
				}else if($rayosxEquipo[$i]->categoria2_1 == 2){
					$categoria2_1 = "Tomógrafo Odontológico";
				}else if($rayosxEquipo[$i]->categoria2_1 == 3){
					$categoria2_1 = "Tomógrafo";
				}else if($rayosxEquipo[$i]->categoria2_1 == 4){
					$categoria2_1 = "Equipo de RX Portátil";
				}else if($rayosxEquipo[$i]->categoria2_1 == 5){
					$categoria2_1 = "Equipo de RX Odontológico";
				}else if($rayosxEquipo[$i]->categoria2_1 == 6){
					$categoria2_1 = "Panorámico Cefálico";
				}else if($rayosxEquipo[$i]->categoria2_1 == 7){
					$categoria2_1 = "Fluoroscopio";
				}else if($rayosxEquipo[$i]->categoria2_1 == 8){
					$categoria2_1 = "SPECT-CT";
				}else if($rayosxEquipo[$i]->categoria2_1 == 9){
					$categoria2_1 = "Arco en C";
				}else if($rayosxEquipo[$i]->categoria2_1 == 10){
					$categoria2_1 = "Mamógrafo";
				}else if($rayosxEquipo[$i]->categoria2_1 == 11){
					$categoria2_1 = "Litotriptor";
				}else if($rayosxEquipo[$i]->categoria2_1 == 12){
					$categoria2_1 = "Angiógrafo";
				}else if($rayosxEquipo[$i]->categoria2_1 == 13){
					$categoria2_1 = "PET-CT";					
				}else if($rayosxEquipo[$i]->categoria2_1 == 14){
					$categoria2_1 = "Acelerador lineal";					
				}else if($rayosxEquipo[$i]->categoria2_1 == 15){
					$categoria2_1 = "Sistema de radiocirugia robótica";					
				}else if($rayosxEquipo[$i]->categoria2_1 == 16){
					$categoria2_1 = $rayosxEquipo[$i]->otro_equipo;					
				}else{
					$categoria2_1 = "";					
				}
				
				if($categoria2_1 != ""){
					$equipo_doc = $categoria2_1;	
				}		
			}
				
                ?>
				<table style="border-collapse: collapse;" width="100%">
					<tr>
						<td style="border:1px solid black;">Clase de Equipo:</td>
						<td style="border:1px solid black;"><?php echo $equipo_doc?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Marca: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->marca_equipo?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Modelo: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->modelo_equipo?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Serie: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->serie_equipo?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Tubo: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->marca_tubo_rx?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Modelo Tubo: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->modelo_tubo_rx?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Serie Tubo: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->serie_tubo_rx?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Tensión Máxima tubo RX [Kv]: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->tension_tubo_rx?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Corriente Máxima tubo RX [mA]: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->contiene_tubo_rx?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Energía Fotones[MeV]: </td>
						<td style="border:1px solid black;"> <?php echo $rayosxEquipo[$i]->energia_fotones?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Energía Electrones [MeV]: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->energia_electrones?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;">Carga de trabajo [mA.min/semana]: </td>
						<td style="border:1px solid black;"><?php echo $rayosxEquipo[$i]->carga_trabajo?></td>
					</tr>
				</table>                
                <?php
            }
          
		  if($datos_tramite->categoria == 1){
			  $termino = "CINCO (5)";
		  }else{
			  $termino = "CUATRO (4)";
		  }
		  
          ?>
          
          
        
        <p align="justify"><b>ARTÍCULO CUARTO:</b> La presente licencia se concede por el término de <?php echo $termino?> años, contados a partir
          de la fecha de expedición del presente acto administrativo, y podrá ser renovada por un término igual mediante solicitud
          presentada con sesenta (60) días de antelación de su vencimiento, de conformidad con los artículos 22 y 25 de la
          Resolución 482 de 2018.</p>
        <p align="justify"><b>ARTÍCULO QUINTO:</b> Conforme al Artículo 29 de la Resolución 482 de 2018 el titular de la licencia de
          práctica médica categoría I o II podrá solicitar la modificación de algunas de las condiciones que se señalan en el
          literal 29.1 y literal 29.2.</p>
        <p align="justify"><b>ARTÍCULO SEXTO:</b> Notificar el contenido de esta providencia al representante legal, o a un tercero
          debidamente autorizado, de conformidad con lo dispuesto en los artículos 67 y 69 del Código  de Procedimiento
          Administrativo y de lo Contencioso Administrativo, haciéndole (s) saber que contra la presente proceden los recursos
          de reposición ante este Despacho y de apelación ante el Secretario Distrital de Salud, dentro de los cinco (5) días
          siguientes a la notificación del presente acto administrativo, de conformidad con lo establecido en el Artículo 74
          del Código  de Procedimiento Administrativo y de lo Contencioso Administrativo.</p>
        <p align="center"><b>NOTIFÍQUESE Y CÚMPLASE</b></p>
        <p align="center">Dado en Bogotá, D.C. a los <?php echo date('d')." días del mes de ".$mes." del a&ntilde;o ".date('Y');?></p>
		<br><br><br>
		<?php
	
		if($firma == TRUE){
			?>
			<img src="<?php echo FCPATH.'assets/imgs/firma_docmarthaBK.JPG'?>" width="300px"><br>
			<?php
		}
		
		?>
        <p align="justify"><b>YOLIMA AGUDELO SEDANO</b></p>
        <p align="justify">Subdirectora de Inspección Vigilancia y Control de Servicios de Salud</p>
		<?php
	
		if(isset($codigo_verificacion) && $codigo_verificacion != ''){
			?>
			<p class="justificado">Código de verificación: <?php echo $codigo_verificacion?></p>
			<?php
		}
		
		?>

      </div>
  </div>
  <div id="footer" style="margin-left:45px;margin-right:45px">



  </div>

</body>
</html>
