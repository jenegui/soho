<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}


?>

		<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Trámites pendientes de validar.</b></h2>
					<h3>Licencia de practica medica categoría I y II</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12">
					<table class="table" id="tabla_tramites" style="font-size:small;">
                        <thead>
                            <tr>
                                <th>ID Trámite</th>
                                <th>Identificaci&oacute;n</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Fecha radicaci&oacute;n</th>                                
                                <th>Estado</th>
								<th>Fecha estado</th>                                
                                <th>Tiempo de respuesta</th>
								<th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($rayosx_pendientes)>0){
                                for($i=0;$i<count($rayosx_pendientes);$i++){                                    
                                ?>
                            <tr>
                                <td>
                                    <?php 
										echo $rayosx_pendientes[$i]->id;
									/*if(!empty($rayosx_pendientes[$i]->fecha_editado)){ 
										echo '<i class="fas fa-exclamation-circle" alt="Trámite Editado"></i><font color="black" font-family="sans-serif;">Ajustado</font> ';
									} */										
									?>
                                </td>
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->descTipoIden." - ".$rayosx_pendientes[$i]->nume_identificacion?>
                                </td>
                                <td>
                                    <?php 
										if($rayosx_pendientes[$i]->tipo_identificacion == 5){
											echo $rayosx_pendientes[$i]->nombre_rs;
										}else{
											echo $rayosx_pendientes[$i]->p_nombre." ".$rayosx_pendientes[$i]->s_nombre." ".$rayosx_pendientes[$i]->p_apellido." ".$rayosx_pendientes[$i]->s_apellido;
										}
									?>
                                </td>                                
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->categoria; ?>
                                </td>
								<td>
                                    <?php echo $rayosx_pendientes[$i]->fecha_envio?>
                                </td>                               
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->descEstado?>
                                </td>
								<td>
                                    <?php echo $rayosx_pendientes[$i]->fecha_estado?>
                                </td>                                
                                <td>
                                    <?php
									if($rayosx_pendientes[$i]->estado == 2 || $rayosx_pendientes[$i]->estado == 3 || $rayosx_pendientes[$i]->estado == 4){
										$from = $rayosx_pendientes[$i]->fecha_envio;
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+0 day');										
										$interval = new DateInterval('P1D');										
										$periods = new DatePeriod($from, $interval, $to);
										
										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}
										

										$tiempo_validacion = 20;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 17 && $days <=20){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 11 && $days <17){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 10){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}else if($rayosx_pendientes[$i]->estado == 14 || $rayosx_pendientes[$i]->estado == 15 || $rayosx_pendientes[$i]->estado == 16){										
										$resultEstado = $this->rx_model->consultar_estado_tramite($rayosx_pendientes[$i]->id, 13);
										
										if(count($resultEstado) > 0 && $resultEstado->fecha_estado != ''){
											$exd = date_create($resultEstado->fecha_estado);
											$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
											$from = $fecha_radicado;
										}else{
											$from = $rayosx_pendientes[$i]->fecha_envio;
										}											
										
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+0 day');										
										$interval = new DateInterval('P1D');										
										$periods = new DatePeriod($from, $interval, $to);
										
										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}
										

										$tiempo_validacion = 20;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 17 && $days <=20){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 11 && $days <17){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 10){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}else if($rayosx_pendientes[$i]->estado == 19 || $rayosx_pendientes[$i]->estado == 41){			
									
										$resultEstado = $this->rx_model->consultar_estado_tramite($rayosx_pendientes[$i]->id, $rayosx_pendientes[$i]->estado);
										
										if(count($resultEstado) > 0 && $resultEstado->fecha_estado != ''){
											$exd = date_create($resultEstado->fecha_estado);
											$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
											$from = $fecha_radicado;
										}else{
											$from = $rayosx_pendientes[$i]->fecha_envio;
										}	
										
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+0 day');										
										$interval = new DateInterval('P1D');										
										$periods = new DatePeriod($from, $interval, $to);
										
										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}
										

										$tiempo_validacion = 60;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 50 && $days <=60){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 11 && $days <49){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 10){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}else if($rayosx_pendientes[$i]->estado == 23){
										$resultEstado = $this->rx_model->consultar_estado_tramite($rayosx_pendientes[$i]->id, 23);
										
										if(count($resultEstado) > 0 && $resultEstado->fecha_estado != ''){
											$exd = date_create($resultEstado->fecha_estado);
											$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
											$from = $fecha_radicado;
										}else{
											$from = $rayosx_pendientes[$i]->fecha_envio;
										}	
										
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+0 day');										
										$interval = new DateInterval('P1D');										
										$periods = new DatePeriod($from, $interval, $to);
										
										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}
										

										$tiempo_validacion = 20;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 17 && $days <=20){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 11 && $days <16){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 10){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}else if($rayosx_pendientes[$i]->estado == 24 || $rayosx_pendientes[$i]->estado == 25){
										$resultEstado = $this->rx_model->consultar_estado_tramite($rayosx_pendientes[$i]->id, 23);
										
										if(count($resultEstado) > 0 && $resultEstado->fecha_estado != ''){
											$exd = date_create($resultEstado->fecha_estado);
											$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
											$from = $fecha_radicado;
										}else{
											$from = $rayosx_pendientes[$i]->fecha_envio;
										}	
										
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+0 day');										
										$interval = new DateInterval('P1D');										
										$periods = new DatePeriod($from, $interval, $to);
										
										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}
										

										$tiempo_validacion = 20;
									
										if($days > $tiempo_validacion){
											echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
										}else if($days >= 17 && $days <=20){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
										}else if($days >= 11 && $days <16){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
										}else if($days <= 10){
											$faltan= $tiempo_validacion - $days;
											echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
										}
									}

																	
                                    ?>
                                </td>
								<td>
                                   <?php

                                            if($rayosx_pendientes[$i]->estado == 2 || $rayosx_pendientes[$i]->estado == 3 || $rayosx_pendientes[$i]->estado == 4){
                                                ?>
                                                <center>
                                                  <a href="<?php echo base_url('validacion/validar_documentos_rx/'.$rayosx_pendientes[$i]->id)?>">
                                                    <img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="40px">
                                                    <br>Validar Informaci&oacute;n
                                                    </a>
                                                </center>
                                                <?php
											}else if($rayosx_pendientes[$i]->estado == 14 || $rayosx_pendientes[$i]->estado == 15 || $rayosx_pendientes[$i]->estado == 16 || $rayosx_pendientes[$i]->estado == 19 || $rayosx_pendientes[$i]->estado == 23 || $rayosx_pendientes[$i]->estado == 24 || $rayosx_pendientes[$i]->estado == 25 || $rayosx_pendientes[$i]->estado == 41){
												?>
                                                <center>
                                                  <a href="<?php echo base_url('validacion/validar_documentos_rx/'.$rayosx_pendientes[$i]->id)?>">
                                                    <img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="40px">
                                                    <br>Validar Informaci&oacute;n
                                                    </a>
                                                </center>
                                                <?php
											}else if($rayosx_pendientes[$i]->estado == 13 || $rayosx_pendientes[$i]->estado == 22){
												
												if($rayosx_pendientes[$i]->estado == 13){
													$id_subsanacion = 1;
													$fecha_subsana = $rayosx_pendientes[$i]->fecha_subsanacion1;
													if($fecha_subsana != ''){
														$exd = date_create($fecha_subsana);
														$fecha_subsana = date_format($exd,"d/m/Y");	
													}													
												}else if($rayosx_pendientes[$i]->estado == 22){
													$id_subsanacion = 2;
													$fecha_subsana = $rayosx_pendientes[$i]->fecha_subsanacion2;
													if($fecha_subsana != ''){
														$exd = date_create($fecha_subsana);
														$fecha_subsana = date_format($exd,"d/m/Y");	
													}
												}
												
												?>
													<center>
														Fecha notificación<br>
														<?php
														if($fecha_subsana != ''){
															echo $fecha_subsana;	
														}else{
															?>
																<input class="form-control-sm" type="date" name="fecha_noti<?php echo $rayosx_pendientes[$i]->id?>" id="fecha_noti<?php echo $rayosx_pendientes[$i]->id?>">
																<button  class="btn btn-info" onclick="guardarFecha(<?php echo $rayosx_pendientes[$i]->id?>,<?php echo $id_subsanacion?>)">Guardar</button>
															<?php
														}
														?>
													</center>
												<?php
											}else{
                                                
                                            }

                                    ?>

                                </td>
                            </tr>
                            <?php
        }
        }
		

        ?>
                        </tbody>
                    </table>

            </div>
		</div>
		
