﻿<script type="text/javascript">

    $(document).ready(function(){
        $(".actualizarVisorAction").click(function() {
            $("#visorPdf").attr('src', $(this).attr('title'));
        });
    });

</script>
<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>
        <div class="form-registro">
            <form class="form-horizontal" id="form_tramite" name="form_tramite" action="<?php echo base_url('validacion/guardarEstado/'.$tramites_pendientes->id_titulo)?>" method="post">
                <input type="hidden" name="id_persona" value="<?php echo $tramites_pendientes->id_persona?>">
                <input type="hidden" name="id_titulo" value="<?php echo $tramites_pendientes->id_titulo?>">
                <fieldset class="header-form">
                    <legend>Tr&aacute;mite
                        <?php echo $tramites_pendientes->id_titulo?> - Estado:
                        <?php echo $tramites_pendientes->descEstado?>
												<font size="12px">
												<br>
												<b>Fecha Trámite:</b> <?php echo $tramites_pendientes->fecha_tramite?>
												</font>
                    </legend>

                    <div class="col-md-1">
                        <i class="fas fa-user fa-8x"></i>
                    </div>
                    <div class="col-md-11">
                        <div class="form-inline">
                            <label for="">Tipo Identificaci&oacute;n:</label>
                            <select id="tipo_identificacion" name="tipo_identificacion" class="form-control validate[required]">
                              <option value="">Seleccione...</option>
                                <?php

                                for($i=0;$i<count($tipo_identificacion);$i++){
                                    
                                    if($tramites_pendientes->tipo_identificacion == $tipo_identificacion[$i]->IdTipoIdentificacion){
                                     
                                        echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."' selected>".$tipo_identificacion[$i]->Descripcion."</option>";
                                        
                                    }else{
                                        
                                        echo "<option value='".$tipo_identificacion[$i]->IdTipoIdentificacion."'>".$tipo_identificacion[$i]->Descripcion."</option>";
                                        
                                    }                                    
                                }

                                ?>
                            </select>
                        </div>
                        <div class="form-inline">
                            <label for="">N&uacute;mero Identificaci&oacute;n:</label>
                            <input id="nume_documento" name="nume_documento" placeholder="Número de documento de identidad" class="form-control input-md validate[required, maxSize[11], custom[number]]" required="" type="text" value="<?php echo $tramites_pendientes->nume_identificacion?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Primer nombre:</label>
                            <input id="p_nombre" name="p_nombre" placeholder="Primer Nombre" class="form-control validate[required, maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->p_nombre?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Segundo nombre:</label>
                            <input id="s_nombre" name="s_nombre" placeholder="Segundo Nombre" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->s_nombre?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Primer apellido:</label>
                            <input id="p_apellido" name="p_apellido" placeholder="Primer apellido" class="form-control validate[required, maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->p_apellido?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Segundo apellido:</label>
                            <input id="s_apellido" name="s_apellido" placeholder="Segundo apellido" class="form-control validate[maxSize[80]]" type="text" value="<?php echo $tramites_pendientes->s_apellido?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Correo electr&oacute;nico:</label>
                            <input id="email" name="email" placeholder="Correo eletrónico " class="form-control input-md validate[required, custom[email]]" required="" type="text" value="<?php echo $tramites_pendientes->email?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Teléfono fijo:</label>
                            <input id="telefono_fijo" name="telefono_fijo" placeholder="Teléfono fijo" class="form-control input-md validate[required, maxSize[10], custom[number]]" type="text" value="<?php echo $tramites_pendientes->telefono_fijo?>">
                        </div>
                        <div class="form-inline">
                            <label for="">Teléfono celular:</label>
                            <input id="telefono_celular" name="telefono_celular" placeholder="Teléfono celular" class="form-control input-md validate[required, maxSize[10], custom[number]]" type="text" value="<?php echo $tramites_pendientes->telefono_celular?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset class="header-form">
                    <div class="col-xs-6" >
                        <div class="col-xs-2">
                            <label>Informaci&oacute;n del t&iacute;tulo</label>
                            <i class="fas fa-file fa-8x"></i>
                        </div>
                        <div class="col-xs-10">
                            <div class="inp-form">
                                <label for="">Tipo de T&iacute;tulo:</label>
                                <?php
                                    if($tramites_pendientes->tipo_titulo == '1'){
                                        echo "Nacional";
                                        $div_nacional = "style='display:block'";
                                        $div_extranjero = "style='display:none'";
                                    }else{
                                        echo "Extranjero";
                                        $div_nacional = "style='display:none'";
                                        $div_extranjero = "style='display:block'";
                                    }
                                ?>
                            </div>
                            <div id="div_nacional" <?php echo $div_nacional;?>>

                                <div class="inp-form">
                                    <label for="">Instituci&oacute;n educativa:</label>
                                    <select id="institucion_educativa" name="institucion_educativa" class="form-control select2 validate[required]">
                                        <option value="">Seleccione...</option>
                                        <?php

                                        for($i=0;$i<count($instituciones);$i++){

                                            if($tramites_pendientes->institucion_educativa2 == $instituciones[$i]->id_institucion){

                                                echo "<option value='".$instituciones[$i]->id_institucion."' selected>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";

                                            }else{
                                                echo "<option value='".$instituciones[$i]->id_institucion."'>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
                                            }

                                        }

                                        ?>
                                    </select>
                                </div>
                                <div class="inp-form">
                                    <?php
									//var_dump($tramites_pendientes);
                                        $programas_result = $this->login_model->programasInstitucion($tramites_pendientes->institucion_educativa2);
                                    ?>
                                    <label for="">Profesi&oacute;n:</label>
                                    <select id="profesion" name="profesion" class="form-control select2 validate[required]">
                                        <option value="">Seleccione...</option>
                                        <?php

                                        for($i=0;$i<count($programas_result);$i++){

                                            if($tramites_pendientes->profesion == $programas_result[$i]->id_programa){

                                                echo "<option value='".$programas_result[$i]->id_programa."' selected>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";

                                            }else{
                                                echo "<option value='".$programas_result[$i]->id_programa."'>".$programas_result[$i]->nombre_programa." - ".$programas_result[$i]->id_programa."</option>";
                                            }

                                        }

                                        ?>
                                    </select>
                                </div>
                                <div class="inp-form">
                                    <label>Fecha Terminación:</label>
                                    <input id="fecha_term" name="fecha_term" placeholder="Fecha Terminación" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_term;?>">
                                </div>
                                <div class="inp-form">
                                    <label>Tarjeta Profesional:</label>
                                    <input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md" type="text" value="<?php echo $tramites_pendientes->tarjeta;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Diploma: </label>
                                    <input id="diploma" name="diploma" placeholder="Diploma No." class="form-control input-md validate[minSize[1], maxSize[30]]" type="text" value="<?php echo $tramites_pendientes->diploma;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Acta:</label>
                                    <input id="acta" name="acta" placeholder="Acta de grado" class="form-control input-md validate[minSize[1], maxSize[30]]" type="text" value="<?php echo $tramites_pendientes->acta;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Libro:</label>
                                    <input id="libro" name="libro" placeholder="Libro" class="form-control input-md validate[minSize[1], maxSize[6]]" type="text" value="<?php echo $tramites_pendientes->libro;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Folio: </label>
                                    <input id="folio" name="folio" placeholder="Folio" class="form-control input-md validate[minSize[1], maxSize[6]]" type="text" value="<?php echo $tramites_pendientes->folio;?>">
                                </div>

                                <div class="inp-form">
                                    <label>A&ntilde;o:</label>
                                    <input id="anio" name="anio" placeholder="A&ntilde;o" class="form-control input-md validate[required, minSize[4], maxSize[4]]" type="text" value="<?php echo $tramites_pendientes->anio;?>">
                                </div>


                            </div>
                            <div id="div_extranjero" <?php echo $div_extranjero;?>>

                                <div class="inp-form">
                                    <label>C&oacute;digo Universidad:</label>
                                    <input id="cod_universidad" name="cod_universidad" placeholder="C&oacute;digo Universidad internacional" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->cod_universidad;?>">
                                </div>

								<div class="inp-form">
                                    <label>Tarjeta Profesional:</label>
                                    <input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md" type="text" value="<?php echo $tramites_pendientes->tarjeta;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Fecha Terminación:</label>
                                    <input id="fecha_term_ext" name="fecha_term_ext" placeholder="Fecha Terminación" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_term_ext;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Resoluci&oacute;n de convalidaci&oacute;n:</label>
                                    <input id="resolucion" name="resolucion" placeholder="Numero de resoluci&oacute;n de convalidaci&oacute;n" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->resolucion;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Fecha Resoluci&oacute;n:</label>
                                    <input id="fecha_resolucion" name="fecha_resolucion" placeholder="Fecha de resoluci&oacute;n" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->fecha_resolucion;?>">
                                </div>

                                <div class="inp-form">
                                    <label>Entidad:</label>
                                    <select id="entidad" name="entidad" class="form-control validate[required]">
                                        <option value="">Seleccione...</option>
                                        <option value="1" <?php if($tramites_pendientes->entidad == 1){echo "selected";}?>>Ministerio de Educaci&oacute;n</option>
                                        <option value="2" <?php if($tramites_pendientes->entidad == 2){echo "selected";}?>>ICFES</option>
                                    </select>
                                </div>

                                <div class="inp-form">
                                    <label>T&iacute;tulo equivalente:</label>
                                    <input id="titulo_equivalente" name="titulo_equivalente" placeholder="Titulo equivalente" class="form-control input-md validate[required]" type="text" value="<?php echo $tramites_pendientes->titulo_equivalente;?>">
                                </div>

                            </div>
                        </div>



                        <div class="col-xs-2">
                            <label>Documentos Adjuntos</label>
                            <i class="fas fa-file-pdf fa-8x"></i>
                        </div>
                        <div class="col-xs-10">
                            <table class="table">
                            <tr>
                                <th>Descripci&oacute;n</th>
                                <th>Ver</th>
                            </tr>

                            <?php
                            if($tramites_pendientes->pdf_documento != 0){
                                $resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_documento);
                                $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
                                ?>
                                <tr>
                                    <td>Documento Identidad </td>
                                    <td>
                                        <a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                        </a>

                                        <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />

                                    </td>
                                </tr>
                                <?php
                            }

                            if($tramites_pendientes->pdf_titulo != 0){
                                $resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_titulo);
                                $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
                                ?>
                                    <tr>
                                        <td>T&iacute;tulo</td>
                                        <td>
                                            <a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
                                                <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                            </a>
                                            <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                                        </td>
                                    </tr>
                                    <?php
                            }

                            if($tramites_pendientes->pdf_acta != 0){
                                $resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_acta);
                                $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
                                ?>
                                        <tr class="inp-form">
                                            <td>Acta de grado</td>
                                            <td>
                                                <a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
                                                    <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                                </a>
                                                <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                                            </td>
                                        </tr>
                                        <?php
                            }

                            if($tramites_pendientes->pdf_tarjeta != 0){
                                $resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_tarjeta);
                                $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
                                ?>
                                            <tr class="inp-form">
                                                <td>Tarjeta Profesional </td>
                                                <td>
                                                    <a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
                                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                                    </a>
                                                    <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                                                </td>
                                            </tr>
                                            <?php
                            }

                            if($tramites_pendientes->pdf_resolucion != 0){
                                $resultado_archivo = $this->validacion_model->consultar_archivo($tramites_pendientes->pdf_resolucion);
                                $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/documentos/'.$resultado_archivo->nombre);
                                ?>
                                                <tr class="inp-form">
                                                    <td>Resoluci&oacute;n</td>
                                                    <td>
                                                        <a href="<?php echo base_url('uploads/documentos/'.$resultado_archivo->nombre)?>" target="_blank">
                                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                                        </a>
                                                        <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
                                                    </td>
                                                </tr>
                                                <?php
                            }
                            ?>
                            </table>
                        </div>

                    </div>

                    <!--
                        Visualizador de pdf, utiliza la libreria pdfjs la cual se encuentra en los recursos (assets) de la herramienta
                        Se soporta en el jquery que se encuentra al inicio de este archivo y los links de cada uno de los tipos de documento
                        Autor: Liliana Espinosa 20032019
                    -->
                    <div class="col-xs-6" >
                        <div class="col-xs-12" style="height:600px; " >
                            <iframe id="visorPdf" style="height: 100%; width: 100%;" src=""></iframe>

                        </div>
                    </div>
                    <!-- Fin visualizador pdf -->


                </fieldset>

                <fieldset class="header-form">
                    <div class="col-md-1">
                        <label>Resultado de la validaci&oacute;n</label>
                        <i class="fas fa-check-double fa-8x"></i>
                    </div>
                    <div class="col-md-11">

                        <select id="resultado_validacion" name="resultado_validacion" class="form-control validate[required]">
                            <option value="">Seleccione...</option>
                            <?php

                            for($i=0;$i<count($resultado_validacion);$i++){
                                echo "<option value='".$resultado_validacion[$i]->id_estado."'>".$resultado_validacion[$i]->descripcion."</option>";
                            }

                            ?>
                        </select>
                        <br><br>

						<!--Author: Mario Beltrán mebeltran@saludcapital.gov.co Since: 12062019
						Validación Oracle lado Ciudadano existencia de Resoluciones-->
						<?php
						if(count($tramitesoracle)>0){
						?>
                        <div class="alert alert-danger">
                          <strong>Apreciado Validador!</strong>
							<p>El sistema ha identificado que el numero de identificación del trámite actual ya cuenta con profesiones registradas en nuestras bases de datos existentes Oracle.<br>
							Agradecemos validar la siguiente información, si la profesión a realizar el trámite se encuentra a continuación. Favor abstenerse de continuar con la gestión del trámite.
							</p>
							<br>
							<?php
									for($i=0;$i<1;$i++){
									  echo "<center><b>Numero de Identificación:</b> ".$tramitesoracle[$i]->NROIDENT." - <b>Nombres y Apellidos:</b> ".$tramitesoracle[$i]->NOMBRES." ".$tramitesoracle[$i]->APELLIDOS."</center>";
									}
							?>
							<table style="width:100%;border: 1px solid #a94442;" border="1">
							  <thead>
								<tr>
								  <th><font color="#a94442"><b>Nombre Profesión</b></font></th>
								  <th><font color="#a94442"><b>Nombre Institución</b></font></th>
								  <th><font color="#a94442"><b>Fecha y No Resolución</b></font></th>
								</tr>
							  </thead>
							  <tbody>
								  <?php
									for($i=0;$i<count($tramitesoracle);$i++){
									  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->NOMBRE_PROFESION."</font></td>";
									  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->NOMBRE_INSTIT."</font></td>";
									  echo "<td><font color='#a94442'>".$tramitesoracle[$i]->FECHA_RESOLUCION." - No Resolución: ".$tramitesoracle[$i]->NUMERO_RESOLUCION."</font></td></tr>";
									}
								  ?>
							  </tbody>
							</table>
                        </div>
						<?php
						}
						?>
                        <div id="div_resuelve" style="display:none">

                            <div id="div_negacion" style="display:none">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="causales_negacion">Causales de negaci&oacute;n: </label>
                                    <div class="col-md-8">
                                        <select id="causales_negacion" name="causales_negacion[]" class="form-control validate[required]" multiple="multiple">
                                        <?php

                                        for($i=0;$i<count($causales_negacion);$i++){
                                            echo "<option value='".$causales_negacion[$i]->id_causal."'>".$causales_negacion[$i]->desc_causal."</option>";
                                        }

                                        ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="otras_causales_negacion">Otras causales de negaci&oacute;n: </label>
                                    <div class="col-md-8">
                                        <textarea name="otras_causales_negacion" id="otras_causales_negacion" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div id="div_recurso" style="display:none">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="argumentos_recurrente">Argumentos del recurrente: </label>
                                    <div class="col-md-8">
                                        <textarea name="argumentos_recurrente" id="argumentos_recurrente" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="consideraciones">Consideraciones de la Direcci&oacute;n de calidad de servicios de salud: </label>
                                    <div class="col-md-8">
                                        <textarea name="consideraciones" id="consideraciones" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="articulos">Articulos:</label>
                                    <div class="col-md-8">
                                        <textarea name="articulos" id="articulos" class="form-control">ARTICULO PRIMERO:
										ARTICULO SEGUNDO:
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div id="div_masinformacion" style="display:none">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="mensaje">Digite la informaci&oacute;n adicional que desea que adjunte o ajuste el usuario:</label>
                                    <div class="col-md-8">
                                        <textarea name="mensaje" id="mensaje" class="form-control validate[required]"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="observaciones">Observaciones</label>
                                    <div class="col-md-8">
                                        <textarea name="observaciones" id="observaciones" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-6" id="div_preliminar" style="display:block">
                                            <button name="preliminar" id="preliminar" type="submit" class="btn btn-lg btn-info" formtarget="_blank">Preliminar</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button name="guardar" id="guardar" type="submit" class="btn btn-lg btn-success" onclick="this.disabled">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </fieldset>
            </form>
        </div>
