<script type="text/javascript">
		
    $(document).ready(function(){
        $(".actualizarVisorAction").click(function() {                   
            $("#visorPdf").attr('src', $(this).attr('title'));            
        });
    });
                        
</script>
<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
    
?>


<fieldset class="header-form">
<div class="row block right" style="width:100%;">
			<div class="col-12 col-md-12">
				<div class="subtitle">
					<h2><b>Tr&aacute;mite ID.<?php echo $tramites_pendientes->id?></b></h2>
					<h4><b>Estado Trámite:</b> <?php echo $tramites_pendientes->descripcion?>
						<br>
						<b>Fecha Trámite:</b> <?php echo $tramites_pendientes->created_at?>
					</h4>
				</div>
            </div>
            
				

			<div class="col-12 col-md-12">
				<div class="subtitle">
					<h2><b>Equipos generadores de radiación ionizante</b> <i class="fas fa-x-ray"></i></b></h2>
				</div>			
            </div>
            <?php
            $verbtnCate = "block";
            ?>
           <div class="col-md-12">
               <span class="text-orange">•</span><label for="categoria">Categoría</label>
               <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">
               <input id="id_categoria_rayosx" name="id_categoria_rayosx" type="hidden" value="<?php if(isset($tramite_info)&& $tramite_info->categoria != ''){ echo $tramite_info->categoria;} ?>">
               <select id="categoria" name="categoria" class="form-control validate[required]" required <?php if(isset($tramite_info->categoria) && ($tramite_info->categoria == 1 || $tramite_info->categoria == 2)){echo "disabled";}?>>
                  <option value="">Seleccione...</option>
                  <option value="1"
                  <?php if(isset($tramite_info->categoria) && $tramite_info->categoria == 1){echo 'selected';$vercate1="block";$verbtnCate = "none";}else{$vercate1="none";} ?>>
                    Categoría  I
                  </option>
                  <option value="2"
                  <?php if(isset($tramite_info->categoria) && $tramite_info->categoria == 2){echo 'selected';$verbtnCate = "none";$vercate2="block";}else{$vercate2="none";}?>>
                    Categoría  II
                  </option>
               </select>
            </div>
            <div id="resultado_seccion2_1" class="col-12 col-md-12 table-responsive">
                <table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Marca Equipo</th>
						<th>Modelo Equipo</th>
						<th>Serie Equipo</th>										
						<th>Ver más</th>
					</tr>
				</thead>
				<tbody>
				<?php
				
				for($i=0;$i<count($rayosxEquipo);$i++){
					?>
					<tr>
						<td><?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></td>
						<td><?php echo $rayosxEquipo[$i]->marca_equipo?></td>
						<td><?php echo $rayosxEquipo[$i]->modelo_equipo?></td>
						<td><?php echo $rayosxEquipo[$i]->serie_equipo?></td>										
						<td>
							<a class="btn btn-info" onClick="abrirModal('Equipo <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>','#ex<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>')">Ver más...</a>
						</td>						
					</tr>
					<tr>
						<td colspan="6">
							<!--<div class="collapse" id="obs<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">-->
							<div>
								<div class="card card-body">
									<form class="form-horizontal"  id="form_equipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="form_equipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" action="<?php echo base_url('validacion/guardarObservacionInfra/')?>" method="post">
										<input type="hidden" name="id_tramite" value="<?php echo $tramites_pendientes->id?>">
										<input type="hidden" name="id_equipo" value="<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
										<input type="hidden" name="estado" value="<?php echo $tramites_pendientes->estado?>">
										<div class="row" >
											<h5>Resolución 482 de 2018</h5>
											<p>
												23.7 Plano general de las instalaciones de acuerdo con lo establecido en la Resolución 4445 de 1996, expedida por el entonces Ministerio de Salud o la norma de la modifique o sustituya, el cual debe contener:
											</p>
										</div>
										<?php
										
										$items482 = $this->rx_model->itemsInfraestructura('482');
										
										for($it=0;$it<count($items482);$it++){
											
											
											$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
											$itemCons['id_tramite'] = $tramites_pendientes->id; 
											$itemCons['id_item'] = $items482[$it]->id_item; 
											$itemCons['id_estado'] = $tramites_pendientes->estado; 
											
											$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
											
											if(count($resultadoEquipoInfra)>0){
												$obs = $resultadoEquipoInfra->observaciones;
											}else{
												
												$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
												$itemCons['id_tramite'] = $tramites_pendientes->id; 
												$itemCons['id_item'] = $items482[$it]->id_item; 
												
												if($tramites_pendientes->estado == 15){
													$itemCons['id_estado'] = 3; 	
												}else if($tramites_pendientes->estado == 24){
													$itemCons['id_estado'] = 15; 	
												}												
												
												$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
												
												if(count($resultadoEquipoInfra)>0){
													$obs = $resultadoEquipoInfra->observaciones;
												}else{
													$obs = "";
												}
											}	
											
											?>
											<hr>
											<div class="row">
												<div class="col-md-6">
													<p>
														<?php echo $items482[$it]->desc_item?>
													</p>
												</div>
												<div class="col-md-6">
													<textarea type="text" class="form-control" id="item_<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>_<?php echo $items482[$it]->id_item?>" name="item_<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>_<?php echo $items482[$it]->id_item?>"><?php echo $obs?></textarea>
												</div>
											</div>
											<?php							
										}
										
										?>
										<hr>
										<hr>
										<div class="row">
											<h5>Articulo 33 Resolución 4445 de 1996 </h5>
										</div>
										<?php
										
										$items4445 = $this->rx_model->itemsInfraestructura('4445');
										
										for($it=0;$it<count($items4445);$it++){
											
											$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
											$itemCons['id_tramite'] = $tramites_pendientes->id; 
											$itemCons['id_item'] = $items4445[$it]->id_item; 
											$itemCons['id_estado'] = $tramites_pendientes->estado; 
											
											$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
											
											if(count($resultadoEquipoInfra)>0){
												$obs = $resultadoEquipoInfra->observaciones;
											}else{

												$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
												$itemCons['id_tramite'] = $tramites_pendientes->id; 
												$itemCons['id_item'] = $items4445[$it]->id_item; 
												
												if($tramites_pendientes->estado == 15){
													$itemCons['id_estado'] = 3; 	
												}else if($tramites_pendientes->estado == 24){
													$itemCons['id_estado'] = 15; 	
												}												
												
												$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
												
												if(count($resultadoEquipoInfra)>0){
													$obs = $resultadoEquipoInfra->observaciones;
												}else{
													$obs = "";
												}
											}	
											
											
											?>
											<hr>
											<div class="row">
												<div class="col-md-6">
													<p>
														<?php echo $items4445[$it]->desc_item?>
													</p>
												</div>
												<div class="col-md-6">
													<textarea type="text" class="form-control" id="item_<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>_<?php echo $items4445[$it]->id_item?>" name="item_<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>_<?php echo $items4445[$it]->id_item?>"><?php echo $obs?></textarea>
												</div>
											</div>
											<?php							
										}
										
										?>
										<!--<center><button type="submit" name="guardarObservacion" class="btn btn-info" onclick="guardarObserInfra(<?php echo $tramites_pendientes->id?>, <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">Guardar Observación</button></center>-->
										
									</form>
									<center><input type="submit" form="form_equipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="btn btn-info" value="Guardar Observación"></center>
								</div>
							</div>
						</td>
					</tr>
					<div id="ex<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="modal">
					  <p><h2><b>Equipo ID:<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></b></h2></p>
						<ul>
							<?php
										if($rayosxEquipo[$i]->categoria1 != 0 || $rayosxEquipo[$i]->categoria1 != NULL){
											if($rayosxEquipo[$i]->categoria1 == 1){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiolog&iacute;a odontol&oacute;gica periapical</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria1 == 2){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Equipo de RX</li>
												<?php
											}
										}
								   
										if($rayosxEquipo[$i]->categoria2 != 0 || $rayosxEquipo[$i]->categoria2 != NULL){
											if($rayosxEquipo[$i]->categoria2 == 1){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radioterapia</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2 == 2){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de alta complejidad</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2 == 3){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de media complejidad</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2 == 4){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de baja complejidad</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2 == 5){
												?>
												<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</li>
												<?php
											}
										}
							
										if($rayosxEquipo[$i]->categoria1_1 != 0 || $rayosxEquipo[$i]->categoria1_1 != NULL){
											if($rayosxEquipo[$i]->categoria1_1 == 1){
												?>
												<li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria1_1 == 2){
												?>
												<li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical portat&iacute;l</li>
												<?php
											}
										}
							
										if($rayosxEquipo[$i]->categoria1_2 != 0 || $rayosxEquipo[$i]->categoria1_2 != NULL){
											if($rayosxEquipo[$i]->categoria1_2 == 1){
												?>
												<li><b>Equipo de RX: </b>Densit&oacute;metro &oacute;seo</li>
												<?php
											}
										}
										
										if($rayosxEquipo[$i]->categoria2_1 != 0 || $rayosxEquipo[$i]->categoria2_1 != NULL){
											if($rayosxEquipo[$i]->categoria2_1 == 1){
												?>
												<li><b>Equipo de RX: </b>Equipo de RX convencional</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 2){
												?>
												<li><b>Equipo de RX: </b>Tomógrafo Odontológico</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 3){
												?>
												<li><b>Equipo de RX: </b>Tomógrafo</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 4){
												?>
												<li><b>Equipo de RX: </b>Equipo de RX Portátil</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 5){
												?>
												<li><b>Equipo de RX: </b>Equipo de RX Odontológico</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 6){
												?>
												<li><b>Equipo de RX: </b>Panorámico Cefálico</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 7){
												?>
												<li><b>Equipo de RX: </b>Fluoroscopio</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 8){
												?>
												<li><b>Equipo de RX: </b>SPECT-CT</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 9){
												?>
												<li><b>Equipo de RX: </b>Arco en C</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 10){
												?>
												<li><b>Equipo de RX: </b>Mamógrafo</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 11){
												?>
												<li><b>Equipo de RX: </b>Litotriptor</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 12){
												?>
												<li><b>Equipo de RX: </b>Angiógrafo</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 13){
												?>
												<li><b>Equipo de RX: </b>PET-CT</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 14){
												?>
												<li><b>Equipo de RX: </b>Acelerador lineal</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 15){
												?>
												<li><b>Equipo de RX: </b>Sistema de radiocirugia robótica</li>
												<?php
											}else if($rayosxEquipo[$i]->categoria2_1 == 16){
												?>
												<li><b>Equipo de RX: </b><?php echo $rayosxEquipo[$i]->otro_equipo?></li>
												<?php
											}
										}					
							
										if($rayosxEquipo[$i]->tipo_visualizacion != 0 || $rayosxEquipo[$i]->tipo_visualizacion != NULL){
											if($rayosxEquipo[$i]->tipo_visualizacion == 1){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Digital</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 2){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Digitalizado</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 3){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Análogo</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 4){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Revelado Automático</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 5){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Revelado Manual</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 6){
												?>
												<li><b>Tipo de visualización de la imagen: </b>Monitor Análogo</li>
												<?php
											}else if($rayosxEquipo[$i]->tipo_visualizacion == 7){
												?>
												<li><b>Tipo de visualización de la imagen: </b>No Aplica</li>
												<?php
											}
										}
									?>
							<li><b>Marca Equipo: </b><?php echo $rayosxEquipo[$i]->marca_equipo?></li>
							<li><b>Modelo Equipo: </b><?php echo $rayosxEquipo[$i]->modelo_equipo?></li>
							<li><b>Serie Equipo: </b><?php echo $rayosxEquipo[$i]->serie_equipo?></li>
							<li><b>Marca Tubo RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx?></li>
							<li><b>Modelo Tubo RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx?></li>
							<li><b>Serie Tubo RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx?></li>
							<li><b>Tensión máxima tubo RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx?></li>
							<li><b>Cont. Max del tubo RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx?></li>
							<li><b>Energía de fotones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones?></li>
							<li><b>Energía de electrones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones?></li>
							<li><b>Carga de trabajo [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo?></li>
							<li><b>Ubicación del equipo de la instalación: </b><?php echo $rayosxEquipo[$i]->ubicacion_equipo?></li>
							<li><b>Año de fabricación del equipo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion?></li>
							<li><b>Año de fabricación del tubo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo?></li>
							<?php
								
								if($rayosxEquipo[$i]->fi_blindajes != ''){
									$fi_blindajes = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes);
									
									if($fi_blindajes){
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
										<?php
									}		
								}else{
									?>
									<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
									<?php
								}
								
								
								
								if($rayosxEquipo[$i]->fi_control_calidad != ''){
									$fi_control_calidad = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad);
									
									if($fi_control_calidad){
										?>
										<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
									<?php
								}	

								if($rayosxEquipo[$i]->fi_plano != ''){
									$fi_plano = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_plano);
									
									if($fi_plano){
										?>
										<li><b>Plano general de las instalaciones: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_plano->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
									<?php
								}		
								
								if($rayosxEquipo[$i]->fi_pruebas_caracterizacion != ''){
									$fi_pruebas_caracterizacion = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion);
									
									if($fi_pruebas_caracterizacion){
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
									<?php
								}									
							
								if($rayosxEquipo[$i]->categoria2_1 == 16){
									
									?>
									<h3>Información Tubo RX 2</h3>
									
									<li><b>Marca Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx2?></li>
									<li><b>Modelo Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx2?></li>
									<li><b>Serie Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx2?></li>
									<li><b>Tensión máxima tubo 2 RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx2?></li>
									<li><b>Cont. Max del tubo 2 RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx2?></li>
									<li><b>Energía de fotones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones2?></li>
									<li><b>Energía de electrones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones2?></li>
									<li><b>Carga de trabajo 2 [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo2?></li>
									<li><b>Año de fabricación del tubo2: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo2?></li>
									<?php
									
									if($rayosxEquipo[$i]->fi_blindajes2 != ''){
										$fi_blindajes2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes2);
										
										if($fi_blindajes2){
											?>
											<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
											<?php
										}		
									}else{
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
										<?php
									}
									
									if($rayosxEquipo[$i]->fi_control_calidad2 != ''){
										$fi_control_calidad2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad2);
										
										if($fi_control_calidad2){
											?>
											<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
											<?php
										}
									}else{
										?>
										<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
										<?php
									}	

									
									if($rayosxEquipo[$i]->fi_pruebas_caracterizacion2 != ''){
										$fi_pruebas_caracterizacion2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion2);
										
										if($fi_pruebas_caracterizacion2){
											?>
											<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
											<?php
										}
									}else{
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
										<?php
									}	
									
								}
							?>
						</ul>					  
					</div>
					
					
					
					<?php
				}
				?>								
				</tbody>
			</table>
            </div>
			
			<div class="row block w-100 newsletter ">
			<div class="col-12 col-md-12 text-center ">
			<br><br>
				<h3>
					<table style="width:100%;">
					<tr>
					<td align="left"><b>Visualizador Archivos Adjuntos</b> <i class="far fa-file-pdf"></i></td>
					<td align="right"><font color="#3D5DA6"><i id="mindiv6" class="fas fa-angle-up"></i><i id="morediv6" class="fas fa-angle-down"></i></font></td>
					</tr>
					</table>				
				</h3>
            </div>
            
<?php
if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 1))
{	
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_1 = "display:block";
	$display_form4_2 = "display:none";
}else if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 2)){
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_2 = "display:block";
	$display_form4_1 = "display:none";
}else{
	$categoria_form = 1;
	$display_form4_1 = "display:none";
	$display_form4_2 = "display:none";
}

if($tramites_pendientes->tipo_identificacion != 5){
	$display_cedula = "display:block";
	$display_rut = "display:none";
}else{
	$display_rut = "display:block";
	$display_cedula = "display:none";
}

if($tramite_info->visita_previa && $tramite_info->visita_previa == 2){
	$display_docTalento = "display:none";
}else{
	$display_docTalento = "display:block";
}
        
       /* echo "<pre>";
        print_r($documentosTramite);
        echo "</pre>";*/
        
?>
        



     <div class="col-6 col-md-6 table-responsive" style="<?php echo $display_form4_1;?>">
       <h4><b><span class="text-orange">•</span>Documentos Categoría I</b></h4>
		<div class="col-12 col-md-12 table-responsive">
		   <table class="table table-hover">
			   <thead>
				  <tr>
					 <th>Descripción</th>
					 <th>Cargar Documento</th>
				  </tr>
			   </thead>
			   <tbody>
			   <?php
			   
				   if($display_cedula == 'display:block'){
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pn_doc");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                            <tr>
                                <td>Fotocopia documento de identificación</td>
                                <td>
									<?php
									if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
									{										
									?>
                                    <a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
                                    <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
									<?php
									   }else{
											echo "Sin archivo";
									   }
									?>
                                </td>
                            </tr>
                            <?php
				   }
				   
				   
				   if($display_rut == 'display:block'){
					   
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pj_doc");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                            <tr>
                                <td>Fotocopia del Registro Unico Tributario - RUT</td>
                                <td>
                                    <?php
									if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
									{										
									?>
                                    <a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
                                    <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
									<?php
									   }else{
											echo "Sin archivo";
									   }
									?>
                                </td>
                            </tr>
                            <?php                   
				   }
				   
				   if($display_rut == 'display:block'){
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pj_cyc");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                            <tr>
                                <td>Registro cámara y comercio</td>
                                <td>
                                    <?php
									if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
									{										
									?>
                                    <a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
                                    <img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
									<?php
									   }else{
											echo "Sin archivo";
									   }
									?>
                                </td>
                            </tr>
                            <?php                        
				   }
                   
                   $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_doc_encargado");
                   $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Copia documento identificación del encargado de protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php 
                   $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_encargado");
                   $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Copia del diploma del encargado de protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>    
					<?php
					$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_blindajes");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_control_calidad");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Informe sobre los resultados del control de calidad</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               	
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_registro_dosimetrico");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php 
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_registro_niveles");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    
                    <?php 
                    
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_certificado_capacitacion");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php 
    
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_programa_capacitacion");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Programa de capacitación en protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
    
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_procedimiento_mantenimiento");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    
                    <?php 
    
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_programa_tecno");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Programa de Tecno vigilancia</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php 
                   
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_programa_proteccion");
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                   ?>
                   <tr>
                        <td>Programa de protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php     
    
					
					$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_plano");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Plano general de las instalaciones</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }	
					
				   if($display_docTalento == 'display:block'){
					   
                   
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_soporte_talento");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo 1 del artículo 21</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php 
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_director");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php 
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_res_convalida_director");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico </td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php 
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_pos_profe");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php  
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_res_convalida_profe");
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php  
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_cert_calibracion");
                        
                        if($resultado_archivo){
                            $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php 
                        }
                               
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_declaraciones");
                        
                        if($resultado_archivo){
                            $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);    
                        ?>
                        <tr>
                            <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                            <td>
                               <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php                  
                        }
				   }
				   
				   ?>
				  
				  
			   </tbody>
			</table>
		</div>        
     </div>

     <div class="col-6 col-md-6 table-responsive" style="<?php echo $display_form4_2;?>">
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>
        <input id="categoria_docs" name="categoria_docs" type="hidden" value="<?php echo $categoria_form?>">     
		<div class="col-12 col-md-12 table-responsive">		
			<table class="table table-hover">
			   <thead>
				  <tr>
					 <th>Descripción</th>
					 <th>Documento</th>
				  </tr>
			   </thead>
			   <tbody>
				 <?php
				   if($display_cedula == 'display:block'){
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pn_doc");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia documento de identificación</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
				   }
				   
				   
				   if($display_rut == 'display:block'){
                       $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pj_doc");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia del Registro Único Tributario - RUT</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
				   }
				   
				   if($display_rut == 'display:block'){
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "pj_cyc");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Registro cámara y comercio</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
				   }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_doc_oficial");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Copia documento identificación del oficial de protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_oficial");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Copia del diploma del oficial de protección radiológica</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_blindajes");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_control_calidad");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Informe sobre los resultados del control de calidad</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_registro_dosimetrico");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosímetro</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
               
                    $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_plano");
                    if($resultado_archivo){
                    $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                    ?>
                    <tr>
                        <td>Plano general de las instalaciones</td>
                        <td>
                            <?php
							if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
							{										
							?>
							<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
								<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
							</a>
							<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
							<?php
							   }else{
									echo "Sin archivo";
							   }
							?>
                        </td>
                    </tr>
                    <?php
                    }
					
				   if($display_docTalento == 'display:block'){
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_soporte_talento");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo del articulo 23</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_director");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_res_convalida_director");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_diploma_pos_profe");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_res_convalida_profe");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_cert_calibracion");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "fi_declaraciones");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
                       
				   }
				   if($tramites_pendientes->categoria == 2){
					   ?>
						<tr>
							<td colspan="2">Documentos Visita</td>
						</tr>
						<?php
                        
                        $resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "resultado_visita");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Acta de la visita</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_certificado");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Certificado expedido por una institución de educación superior o por una institución de Educación para el Trabajo y el Desarrollo Humano, en el que se acredite la capacitación en materira de protección radiológica de los trabajadores ocupacionalmente expuestos</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_prote_radio");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Programa de capacitación en protección radiológica</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_niveles");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Registro de los niveles de referencia para diagnóstico, respecto de los procedimientos más comunes</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_elementos");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Descripción de los elementos, sistemas y componenetes necesarios en la práctica médica categoria II que se realice</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_procedimientos");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Procedimientos de mantenimiento de los equipos generadores de radicación ionizante, de conformidad con lo establecido por el fabricante</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_resultados");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documentos suministrado por el instalador del equipo o equipos, que contenga los resultados de las pruebas iniciales de caracterización y puesta en marcha de dicho equipo o equipos</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_vigilancia_radio");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento que contenga el programa de vigilancia radiológica</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_tecnovigilancia");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento que contenga el programa institucional de tecnovigilancia</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						$resultado_archivo = $this->rx_model->consultar_archivo($tramites_pendientes->id, "visita_docu_prote");
                        if($resultado_archivo){
                        $visorFilePdf=base_url('assets/pdfjs/web/viewer.html')."?file=".base_url('uploads/rayosx/'.$resultado_archivo->nombre);
                        ?>
                        <tr>
                            <td>Documento que contenga un programa de protección radiológica</td>
                            <td>
                                <?php
								if(count($resultado_archivo) > 0 && $resultado_archivo->nombre != '')
								{										
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<img title="<?php echo $visorFilePdf?>"  style="cursor: pointer;" class="actualizarVisorAction" src="<?php echo base_url('assets/imgs/pdfVisor.png')?>" width="40px" />
								<?php
								   }else{
										echo "Sin archivo";
								   }
								?>
                            </td>
                        </tr>
                        <?php
                        }
						
						
					   
				   }
				   
				   
				   ?>
				  
			   </tbody>
			</table>
		</div>
     </div>
        			
	<div class="col-6 col-md-6 " style="height: 600px;" id="divdatoarchvisor">
		<iframe id="visorPdf" style="height: 100%; width: 100%;" src=""></iframe>
	</div>
    
<form class="form-horizontal" id="form_tramite" name="form_tramite" action="<?php echo base_url('validacion/guardarEstadoInfraRx/'.$tramites_pendientes->id)?>" method="post">
<input type="hidden" name="id_persona" value="<?php echo $tramites_pendientes->id_persona?>">
<input type="hidden" name="id_tramite" value="<?php echo $tramites_pendientes->id?>">
    
<div class="row" style="width:100%;">
	<div class="col-12 col-md-12 pl-4">
		<div class="subtitle">
			<h2><b>Resultado de la validaci&oacute;n</b> <i class="fas fa-check-double"></i></h2>
		</div>
	</div>
	
	<div class="col-10 col-md-10 pl-4">
	<select id="resultado_validacion" name="resultado_validacion" class="form-control validate[required]">
		<option value="">Seleccione...</option>
		<?php
		for($i=0;$i<count($resultado_validacion);$i++){
			
			if($tramites_pendientes->id_estado == 3 && $resultado_validacion[$i]->id_estado == 4){
				echo "<option value='".$resultado_validacion[$i]->id_estado."'>".$resultado_validacion[$i]->descripcion."</option>";	
			}else if($tramites_pendientes->id_estado == 15 && $resultado_validacion[$i]->id_estado == 16){
				echo "<option value='".$resultado_validacion[$i]->id_estado."'>".$resultado_validacion[$i]->descripcion."</option>";	
			}else if($tramites_pendientes->id_estado == 24 && $resultado_validacion[$i]->id_estado == 25){
				echo "<option value='".$resultado_validacion[$i]->id_estado."'>".$resultado_validacion[$i]->descripcion."</option>";	
			}
			
		}
		?>
	</select>
	
	<div class="col-12 col-md-12 pl-4">
		<div id="div_resuelve" style="display:none">				
			<div>
				<label><b>Observaciones</b></label>
				<textarea name="observaciones" id="observaciones" class="form-control" style="width:100%;height:80px"></textarea>
			</div>
			
			<div>
				<br><br>
				<button name="guardar" id="guardar" class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button" onclick="this.disabled">Guardar</button>
			</div>
		</div>

	</div>
	</div>
	<div class="col-2 col-md-2 pl-4" align="center">
		<a id="btn_seguimiento" onClick="abrirModal('Tabla de Seguimiento','#modalSeguimiento')"><img src="<?php echo base_url('assets/imgs/audit.png')?>"><br>Seguimiento Auditoría</a>
		<br><br>
	</div>
</div>
	
</div>
</fieldset>
</form>

<div id="modalSeguimiento" class="modal">
  	<legend>Tabla de Seguimiento</legend>
	<table width="100%" border="1" id="customers">
		<thead>
			<tr>
				<th width="12%"><b>Fecha Seguimiento</b></th>
				<th width="25%"><b>Usuario</b></th>
				<th width="15%"><b>Estado</b></th>
				<th width="48%"><b>Observación</b></th>
			</tr>
		</thead>
		<tbody>
		<?php
		if(count($tramites_seguimientos)>0){
		for($i=0;$i<count($tramites_seguimientos);$i++){
		?>
		<tr>
			<td style="height:55px;">
				<?php echo date("Y-m-d",strtotime($tramites_seguimientos[$i]->fecha_estado));?>
			</td>
			<td style="height:55px;">
				<?php echo $tramites_seguimientos[$i]->username;?> 
			</td>
			<td style="height:55px;">
				<?php echo $tramites_seguimientos[$i]->descEstado?>
			</td>
			<td style="height:55px;">
				<?php echo $tramites_seguimientos[$i]->observaciones;?>
			</td>
		</tr>
		<?php									
		}
		}
		?>
		</tbody>
	</table>		  
</div>

<script type="text/javascript">

	$("#mindatospersonal").click(function () {
		$('#divdatopertipoiden').hide();
		$('#divdatopernumiden').hide();
		$('#divdatoperpnombre').hide();
		$('#divdatopersnombre').hide();
		$('#divdatoperpapellido').hide();
		$('#divdatopersapellido').hide();
		$('#divdatopertelcelu').hide();
		$('#divdatopertelfijo').hide();
		$('#divdatoperemail').hide();
		$('#divdatoperfecnac').hide();
		$('#divdatopersexo').hide();
		$('#divdatopergenero').hide();
		$('#divdatoperorienta').hide();
		$('#divdatoperetnia').hide();
		$('#divdatoperestciv').hide();
		$('#divdatopernivedu').hide();
	});
	
	$("#moredatospersonal").click(function () {
		$('#divdatopertipoiden').show();
		$('#divdatopernumiden').show();
		$('#divdatoperpnombre').show();
		$('#divdatopersnombre').show();
		$('#divdatoperpapellido').show();
		$('#divdatopersapellido').show();
		$('#divdatopertelcelu').show();
		$('#divdatopertelfijo').show();
		$('#divdatoperemail').show();
		$('#divdatoperfecnac').show();
		$('#divdatopersexo').show();
		$('#divdatopergenero').show();
		$('#divdatoperorienta').show();
		$('#divdatoperetnia').show();
		$('#divdatoperestciv').show();
		$('#divdatopernivedu').show();
	});
	
	$("#mindatosgeo").click(function () {
		$('#divdatogeonac').hide();
		$('#divdatogeodepanac').hide();
		$('#divdatogeociudnac').hide();
		$('#divdatogeodeparesi').hide();
		$('#divdatogeociudresi').hide();
	});	

	$("#moredatosgeo").click(function () {
		$('#divdatogeonac').show();
		$('#divdatogeodepanac').show();
		$('#divdatogeociudnac').show();
		$('#divdatogeodeparesi').show();
		$('#divdatogeociudresi').show();
	});	

	$("#mindatosaca").click(function () {
		$('#div_nacional').hide();
		$('#div_nacional2').hide();
		$('#div_extranjero').hide();
	});	

	$("#moredatosaca").click(function () {
		if($("#tipotitulovalue").val() == 1){
		$('#div_nacional').show();
		$('#div_nacional2').show();}
		else{
		$('#div_extranjero').show();
		}
	});	

	$("#mindatosarch").click(function () {
		$('#divdatoarch').hide();
		$('#divdatoarchvisor').hide();
	});	

	$("#moredatosarch").click(function () {
		$('#divdatoarch').show();
		$('#divdatoarchvisor').show();
	});		

	$("#preliminar").click(function () {
		$('#divdatopertipoiden').show();
		$('#divdatopernumiden').show();
		$('#divdatoperpnombre').show();
		$('#divdatopersnombre').show();
		$('#divdatoperpapellido').show();
		$('#divdatopersapellido').show();
		$('#divdatopertelcelu').show();
		$('#divdatopertelfijo').show();
		$('#divdatoperemail').show();
		$('#divdatoperfecnac').show();
		$('#divdatopersexo').show();
		$('#divdatopergenero').show();
		$('#divdatoperorienta').show();
		$('#divdatoperetnia').show();
		$('#divdatoperestciv').show();
		$('#divdatopernivedu').show();	
		$('#divdatogeonac').show();
		$('#divdatogeodepanac').show();
		$('#divdatogeociudnac').show();
		$('#divdatogeodeparesi').show();
		$('#divdatogeociudresi').show();
		if($("#tipotitulovalue").val() == 1){
		$('#div_nacional').show();
		$('#div_nacional2').show();}
		else{
		$('#div_extranjero').show();
		}
		$('#divdatoarch').show();
		$('#divdatoarchvisor').show();		
	});
	
	$("#guardar").click(function () {
		$('#divdatopertipoiden').show();
		$('#divdatopernumiden').show();
		$('#divdatoperpnombre').show();
		$('#divdatopersnombre').show();
		$('#divdatoperpapellido').show();
		$('#divdatopersapellido').show();
		$('#divdatopertelcelu').show();
		$('#divdatopertelfijo').show();
		$('#divdatoperemail').show();
		$('#divdatoperfecnac').show();
		$('#divdatopersexo').show();
		$('#divdatopergenero').show();
		$('#divdatoperorienta').show();
		$('#divdatoperetnia').show();
		$('#divdatoperestciv').show();
		$('#divdatopernivedu').show();
		$('#divdatogeonac').show();
		$('#divdatogeodepanac').show();
		$('#divdatogeociudnac').show();
		$('#divdatogeodeparesi').show();
		$('#divdatogeociudresi').show();
		if($("#tipotitulovalue").val() == 1){
		$('#div_nacional').show();
		$('#div_nacional2').show();}
		else{
		$('#div_extranjero').show();
		}
		$('#divdatoarch').show();
		$('#divdatoarchvisor').show();		
	});	
	
	$("#resultado_validacion").change(function(){
        if($("#resultado_validacion").val() != ''){
            $("#div_resuelve").show();

        }else{
            $("#div_resuelve").hide();
        }
    });
	
	$("#btn_seguimiento").click(function () {
		$('#modalseguimiento').modal('show');

	});

</script>
