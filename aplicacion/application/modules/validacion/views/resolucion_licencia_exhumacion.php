<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Licencia de Exhumación</title>
    </head>
    <body>
        <table width="100%" cellpadding='0' cellspacing="0" border="1">
            <tr>
                <td>
                       <!--<img src="<?php echo FCPATH . 'assets/imgs/logo_pdf_alcaldia.png' ?>" width="200px">-->
                    <img src="<?php echo FCPATH . 'assets/imgs/logo_pdf_alcaldia.png' ?>" width="70px">
                </td>
                <td style="text-align: center; font-size: 10px">DIRECCION DE CALIDAD DE SERVICIOS DE SALUD 
                    <br>SUBDIRECCION INSPECCION VIOGILANCIA Y CONTROL DE <br>SERVICIOS DE SALUD<br> 
                    SISTEMA INTEGRADO DE GESTIÓN<br> CONTROL DOCUMENTAL<br> FORMATO LICENCIA DE EXHUMACIÓN
                </td>
                <td style="text-align: left; font-size: 10px">    Elaborado por: Gestor de calidad<br>
                    Revisado por: Dalbeth Elena Henriquez Iguarán<br>
                    Aprobado por: Isabel Cristina Artunduaga Pastrana
                    <br>
                    Control documental: Dirección Planeación -SIG
                </td>
                <td> 
                    <img src="<?php echo FCPATH . 'assets/imgs/logoexhumacion.png' ?>" width="70px">
                </td>
            </tr>   
        </table>
        <table width="100%" cellpadding='0' cellspacing="0" border="0">   
            <tr>
                <td width="40%" style="font-size: 16px;text-align: center">
					<br>
                    <b>LICENCIA DE EXHUMACION</b>
                </td>
				<td style="text-align: right;">
					<br>
					<b>No. <?php
                    $fecha = new DateTime($fecha_aprob);
                    $fechaN = $fecha->format('d/m/Y');
					//$idLE = $this->mlicencia_exhumacion->numLicenciaExh();
                    echo $numero_lic_exhu;
                     ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
				</td>
            </tr>
			<tr>
			    <td colspan="2" style="font-size: 16px;text-align: center">
				<br>
				Fecha <?php echo '  ' . $fechaN ?> Lugar y Expedición de la Licencia de Exhumación: Bogotá D.C
				<br>
				<br>
                </td>
			</tr>
            <tr>
				<td style="background-color:black" colspan="2">
					<img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>">					
				</td>
            <tr><td colspan="2"><br></td></tr>
            </tr>
            <tr>
                <td><span><b>SE CONCEDE LICENCIA AL SEÑOR(A):</b></span> </td>
                <td><?php echo strtoupper($info_aprobacionlicencia->p_nombre . " " . $info_aprobacionlicencia->s_nombre . " " . $info_aprobacionlicencia->p_apellido . " " . $info_aprobacionlicencia->s_apellido) . " " ?></td>
            </tr>
            <tr>
                <td> 
                </td>
                <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>


             <tr><td colspan="2"><br></td></tr>
			 
            <tr >
                <td><span><b>DE PARENTESCO:</b></span></td>
				<td><?php echo strtoupper($info_aprobacionlicencia->parentesco)?></td>
            </tr>
            <tr>
                <td> 
                </td>
				<td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
			<tr><td colspan="2"><br></td></tr>
			 
			<?php if (strpos($info_aprobacionlicencia->parentesco, 'Otro') !== false){
			?>
            <tr>
				<td colspan="2" align="center"> De acuerdo con el anterior parentesco, este se encuentra soportado según lo establecido en el ART. 35 CCC Y S.S</td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
			 <?php
			}
			 ?>
            <tr>
                <td><span><b>PARA EXHUMAR LOS RESTOS DE:</b></span></td>
				<td><?php echo strtoupper($nombre_difunto) ?></td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td><span><b>DEPOSITADOS EN EL CEMENTERIO:</b></span> </td>
				<td><?php echo strtoupper($cementerio) ?></td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td><span><b>NÚMERO Y FECHA DE LA <br> LICENCIA DE INHUMACIÓN:</b> &nbsp;&nbsp;</span></td>
				<td>
                <?php
                    echo (' '. $num_licencia_inhumacion . ' del ' . $fecha_inhumacion.'');
				?>
				</td>
            </tr>
			<tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td><span><b>LUGAR Y EXPEDICIÓN DE LA<br> LICENCIA DE INHUMACIÓN:</b></span></td>
				<td>
                 BOGOTÁ D.C.<br>
				</td>
            </tr>
			<tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
				<tr><td colspan="2"><br></td></tr>
			<tr>
                <td><span><b>OBSERVACIONES IMPORTANTES:</b></span></td><td><?php echo strtoupper($observaciones) ?></td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
        </table>
        <br>
        <table border="1" width="100%" cellpadding='0' cellspacing="0">
            <tr>
                <td>
                    <b>FIRMA DEL FUNCIONARIO AUTORIZADO:</b>
                    
				</td>
				<td>
					<?php
                    if ($firma == true) {
                        ?>
                        <img src="<?php echo FCPATH . 'assets/imgs/firma2.png' ?>" width="100px">
						<br>Yolima Agudelo Sedano<br>
						Subdirector(a) Inspección Vigilancia y Control

                        <?php
                    }
                    ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				        C&oacute;digo de verificaci&oacute;n: <?php echo $info_aprobacionlicencia->numero_verificacion; ?>
				</td>
            </tr>
            <tr>
				<td colspan="2">
                <b>Elaboró: (nombre y cargo):</b> <?php echo $usuariosReviso->p_nombre . " " . $usuariosReviso->s_nombre . " " . $usuariosReviso->p_apellido . " " . $usuariosReviso->s_apellido . "- Validador(a) del Trámite" ?> 
                
                </td>
            </tr>
        </table>
        <table border="0"><tr><td></td></tr></table>
        <table border="1" width="100%" cellpadding='0' cellspacing="0">
            <tr>
                <td>
                  <b>Revisó y aprobó: (nombre y cargo):</b>  <?php echo $usuarios->p_nombre . " " . $usuarios->s_nombre . " " . $usuarios->p_apellido . " " . $usuarios->s_apellido . "" ?> 
                </td>
            
        </table>
    

    </body>

</body>
</html>