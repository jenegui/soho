<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}


?>

		<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Trámites pendientes de validar.</b></h2>
					<h3>Licencia de practica medica categoría I y II</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12">
					<table class="table" id="tabla_consulta_rx" style="font-size:small;">
                        <thead>
                            <tr>
                                <th>ID Trámite</th>
                                <th>Identificaci&oacute;n</th>
                                <th>Nombre</th>
                                <th>Categoría</th>
                                <th>Fecha radicaci&oacute;n</th>                                
                                <th>Estado</th>
								<th>Fecha estado</th>                                
								<th>Seguimiento</th> 
								<th>Ver Trámite</th>	
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($rayosx_pendientes)>0){
                                for($i=0;$i<count($rayosx_pendientes);$i++){                                    
                                ?>
                            <tr>
                                <td>
                                    <?php 
										echo $rayosx_pendientes[$i]->id;
									/*if(!empty($rayosx_pendientes[$i]->fecha_editado)){ 
										echo '<i class="fas fa-exclamation-circle" alt="Trámite Editado"></i><font color="black" font-family="sans-serif;">Ajustado</font> ';
									} */										
									?>
                                </td>
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->descTipoIden." - ".$rayosx_pendientes[$i]->nume_identificacion?>
                                </td>
                                <td>
                                    <?php 
										if($rayosx_pendientes[$i]->tipo_identificacion == 5){
											echo $rayosx_pendientes[$i]->nombre_rs;
										}else{
											echo $rayosx_pendientes[$i]->p_nombre." ".$rayosx_pendientes[$i]->s_nombre." ".$rayosx_pendientes[$i]->p_apellido." ".$rayosx_pendientes[$i]->s_apellido;
										}
									?>
                                </td>                                
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->categoria; ?>
                                </td>
								<td>
                                    <?php echo $rayosx_pendientes[$i]->fecha_envio?>
                                </td>                               
                                <td>
                                    <?php echo $rayosx_pendientes[$i]->descEstado?>
                                </td>
								<td>
                                    <?php echo $rayosx_pendientes[$i]->fecha_estado?>									
                                </td>                                
								<td>
									<center>
									  <a data-toggle="modal" data-target="#modalSeguimiento<?php echo $i?>">
										<img src="<?php echo base_url('assets/imgs/seguimiento.png')?>" width="40px">
									  </a>
									</center>
									<!-- Modal -->
									<div class="modal fade" id="modalSeguimiento<?php echo $i?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Seguimiento</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">&times;</span>
											</button>
										  </div>
										  <div class="modal-body">
												<legend>Tabla de Seguimiento</legend>
												<table width="100%" border="1" class="table table-striped">
													<thead>
														<tr class="table-primary">
															<th width="12%"><b>Fecha Seguimiento</b></th>
															<th width="25%"><b>Usuario</b></th>
															<th width="15%"><b>Estado</b></th>
															<th width="48%"><b>Observación</b></th>
														</tr>
													</thead>
													<tbody>
													<?php
													$tramites_seguimientos = $this->rx_model->seguimiento_tramite($rayosx_pendientes[$i]->id);
													if(count($tramites_seguimientos)>0){
														for($j=0;$j<count($tramites_seguimientos);$j++){
														?>
														<tr>
															<td style="height:55px;">
																<?php echo date("Y-m-d",strtotime($tramites_seguimientos[$j]->fecha_estado));?>
															</td>
															<td style="height:55px;">
																<?php echo $tramites_seguimientos[$j]->username;?> 
															</td>
															<td style="height:55px;">
																<?php echo $tramites_seguimientos[$j]->descEstado?>
															</td>
															<td style="height:55px;">
																<?php echo $tramites_seguimientos[$j]->observaciones;?>
															</td>
														</tr>
														<?php									
														}
													}
													?>
													</tbody>
												</table>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										  </div>
										</div>
									  </div>
									</div>		
								</td>
								<td>
									<center>
									  <a href="<?php echo base_url('validacion/info_tramite_rx/'.$rayosx_pendientes[$i]->id)?>">
										<img src="<?php echo base_url('assets/imgs/file.png')?>" width="40px">
										</a>
									</center>
								</td>
                            </tr>
							
												
                            <?php
				}
        }
		

        ?>
                        </tbody>
                    </table>

            </div>
		</div>
		
