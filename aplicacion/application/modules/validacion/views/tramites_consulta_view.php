<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>
        <div class="row">
            <h1>Consulta de tr&aacute;mites realizados</h1>
            <table class="table" id="tabla_consulta">
                <thead>
                    <tr>
                        <th>ID Tramite</th>
                        <th>Identificacion</th>
                        <th>Nombre</th>
                        <th class="busca">Tipo de titulo</th>
                        <th>Fecha radicaci&oacute;n</th>
                        <th class="busca">Estado</th>
                        <th>Fecha resoluci&oacute;n/observaci&oacute;n</th>
                        <th>Observaciones/PDF</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(count($tramites_aprobados)>0){
                        for($i=0;$i<count($tramites_aprobados);$i++){

                        ?>
                    <tr>
                        <td>
                            <?php echo $tramites_aprobados[$i]->id_titulo;?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->descTipoIden." - ".$tramites_aprobados[$i]->nume_identificacion?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->p_nombre." ".$tramites_aprobados[$i]->s_nombre." ".$tramites_aprobados[$i]->p_apellido." ".$tramites_aprobados[$i]->s_apellido?>
                        </td>                        
                        <td>
                            <?php
                                if($tramites_aprobados[$i]->tipo_titulo == '1'){
                                    echo "Nacional";
                                }else{
                                    echo "Extranjero";
                                }
                                ?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->fecha_tramite?>
                        </td>
                        <td>
                            <?php echo $tramites_aprobados[$i]->descEstado?>
                        </td>
                        <td>
                           <?php
                            if($tramites_aprobados[$i]->estado == 14){
                                
                                echo $tramites_aprobados[$i]->fech_reso;
                                
                            }else if($tramites_aprobados[$i]->estado == 13){
                                
                                $busca_validacion = $this->validacion_model->buscar_validacion($tramites_aprobados[$i]->id_titulo, 13);
                                
                                if($busca_validacion){
                                    echo $busca_validacion->fecha_registro;
                                }
                                
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            
                            if($tramites_aprobados[$i]->estado == 14){
                                
                                $busca_resolucion = $this->validacion_model->buscar_resolucion($tramites_aprobados[$i]->id_titulo);
                                
                                if($busca_resolucion){
                                    ?>
                                    
                                    <center>
                                      <a href="<?php echo base_url($busca_resolucion->ruta.$busca_resolucion->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="20px">
                                        </a>
                                    </center>
                                    
                                    <?php
                                }
                                
                            }else if($tramites_aprobados[$i]->estado == 13){
                                
                                $busca_validacion = $this->validacion_model->buscar_validacion($tramites_aprobados[$i]->id_titulo, 13);
                                
                                if($busca_validacion){
                                    echo $busca_validacion->observaciones;
                                }
                                
                            }
                            
                            ?>
                        </td>
                    </tr>
                    <?php
}
}


?>
                </tbody>
                <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
            </table>
        </div>
