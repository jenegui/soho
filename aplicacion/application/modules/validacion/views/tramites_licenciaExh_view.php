<?php
$retornoError = $this->session->flashdata('error');
if ($retornoError) {
    ?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
    ?>
    <div class="alert alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <?php echo $retornoExito ?>
    </div>
    <?php
}
?>   
<form action="<?php echo base_url('validacion/exhumacion/') ?>" method="post" class="form-horizontal">
	<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-">
                <div class="subtitle">
                    <h2><b>Filtro Consulta y Descarga Excel Tr&aacute;mites Solicitados</b></h2>
					<h3>Licencia Exhumación de Cadáveres</h3>
                </div>
            </div>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Inicial:</b></label>
                    <input id="fecha_i" name="fecha_i" class="form-control" placeholder="Fecha Seguimiento Inicio" style="width:100%;">
                </div>
            </div>
			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Final:</b></label>
                    <input id="fecha_f" name="fecha_f" class="form-control" placeholder="Fecha Seguimiento Fin"  style="width:100%;">
                </div>
            </div>

			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input type="submit" class="btn btn-info" value="Consultar" style="width:100%;">
                </div>
            </div>

			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
					<input  type="button" class="btn btn-success" id="Excel" value="Descargar Excel" style="width:100%;">
                </div>
            </div>			
	
</form>
		<div class="col-12 col-md-12">
		<br>
			<div class="alert alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			<b>Apreciado Usuario!</b>
			<p>La consulta y el reporte de tr&aacute;mites que se han solicitado con su ultimo estado de gesti&oacute;n, es por rango de fecha de acuerdo a la fecha del registro generada por el sistema.<br>
			Los Tr&aacute;mites visualizados corresponden a los últimos 30 d&iacute;as desde la fecha actual.
			</p>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
		<br>
			<table class="table" id="tabla_tramites"  style="font-size:small;">
					<thead>
						<tr>
							<th>ID Solicitud</th>
							<th>Número licencia Inh</th>
							<th>Fecha Inhumación</th>
							<th>Identificación Solicitante</th>
							<th>Nombre solicitante</th>
							<th>Fecha radicaci&oacute;n</th>
							<th>Estados</th>
							<th>Observaci&oacute;n</th>
							<th>Fecha Observaci&oacute;n</th>
							<th>PDF</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if (count($listado_soli) > 0) {
							for ($i = 0; $i < count($listado_soli); $i++) {
								if ($listado_soli[$i]->id_archivo)
									$archivo = $this->mlicencia_exhumacion->consultar_archivo($listado_soli[$i]->id_archivo);
								?>
								<tr>
									<td>
										<?php echo $listado_soli[$i]->idap; ?>
									</td>
									<td>
										<?php echo $listado_soli[$i]->numero_licencia; ?>
									</td>
									<td>
										<?php echo $listado_soli[$i]->fechaI ?> </td>
									<td>
										<?php echo $listado_soli[$i]->nume_identificacion ?>
									</td>
									<td>
										<?php echo $listado_soli[$i]->p_nombre . " " . $listado_soli[$i]->s_nombre . " " . $listado_soli[$i]->p_apellido . " " . $listado_soli[$i]->s_apellido ?>

									</td>

									<td>
										<?php echo $listado_soli[$i]->fecha_solicitud ?>
									</td>
									<td>
										<?php if (isset($listado_soli[$i]->des_estado))
												echo $listado_soli[$i]->des_estado;
											else
												echo $listado_soli[$i]->estadoNotif;
										?>
									</td>
									<td>
										<?php echo $listado_soli[$i]->obs ?>
									</td>
									<td>
										<?php echo $listado_soli[$i]->fecha_registro ?>
									</td>
									<td>
										<?php if ($listado_soli[$i]->estado_apro == 4) { ?>
								<center>
									<a href="<?php echo base_url($archivo->ruta . $archivo->nombre) ?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png') ?>" width="20px">
									</a>
								</center>
							<?php } ?>
						</td>
						</tr>
						<?php
					}
				}
				?>
					</tbody>
			</table>
		</div>
	</div>
       
 <script type="text/javascript">
    $("#Excel").click(function (){
    var fecha_i= $("#fecha_i").val();
    var fecha_f= $("#fecha_f").val();
   window.location.href =base_url+'validacion/generar_excel?fecha_i='+fecha_i+'&fecha_f='+fecha_f;  
   //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"  
   
  });
  
  </script>

