﻿<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}


?>

		<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Trámites pendientes de validar.</b></h2>
					<h3>Licencia de Exhumación de Cadaveres</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12">
					<table class="table" id="tabla_tramitesExh" style="font-size:small;">
						<thead>
							<tr>
								<th>ID Trámite</th>
								<th>Identificaci&oacute;n</th>
								<th>Parentesco</th>
								<th>Fecha Licencia Inhumación</th>
								<th>Licencia Inhumaci&oacute;n</th>
								<th>Fecha radicaci&oacute;n</th>
								<th>Estado</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if (count($listadosolicitudesExh_vali) > 0) {
							for ($i = 0; $i < count($listadosolicitudesExh_vali); $i++) {
						?>
							<tr>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->idlicencia_exhumacion; ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->nume_identificacion ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->parentesco ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->fecha_inhumacion ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->numero_licencia ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->fecha_solicitud ?>
								</td>
								<td>
								<?php echo $listadosolicitudesExh_vali[$i]->des_estado ?>
								</td>
								<td>
								<center>
									<a href="<?php echo base_url('validacion/validacion_exh/' . $listadosolicitudesExh_vali[$i]->idlicencia_exhumacion) ?>">
										<img src="<?php echo base_url('assets/imgs/aprobar.png') ?>" width="20px">
										<br>Validar Solicitud
									</a>
								</center>
								</td>
							</tr>
			<?php
							}
						}
			?>
			
						</tbody>
					</table>

            </div>
		</div>
		
