<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Licencia de Exhumación</title>
    </head>
    <body>
        <table width="100%" cellpadding='0' cellspacing="0" border="1">
            <tr>
                <td>
                       <!--<img src="<?php echo FCPATH . 'assets/imgs/logo_pdf_alcaldia.png' ?>" width="200px">-->
                    <img src="<?php echo FCPATH . 'assets/imgs/logo_pdf_alcaldia.png' ?>" width="70px">
                </td>
				<td style="text-align: center; font-size: 10px">DIRECCION DE CALIDAD DE SERVICIOS DE SALUD 
                    <br>SUBDIRECCION INSPECCION VIGILANCIA Y CONTROL DE <br>SERVICIOS DE SALUD<br> 
                    SISTEMA INTEGRADO DE GESTIÓN<br> CONTROL DOCUMENTAL<br> FORMATO LICENCIA DE EXHUMACIÓN
                </td>
                <td style="text-align: left; font-size: 10px">    Elaborado por: Gestor de calidad<br>
                    Revisado por: Dalbeth Elena Henriquez Iguarán<br>
                    Aprobado por: Isabel Cristina Artunduaga Pastrana
                    <br>
                    Control documental: Dirección Planeación -SIG
                </td>
                <td width="15%"> 
                    <img src="<?php echo FCPATH . 'assets/imgs/logoexhumacion.png' ?>" width="70px">
                </td>
            </tr>   
        </table>
        <table width="100%" cellpadding='0' cellspacing="0" border="0">   
            <tr>
                <td width="40%" style="font-size: 16px;text-align: center">
                    LICENCIA DE EXHUMACIÓN
                </td>
                <td style="text-align: right;">No. <?php
                      $fechaN=date('d/m/Y')
                     ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <br><br>Fecha <?php echo '  ' . $fechaN ?><br>
                </td>
            </tr>
            <tr>
                <td style="background-color:black" colspan="2"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>">
            </td>
            <tr><td colspan="2"><br></td></tr>
            </tr>
            <tr>
                <td><span>SE CONCEDE LICENCIA AL SEÑOR (A):</span> </td> 
                <td><?php echo strtoupper($listadosolicitudesExh_vali->p_nombre . " " . $listadosolicitudesExh_vali->s_nombre . " " . $listadosolicitudesExh_vali->p_apellido . " " . $listadosolicitudesExh_vali->s_apellido) . " " ?></td>
            </tr>
            <tr>
                <td> 
                </td>
                <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr >
                <td><span>DE PARENTESCO:</span></td><td><?php echo strtoupper($listadosolicitudesExh_vali->parentesco) ?></td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr> 
                <td><span>PARA EXHUMAR LOS RESTOS DE:</span></td><td> <?php echo strtoupper($difunto)?> </td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td><span>DEPOSITADOS EN EL CEMENTERIO:</span> </td><td><?php echo strtoupper($cementerio);?></td>
            </tr>
            <tr>
                <td> 
                </td>
              <td style="background-color:black"><img src="<?php echo FCPATH . 'assets/imgs/lineapdf.png' ?>"></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td colspan="2"><span>NUMERO Y FECHA DE LA LICENCIA DE INHUMACIÓN: &nbsp;&nbsp;</span>
                <?php
                    $fechaINH = new DateTime($fecha);
                    $fechaIN = $fechaINH->format('d/m/Y');
                    echo $licenciai . ' del ' . $fechaIN.'<br>';
              ?></td>
            </tr>
             <tr><td colspan="2"><br></td></tr>
            <tr>
                <td colspan="2">LUGAR Y EXPEDICION DE LA LICENCIA:  BOGOTÁ D.C.<br></td>
            </tr>
        </table>
        <br>
        <table border="1" width="100%" cellpadding='0' cellspacing="0">
            <tr>
                <td>
                    FIRMA DEL FUNCIONARIO AUTORIZADO:
                    <?php
                    if ($firma == true) {
                        ?>
						<br>
                        <img src="<?php echo FCPATH . 'assets/imgs/firma2.png' ?>" width="100px">
                        <br>
                        C&oacute;digo de verificaci&oacute;n: 
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <tr><td>
                Elaboró: (nombre y cargo): <?php echo $usuariosReviso->p_nombre . " " . $usuariosReviso->s_nombre . " " . $usuariosReviso->p_apellido . " " . $usuariosReviso->s_apellido . " Validador(a) del Trámite" ?> 
                
                </td>
            </tr>
        </table>
        <table border="0"><tr><td></td></tr></table>
        <table border="1" width="100%" cellpadding='0' cellspacing="0">
            <tr>
                <td>
                    Revisó y aprobó: (nombre y cargo): 
            
        </table>
    

    </body>

</body>
</html>