<html>
<head>
  <title>Negación - Tramites en Linea SDS</title>
  <meta charset="utf-8">
  <style type="text/css">

    body {
		background-color: #fff;
		font-family: Lucida Grande, Verdana, Sans-serif;
		font-size: 12px;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		font-weight: bold;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}
	
	h4 {
		color: #444;
		font-size: 12px;
		font-weight: bold;
	}

	.encabezados {
		color: #000000;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	h2 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 14px;
		font-weight: bold;
		margin: 5px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	code {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	.centro {
		text-align: center;
	}
	
	.justificado {
		text-align: justify;
	}

	.derecha {
		text-align: right;
	}

	.total {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #000000;
		border-bottom: 1px solid #000000;
		border-top: 1px solid #000000;
		color: #002166;
	}

	.marca-de-agua {
		padding: 0;
		width: 100%;
		height: auto;
		opacity: 0.7;
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 25px;
	}

	.contenido{
		margin-top: 60px;
		margin-bottom: 55px;
	} 

	.cursiva{
		font-style: italic;
	}	
		

  </style>
</head>
<body>
	<p class="centro">
		<br><b><em>Dirección de Calidad de Servicios de Salud<br>
		  Subdirección Inspección, Vigilancia y Control de Servicios de Salud</em>
			</b>
	</p>
	<p class="centro">
		<b><em>Resolución No. <?php echo $nume_resolucion?> de <?php echo date('d')." del mes de ".$mes." del a&ntilde;o ".date('Y');?><br>
		  "Por la cual se niegan Las Licencias de Práctica Médica Categoría <?php echo $datos_tramite->categoria?> para Equipo de radiaciones Ionizantes"
		  </em></b>
	</p>
	<p class="justificado">En uso de sus facultades legales conferidas en el Decreto 507 del 06 de noviembre de 2013 de la Alcaldía Mayor de Bogotá y 
	Resolución 482 de 2018,</p>
	<p class="centro">CONSIDERANDO: </p>
	<?php
	
		if($rayosxEquipo[0]->categoria1 != 0 || $rayosxEquipo[0]->categoria1 != NULL){
		if($rayosxEquipo[0]->categoria1 == 1){
			$categoria1 = "Radiolog&iacute;a odontol&oacute;gica periapical";
			
		}else if($rayosxEquipo[0]->categoria1 == 2){
			$categoria1 = "Equipo de RX";					
		}				
	}

	if($rayosxEquipo[0]->categoria2 != 0 || $rayosxEquipo[0]->categoria2 != NULL){
		if($rayosxEquipo[0]->categoria2 == 1){
			$categoria2 = "Radioterapia";					
		}else if($rayosxEquipo[0]->categoria2 == 2){
			$categoria2 = "Radio diagn&oacute;stico de alta complejida";
		}else if($rayosxEquipo[0]->categoria2 == 3){
			$categoria2 = "Radio diagn&oacute;stico de media complejidad";					
		}else if($rayosxEquipo[0]->categoria2 == 4){
			$categoria2 = "Radio diagn&oacute;stico de baja complejidad";										
		}else if($rayosxEquipo[0]->categoria2 == 5){
			$categoria2 = "Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales";										
		}
	}

	if($rayosxEquipo[0]->categoria1_1 != 0 || $rayosxEquipo[0]->categoria1_1 != NULL){
		if($rayosxEquipo[0]->categoria1_1 == 1){
			$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical";
			$equipo_doc = $categoria1_1;
		}else if($rayosxEquipo[0]->categoria1_1 == 2){
			$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical portat&iacute;l";
			$equipo_doc = $categoria1_1;
		}				
	}

	if($rayosxEquipo[0]->categoria1_2 != 0 || $rayosxEquipo[0]->categoria1_2 != NULL){
		if($rayosxEquipo[0]->categoria1_2 == 1){
			$categoria1_2 = "Densit&oacute;metro &oacute;seo";
			$equipo_doc = $categoria1_2;
		}
	}
	
	if($rayosxEquipo[0]->categoria2_1 != 0 || $rayosxEquipo[0]->categoria2_1 != NULL){
		if($rayosxEquipo[0]->categoria2_1 == 1){
			$categoria2_1 = "Equipo de RX convencional";					
		}else if($rayosxEquipo[0]->categoria2_1 == 2){
			$categoria2_1 = "Tomógrafo Odontológico";
		}else if($rayosxEquipo[0]->categoria2_1 == 3){
			$categoria2_1 = "Tomógrafo";
		}else if($rayosxEquipo[0]->categoria2_1 == 4){
			$categoria2_1 = "Equipo de RX Portátil";
		}else if($rayosxEquipo[0]->categoria2_1 == 5){
			$categoria2_1 = "Equipo de RX Odontológico";
		}else if($rayosxEquipo[0]->categoria2_1 == 6){
			$categoria2_1 = "Panorámico Cefálico";
		}else if($rayosxEquipo[0]->categoria2_1 == 7){
			$categoria2_1 = "Fluoroscopio";
		}else if($rayosxEquipo[0]->categoria2_1 == 8){
			$categoria2_1 = "SPECT-CT";
		}else if($rayosxEquipo[0]->categoria2_1 == 9){
			$categoria2_1 = "Arco en C";
		}else if($rayosxEquipo[0]->categoria2_1 == 10){
			$categoria2_1 = "Mamógrafo";
		}else if($rayosxEquipo[0]->categoria2_1 == 11){
			$categoria2_1 = "Litotriptor";
		}else if($rayosxEquipo[0]->categoria2_1 == 12){
			$categoria2_1 = "Angiógrafo";
		}else if($rayosxEquipo[0]->categoria2_1 == 13){
			$categoria2_1 = "PET-CT";					
		}else if($rayosxEquipo[0]->categoria2_1 == 14){
			$categoria2_1 = "Acelerador lineal";					
		}else if($rayosxEquipo[0]->categoria2_1 == 15){
			$categoria2_1 = "Sistema de radiocirugia robótica";					
		}else if($rayosxEquipo[0]->categoria2_1 == 16){
			$categoria2_1 = $rayosxEquipo[0]->otro_equipo;					
		}else{
			$categoria2_1 = "";					
		}
		
		if($categoria2_1 != ""){
			$equipo_doc = $categoria2_1;	
		}		
	}
	
	$exd = date_create($datos['datos_tramite']->fecha_envio);
	$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
	
		if($datos_tramite->tipo_identificacion == 5){
			$nombre_rs = $datos_tramite->nombre_rs;
			$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			switch ($datos_tramite->tipo_iden_rl) {
				case 1:
					$tipo_iden_rl = "Cédula de ciudadanía";
					break;
				case 2:
					$tipo_iden_rl = "Cédula de extranjería";
					break;
				case 3:
					$tipo_iden_rl = "Tarjeta de identidad";
					break;
				case 4:
					$tipo_iden_rl = "Permiso especial de permanencia";
					break;
				case 5:
					$tipo_iden_rl = "NIT";
					break;
			}				
			$nume_iden_rl = $datos_tramite->nume_iden_rl;
			$tipo_iden_rs = $datos_tramite->descTipoIden;
			$nume_iden_rs = $datos_tramite->nume_identificacion;
		}else{
			$nombre_rs = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			
			$tipo_iden_rl = $datos_tramite->descTipoIden;
			$nume_iden_rl = $datos_tramite->nume_identificacion;
			$tipo_iden_rs = $datos_tramite->descTipoIden;
			$nume_iden_rs = $datos_tramite->nume_identificacion;
		}
			
	
	?>
	<p class="justificado">
		Que la institución <?php echo $nombre_rs?>, 
		con <?php echo $tipo_iden_rs?> <?php echo $nume_iden_rs?>, solicitó mediante radicado <?php echo $datos_tramite->id?> 
		de <?php echo $fecha_radicado?>, Solicitud de la Licencia de Práctica Médica, para el equipo de rayos X <?php echo $equipo_doc?> 
		marca <?php echo $rayosxEquipo[0]->marca_equipo?> modelo <?php echo $rayosxEquipo[0]->modelo_equipo?>, quien anexa los documentos que soportan su solicitud. 
		Folio 1-51)
	</p>
	<p class="justificado">
		Que de conformidad con la Resolución 482 del 22 de febrero de 2018, expedida por el Ministerio de Salud y Protección Social, por la cual se reglamenta 
		el uso de equipos generadores de radiación ionizante, su control de calidad, la prestación de servicios de protección radiológica y se dictan otras 
		disposiciones, establece:
	</p>
	<p class="justificado">
		Artículo 26. Trámite de las licencias de prácticas médicas categoría I o II. El estudio de la documentación para el otorgamiento de la licencia de prácticas médicas categoría I o II, estará sujeto al siguiente procedimiento:
	</p>
	<p class="justificado">
		26.1. Radicada la solicitud en el formato dispuesto en el Anexo No. 3, con los documentos requeridos en los artículos 21 o 23, según la categoría, la entidad 
		territorial de salud departamental o distrital, según corresponda, procederá a revisarla dentro de los veinte (20) días hábiles siguientes y, 
		de encontrar la documentación incompleta, requerirá al solicitante para que la suministre dentro de los veinte (20) días hábiles siguientes.
	</p>
	<p class="justificado">
		26.2. Si no se completa la solicitud, se entenderá que se desiste de esta, salvo que antes de vencer el plazo concedido, el peticionario solicite 
		prórroga, la cual se concederá hasta por un término igual.
	</p>
	<p class="justificado">
		26.3. Completa la solicitud, se surtirá el siguiente procedimiento:
	</p>
	<p class="justificado">
		26.3.1. Para las licencias de práctica médica categoría I, se procederá a estudiar la documentación y dentro de los cuarenta y cinco (45) días 
		hábiles siguientes, a emitir el acto administrativo.
	</p>
	<p class="justificado">
		26.3.2. Para las licencias de práctica médica categoría II se programará visita con enfoque de riesgo, encaminada a la verificación de los 
		requisitos a que refiere el artículo 24, la cual se realizará en un término no superior a sesenta (60) días hábiles, contado a partir de la 
		radicación de la solicitud o de la complementación de esta, según sea el caso.
	</p>
	<p class="justificado">
		26.3.3. Si como consecuencia de la mencionada visita se determina la necesidad de complementar la información o la documentación de que trata 
		el artículo 24 de la presente resolución, en el acta que se suscriba, producto de la referida visita, se dejará constancia de ello y el 
		solicitante dispondrá de veinte (20) días hábiles para allegar dicha información.
	</p>
	<p class="justificado">
		26.3.4. Rebasado el término del requerimiento o el de la visita, la entidad territorial de salud de carácter departamental o distrital, 
		según corresponda, dentro de los cuarenta y cinco (45) días hábiles siguientes, entrará a resolver de fondo la solicitud con la documentación e 
		información de que disponga, decisión que será notificada de acuerdo con lo establecido por el Código de Procedimiento Administrativo y de lo 
		Contencioso Administrativo y susceptible de los recursos en este contemplados.
	</p>
	<p class="justificado">
		Que la Dirección de Calidad de Servicios de Salud, mediante radicado <?php echo $datos_tramite->id ?> de <?php echo $fecha_radicado ?>, 
		otorgó plazo de 20 días hábiles para allegar los documentos faltantes para continuar con el trámite de la Licencia de Práctica Médica Categoría <?php echo $datos_tramite->categoria ?>.
		(Folio 53-54)
	</p>
	<p class="justificado">
		Que mediante radicado  <?php echo $datos_tramite->id ?> de <?php echo $fecha_radicado ?> el prestador <?php echo $nombre_rs?>, 
		envío la documentación solicitada por la Dirección de Calidad de Servicios de Salud. (Folio 55-57 y 1CD) Una vez revisada, se encontró lo siguiente:
	</p>
	<p class="justificado">
		23.7 Plano general de las instalaciones de acuerdo con lo establecido en la Resolución 4445 de 1996, expedida por el entonces Ministerio de Salud 
		o la norma de la modifique o sustituya, el cual debe contener:
	</p>
	<?php
	for($i=0;$i<count($rayosxEquipo);$i++){
			?>
			<p class="justificado">
				Marca del equipo: <?php echo $rayosxEquipo[$i]->marca_equipo?> Modelo: <?php echo $rayosxEquipo[$i]->modelo_equipo?><br>
				Mili amperaje:	<?php echo $rayosxEquipo[$i]->contiene_tubo_rx?> m.A

			</p>
			<p class="justificado">
				<h4>Resolución 482 de 2018</h4><br>					
				23.7 Plano general de las instalaciones de acuerdo con lo establecido en la Resolución 4445 de 1996, expedida por el entonces Ministerio 
				de Salud o la norma de la modifique o sustituya, el cual debe contener:					
			</p>
			<?php
			
			$items482 = $this->rx_model->itemsInfraestructura('482');
			
			?>
			<table style="border-collapse: collapse;">
			<?php
			for($it=0;$it<count($items482);$it++){
				
				
				$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
				$itemCons['id_tramite'] = $datos_tramite->id; 
				$itemCons['id_item'] = $items482[$it]->id_item; 
				if($datos_tramite->estado == 4){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 7){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 16){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 19){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 24){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 21){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 25){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 26){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 27){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 28){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 29){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 30){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 31){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 32){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 33){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 34){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 35){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 36){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 37){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 38){
					$itemCons['id_estado'] = 24; 	
				}
				
				$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
				
				if(count($resultadoEquipoInfra)>0){
					$obs = $resultadoEquipoInfra->observaciones;
				}else{
					$obs = "";
				}	
				
				?>
				<tr>
					<td style="border:1px solid black;"><?php echo $items482[$it]->desc_item?></td>
					<td class="justificado" style="border:1px solid black;"><?php echo $obs?></td>
				</tr>
				<?php							
			}
			?>
			</table>
			<?php
			
			?>				
			<div class="row">
				<h4>Articulo 33 Resolución 4445 de 1996 </h4>
			</div>
			<?php
			
			$items4445 = $this->rx_model->itemsInfraestructura('4445');
			
			?>
			<table style="border-collapse: collapse;">
			<?php
			
			for($it=0;$it<count($items4445);$it++){
				
				$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
				$itemCons['id_tramite'] = $datos_tramite->id; 
				$itemCons['id_item'] = $items4445[$it]->id_item; 
				if($datos_tramite->estado == 4){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 7){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 16){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 19){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 24){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 21){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 25){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 26){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 27){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 28){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 29){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 30){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 31){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 32){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 33){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 34){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 35){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 36){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 37){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 38){
					$itemCons['id_estado'] = 24; 	
				}
				
				$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
				
				if(count($resultadoEquipoInfra)>0){
					$obs = $resultadoEquipoInfra->observaciones;
				}else{
					$obs = "";
				}	
				
				
				?>
				<tr>
					<td style="border:1px solid black;"><?php echo $items4445[$it]->desc_item?></td>
					<td class="justificado" style="border:1px solid black;"><?php echo $obs?></td>
				</tr>
				<?php							
			}
			?>
			</table>
			<?php
			
		}
	?>
	
	<p class="justificado">
		Que el Artículo 26. Trámite de las licencias de prácticas médicas categoría I o II. Establece el numeral 26.3. Completa la solicitud, se surtirá 
		el siguiente procedimiento: 26.3.1. Para las licencias de práctica médica categoría I, se procederá a estudiar la documentación y dentro de los 
		cuarenta y cinco (45) días hábiles siguientes, a emitir el acto administrativo.
	</p>
	<p class="justificado">
		(…) 26.3.4. Rebasado el término del requerimiento o el de la visita, la entidad territorial de salud de carácter departamental o distrital, 
		según corresponda, dentro de los cuarenta y cinco (45) días hábiles siguientes, entrará a resolver de fondo la solicitud con la documentación e 
		información de que disponga, decisión que será notificada de acuerdo con lo establecido por el Código de Procedimiento Administrativo y de lo 
		Contencioso Administrativo y susceptible de los recursos en este contemplados.
	</p>
	<p class="justificado">
		Que, en mérito a lo expuesto, esta Subdirección
	</p>
	<p class="centro">
		RESUELVE:
	</p>
	<p class="justificado">
		<b>ARTÍCULO PRIMERO: </b>Negar la expedición de la Licencia de Práctica Médica Categoría <?php echo $datos_tramite->categoria ?> a la Institución 
		<?php echo $nombre_rs?>, con 
		<?php echo $tipo_iden_rs?> <?php echo $nume_iden_rs?>, por las razones expuestas en la parte considerativa 
		de ésta Resolución.
	</p>
	<p>
		<b>ARTICULO SEGUNDO: </b>Notificar a <?php echo $nombre_rl?>, 
		identificado con <?php echo $tipo_iden_rl?> No <?php echo $nume_iden_rl?> 
		representante legal de la institución <?php echo $nombre_rs?>, 
		con <?php echo $tipo_iden_rs?> No <?php echo $nume_iden_rs?>, o quien haga sus veces, el contenido de la presente 
		Resolución, informándole que de conformidad con el artículo 74 del Código de Procedimiento Administrativo y de lo Contencioso Administrativo 
		(Ley 1437 de 2011) contra la misma proceden los recursos de reposición y en subsidio apelación, los cuales podrá interponer ante ésta Dirección, 
		dentro de los diez (10) días hábiles siguientes a la notificación de este acto administrativo.
	</p>
	<p class="centro"><b>NOTIFÍQUESE Y CÚMPLASE</b></p>
	<p class="centro">Dado en Bogotá, D.C. a los <?php echo date('d')." días del mes de ".$mes." del a&ntilde;o ".date('Y');?></p>
	<br><br><br>
	<?php
	
	if($firma == TRUE){
		?>
		<img src="<?php echo FCPATH.'assets/imgs/firma_docmarthaBK.JPG'?>" width="300px"><br>
		<?php
	}
	
	?>
	<p class="justificado"><b>YOLIMA AGUDELO SEDANO</b></p>
	<p class="justificado">Subdirectora de Inspección Vigilancia y Control de Servicios de Salud</p>
	<?php
	
	if(isset($codigo_verificacion) && $codigo_verificacion != ''){
		?>
		<p class="justificado">Código de verificación: <?php echo $codigo_verificacion?></p>
		<?php
	}
	
	?>

</body>
</html>