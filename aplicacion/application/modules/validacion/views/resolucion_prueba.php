<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Resoluci&oacute;n Aprobaci&oacute;n</title>

    <style type="text/css">
        body {
            background-color: #fff;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #4F5155;
        }

        table {
            border: 1px solid #fff;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        .encabezados {
            color: #000000;
            border-bottom: 1px solid #D0D0D0;
            font-size: 16px;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        h2 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 14px;
            font-weight: bold;
            margin: 5px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        code {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        .centro {
            text-align: center;
        }
        
        .justificado {
            text-align: justify;
        }

        .derecha {
            text-align: right;
        }

        .total {
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #000000;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
            color: #002166;
        }

        .marca-de-agua {
            padding: 0;
            width: 100%;
            height: auto;
            opacity: 0.7;
            font-family: Monaco, Verdana, Sans-serif;
            font-size: 25px;
        }

		.contenido{
			margin-top: 25px;
		}
    </style>
</head>

<body> 
	<div class="contenido">
		<p>
			22100<br>
			Bogotá D.C.<br><br>
			Señor(a)<br>
			<?php echo $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido?><br>
			<?php echo $datos_tramite->email?><br>
			<?php echo $datos_tramite->dire_resi?><br>
			Bogotá D.C
		</p>
		<p class="justificado">
			Asunto: Respuesta Radicado No <?php echo $datos_tramite->id?> del <?php echo $datos_tramite->fecha_envio?>
		</p>		
		<p class="justificado">
			Cordial Saludo:	
		</p>
		<p class="justificado">
			En respuesta a su solicitud y en virtud de la Resolución 482 de 2018, la cual establece Articulo 26: “Tramite de las licencias de prácticas médicas categoría I o II. El estudio de la documentación para el otorgamiento de la licencia de prácticas médicas categoría I o II estará sujeto al siguiente procedimiento: 
		</p>
		<p class="justificado">
			26.3.2. Para las licencias de practica medica categoría II se programara visita con  enfoque de riesgo, encaminada a la verificación de los requisitos a que refiere el artículo 24, la cual se realizara en un término no superior a sesenta (60) días hábiles, contado a partir de la radicación de la solicitud o de la complementación de esta, según sea el caso”. (Subrayado fuera de texto).
		</p>
		<p class="justificado">
			Por lo anterior me permito informarle que la visita con enfoque de riesgo será programada de acuerdo a los términos legales establecidos. 
		</p>		
		<p class="justificado">
			Cordialmente,
		</p>
		<p class="justificado"><br><br><br>
			<!--<img src="<?php echo FCPATH.'assets/imgs/firma_docrosmira.JPG'?>" width="300px"><br>-->
			SANDRA MILENA GUTIERREZ REY<br>
			Subdirectora de Inspección Vigilancia y Control de Servicios de Salud (E)		
		</p>
	</div>
</body>

</html>
