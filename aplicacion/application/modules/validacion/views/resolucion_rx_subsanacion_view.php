<html>
<head>
<title>Generación documentos</title>
<style>
    body {
		background-color: #fff;
		font-family: Lucida Grande, Verdana, Sans-serif;
		font-size: 12px;
		color: #4F5155;
	}

	table {
		border: 1px solid #fff;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		font-weight: bold;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}
	
	h4 {
		color: #444;
		font-size: 12px;
		font-weight: bold;
	}

	.encabezados {
		color: #000000;
		border-bottom: 1px solid #D0D0D0;
		font-size: 16px;
		margin: 24px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	h2 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 14px;
		font-weight: bold;
		margin: 5px 0 2px 0;
		padding: 5px 0 6px 0;
	}

	code {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	.centro {
		text-align: center;
	}
	
	.justificado {
		text-align: justify;
	}

	.derecha {
		text-align: right;
	}

	.total {
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #000000;
		border-bottom: 1px solid #000000;
		border-top: 1px solid #000000;
		color: #002166;
	}

	.marca-de-agua {
		padding: 0;
		width: 100%;
		height: auto;
		opacity: 0.7;
		font-family: Monaco, Verdana, Sans-serif;
		font-size: 25px;
	}

	.contenido{
		margin-top: 60px;
		margin-bottom: 55px;
	} 

	.cursiva{
		font-style: italic;
	}	
</style>
</head>
<body>
		<?php
		$exd = date_create($datos_tramite->fecha_envio); 
		$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
		
		if($datos_tramite->tipo_identificacion == 5){
			$nombre_rs = $datos_tramite->nombre_rs;
			$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			switch ($datos_tramite->tipo_iden_rl) {
				case 1:
					$tipo_iden_rl = "Cédula de ciudadanía";
					break;
				case 2:
					$tipo_iden_rl = "Cédula de extranjería";
					break;
				case 3:
					$tipo_iden_rl = "Tarjeta de identidad";
					break;
				case 4:
					$tipo_iden_rl = "Permiso especial de permanencia";
					break;
				case 5:
					$tipo_iden_rl = "NIT";
					break;
			}				
			$nume_iden_rl = $datos_tramite->nume_iden_rl;
			$tipo_iden_rs = $datos_tramite->descTipoIden;
			$nume_iden_rs = $datos_tramite->nume_identificacion;
		}else{
			$nombre_rs = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			$nombre_rl = $datos_tramite->p_nombre." ".$datos_tramite->s_nombre." ".$datos_tramite->p_apellido." ".$datos_tramite->s_apellido;
			
			$tipo_iden_rl = $datos_tramite->descTipoIden;
			$nume_iden_rl = $datos_tramite->nume_identificacion;
			$tipo_iden_rs = $datos_tramite->descTipoIden;
			$nume_iden_rs = $datos_tramite->nume_identificacion;
		}
		
		?>
		<p class="justificado">
			22100<br>
			Bogotá D.C.<br><br>
			Señor(a)<br>
			<?php echo $nombre_rl?><br>
			<?php echo $datos_tramite->email?><br>
			<?php echo $datos_tramite->dire_resi?><br>
			Bogotá D.C
		</p>
		<p class="justificado">
			Asunto: Respuesta Radicado No <?php echo $datos_tramite->id?> del <?php echo $fecha_radicado?>
		</p>		
		<p class="justificado">
			Cordial Saludo:	
		</p>
		<?php
		if(count($rayosxEquipo)>1){
			?>
			<p class="justificado">
				Atentamente doy respuesta a la comunicación de la referencia en la que solicita la obtención de la licencia de práctica médica para los equipos 
				de Rayos X, de conformidad con lo dispuesto en la Resolución 482 de 2018. 
			</p>
			<?php		
		}else{
			
			if($rayosxEquipo[0]->categoria1 != 0 || $rayosxEquipo[0]->categoria1 != NULL){
				if($rayosxEquipo[0]->categoria1 == 1){
					$categoria1 = "Radiolog&iacute;a odontol&oacute;gica periapical";
					
				}else if($rayosxEquipo[0]->categoria1 == 2){
					$categoria1 = "Equipo de RX";					
				}				
			}
	   
			if($rayosxEquipo[0]->categoria2 != 0 || $rayosxEquipo[0]->categoria2 != NULL){
				if($rayosxEquipo[0]->categoria2 == 1){
					$categoria2 = "Radioterapia";					
				}else if($rayosxEquipo[0]->categoria2 == 2){
					$categoria2 = "Radio diagn&oacute;stico de alta complejida";
				}else if($rayosxEquipo[0]->categoria2 == 3){
					$categoria2 = "Radio diagn&oacute;stico de media complejidad";					
				}else if($rayosxEquipo[0]->categoria2 == 4){
					$categoria2 = "Radio diagn&oacute;stico de baja complejidad";										
				}else if($rayosxEquipo[0]->categoria2 == 5){
					$categoria2 = "Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales";										
				}
			}

			if($rayosxEquipo[0]->categoria1_1 != 0 || $rayosxEquipo[0]->categoria1_1 != NULL){
				if($rayosxEquipo[0]->categoria1_1 == 1){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical";
					$equipo_doc = $categoria1_1;
				}else if($rayosxEquipo[0]->categoria1_1 == 2){
					$categoria1_1 = "Equipo de RX odontol&oacute;gico periapical portat&iacute;l";
					$equipo_doc = $categoria1_1;
				}				
			}

			if($rayosxEquipo[0]->categoria1_2 != 0 || $rayosxEquipo[0]->categoria1_2 != NULL){
				if($rayosxEquipo[0]->categoria1_2 == 1){
					$categoria1_2 = "Densit&oacute;metro &oacute;seo";
					$equipo_doc = $categoria1_2;
				}
			}
			
			if($rayosxEquipo[0]->categoria2_1 != 0 || $rayosxEquipo[0]->categoria2_1 != NULL){
				if($rayosxEquipo[0]->categoria2_1 == 1){
					$categoria2_1 = "Equipo de RX convencional";					
				}else if($rayosxEquipo[0]->categoria2_1 == 2){
					$categoria2_1 = "Tomógrafo Odontológico";
				}else if($rayosxEquipo[0]->categoria2_1 == 3){
					$categoria2_1 = "Tomógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 4){
					$categoria2_1 = "Equipo de RX Portátil";
				}else if($rayosxEquipo[0]->categoria2_1 == 5){
					$categoria2_1 = "Equipo de RX Odontológico";
				}else if($rayosxEquipo[0]->categoria2_1 == 6){
					$categoria2_1 = "Panorámico Cefálico";
				}else if($rayosxEquipo[0]->categoria2_1 == 7){
					$categoria2_1 = "Fluoroscopio";
				}else if($rayosxEquipo[0]->categoria2_1 == 8){
					$categoria2_1 = "SPECT-CT";
				}else if($rayosxEquipo[0]->categoria2_1 == 9){
					$categoria2_1 = "Arco en C";
				}else if($rayosxEquipo[0]->categoria2_1 == 10){
					$categoria2_1 = "Mamógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 11){
					$categoria2_1 = "Litotriptor";
				}else if($rayosxEquipo[0]->categoria2_1 == 12){
					$categoria2_1 = "Angiógrafo";
				}else if($rayosxEquipo[0]->categoria2_1 == 13){
					$categoria2_1 = "PET-CT";					
				}else if($rayosxEquipo[0]->categoria2_1 == 14){
					$categoria2_1 = "Acelerador lineal";					
				}else if($rayosxEquipo[0]->categoria2_1 == 15){
					$categoria2_1 = "Sistema de radiocirugia robótica";					
				}else if($rayosxEquipo[0]->categoria2_1 == 16){
					$categoria2_1 = $rayosxEquipo[0]->otro_equipo;					
				}else{
					$categoria2_1 = "";					
				}
				
				if($categoria2_1 != ""){
					$equipo_doc = $categoria2_1;	
				}				
			}					
			
			?>
			<p class="justificado">
				Atentamente doy respuesta a la comunicación de la referencia en la que solicita la obtención de la licencia de práctica médica para el equipo 
				de Rayos X <?php echo $equipo_doc?> marca <?php echo $rayosxEquipo[0]->marca_equipo?> modelo <?php echo $rayosxEquipo[0]->modelo_equipo?>, de 
				conformidad con lo dispuesto en la Resolución 482 de 2018. 
			</p>
			<?php		
		}
		?>
		
		<p class="justificado">
			Al respecto le informo que una vez revisada la documentación aportada por usted, esta Secretaría de Salud emitió concepto con las observaciones 
			que a continuación se describen:  	
		</p>
		<?php
		$item = 1;
		if(isset($obs_val1) && $obs_val1 != ''){
				
			?>
			<h4><?php echo $item?>. Observaciones Localización</h4>
			<p class="justificado">				
				<?php echo trim($obs_val1)?>
			</p>
			<?php
			$item++;
		}
		
		if(isset($obs_val2) && $obs_val2 != ''){
			
			?>
			<h4><?php echo $item?>. Observaciones Equipos</h4>
			<p class="justificado">
				<?php echo trim($obs_val2)?>
			</p>
			<?php
			$item++;
		}
		
		for($i=0;$i<count($rayosxEquipo);$i++){
			?>
			<p class="justificado">
				Marca del equipo: <?php echo $rayosxEquipo[$i]->marca_equipo?> Modelo: <?php echo $rayosxEquipo[$i]->modelo_equipo?><br>
				Mili amperaje:	<?php echo $rayosxEquipo[$i]->contiene_tubo_rx?> m.A

			</p>
			<?php
			$items482 = $this->rx_model->itemsInfraestructura('482');
			
			$bandera482 = 0;
			$o482 = 0;
			for($it=0;$it<count($items482);$it++){
				
				$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
				$itemCons['id_tramite'] = $datos_tramite->id; 
				$itemCons['id_item'] = $items482[$it]->id_item; 
				
				if($datos_tramite->estado == 4){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 7){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 16){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 19){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 24){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 21){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 25){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 26){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 27){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 28){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 29){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 30){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 31){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 32){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 33){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 34){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 35){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 36){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 37){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 38){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 41){
					$itemCons['id_estado'] = 3; 	
				}
				
				$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
				
				if(count($resultadoEquipoInfra)>0){
					
					if(trim($resultadoEquipoInfra->observaciones) != ''){
						$obs482['item'.$o482] = $items482[$it]->desc_item;
						$obs482['obs'.$o482] = $resultadoEquipoInfra->observaciones;
						$bandera482 = 1;
						$o482++;	
					}
					
				}else{
				}	
											
			}
			
			if($bandera482 == 1){
			?>
			<p class="justificado">
				<h4>Resolución 482 de 2018</h4><br>					
				23.7 Plano general de las instalaciones de acuerdo con lo establecido en la Resolución 4445 de 1996, expedida por el entonces Ministerio de 
				Salud o la norma de la modifique o sustituya, el cual debe contener:					
			</p>
				<table style="border-collapse: collapse;">
				<?php
				for($i482 = 0;$i482 < $o482; $i482++)
				{
					?>
					<tr>
						<td class="justificado" style="border:1px solid black;"><?php echo $obs482['item'.$i482]?></td>
						<td class="justificado" style="border:1px solid black;"><?php echo $obs482['obs'.$i482]?></td>
					</tr>
					<?php	
				}
				?>
				</table>
			<?php
			}
			
			
			$items4445 = $this->rx_model->itemsInfraestructura('4445');
			$bandera4445 = 0;
			$o4445 = 0;
			
			for($it=0;$it<count($items4445);$it++){
				
				$itemCons['id_equipo'] = $rayosxEquipo[$i]->id_equipo_rayosx; 
				$itemCons['id_tramite'] = $datos_tramite->id; 
				$itemCons['id_item'] = $items4445[$it]->id_item; 
				
				if($datos_tramite->estado == 4){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 7){
					$itemCons['id_estado'] = 3; 	
				}else if($datos_tramite->estado == 16){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 19){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 24){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 21){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 25){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 26){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 27){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 28){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 29){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 30){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 31){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 32){
					$itemCons['id_estado'] = 15; 	
				}else if($datos_tramite->estado == 33){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 34){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 35){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 36){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 37){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 38){
					$itemCons['id_estado'] = 24; 	
				}else if($datos_tramite->estado == 41){
					$itemCons['id_estado'] = 3; 	
				}
				
				$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
				
				if(count($resultadoEquipoInfra)>0){
					
					if(trim($resultadoEquipoInfra->observaciones) != ''){
						$obs = $resultadoEquipoInfra->observaciones;
						$obs4445['item'.$o4445] = $items4445[$it]->desc_item;
						$obs4445['obs'.$o4445] = $resultadoEquipoInfra->observaciones;
						$bandera4445 = 1;
						$o4445++;
					}
				}else{
				}							
			}
			
			
			if($bandera4445 == 1){
				?>
				
				<div class="row">
					<h4>Articulo 33 Resolución 4445 de 1996 </h4>
				</div>
				<table style="border-collapse: collapse;">
				<?php
				
					for($i4445 = 0;$i4445 < $o4445; $i4445++)
					{
						?>
						<tr>
							<td class="justificado" style="border:1px solid black;"><?php echo $obs4445['item'.$i4445]?></td>
							<td class="justificado" style="border:1px solid black;"><?php echo $obs4445['obs'.$i4445]?></td>
						</tr>
						<?php	
					}
				?>
				
				</table>
				<?php
			}
		}
		
		if(isset($obs_val3) && $obs_val3 != ''){				
			?>
			<h4><?php echo $item?>. Observaciones Trabajadores TOE</h4>
			<p class="justificado">
				<?php echo trim($obs_val3)?>
			</p>
			<?php
			$item++;
		}
		
		if(isset($obs_val4) && $obs_val4 != ''){
			
			?>
			<h4><?php echo $item?>. Observaciones Talento Humano</h4>
			<p class="justificado">
				<?php echo trim($obs_val4)?>
			</p>
			<?php
			$item++;
		}
		
		if(isset($obs_val5) && $obs_val5 != ''){
			
			?>
			<h4><?php echo $item?>. Observaciones Objetos de prueba</h4>
			<p class="justificado">
				<?php echo trim($obs_val5)?>
			</p>
			<?php
			$item++;
		}
		
		if(isset($obs_val6) && $obs_val6 != ''){
			
			?>
			<h4><?php echo $item?>. Observaciones Documentos Adjuntos</h4>
			<p class="justificado">
				<?php echo trim($obs_val6)?>
			</p>
			<?php
			$item++;
		}
		?>
		<p class="justificado">
			Para continuar con el trámite de licencia de práctica médica, es necesario el cumplimiento de los aspectos descritos en el concepto, para lo cual 
			contará con 20 días hábiles, de acuerdo a lo establecido en el Artículo 26, numeral 26.1de la Resolución 482 de 2018. En caso contrario, se entenderá 
			que se desiste de ésta, salvo que antes de vencer el plazo concedido, solicite prórroga, la cual se concederá hasta por un término igual, 
			(Artículo 26, numeral 26.2 de la Resolución 482 de 2018).
		</p>
		<p class="justificado">
			Cordialmente,
		</p>
		<br><br><br>
		<?php
	
		if($firma == TRUE){
			?>
			<img src="<?php echo FCPATH.'assets/imgs/firma_docmarthaBK.JPG'?>" width="300px"><br>
			<?php
		}
		
		?>
		<p class="justificado"><b>YOLIMA AGUDELO SEDANO</b></p>
		<p class="justificado">Subdirectora de Inspección Vigilancia y Control de Servicios de Salud</p>

</body>
</html>
