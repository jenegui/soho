<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
}
?>

<form action="<?php echo base_url('validacion/recurso/') ?>" method="post" autocomplete="off" class="form-horizontal">
	<div class="row block right" style="width:100%" >
			<div class="col-12 col-md-12 pl-">
                <div class="subtitle">
                    <h2><b>Filtro Consulta y Descarga Excel Tr&aacute;mites Recurso</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Inicial:</b></label>
                    <input id="fecha_i" name="fecha_i" class="form-control" placeholder="Fecha Seguimiento Inicio" style="width:100%;">
                </div>
            </div>
			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br>
                    <label for="num_doc"><b>Fecha Final:</b></label>
                    <input id="fecha_f" name="fecha_f" class="form-control" placeholder="Fecha Seguimiento Fin"  style="width:100%;">
                </div>
            </div>

			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input type="submit" class="btn btn-info" value="Consultar" style="width:100%;">
                </div>
            </div>
			<?php
				if($this->session->userdata('perfil')== 7){
			?>
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="ExcelAC" value="Descargar Excel AC" style="width:100%;">
                </div>
            </div>			
			<?php
			}
			else if($this->session->userdata('perfil')== 3 OR $this->session->userdata('perfil')== 4 OR $this->session->userdata('perfil')== 5){
			?>			
			<div class="col-12 col-md-3 pl-4">
                <div class="paragraph">
					<br><br><br>
                    <input  type="button" class="btn btn-success" id="Excel" value="Descargar Excel VC" style="width:100%;">
                </div>
            </div>		
			<?php										
			}
			?> 			
</form>
		<div class="col-12 col-md-12">
		<br>
			<div class="alert alert alert-info" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			<b>Apreciado Usuario!</b>
			<p>La consulta y el reporte de tr&aacute;mites en estado de Recurso, es por rango de fecha de acuerdo a la fecha del seguimiento generada por el sistema.<br>
			Los Tr&aacute;mites visualizados corresponden a los últimos 30 d&iacute;as desde la fecha actual.
			</p>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
		<br>
			<table class="table" id="tabla_tramites"  style="font-size:small;">
					<thead>
                    <tr>
                        <th>ID Tr&aacute;mite</th>
                        <th>Identificaci&oacute;n</th>
                        <th>Nombres y Apellidos</th>
						<th>Fecha Radicaci&oacute;n</th>
                        <th>Fecha Resoluci&oacute;n</th>
                        <th>Tipo de t&iacute;tulo</th>
                        <th>Ver M&aacute;s</th>
                        <th>PDF</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                //Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 28052019
                //Listado de tramites Recurso generados por resolucion firmada
                if(count($tramites_recurso)>0){
                        for($i=0;$i<count($tramites_recurso);$i++){
                        ?>
                    <tr>
                        <td>
                            <?php echo $tramites_recurso[$i]->id_titulo;?>
                        </td>
                        <td>
                            <?php echo $tramites_recurso[$i]->descTipoIden." - ".$tramites_recurso[$i]->nume_identificacion?>
                        </td>
                        <td>
                            <?php echo $tramites_recurso[$i]->p_nombre." ".$tramites_recurso[$i]->s_nombre." ".$tramites_recurso[$i]->p_apellido." ".$tramites_recurso[$i]->s_apellido?>
                        </td>
						<td>
                            <?php echo $tramites_recurso[$i]->fecha_tramite?>
                        </td>
                        <td>
                            <?php echo $tramites_recurso[$i]->fecha_seguimiento?>
                        </td>
                        <td>
                            <?php
                                if($tramites_recurso[$i]->tipo_titulo == '1'){
                                    echo "Nacional";
                                }else{
                                    echo "Extranjero";
                                }
                                ?>
                        </td>
                        <td>
							<center>
								<a href="<?php echo base_url('validacion/visualizar_documentos/'.$tramites_recurso[$i]->id_titulo)?>"  target="_blank">
								<img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="20px">
								<br>Visualizar Informaci&oacute;n
								</a>
							</center>
                        </td>
                        <td>
                            <?php
                            if($tramites_recurso[$i]->estado == 14){
                                $busca_resolucion = $this->validacion_model->buscar_resolucion($tramites_recurso[$i]->id_titulo);
                                if($busca_resolucion){
                                    ?>
                                    <center>
                                      <a href="<?php echo base_url($busca_resolucion->ruta.$busca_resolucion->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="20px">
                                        </a>
                                    </center>
                                    <?php
                                }
                            }else if($tramites_recurso[$i]->estado == 13){
                                $busca_validacion = $this->validacion_model->buscar_validacion($tramites_recurso[$i]->id_titulo, 13);
                                if($busca_validacion){
                                    echo $busca_validacion->observaciones;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                <?php
						}
				}
				?>
                </tbody>
            </table>
		</div>
	</div>
<!--Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 17062019
//Script Generar Excel-->
		<script type="text/javascript">
           $("#Excel").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelrecurso?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>
		<script type="text/javascript">
           $("#ExcelAC").click(function (){
           var fecha_i= $("#fecha_i").val();
           var fecha_f= $("#fecha_f").val();
          window.location.href =base_url+'validacion/generar_excelrecursoAC?fecha_i='+fecha_i+'&fecha_f='+fecha_f;
          //window.location.href ="<?php //echo base_url("validacion/generar_excel/")?>"

         });

        </script>			