<?php

class Mlicencia_Exhumacion extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Registrar solicitud lucencia exhumacion
     * @Exhumacion
     * @ura
     */
    function registrarSolicitudLicencia($parametros) {
        $datos = array(
            'id_persona' => $parametros['id_persona'],
            'fecha_solicitud' => $parametros['fecha_solicitud'],
            //'numero_regdefuncion' => $parametros['numero_regdefuncion'],
            'numero_licencia' => $parametros['numero_licencia'],
            'numero_docfallecido' => $parametros['numero_docfallecido'],
            'pdf_cedulasolicitante' => $parametros['pdf_cedulasolicitante'],
            'pdf_certificadocementerio' => $parametros['pdf_certificadocementerio'],
            'pdf_ordenjudicial' => $parametros['pdf_ordenjudicial'],
            'pdf_autorizacionfiscal' => $parametros['pdf_autorizacionfiscal'],
            'pdf_certificado_per4' => $parametros['pdf_certificado_per4'],
            'pdf_certificado_per3' => $parametros['pdf_certificado_per3'],
            'pdf_otro' => $parametros['pdf_otro'],
            'estado' => $parametros['estado'],
            'parentesco' => $parametros['parentesco'],
            'fecha_inhumacion' => $parametros['fecha_inhumacion'],
			'interviene_medlegal' => $parametros['interviene_medlegal'],
            'declaracion_juramentada' => $parametros['declaracion_juramentada']
        );

        $this->db->insert('licencia_exhuma', $datos);
        return $this->db->insert_id();
    }

    /**
     * listado de tramites de licencia exhumacion
     * solicitados por el usuario
     * @param type $id_persona
     * @return type
     */
    public function mistramites_exh($id_persona) {
        $sql = "SELECT e.des_estado,l.* FROM licencia_exhuma as l, persona as p,estado_tramite_licenciaExh as e
              where p.id_persona=l.id_persona and l.estado=e.id_estado and l.id_persona=$id_persona and estado IN(1,2,3,4,5,6)";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /**
     * listado de solicitudes de licencia exhumacion
     * @return type
     */
    public function listadotramites_exhumacion() {
        $sql = "SELECT * FROM licencia_exhuma as l ";
        $sql .= " LEFT JOIN  archivos AS a ON a.id_archivo=l.pdf_cedulasolicitante";
        $sql .= " LEFT JOIN archivos a1 ON a1.id_archivo=l.pdf_certificadocementerio";
        $sql .= " LEFT JOIN archivos a2 ON a2.id_archivo=l.pdf_certificado_per4";
        $sql .= " LEFT JOIN archivos a3 ON a3.id_archivo=l.pdf_certificado_per3";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /*     * listado solicitudes exhumacion general
     * 
     * @return type
     */
    public function listadosolicitudesExh() {
        $sql = "SELECT p.tipo_identificacion,p.nume_identificacion,p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,p.telefono_fijo,p.telefono_celular,l.* FROM licencia_exhuma as l, persona as p 
              where p.id_persona=l.id_persona and l.id_persona=p.id_persona";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /**
     * listado solicitudes para la gestion del coordinador
     * @return type
     */
    public function listadosolicitudesExh_coor() {
        $sql = "SELECT p.tipo_identificacion,p.nume_identificacion,p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,p.telefono_fijo,p.telefono_celular,e.des_estado,l.* FROM licencia_exhuma as l, persona as p,
              estado_tramite_licenciaExh as e
              where p.id_persona=l.id_persona and l.id_persona=p.id_persona and l.estado=e.id_estado and estado IN(3)";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /**
     * listado solicitudes para la gestion del validador
     * @return type
     */
    public function listadosolicitudesExh_vali() {
        $sql = "SELECT p.tipo_identificacion,p.nume_identificacion,p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,p.telefono_fijo,p.telefono_celular,e.des_estado,l.* FROM licencia_exhuma as l, persona as p,
              estado_tramite_licenciaExh as e
              where p.id_persona=l.id_persona and l.id_persona=p.id_persona and l.estado=e.id_estado and estado IN(1,6)";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    /**
     * informacio solicitud exhumacion por idsolicitud
     * @param type $id
     * @return type
     */
    public function exhumacionfetch($id) {
        $sql = " SELECT p.id_persona,p.tipo_identificacion,p.nume_identificacion,p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,p.telefono_fijo,p.telefono_celular, interviene_medlegal, es.des_estado,group_concat(n.observaciones) observaciones,l.* FROM licencia_exhuma as l ";
        $sql .= " LEFT JOIN persona as p ON p.id_persona=l.id_persona ";
        $sql .= " LEFT JOIN estado_tramite_licenciaExh as es ON es.id_estado=l.estado ";
        $sql .= "LEFT JOIN notificacion_exh as n on n.idlicencia_exhumacion=l.idlicencia_exhumacion";
        $sql .= " WHERE  l.idlicencia_exhumacion=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    /**
     * funcion que permite actualizar la solicitud del tramite
     * @param type $parametros
     * @return type
     */
    public function actualizarTramiteExh($parametros) {

        $datos = array(
            'id_persona' => $parametros['id_persona'],
            //'numero_regdefuncion' => $parametros['numero_regdefuncion'],
            'numero_licencia' => $parametros['numero_licencia'],
            //'numero_docfallecido' => $parametros['numero_docfallecido'],
            'estado' => $parametros['estado'],
            'parentesco' => $parametros['parentesco'],
			'interviene_medlegal' => $parametros['interviene_medlegal'],
            'fecha_inhumacion' => $parametros['fecha_inhumacion']
        );

        if ($parametros['pdf_cedulasolicitante'] != 0) {
            $datos['pdf_cedulasolicitante'] = $parametros['pdf_cedulasolicitante'];
        }

        if ($parametros['pdf_certificadocementerio'] != 0) {
            $datos['pdf_certificadocementerio'] = $parametros['pdf_certificadocementerio'];
        }

        if ($parametros['pdf_ordenjudicial'] != 0) {
            $datos['pdf_ordenjudicial'] = $parametros['pdf_ordenjudicial'];
        }

        if ($parametros['pdf_autorizacionfiscal'] != 0) {
            $datos['pdf_autorizacionfiscal'] = $parametros['pdf_autorizacionfiscal'];
        }

        if ($parametros['pdf_certificado_per4'] != 0) {
            $datos['pdf_certificado_per4'] = $parametros['pdf_certificado_per4'];
        }

        if ($parametros['pdf_certificado_per3'] != 0) {
            $datos['pdf_certificado_per3'] = $parametros['pdf_certificado_per3'];
        }
        if ($parametros['pdf_otro'] != 0) {
            $datos['pdf_otro'] = $parametros['pdf_otro'];
        }

        $this->db->where('idlicencia_exhumacion', $parametros['idlicencia_exhumacion']);
        return $this->db->update('licencia_exhuma', $datos);
    }

    /*     * *
     * func consultar archivo generado para la lcencia 
     */
    public function consultar_archivo($id_archivo) {
        $cadena_sql = " SELECT
                            id_archivo,
                            ruta,
                            nombre,
                            fecha,
                            tags,
                            es_publico,
                            estado
                        FROM
                            archivos
                        WHERE id_archivo = " . $id_archivo . " ";
        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
    }

    /**
     * Infoemacion correspondiente con la aprobacion de la licencia
     * @param type $id
     * @return type
     */
    function info_aprobacionlicencia($id) {
        $sql = "SELECT p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,l.*,a.* FROM aprobacion_licencia as a";
        $sql .= " LEFT JOIN licencia_exhuma as l ON l.idlicencia_exhumacion=a.idlicencia_exhumacion";
        $sql .= " LEFT JOIN persona as p ON p.id_persona=l.id_persona";
        #$sql.= " LEFT JOIN usuarios as u ON u.id=a.aprobado";
        $sql .= " WHERE a.idlicencia_exhumacion=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    function info_licencia($id) {
        $sql = " SELECT p.p_nombre,s_nombre,p.p_apellido, p.s_apellido,p.email,l.* FROM licencia_exhuma as l ";
        $sql .= " JOIN persona as p ON p.id_persona=l.id_persona";
        #$sql.= " LEFT JOIN usuarios as u ON u.id=a.aprobado";
        $sql .= " WHERE l.idlicencia_exhumacion=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }	
	
    /**
     * fincion para conocer el funcionario que realizo la gestion
     * @param type $id
     * @return type
     */
    function usuarios($id) {
        $sql .= "SELECT p.* from persona p";
        $sql .= " LEFT JOIN usuarios u on u.id_persona=p.id_persona";
        $sql .= " LEFT JOIN aprobacion_licencia as ap on  ap.aprobado=u.id  WHERE ap.idlicencia_exhumacion=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    function sesionU($id) {
        $sql .= "SELECT p.* from persona p";
        $sql .= " LEFT JOIN usuarios u on u.id_persona=p.id_persona";
        $sql .= " WHERE u.id=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    /**
     * funcion para conocer el funcionario que realizo la gestion
     * @param type $id
     * @return type
     */
    function consultaUsuario($id) {
        $sql = "SELECT p.* from persona p";
        $sql .= " LEFT JOIN usuarios u on u.id_persona=p.id_persona";
        $sql .= " WHERE u.id=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    
    function usuariosReviso($id) {
        $sql = "SELECT p.* from persona p";
        $sql .= " LEFT JOIN usuarios u on u.id_persona=p.id_persona";
        $sql .= " LEFT JOIN aprobacion_licencia as ap on ap.revisado=u.id WHERE ap.idlicencia_exhumacion=$id";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    /**
     * funcion para actualizar el estado del tramite
     * @param type $datos
     * @return type
     */
    public function actualizarEstado($datos) {

        $data = array(
            'estado' => $datos['estado']
        );
        $this->db->where('idlicencia_exhumacion', $datos['idlicencia_exhumacion']);
        return $this->db->update('licencia_exhuma', $data);
    }

    /**
     * funcion para actualizar el estado del tramite
     * @param type $datos
     * @return type
     */
    public function actualizarDatos($datos) {
        $data = array(
            'nombre_difunto' => $datos['nombre_difunto'],
            'cementerio' => $datos['cementerio'],
        );
       $this->db->where('idlicencia_exhumacion', $datos['idlicencia_exhumacion']);
        return $this->db->update('aprobacion_licencia', $data);
    }

    /**
     * guardar la informacion corespondiente con las notificaciones al ciudadano 
     * @param type $param
     * @return type
     */
    public function registrarNotificacion($param) {
        $this->db->trans_start();
        $this->db->insert('notificacion_exh', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    /**
     * funcion para guardar los documentos
     * @param type $param
     * @return type
     */
    public function insertarArchivo($param) {
        $this->db->trans_start();
        $this->db->insert('archivos', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    /**
     * guardar la informacion correspondiente con la aporvacion de la licencia
     * @param type $param
     * @return type
     */
    public function registro_aprobacionlicencia($param) {
        $this->db->trans_start();
        $this->db->insert('aprobacion_licencia', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    /**
     * funcion que permite actualizar la aprobacion de la licencia
     * @param type $parametros
     * @return type
     */
    public function actualizarAprobacionLicencia($parametros) {
        $datos = array(
            'aprobado' => $parametros['aprobado'],
            'numero_lic_exhu' => $parametros['numero_lic_exhu'],
            'fecha_aprob' => $parametros['fecha_aprob'],
            'nombre_difunto' => $parametros['nombre_difunto'],
			'num_docdifunto' => $parametros['num_docdifunto'],
			'num_certdefunsion' => $parametros['num_certdefunsion'],
            'cementerio' => $parametros['cementerio'],
            'num_licencia_inhumacion' => $parametros['num_licencia_inhumacion'],
            'fecha_inhumacion' => $parametros['fecha_inhumacion'],
            'numero_verificacion' => $parametros['numero_verificacion'],
            'observaciones' => $parametros['observaciones'],
            'estado_apro' => $parametros['estado_apro']
        );
       $this->db->where('idlicencia_exhumacion', $parametros['idlicencia_exhumacion']);
      # $this->db->where('idaprobacion', $parametros['idaprobacion']);
        return $this->db->update('aprobacion_licencia', $datos);
    }

    /**
     * funcion para actualizar un archivo
     * @param type $datos
     * @return type
     */
    public function actualizar_id_archivo($datos) {

        $data = array(
            'id_archivo' => $datos['id_archivo']
        );
        $this->db->where('idlicencia_exhumacion', $datos['idlicencia_exhumacion']);
        return $this->db->update('aprobacion_licencia', $data);
    }

    /* $estado     
      AND RE.id_titulo = ".$id_titulo."";
     * archivo preliminar de licencia ques e le otorga al solicitante
     *  */
    public function archivo_preliminar($id) {
        $cadena_sql = " SELECT DISTINCT
                        a.id_archivo,
                        AR.nombre
                    FROM aprobacion_licencia a
                    JOIN archivos AR ON AR.id_archivo = a.id_archivo 
                    WHERE a.idlicencia_exhumacion =$id";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
    }

    /**
     * consulta vista de oracle donde se encuentra la informaciòn relacionada con un difunto 
     * con los parametros de numero inhumacion y fecha inhumacion
     * @param type $datos
     * @return type
     */
    public function informacion_oracle($datos) {
        $fecha = new DateTime($datos->fecha_inhumacion);
        $fechaN = $fecha->format('d/m/Y');
		//var_dump($fechaN);
		//exit;	
        $oracle = $this->load->database('oracle', TRUE);

        $cadena_sql = "SELECT INH_NUM_LICENCIA,to_char(INH_FEC_LICENCIA, 'YYYY-MM-DD') INH_FEC_LICENCIA,
                            FETAL_Y_NO_FETAL, PRIMER_NOMBRE, SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, NROIDENT,
                            NUM_CERTIFICADO_DEFUNCION, CEMENTERIO, TIPO_MUERTE, TIPO_IDENT, COD_INST, RADICADO, RAZON_INST
                        FROM V_MUERTOS ";
        $cadena_sql .= " WHERE INH_NUM_LICENCIA =" . $datos->numero_licencia;
        $cadena_sql .= " AND INH_FEC_LICENCIA ='$fechaN'";

        $query = $oracle->query($cadena_sql);        //var_dump($cadena_sql);var_dump($fechaN);
        $result = $query->row();

        return $result;
    }

    /**
     * consulta vista de oracle donde se encuentra la informaciòn relacionada con un difunto 
     * con los parametros de numero inhumacion y fecha inhumacion
     * @param type $datos
     * @return type
     */
    public function validacionUsuario_oracle($numeroL, $fecha) {
        $fecha1 = new DateTime($fecha);
        $fechaN = $fecha1->format('d/m/Y');
        $oracle = $this->load->database('oracle', TRUE);
        $cadena_sql = " SELECT INH_NUM_LICENCIA,to_char(INH_FEC_LICENCIA, 'YYYY-MM-DD') INH_FEC_LICENCIA,
                            FETAL_Y_NO_FETAL, PRIMER_NOMBRE, SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, NROIDENT,
                            NUM_CERTIFICADO_DEFUNCION, CEMENTERIO, TIPO_MUERTE, TIPO_IDENT, COD_INST, RADICADO, RAZON_INST
                        FROM V_MUERTOS ";
        $cadena_sql .= " WHERE INH_NUM_LICENCIA =" . $numeroL;
        $cadena_sql .= " AND INH_FEC_LICENCIA ='$fechaN'";
        $query = $oracle->query($cadena_sql);
        $result = $query->row();
        if (isset($result)) {
            echo $result->INH_NUM_LICENCIA;
            exit();
        }
    }


    public function validacionFuncionario_oracle($numeroL, $fecha) {
        $fecha1 = new DateTime($fecha);
        $fechaN = $fecha1->format('d/m/Y');
        $oracle = $this->load->database('oracle', TRUE);
        $cadena_sql = " SELECT INH_NUM_LICENCIA,to_char(INH_FEC_LICENCIA, 'YYYY-MM-DD') INH_FEC_LICENCIA,
                            FETAL_Y_NO_FETAL, PRIMER_NOMBRE, SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, NROIDENT,
                            NUM_CERTIFICADO_DEFUNCION, CEMENTERIO, TIPO_MUERTE, TIPO_IDENT, COD_INST, RADICADO, RAZON_INST
                        FROM V_MUERTOS ";
        $cadena_sql .= " WHERE INH_NUM_LICENCIA =" . $numeroL;
        $cadena_sql .= " AND INH_FEC_LICENCIA ='$fechaN'";
        $query = $oracle->query($cadena_sql);
        $result = $query->row();
		return $result;
    }
    /**
     * 
     * @param type $numeroLINH
     * @param type $fecha INH
     */
    public function informacionLEXH_oracle($datos) {
        $fecha = new DateTime($datos->fecha_inhumacion);
        $fechaN = $fecha->format('d/m/Y');
        $oracle = $this->load->database('oracle', TRUE);

        $cadena_sql = " SELECT INH_NUM_LICENCIA,to_char(INH_FEC_LICENCIA,'YYYY-MM-DD') INH_FEC_LICENCIA,
                            FETAL_Y_NO_FETAL, EXH_NUM_LICENCIA, EXH_FEC_LICENCIA FROM v_muertos_exh ";
        $cadena_sql .= " WHERE INH_NUM_LICENCIA =" . $datos->numero_licencia;
        $cadena_sql .= " AND INH_FEC_LICENCIA ='$fechaN'";
        $cadena_sql .= " ORDER BY  EXH_FEC_LICENCIA DESC";

        $query = $oracle->query($cadena_sql);
        $result = $query->row();

        return $result;
    }

    /**
     * Infoemacion correspondiente con la aprobacion de la licencia
     * @param type $id
     * @return type
     */
    function info_aprobacionL($datos) {
        $sql = "SELECT a.* FROM aprobacion_licencia as a";
        $sql .= " WHERE a.num_licencia_inhumacion=" . $datos->numero_licencia;
        $sql .= " and a.fecha_inhumacion='$datos->fecha_inhumacion' AND estado_apro=4";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result;
    }

    /**
     * 
     * @param type $fechai fecha inicial
     * @param type $fechaf fecha fin 
     * @param type $numlicExh numero lic exhumacion
     * @return type
     */
    function listarSolicitudesLE($fechai, $fechaf) {
        $fec1 = date("Y-m-d");
        $fec = strtotime('-1 month', strtotime($fec1));
        $fec2 = date('Y-m-d', $fec);
        $fecha = strtotime('+1 day', strtotime($fec1));
        $fecha2 = date('Y-m-d', $fecha);
        $sql = "SELECT l.idlicencia_exhumacion idap,l.fecha_solicitud,l.fecha_inhumacion as fechaI,l.numero_regdefuncion,l.numero_licencia,l.parentesco,la.numero_lic_exhu,";
        $sql.=" la.fecha_aprob,la.nombre_difunto,la.cementerio,la.numero_verificacion,la.id_archivo, ne.estado estado_apro,l.numero_docfallecido,";
        $sql.=" est.des_estado estadoNotif, el.des_estado,
                    p.nume_identificacion,p.p_nombre,p.s_nombre,p.p_apellido,p.s_apellido,ne.observaciones obs,";
        $sql .= " ne.fecha_registro ,pe.p_nombre pnom_funcionario,pe.s_nombre snom_funcionario,pe.p_apellido pape_funcionario,pe.s_apellido sape_funcionario";
        $sql .= " ,pval.p_nombre pnom_val,pval.s_nombre snom_val,";
        $sql .= " pval.p_apellido pape_val,pval.s_apellido sape_val ";
        $sql .= ",paprob.p_nombre pnom_aprob,paprob.s_nombre snom_aprob,";
        $sql .= "paprob.p_apellido pape_aprob,paprob.s_apellido sape_aprob ";
        $sql .= " FROM licencia_exhuma l ";
        $sql .= " LEFT JOIN persona as p ON p.id_persona=l.id_persona";
        $sql .= " LEFT JOIN estado_tramite_licenciaexh est ON est.id_estado=l.estado";
        $sql .= " LEFT JOIN aprobacion_licencia la ON la.idlicencia_exhumacion=l.idlicencia_exhumacion";
        $sql .= " LEFT JOIN notificacion_exh ne ON ne.idlicencia_exhumacion=l.idlicencia_exhumacion ";
        $sql .= " LEFT JOIN estado_tramite_licenciaexh el ON el.id_estado=ne.estado";
        $sql .= " LEFT JOIN usuarios as u on u.id=ne.id_usuario ";
        $sql .= " LEFT JOIN persona pe ON pe.id_persona=u.id_persona";
        $sql .= " LEFT JOIN usuarios us on us.id=la.revisado ";
        $sql .= " LEFT JOIN persona as pval on pval.id_persona=us.id_persona";
        $sql .= " LEFT JOIN usuarios usc on usc.id=la.aprobado";
        $sql .= " LEFT JOIN persona paprob  on paprob.id_persona=usc.id_persona";
               if ($fechai != "" && $fechaf != "")
            $sql .= " WHERE l.fecha_solicitud BETWEEN '$fechai' AND '$fechaf 23:59' ";
        else
            $sql .= "  WHERE l.fecha_solicitud BETWEEN '$fec2' AND '$fecha2 23:59' ";
        $sql .= "group by ne.idnotificacion ORDER BY l.idlicencia_exhumacion"; //echo $sql;exit;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }
    
    function idAprobacionLE($id){
        $sql=" SELECT idaprobacion FROM aprobacion_licencia WHERE idlicencia_exhumacion=$id";
        $query =$this->db->query($sql); 
        $result = $query->row();
        if (isset($result)) {
            return $result->idaprobacion;
        }
        $this->db->close();
        
    }
    
    function numLicenciaExh(){
        $query =$this->db->query("call PA_LICENCIA()"); 
        $result = $query->row();
        
        if (count($result) > 0) {
            return $result->NUMERO;
        }
       $query = null;
    }

	
	public function licenciaexh_seguimientociudadanano_id($id_licenciaexh){
	    $cadena_sql = " SELECT SEG.fecha_registro, USR.perfil, PER.p_apellido, PER.s_apellido, PER.p_nombre, PER.s_nombre, EST.descripcion, SEG.observaciones
        FROM notificacion_exh SEG 
		JOIN usuarios USR ON USR.id = SEG.id_usuario
		LEFT JOIN persona PER ON PER.id_persona = USR.id_persona
		JOIN pr_estado_tramite EST ON EST.id_estado = SEG.estado
        WHERE SEG.estado in (2,5) AND SEG.idlicencia_exhumacion = ".$id_licenciaexh;

	    $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }
	
    public function actualizarDatosPersona($datos) {

        $data = array(
            'tipo_identificacion' => $datos['tipo_identificacion'],
            //'nume_identificacion' => $datos['nume_documento'],
            'p_nombre' => $datos['p_nombre'],
            's_nombre' => $datos['s_nombre'],
            'p_apellido' => $datos['p_apellido'],
            's_apellido' => $datos['s_apellido'],
            'email' => $datos['email'],
            'telefono_fijo' => $datos['telefono_fijo'],
			'telefono_celular' => $datos['telefono_celular'],
			
        );
        $this->db->where('id_persona', $datos['id_persona']);
        return $this->db->update('persona', $data);
    }
	
	
	public function actualizarAuditoriaUsuario($datos) {

        $data = array(
            'id_llave' => $datos['id_persona'],
            //'nume_identificacion' => $datos['nume_documento'],
            'usuario' => $datos['id_usuario'],
			'modificado' => $datos['modificado'],
			
        );
        $this->db->where('id_llave', $datos['id_persona']);
		$this->db->where('modificado >', $datos['modificado']);
		$this->db->where('tabla_nombre', 'persona');
		//var_dump($data);
		//exit;
		
        return $this->db->update('ts_auditoriapersona', $data);

    }
	

    public function actualizarDatosLicencia($datos) {

        $data = array(
            //'numero_licencia' => $datos['numero_licencia'],
            //'numero_regdefuncion' => $datos['numero_regdefuncion'],
			//'numero_docfallecido' => $datos['numero_docfallecido'],
            'parentesco' => $datos['parentesco'],
            'interviene_medlegal' => $datos['interviene_medlegal'],
            //'fecha_inhumacion' => $datos['fecha_inhumacion'],
        );
        $this->db->where('idlicencia_exhumacion', $datos['idlicencia_exhumacion']);
        return $this->db->update('licencia_exhuma', $data);
    }

    public function actualizarNumFecInhuma($datos) {

        $data = array(
            'numero_licencia' => $datos['numero_licencia'],
            'fecha_inhumacion' => $datos['fecha_inhumacion'],
        );
        $this->db->where('idlicencia_exhumacion', $datos['idlicencia_exhumacion']);
        return $this->db->update('licencia_exhuma', $data);
    }
	
    public function tramites_seguimientocompleto_id($id){
	    $cadena_sql = " SELECT SEG.fecha_registro, USR.perfil, PER.p_apellido, PER.s_apellido, PER.p_nombre, PER.s_nombre, EST.des_estado, SEG.observaciones
        FROM notificacion_exh SEG 
		JOIN usuarios USR ON USR.id = SEG.id_usuario
		LEFT JOIN persona PER ON PER.id_persona = USR.id_persona
		JOIN estado_tramite_licenciaexh EST ON EST.id_estado = SEG.estado
        WHERE SEG.idlicencia_exhumacion = ".$id;

	    $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }	
	
}

?>