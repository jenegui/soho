<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('America/Bogota');
/**
 *
 */
class Validacion extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('validacion_model');
        $this->load->model('usuarios_model');
		$this->load->model('direccion_model');
        $this->load->model('login_model');
		$this->load->model('rx_model');
        $this->load->model('mlicencia_exhumacion'); #Exhumacion Model
        $this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form'));
        $this->load->database('default');

		if(!$this->session->userdata('id_usuario') || $this->session->userdata('id_usuario') == ''){
			$this->session->sess_destroy();
			redirect(base_url());
		}


    }

    public function index()
    {		
			$data['listadosolicitudesExh_vali'] = $this->mlicencia_exhumacion->listadosolicitudesExh_vali();
            $data['tramites_pendientes'] = $this->validacion_model->tramites_pendientes();
            $data['rayosx_pendientes'] = $this->validacion_model->rayosx_pendientes($this->session->userdata('perfil'));
			$datosAr1 = $this->session->userdata('username');
			$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
			if($data['validacion']->perfil==3 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==1){
				//var_dump($data['validacion']);
				//exit;
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientes_view';
				$this->load->view('templates/layout_general',$data);
			}
			elseif($data['validacion']->perfil==3 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==2){
				//var_dump($data['validacion']);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientesexh_view';
				$this->load->view('templates/layout_general',$data);
			}else if($data['validacion']->perfil==3 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==3){
				//var_dump($data['validacion']);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientesrx_view';
				$this->load->view('templates/layout_general',$data);
			}else if($data['validacion']->perfil==8 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==3){
				//var_dump($data['validacion']);
				$data['js'] = array(base_url('assets/js/validacion.js'),base_url('assets/js/validacion_rx.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientesrx_view';
				$this->load->view('templates/layout_general',$data);
			}else if($data['validacion']->perfil==9 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==3){
				//var_dump($data['validacion']);
				$data['js'] = array(base_url('assets/js/validacion.js'),base_url('assets/js/validacion_rx.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientesrx_view';
				$this->load->view('templates/layout_general',$data);
			}else if($data['validacion']->perfil==3 && $data['validacion']->estado=='AC' && $data['validacion']->tramites==4){
				//var_dump($data['validacion']);
				$data['js'] = array(base_url('assets/js/validacion.js'),base_url('assets/js/validacion_rx.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_pendientesrx_view';
				$this->load->view('templates/layout_general',$data);	
			}	
			

    }

    public function aprobadosbk()
    {
            $data['tramites_aprobados'] = $this->validacion_model->tramites_aprobados();
            $data['js'] = array(base_url('assets/js/validacion.js'));
            $data['titulo'] = 'Perfil Validaci&oacute;n';
            $data['contenido'] = 'validacion/tramites_aprobados_view';
            $this->load->view('templates/layout_general',$data);

    }


		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Vista de Datos Bandeja de Tramites Aprobados
    public function resolucion3030() {
		
			$datosAr1 = $this->session->userdata('username');
			$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
            $fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
            $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

			if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
			}else{
				$data['tramites_resolucion'] = $this->validacion_model->tramites_resolucion($fechai,$fechaf,0);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_resolucion_view';
				$this->load->view('templates/layout_general', $data);
			}
    }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 01012019
		//Vista de Datos Bandeja de Tramites Aclarados Resolucion 3030
    public function resolucion3030aclaracion() {

			$datosAr1 = $this->session->userdata('username');
			$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
            $fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
            $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

			if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
			}else{
				$data['tramites_resolucion'] = $this->validacion_model->tramites_resolucionaclaracion($fechai,$fechaf,0);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_resolucionacla_view';
				$this->load->view('templates/layout_general', $data);
			}
    }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Vista de Datos Busqueda Tramites por Numero documento
    public function reporteporcedula() {
				
				$datosAr1 = $this->session->userdata('username');
				$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
				$numdoc= isset($_POST['num_doc']) ? $_POST['num_doc']:'';
				if($numdoc == null){
					$data['tramites_aprobados'] = null;
				}
				else{
				$data['tramites_aprobados'] = $this->validacion_model->tramites_pornumdoc($numdoc,0);
				}
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_porcedula_view';
				$this->load->view('templates/layout_general', $data);
			
    }

    public function buscarporcedula() {
				
				$datosAr1 = $this->session->userdata('username');
				$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
				$numdoc= isset($_POST['num_doc']) ? $_POST['num_doc']:'';
				$data['tramites_aprobados'] = $this->validacion_model->tramites_pornumdoc($numdoc,0);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_porcedula_view';
				$this->load->view('templates/layout_general', $data);
			
    }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Vista de Datos Bandeja de Tramites Aprobados
    public function aprobados() {
			
			$datosAr1 = $this->session->userdata('username');
			$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
		        $fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
		        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

			if($fechai>$fechaf){
                		$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
		                redirect(base_url('validacion/aprobados'), 'refresh');
		                exit;
			}else{
				$data['tramites_aprobados'] = $this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_aprobados_view';
				$this->load->view('templates/layout_general', $data);
			}
    }


    public function aclarados() {

			$datosAr1 = $this->session->userdata('username');
			$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
		        $fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
		        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

			if($fechai>$fechaf){
                		$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
		                redirect(base_url('validacion/aprobados'), 'refresh');
		                exit;
			}else{
				$data['tramites_aclarados'] = $this->validacion_model->tramites_aclarados($fechai,$fechaf,0);
				$data['js'] = array(base_url('assets/js/validacion.js'));
				$data['titulo'] = 'Perfil Validaci&oacute;n';
				$data['contenido'] = 'validacion/tramites_aclarados_view';
				$this->load->view('templates/layout_general', $data);
			}
    }

    public function generar_excelresolucion(){
    		$fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
		$fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';
		$strfechaf = str_replace("-","",$fechaf);

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                	$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
	                redirect(base_url('validacion/aprobados'), 'refresh');
	                exit;
		   }elseif($fechai<$fec2){
			$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
        		redirect(base_url('validacion/aprobados'), 'refresh');
	                exit;
		   }
		   else{
			$namefile = "THS130ATHS" . $strfechaf . "DI000000011001.txt";
			$text2="";
			$text3="";
			
			$tramites_resolucion2= $this->validacion_model->tramites_resolucion2($fechai,$fechaf,0);
			$tramites_resolucion3= $this->validacion_model->tramites_resolucion($fechai,$fechaf,0);
			
			$acentosr = array('À','È','Ì','Ò','Ù','à','è','ì','ò','ù');
			$acentos = array('Á','É','Í','Ó','Ú','í','á','é','ó','ú');
			$cacentos = array('A','E','I','O','U');
			$saltoLinea="\r\n";
			
			$cont=0;
			
			foreach($tramites_resolucion2 as $l){
				$cont++;
			$text2 = $text2 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->primer_apellido)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->segundo_apellido)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->primer_nombre)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->segundo_nombre)) . ';'  . $l->genero . ';'  . $l->departamentonacimiento . ';'  . $l->ciudadnacimiento . ';'  . $l->nacionalidad . ';'  . $l->fecha_nacimiento . ';'  . $l->estadocivil . ';'  . $l->CodPaisResidencia . ';'  . $l->depa_resi . ';'  . $l->ciudad_resi . ';'  . $l->direccionresi . ';'  . $l->telefono_fijo . ';'  . $l->telefono_celular . ';'  . $l->emailpersona . ';' . $l->etnia . $saltoLinea;
			}	
			
			foreach($tramites_resolucion3 as $l){
			
			$cont++;
			

				if ($l === end($tramites_resolucion3)){
					
					$text3 = $text3 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . $l->ObtencionTitulo . ';' . $l->departamentoinst . ';' . $l->municipioinst . ';' . $l->paisinstitucion . ';' . $l->TipoInstitucion . ';' . $l->codigoinstitucion . ';' . $l->tipoprograma . ';' . $l->codigoprograma . ';' . $l->fechagrado . ';' . $l->numeroconvalidacion . ';' . $l->fechaconvalidacion . ';' . $l->tituloequivalente . ';' . $l->grupotituloequivalente . ';' . $l->noactoadmin . ';' . $l->fecha_actoadmin;	
					break;
				}
				
				$text3 = $text3 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . $l->ObtencionTitulo . ';' . $l->departamentoinst . ';' . $l->municipioinst . ';' . $l->paisinstitucion . ';' . $l->TipoInstitucion . ';' . $l->codigoinstitucion . ';' . $l->tipoprograma . ';' . $l->codigoprograma . ';' . $l->fechagrado . ';' . $l->numeroconvalidacion . ';' . $l->fechaconvalidacion . ';' . $l->tituloequivalente . ';' . $l->grupotituloequivalente . ';' . $l->noactoadmin . ';' . $l->fecha_actoadmin . $saltoLinea;
			}
			
			$text1 = "1;". $fechai . ";". $fechaf . ";DI;11001;". $cont . $saltoLinea;
			$textf = $text1 . $text2 . $text3; 
				
			//var_dump($text);
			//exit;
			//save file
			$file = fopen($_SERVER['DOCUMENT_ROOT'].$namefile, "a") or die("Unable to open file!");
			fwrite($file, $textf);
			fclose($file);

			//header download
			header("Content-Disposition: attachment; filename=\"" . $namefile . "\"");
			header("Content-Type: application/force-download");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header("Content-Type: text/plain");
			
			echo $textf;
			

            
		   }
   }


    public function generar_excelresolucionacla(){
    	
		$fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
		$fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';
		$strfechaf = str_replace("-","",$fechaf);

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                	$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
	                redirect(base_url('validacion/aprobados'), 'refresh');
	                exit;
		   }elseif($fechai<$fec2){
			$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
        		redirect(base_url('validacion/aprobados'), 'refresh');
	                exit;
		   }
		   else{
			$namefile = "THS130ATHS" . $strfechaf . "DI000000011001.txt";
			$text2="";
			$text3="";

			$tramites_resolucion2= $this->validacion_model->tramites_resolucion2aclaracion($fechai,$fechaf,0);
			$tramites_resolucion3= $this->validacion_model->tramites_resolucionaclaracion($fechai,$fechaf,0);

			$acentosr = array('À','È','Ì','Ò','Ù','à','è','ì','ò','ù');
			$acentos = array('Á','É','Í','Ó','Ú','í','á','é','ó','ú');
			$cacentos = array('A','E','I','O','U');
			$saltoLinea="\r\n";

			$cont=0;

			foreach($tramites_resolucion2 as $l){
				$cont++;
			$text2 = $text2 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->primer_apellido)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->segundo_apellido)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->primer_nombre)) . ';'  . str_replace($acentosr,$cacentos,str_replace($acentos,$cacentos,$l->segundo_nombre)) . ';'  . $l->genero . ';'  . $l->departamentonacimiento . ';'  . $l->ciudadnacimiento . ';'  . $l->nacionalidad . ';'  . $l->fecha_nacimiento . ';'  . $l->estadocivil . ';'  . $l->CodPaisResidencia . ';'  . $l->depa_resi . ';'  . $l->ciudad_resi . ';'  . $l->direccionresi . ';'  . $l->telefono_fijo . ';'  . $l->telefono_celular . ';'  . $l->emailpersona . ';' . $l->etnia . $saltoLinea;
			}

			foreach($tramites_resolucion3 as $l){

			$cont++;


				if ($l === end($tramites_resolucion3)){

					$text3 = $text3 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . $l->ObtencionTitulo . ';' . $l->departamentoinst . ';' . $l->municipioinst . ';' . $l->paisinstitucion . ';' . $l->TipoInstitucion . ';' . $l->codigoinstitucion . ';' . $l->tipoprograma . ';' . $l->codigoprograma . ';' . $l->fechagrado . ';' . $l->numeroconvalidacion . ';' . $l->fechaconvalidacion . ';' . $l->tituloequivalente . ';' . $l->grupotituloequivalente . ';' . $l->noactoadmin . ';' . $l->fecha_actoadmin;
					break;
				}

				$text3 = $text3 . $l->TipoRegistro . ';'. $cont . ';' .$l->Codigo. ';' . $l->nume_identificacion . ';' . $l->ObtencionTitulo . ';' . $l->departamentoinst . ';' . $l->municipioinst . ';' . $l->paisinstitucion . ';' . $l->TipoInstitucion . ';' . $l->codigoinstitucion . ';' . $l->tipoprograma . ';' . $l->codigoprograma . ';' . $l->fechagrado . ';' . $l->numeroconvalidacion . ';' . $l->fechaconvalidacion . ';' . $l->tituloequivalente . ';' . $l->grupotituloequivalente . ';' . $l->noactoadmin . ';' . $l->fecha_actoadmin . $saltoLinea;
			}

			$text1 = "1;". $fechai . ";". $fechaf . ";DI;11001;". $cont . $saltoLinea;
			$textf = $text1 . $text2 . $text3;

			//var_dump($text);
			//exit;
			//save file
			$file = fopen($_SERVER['DOCUMENT_ROOT'].$namefile, "a") or die("Unable to open file!");
			fwrite($file, $textf);
			fclose($file);

			//header download
			header("Content-Disposition: attachment; filename=\"" . $namefile . "\"");
			header("Content-Type: application/force-download");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header("Content-Type: text/plain");

			echo $textf;



		   }
   }


	public function generar_excelresolucion2(){
           $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_aprobados']=$this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $tramites_aprobados= $this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_aprobados_view';
           $this->load->view('templates/layout_general', $data);
           if(count($tramites_aprobados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAprobados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'No Resolución');
               $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Fecha Resolución');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Eficiencia');



               foreach($tramites_aprobados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->id_resolucion);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->fecha_resolucion);

					//Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional calculo dias para columna eficiencia
				  if( $l->fecha_resolucion > $l->fecha_tramite){
					  $start = new DateTime($l->fecha_tramite);
					  $end = new DateTime($l->fecha_resolucion);
					  $days = round(($end->format('U') - $start->format('U')) / (60*60*24));
					  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $days);
				  }

               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_aprobados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Metodo Excel tramites aprobados.
    public function generar_excelaprobados(){
           $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_aprobados']=$this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $tramites_aprobados= $this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_aprobados_view';
           $this->load->view('templates/layout_general', $data);
           if(count($tramites_aprobados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAprobados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'No Resolución');
               $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Fecha Resolución');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Eficiencia');



               foreach($tramites_aprobados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->id_resolucion);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->fecha_resolucion);

					//Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional calculo dias para columna eficiencia
				  if( $l->fecha_resolucion > $l->fecha_tramite){
					  $start = new DateTime($l->fecha_tramite);
					  $end = new DateTime($l->fecha_resolucion);
					  $days = round(($end->format('U') - $start->format('U')) / (60*60*24));
					  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $days);
				  }

               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_aprobados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

 public function generar_excelaprobadosAC()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aprobados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_aprobados']=$this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $tramites_aprobados= $this->validacion_model->tramites_aprobados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_aprobados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_aprobados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAprobados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("P")->setWidth(30);

               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'No Resolución');
               $this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Fecha Resolución');
			   $this->excel->getActiveSheet()->setCellValue("P{$contador}", 'Eficiencia');




               foreach($tramites_aprobados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas
			  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				    $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
					$this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
					$this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
					$this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->id_resolucion);
                  $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_resolucion);

					//Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional calculo dias para columna eficiencia
				  if( $l->fecha_resolucion > $l->fecha_tramite){
					  $start = new DateTime($l->fecha_tramite);
					  $end = new DateTime($l->fecha_resolucion);
					  $days = round(($end->format('U') - $start->format('U')) / (60*60*24));
					  $this->excel->getActiveSheet()->setCellValue("P{$contador}", $days);
				  }

               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_aprobados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }


    public function generar_excelaclarados(){
           $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aclarados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aclarados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_aclarados']=$this->validacion_model->tramites_aclarados($fechai,$fechaf,0);
           $tramites_aclarados= $this->validacion_model->tramites_aclarados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_aclarados_view';
           $this->load->view('templates/layout_general', $data);
           if(count($tramites_aclarados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAclarados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'No Resolución');
               $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Fecha Resolución');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Eficiencia');



               foreach($tramites_aprobados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
					  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
					  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->id_resolucion);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->fecha_resolucion);

					//Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional calculo dias para columna eficiencia
				  if( $l->fecha_resolucion > $l->fecha_tramite){
					  $start = new DateTime($l->fecha_tramite);
					  $end = new DateTime($l->fecha_resolucion);
					  $days = round(($end->format('U') - $start->format('U')) / (60*60*24));
					  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $days);
				  }

               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_aprobados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

 public function generar_excelaclaradosAC()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aclarados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/aclarados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_aclarados']=$this->validacion_model->tramites_aclarados($fechai,$fechaf,0);
           $tramites_aclarados= $this->validacion_model->tramites_aclarados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_aclarados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_aclarados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAprobados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("P")->setWidth(30);

               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'No Resolución');
               $this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Fecha Resolución');
			   $this->excel->getActiveSheet()->setCellValue("P{$contador}", 'Eficiencia');




               foreach($tramites_aprobados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas
			  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				    $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
					$this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
					$this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
					$this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->id_resolucion);
                  $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_resolucion);

					//Author: Mario Beltran mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional calculo dias para columna eficiencia
				  if( $l->fecha_resolucion > $l->fecha_tramite){
					  $start = new DateTime($l->fecha_tramite);
					  $end = new DateTime($l->fecha_resolucion);
					  $days = round(($end->format('U') - $start->format('U')) / (60*60*24));
					  $this->excel->getActiveSheet()->setCellValue("P{$contador}", $days);
				  }

               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_aprobados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }


		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 14062019
		//Metodo bandeja tramites rechazados.
    public function rechazados()
    {
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);		
		$fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
		$fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

		if($fechai>$fechaf){
                	$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
	                redirect(base_url('validacion/rechazados'), 'refresh');
	                exit;
		}else{
			$data['tramites_rechazados'] = $this->validacion_model->tramites_rechazados($fechai,$fechaf,0);
			//var_dump($data['tramites_rechazados']);
			$data['js'] = array(base_url('assets/js/validacion.js'));
			$data['titulo'] = 'Perfil Validaci&oacute;n';
			$data['contenido'] = 'validacion/tramites_rechazados_view';
			$this->load->view('templates/layout_general',$data);
		   }
    }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 14062019
		//Metodo Excel tramites rechazados.
    public function generar_excelrechazados()
    {
	   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/rechazados'), 'refresh');
                exit;
		   }else{
           $data['tramites_rechazados']=$this->validacion_model->tramites_rechazados($fechai,$fechaf,0);
           $tramites_rechazados= $this->validacion_model->tramites_rechazados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_rechazados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_rechazados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesRechazados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Observaciones');
               $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Fecha Seguimiento');

               foreach($tramites_rechazados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				     $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
					 $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
					 $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
					 $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->observaciones);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->fecha_seguimiento);


               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_rechazados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }



		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 14062019
		//Metodo Excel tramites rechazados Atención al Ciudadadano.
    public function generar_excelrechazadosAC()
    {

	   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';
	   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
	   $fec2= date('Y-m-d', $fec);

	   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/rechazados'), 'refresh');
                exit;
	   }elseif($fechai<$fec2){
		$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/rechazados'), 'refresh');
                exit;
	   }
	   else{
        	$data['tramites_rechazados']=$this->validacion_model->tramites_rechazados($fechai,$fechaf,0);
           	$tramites_rechazados= $this->validacion_model->tramites_rechazados($fechai,$fechaf,0);
           	$data['titulo'] = 'Perfil Validaci&oacute;n';
           	$data['contenido'] = 'validacion/tramites_rechazados_view';
           	$this->load->view('templates/layout_general', $data);


           if(count($tramites_rechazados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesRechazados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(40);
			   $this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(30);

               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Observaciones');
               $this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Fecha Seguimiento');

               foreach($tramites_rechazados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				       $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
				       $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
				       $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
				       $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				 $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->observaciones);
                 $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_seguimiento);


               }
               //nombre de archivo que se va a generar.
               $archivo = "tramites_rechazados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo bandeja tramites Negados.
    public function negados()
    {
	$datosAr1 = $this->session->userdata('username');
	$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
	$fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

	if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/negados'), 'refresh');
                exit;
	}else{
		$data['tramites_negados'] = $this->validacion_model->tramites_negados($fechai,$fechaf,0);
		//var_dump($data['tramites_rechazados']);
		$data['js'] = array(base_url('assets/js/validacion.js'));
		$data['titulo'] = 'Perfil Validaci&oacute;n';
		$data['contenido'] = 'validacion/tramites_negados_view';
		$this->load->view('templates/layout_general',$data);
	}

    }
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo Excel tramites Negados.

    public function generar_excelnegados()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
		   $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/negados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/negados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_negados']=$this->validacion_model->tramites_negados($fechai,$fechaf,0);
           $tramites_negados= $this->validacion_model->tramites_negados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_negados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_negados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesNegados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			         $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Fecha Resolución');





               foreach($tramites_negados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				       $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
				       $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
				       $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
				       $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_negados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }


	public function generar_excelnegadosAC()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/negados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/negados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_negados']=$this->validacion_model->tramites_negados($fechai,$fechaf,0);
           $tramites_negados= $this->validacion_model->tramites_negados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_negados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_negados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesNegados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Fecha Resolución');

               foreach($tramites_negados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				       $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
				       $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
				       $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
				       $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_negados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }


		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo bandeja tramites Anulados.
    public function anulados()
    {
	$datosAr1 = $this->session->userdata('username');
	$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
	$fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

	if($fechai>$fechaf){
        	$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/anulados'), 'refresh');
                exit;
	}else{
		$data['tramites_anulados'] = $this->validacion_model->tramites_anulados($fechai,$fechaf,0);
		//var_dump($data['tramites_rechazados']);
		$data['js'] = array(base_url('assets/js/validacion.js'));
		$data['titulo'] = 'Perfil Validaci&oacute;n';
		$data['contenido'] = 'validacion/tramites_anulados_view';
		$this->load->view('templates/layout_general',$data);
	}
    }


		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo Excel tramites Anulados.

    public function generar_excelanulados()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/anulados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/anulados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_anulados']=$this->validacion_model->tramites_anulados($fechai,$fechaf,0);
           $tramites_anulados= $this->validacion_model->tramites_anulados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_anulados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_anulados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAnulados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Fecha Seguimiento');

               foreach($tramites_anulados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);


				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				$this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_anulados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }


    public function generar_excelanuladosAC()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/anulados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/anulados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_anulados']=$this->validacion_model->tramites_anulados($fechai,$fechaf,0);
           $tramites_anulados= $this->validacion_model->tramites_anulados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_anulados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_anulados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesAnulados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Fecha Seguimiento');

               foreach($tramites_anulados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);
				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				      $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
				      $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
				      $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
				      $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_anulados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 18062019
		//Metodo bandeja tramites Solicitados.
    public function solicitados()
    {
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);		
		$fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
	        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';

		if($fechai>$fechaf){
        	        $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                	redirect(base_url('validacion/anulados'), 'refresh');
	                exit;
		}else{
			$data['tramites_solicitados'] = $this->validacion_model->tramites_solicitados($fechai,$fechaf,0);
			//var_dump($data['tramites_solicitados']);
			$data['js'] = array(base_url('assets/js/validacion.js'));
			$data['titulo'] = 'Perfil Validaci&oacute;n';
			$data['contenido'] = 'validacion/tramites_solicitados_view';
			$this->load->view('templates/layout_general',$data);
		}
    }

    public function generar_excelsolicitados()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/solicitados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/solicitados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_solicitados']=$this->validacion_model->tramites_solicitados($fechai,$fechaf,0);
           $tramites_solicitados= $this->validacion_model->tramites_solicitados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_solicitados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_solicitados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesSolicitados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);

               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Titulo');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Estado');




               foreach($tramites_solicitados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);


				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->descEstado);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_solicitados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

    public function generar_excelsolicitadosAC()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/solicitados'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				        $this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/solicitados'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_solicitados']=$this->validacion_model->tramites_solicitados($fechai,$fechaf,0);
           $tramites_solicitados= $this->validacion_model->tramites_solicitados($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_solicitados_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_solicitados) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesSolicitados');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(30);

               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
    	       $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Estado');

               foreach($tramites_solicitados as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				      $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
				      $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
				      $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
				      $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }
				  $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->descEstado);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_solicitados_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo bandeja tramites Recurso.
    public function recurso()
    {

		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);				
		$fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
		$fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';	

		if($fechai>$fechaf){
	                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
        	        redirect(base_url('validacion/anulados'), 'refresh');
	                exit;
		}else{
			$data['tramites_recurso'] = $this->validacion_model->tramites_recurso($fechai,$fechaf,0);
			$data['js'] = array(base_url('assets/js/validacion.js'));
			$data['titulo'] = 'Perfil Validaci&oacute;n';
			$data['contenido'] = 'validacion/tramites_recurso_view';
			$this->load->view('templates/layout_general',$data);
	    	}

    }
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo Excel tramites Recurso.
    public function generar_excelrecurso()
    {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
           $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/recurso'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/recurso'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_recurso']=$this->validacion_model->tramites_recurso($fechai,$fechaf,0);
           $tramites_recurso= $this->validacion_model->tramites_recurso($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_recurso_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_recurso) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesRecurso');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Fecha Resolución');


               foreach($tramites_recurso as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->titulo_equivalente);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->cod_universidad);
				  }
				  else{
				      $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->nombre_programa);
				      $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_institucion);
				  }

				$this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_recurso_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 17062019
		//Metodo Excel tramites Recurso.
   public function generar_excelrecursoAC()
   {

		   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
		   $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';

		   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
		   $fec2= date('Y-m-d', $fec);

		   if($fechai>$fechaf){
                $this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/recurso'), 'refresh');
                exit;
		   }elseif($fechai<$fec2){
				        $this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
                redirect(base_url('validacion/recurso'), 'refresh');
                exit;
		   }
		   else{
           $data['tramites_recurso']=$this->validacion_model->tramites_recurso($fechai,$fechaf,0);
           $tramites_recurso= $this->validacion_model->tramites_recurso($fechai,$fechaf,0);
           $data['titulo'] = 'Perfil Validaci&oacute;n';
           $data['contenido'] = 'validacion/tramites_recurso_view';
           $this->load->view('templates/layout_general', $data);


           if(count($tramites_recurso) > 0){
               require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
               $this->excel = new PHPExcel();

               $this->excel->setActiveSheetIndex(0);
               $this->excel->getActiveSheet()->setTitle('TramitesRecurso');
               //Contador de filas
               $contador = 1;
               // ancho las columnas.
			   $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
               $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(60);
               $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(40);
               $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(30);
               $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(30);
			   $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(30);
               // negrita a los títulos de la cabecera.
               $this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
               #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
               // títulos de la cabecera.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Número Id Trámite');
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Fecha Radicación Trámite');
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Tipo Título');
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Tipo Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Número Documento Identidad');
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Nombres y Apellidos Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Genero Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Orientación Sexual Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Etnía Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Estado Civil Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Nivel Educativo Solicitante');
			   $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Profesión Descripción');
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Institución Descripción');
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Fecha Resolución');

               foreach($tramites_recurso as $l){
                  $contador++;
                  //Informacion de la consulta.
                  $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_titulo);
                  $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->fecha_tramite);
				  $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->tipo_titulo);
                  $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->descTipoIden);
                  $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->nume_identificacion);
                  $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->p_nombre ." ". $l->s_nombre ." ".  $l->p_apellido ." ". $l->s_apellido);
				  $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->genero);
				  $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->orientacion);
				  $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->etnia);
				  $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->estado_civil);
				  $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->nivel_educativo);

				  	//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 13062019
					//Condicional para Extranjeros para pintar el excel en las mismas columnas

				  if($l->tipo_titulo=='Extranjero'){
				        $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->titulo_equivalente);
					    $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->cod_universidad);
				  }
				  else{
					    $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->nombre_programa);
					    $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->nombre_institucion);
				  }

				  $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->fecha_seguimiento);

				}
               //nombre de archivo que se va a generar.
               $archivo = "tramites_recurso_Autorizaciontitulos.xls";
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="'.$archivo.'"');
               header('Cache-Control: max-age=0');
               $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
               // salida al navegador con el archivo Excel.
               $objWriter->save('php://output');
            }
		   }
   }

    public function todos()
    {
            $data['tramites_aprobados'] = $this->validacion_model->todos_los_tramites();
            $data['js'] = array(base_url('assets/js/validacion.js'));
            $data['titulo'] = 'Consulta de tr&aacute;mites';
            $data['contenido'] = 'validacion/tramites_consulta_view';
            $this->load->view('templates/layout_general',$data);

    }

    public function validar_documentos($id_titulo)
    {
        $data['resultado_validacion'] = $this->validacion_model->estado_tramite_validador();
        $data['causales_negacion'] = $this->validacion_model->causales_negacion();
        $data['tramites_pendientes'] = $this->validacion_model->tramites_pendientes_id($id_titulo);
		
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Validacion Oracle lado de Validador
        $data['departamentos_col'] = $this->login_model->departamentos_col();
		$datosAr2['nume_identificacion'] = $data['tramites_pendientes']->nume_identificacion;
		$data['tramitesoracle'] = $this->login_model->validacionUsuario_oracle($datosAr2);
		$data['tipo_identificacion'] = $this->login_model->tipo_identificacion();
		$data['tramites_seguimientos'] = $this->validacion_model->tramites_seguimientocompleto_id($id_titulo);
		//var_dump($data['tramites_seguimientos']);
		
			
		$data['instituciones'] = $this->login_model->instituciones();
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 15082019
		//Datos Extranjeros Profesiones equivalente Rethus
		$data['profesionesequi'] = $this->login_model->profesionesequi();
		$data['paises'] = $this->login_model->paises();
		
        $data['js'] = array(base_url('assets/js/validacion.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
        $data['contenido'] = 'validacion/validar_documentos_view';
        $this->load->view('templates/layout_general',$data);

    }

	public function visualizar_documentos($id_titulo)
    {
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
        $data['resultado_validacion'] = $this->validacion_model->estado_tramite_validador();
        $data['causales_negacion'] = $this->validacion_model->causales_negacion();
        $data['tramites_pendientes'] = $this->validacion_model->tramites_pendientes_id($id_titulo);
		//Author: Mario BeltrĂ¡n mebeltran@saludcapital.gov.co Since: 12062019
		//Validacion Oracle lado de Validador
		$data['tramites_seguimiento'] = $this->validacion_model->tramites_seguimiento_id($id_titulo);
		$data['tramites_seguimientos'] = $this->validacion_model->tramites_seguimientocompleto_id($id_titulo);
		//var_dump($data['tramites_seguimiento']);
		$data['departamentos_col'] = $this->login_model->departamentos_col();
		$datosAr2['nume_identificacion'] = $data['tramites_pendientes']->nume_identificacion;
		$data['tramitesoracle'] = $this->login_model->validacionUsuario_oracle($datosAr2);
		$data['instituciones'] = $this->login_model->instituciones();
		
		$data['profesionesequi'] = $this->login_model->profesionesequi();
		$data['paises'] = $this->login_model->paises();
		
        $data['tipo_identificacion'] = $this->login_model->tipo_identificacion();
        $data['js'] = array(base_url('assets/js/validacion.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
        $data['contenido'] = 'validacion/visualizar_documentos_view';
        $this->load->view('templates/layout_general',$data);

    }

    public function guardarEstado($id_titulo)
    {
		$datos_auditoria['id_persona'] = $_REQUEST['id_persona'];
		$datos_auditoria['id_usuario'] = $this->session->userdata('id_usuario');
        //Actualizar los datos personales
        $datos_personales['id_persona'] = $_REQUEST['id_persona'];
        $datos_personales['tipo_identificacion'] = $_REQUEST['tipo_identificacion'];
        //$datos_personales['nume_documento'] = $_REQUEST['nume_documento'];
        $datos_personales['p_nombre'] = $_REQUEST['p_nombre'];
        $datos_personales['s_nombre'] = $_REQUEST['s_nombre'];
        $datos_personales['p_apellido'] = $_REQUEST['p_apellido'];
        $datos_personales['s_apellido'] = $_REQUEST['s_apellido'];
        $datos_personales['email'] = $_REQUEST['email'];
        $datos_personales['telefono_fijo'] = $_REQUEST['telefono_fijo'];
		
		$datos_personales['fecha_nacimiento'] = $_REQUEST['fecha_nacimiento'];
		$datos_personales['sexo'] = $_REQUEST['sexo'];
		$datos_personales['genero'] = $_REQUEST['genero'];
		
		if(isset($_REQUEST['orientacion'])){
            $datos_personales['orientacion'] = $_REQUEST['orientacion'];
        }else{
            $datos_personales['orientacion'] = '';
        }

        if(isset($_REQUEST['etnia'])){
            $datos_personales['etnia'] = $_REQUEST['etnia'];
        }else{
            $datos_personales['etnia'] = '';
        }	
		
        if(isset($_REQUEST['estado_civil'])){
            $datos_personales['estado_civil'] = $_REQUEST['estado_civil'];
        }else{
            $datos_personales['estado_civil'] = '';
        }		

        if(isset($_REQUEST['nivel_educativo'])){
            $datos_personales['nivel_educativo'] = $_REQUEST['nivel_educativo'];
        }else{
            $datos_personales['nivel_educativo'] = '';
        }	


        $datos_personales['telefono_celular'] = $_REQUEST['telefono_celular'];
		
		$datos_personales['nacionalidad'] = $_REQUEST['nacionalidad'];
		
			if($_REQUEST['nacionalidad'] == 170){
                $datos_personales['departamento'] = $_REQUEST['departamento'];
                $datos_personales['ciudad_nacimiento'] = $_REQUEST['ciudad_nacimiento'];
            }
			else{
				$datos_personales['departamento'] = '';
				$datos_personales['ciudad_nacimiento'] = '';
			}
        $datos_personales['depa_resi'] = $_REQUEST['depa_resi'];
        $datos_personales['ciudad_resi'] = $_REQUEST['ciudad_resi'];

        $actualizarDatosPersona = $this->validacion_model->actualizarDatosPersona($datos_personales);
		$actualizarAuditoriaUsuario = $this->validacion_model->actualizarAuditoriaUsuario($datos_auditoria);

        //Actualizar datos del titulo
        $datos_titulo['id_titulo'] = $_REQUEST['id_titulo'];
        $datos_titulo['institucion_educativa'] = $_REQUEST['institucion_educativa'];
        $datos_titulo['profesion'] = $_REQUEST['profesion'];
        if(isset($_REQUEST['tarjeta'])){
            $datos_titulo['tarjeta'] = $_REQUEST['tarjeta'];
        }else{
            $datos_titulo['tarjeta'] = '';
        }
        $datos_titulo['fecha_term'] = $_REQUEST['fecha_term'];
		
        if(isset($_REQUEST['diploma'])){
            $datos_titulo['diploma'] = $_REQUEST['diploma'];
        }else{
            $datos_titulo['diploma'] = '';
        }
		
        $datos_titulo['diploma'] = $_REQUEST['diploma'];
        $datos_titulo['acta'] = $_REQUEST['acta'];
        $datos_titulo['libro'] = $_REQUEST['libro'];
        $datos_titulo['folio'] = $_REQUEST['folio'];
        if($_REQUEST['anio'] == ''){
			$datos_titulo['anio'] = $_REQUEST['anio2'];
		}
		else{
			$datos_titulo['anio'] = $_REQUEST['anio'];
		}


        if(isset($_REQUEST['cod_universidad'])){
            $datos_titulo['cod_universidad'] = $_REQUEST['cod_universidad'];
        }else{
            $datos_titulo['cod_universidad'] = '';
        }

        if(isset($_REQUEST['fecha_term_ext'])){
            $datos_titulo['fecha_term_ext'] = $_REQUEST['fecha_term_ext'];
        }else{
            $datos_titulo['fecha_term_ext'] = '';
        }

        if(isset($_REQUEST['resolucion'])){
            $datos_titulo['resolucion'] = $_REQUEST['resolucion'];
        }else{
            $datos_titulo['resolucion'] = '';
        }

        if(isset($_REQUEST['fecha_resolucion'])){
            $datos_titulo['fecha_resolucion'] = $_REQUEST['fecha_resolucion'];
        }else{
            $datos_titulo['fecha_resolucion'] = '';
        }

        if(isset($_REQUEST['entidad'])){
            $datos_titulo['entidad'] = $_REQUEST['entidad'];
        }else{
            $datos_titulo['entidad'] = '';
        }
		//var_dump($_REQUEST['titulo_equivalente']);
        if(isset($_REQUEST['titulo_equivalente'])){
            $datos_titulo['titulo_equivalente'] = $_REQUEST['titulo_equivalente'];
        }else{
            $datos_titulo['titulo_equivalente'] = '';
        }

        if(isset($_REQUEST['pais_tituloequi'])){
            $datos_titulo['pais_tituloequi'] = $_REQUEST['pais_tituloequi'];
        }else{
            $datos_titulo['pais_tituloequi'] = '';
        }

        $actualizarDatosTitulo = $this->validacion_model->actualizarDatosTitulo($datos_titulo);


        if($_REQUEST['resultado_validacion'] == '13'){

            $datosTramite = $this->validacion_model->datos_tramite($id_titulo);

            require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
            $mail = new PHPMailer(true);

            $mail->IsSMTP(); // set mailer to use SMTP
            $mail->Host = "172.16.0.238"; // specif smtp server
            $mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
            $mail->Port = 25; // Used instead of 587 when only POP mail is selected
            $mail->SMTPAuth = false;
            $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
            $mail->Password = "Colombia2018"; // SMTP password
            $mail->FromName = "Trámites en línea";
            $mail->From = "tramiteslinea@saludcapital.gov.co";
            $mail->AddAddress($datosTramite->email, $datosTramite->email);
            $mail->WordWrap = 50;
            $mail->CharSet = 'UTF-8';
            $mail->IsHTML(true); // set email format to HTML
            $mail->Subject = 'Solicitud de información - Ventanilla Única de Trámites y servicios Secretraría Distrital de Salud';

            $html = '
              <p>Señor(a)</p>
              <p><b>'.$datosTramite->p_nombre.' '.$datosTramite->p_apellido.',</b></p>

              <p>Una vez realizado el proceso de validación de documentos del Trámite de Autorización de Títulos en Área de la Salud, se encontró la siguiente inconsistencia por favor ingrese a la plataforma <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> y realice los ajustes correspondientes para continuar con su trámite:</p>
              <p>
                '.$_REQUEST['mensaje'].'
              </p>
			  <p>Ante cualquier inquietud o novedad no dude consultar primeramente la documentación dispuesta en el portal de la Ventanilla Única de Trámites y Servicios o por medio del correo: contactenos@saludcapital.gov.co</p>
              <p><b>Secretaría Distrital de Salud.<br>
				    Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
					<a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Autorización de Títulos<br>
					Cra 32 #12-81 Bogotá D.C, Colombia<br>
					Teléfono: (571) 3649090
				</p>';

            $mail->Body = nl2br ($html,false);

            if($mail->Send()) {

                $dataEsTi['id_titulo'] = $id_titulo;
                $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                $actualizarEstado = $this->validacion_model->actualizarEstado($dataEsTi);

                $dataSeg['id_titulo'] = $id_titulo;
                $consecutivo_actual = $this->validacion_model->consulta_consecutivo($id_titulo);
                $consecutivo = $consecutivo_actual->max_cons + 1;
                $dataSeg['id_consecutivo'] = $consecutivo;
                $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
                $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                $dataSeg['observaciones'] = $_REQUEST['mensaje'];

                $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

                $this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos y se remitio correo al usuario para complementar la información</b>');
                redirect(base_url('validacion/'), 'refresh');
                exit;

            }else{

                $this->session->set_flashdata('retorno_error', 'Error al remitir el correo electr&oacute;nico, por favor verifique que el correo del usuario este correcto.');
                redirect(base_url(), 'refresh');
                exit;

            }

		}else if($_REQUEST['resultado_validacion'] == '16'){
            
			
			$datosTramite = $this->validacion_model->datos_tramite($id_titulo);

            require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
            $mail = new PHPMailer(true);

            $mail->IsSMTP(); // set mailer to use SMTP
            $mail->Host = "172.16.0.238"; // specif smtp server
            $mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
            $mail->Port = 25; // Used instead of 587 when only POP mail is selected
            $mail->SMTPAuth = false;
            $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
            $mail->Password = "Colombia2018"; // SMTP password
            $mail->FromName = "Trámites en línea";
            $mail->From = "tramiteslinea@saludcapital.gov.co";
            $mail->AddAddress($datosTramite->email, $datosTramite->email);
            $mail->WordWrap = 50;
            $mail->CharSet = 'UTF-8';
            $mail->IsHTML(true); // set email format to HTML
            $mail->Subject = 'Finalización del Trámite con Observación - Ventanilla Única de Trámites y servicios Secretraría Distrital de Salud';

            $html = '
              <p>Señor(a)</p>
              <p><b>'.$datosTramite->p_nombre.' '.$datosTramite->p_apellido.',</b></p>

              <p>Una vez realizado el proceso de validación de documentos del Trámite de Autorización de Títulos en área de la Salud se encontró la siguiente inconsistencia. Por tal motivo el trámite ha sido finalizado con la siguiente observación:</p>
              <p>
                '.$_REQUEST['mensaje'].'
              </p>
			  <p>Ante cualquier inquietud o novedad no dude contactar primeramente al correo electrónico: contactenos@saludcapital.gov.co</p>
              <p><b>Secretaría Distrital de Salud.<br>
				    Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
					<a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Autorización de Títulos<br>
					Cra 32 #12-81 Bogotá D.C, Colombia<br>
					Teléfono: (571) 3649090
				</p>';

            $mail->Body = nl2br ($html,false);

            if($mail->Send()) {
				$dataEsTi['id_titulo'] = $id_titulo;
				$dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
				$actualizarEstado = $this->validacion_model->actualizarEstado($dataEsTi);
				$dataSeg['id_titulo'] = $id_titulo;
				$consecutivo_actual = $this->validacion_model->consulta_consecutivo($id_titulo);
				$consecutivo = $consecutivo_actual->max_cons + 1;
				$dataSeg['id_consecutivo'] = $consecutivo;
				$dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
				$dataSeg['estado'] = $_REQUEST['resultado_validacion'];
				$dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
				$dataSeg['observaciones'] = $_REQUEST['observaciones'];
				$resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);
            }else{

                $this->session->set_flashdata('retorno_error', 'Error al remitir el correo electr&oacute;nico, por favor verifique que el correo del usuario este correcto.');
                redirect(base_url(), 'refresh');
                exit;

            }
			
            }else{


            $ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "validacion-".$id_titulo."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            //$this->load->library('M_pdf');
            //$mpdf = new mPDF('c', 'Letter');

            //load mPDF library para windows
            require_once APPPATH . 'libraries/vendor/autoload.php';
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = true;
            $mpdf->SetHTMLFooter('<center><img src="' . FCPATH.'assets/imgs/logo_pdf_footer.png" width="700px"/></center>');

            if($_REQUEST['resultado_validacion'] == '2'){

                //Aprobacion
                $datos['id_titulo'] = $id_titulo;
                $datos['id_usuario'] = $this->session->userdata('id_usuario');
                $datos['id_estado_tramite'] = $_REQUEST['resultado_validacion'];
                $datos['fecha'] = date('Y-m-d H:i:s');

                if($_REQUEST['observaciones'] != ''){

                    $datos['observaciones'] = $_REQUEST['observaciones'];

                }else{
                    $datos['observaciones'] = 'Aprobado por usuario validador de documentos';
                }

                if(isset($_REQUEST['guardar'])){
                    $id_validacion = $this->validacion_model->registro_validacion($datos);
                }

                $datos['nume_resolucion'] = 'XXXXXX';
                switch(date('m')){
                    case '01':
                        $datos['mes'] = 'Enero';
                        break;
                    case '02':
                        $datos['mes'] = 'Febrero';
                        break;
                    case '03':
                        $datos['mes'] = 'Marzo';
                        break;
                    case '04':
                        $datos['mes'] = 'Abril';
                        break;
                    case '05':
                        $datos['mes'] = 'Mayo';
                        break;
                    case '06':
                        $datos['mes'] = 'Junio';
                        break;
                    case '07':
                        $datos['mes'] = 'Julio';
                        break;
                    case '08':
                        $datos['mes'] = 'Agosto';
                        break;
                    case '09':
                        $datos['mes'] = 'Septiembre';
                        break;
                    case '10':
                        $datos['mes'] = 'Octubre';
                        break;
                    case '11':
                        $datos['mes'] = 'Noviembre';
                        break;
                    case '12':
                        $datos['mes'] = 'Diciembre';
                        break;

                }
                $datos['datos_tramite'] = $this->validacion_model->datos_tramite($id_titulo);

                $mpdf->SetWatermarkText('Aprobación Validación');
                $datos['firma'] = FALSE;
                //echo $this->load->view('validacion/resolucion_aprobacion', $datos, true);
                //exit;
                $mpdf->WriteHTML($this->load->view('validacion/resolucion_aprobacion', $datos, true));
                //$salvar = $mpdf->Output($ruta_archivos.$nombre_archivo, "F");
                //echo $ruta_archivos.$nombre_archivo;
                //exit;

            }else if($_REQUEST['resultado_validacion'] == '5'){

				//$datos_validacion = $this->direccion_model->datos_validacion($id_titulo, $_REQUEST['validacion_previa']);
				//var_dump($datos_validacion);
				//exit;
				
				
                //Negacion
                $datos['id_titulo'] = $id_titulo;
                $datos['id_usuario'] = $this->session->userdata('id_usuario');
                $datos['id_estado_tramite'] = $_REQUEST['resultado_validacion'];
                $datos['fecha'] = date('Y-m-d H:i:s');
                $datos['causales_negacion'] = '';

                if($_REQUEST['observaciones'] != ''){

                    $datos['observaciones'] = $_REQUEST['observaciones'];

                }else{
                    $datos['observaciones'] = 'Negado por usuario validador de documentos';
                }
				
				/*
				if($datos_validacion->causales_negacion != ''){
					$causales = explode(",",$datos_validacion->causales_negacion);
					
				}else{	
					$causales = "";
				}				
				*/
				if(isset($_REQUEST['causales_negacion'])){
					if(count($_REQUEST['causales_negacion']) > 0){

						for($c=0;$c<count($_REQUEST['causales_negacion']);$c++){
							$causal = $this->validacion_model->consulta_causal($_REQUEST['causales_negacion'][$c]);
							$data['causales'][$c] = $causal->desc_causal;
							$datos['causales_negacion'] .= $_REQUEST['causales_negacion'][$c].",";
						}

					}
				}
				else{
					$datos['causales_negacion'] = '';
				}
                
				$textotrascausas = $_REQUEST['otras_causales_negacion'];
				$textotrascausasline = str_replace("\n", "<br>", $textotrascausas);
                $datos['otras_causales_negacion'] = $textotrascausasline;
				
                if(isset($_REQUEST['guardar'])){
                    $id_validacion = $this->validacion_model->registro_validacion($datos);
                }
                $datos['nume_resolucion'] = 'XXXXXX';

                switch(date('m')){
                    case '01':
                        $datos['mes'] = 'Enero';
                        break;
                    case '02':
                        $datos['mes'] = 'Febrero';
                        break;
                    case '03':
                        $datos['mes'] = 'Marzo';
                        break;
                    case '04':
                        $datos['mes'] = 'Abril';
                        break;
                    case '05':
                        $datos['mes'] = 'Mayo';
                        break;
                    case '06':
                        $datos['mes'] = 'Junio';
                        break;
                    case '07':
                        $datos['mes'] = 'Julio';
                        break;
                    case '08':
                        $datos['mes'] = 'Agosto';
                        break;
                    case '09':
                        $datos['mes'] = 'Septiembre';
                        break;
                    case '10':
                        $datos['mes'] = 'Octubre';
                        break;
                    case '11':
                        $datos['mes'] = 'Noviembre';
                        break;
                    case '12':
                        $datos['mes'] = 'Diciembre';
                        break;

                }
                $datos['datos_tramite'] = $this->validacion_model->datos_tramite($id_titulo);

				if(isset($_REQUEST['causales_negacion'])){				
					$datos['causales'] = $data['causales'];
				}
				else {
					$datos['causales'] = '';
				}
				
                $datos['firma'] = FALSE;

                $mpdf->SetWatermarkText('Negación Validación');

                $mpdf->WriteHTML($this->load->view('validacion/resolucion_negacion', $datos, true));

            }else if($_REQUEST['resultado_validacion'] == '17'){

				//$datos_validacion = $this->direccion_model->datos_validacion($id_titulo, $_REQUEST['validacion_previa']);
				//var_dump($datos_validacion);
				//exit;
				
				
                //Negacion
                $datos['id_titulo'] = $id_titulo;
                $datos['id_usuario'] = $this->session->userdata('id_usuario');
                $datos['id_estado_tramite'] = $_REQUEST['resultado_validacion'];
                $datos['fecha'] = date('Y-m-d H:i:s');
                
                if($_REQUEST['observacion1aclaracion'] != ''){

					$textoibs1aclaracion = $_REQUEST['observacion1aclaracion'];
					$textoibs1aclaracionline = str_replace("\n", "<br>", $textoibs1aclaracion);
					$datos['observacion1aclaracion'] = $textoibs1aclaracionline;


                }else{
                    $datos['observacion1aclaracion'] = '';
                }
				
				
				if($_REQUEST['observacion2aclaracion'] != ''){

					$textoibs2aclaracion = $_REQUEST['observacion2aclaracion'];
					$textoibs2aclaracionline = str_replace("\n", "<br>", $textoibs2aclaracion);
					$datos['observacion2aclaracion'] = $textoibs2aclaracionline;


                }else{
                    $datos['observacion2aclaracion'] = '';
                }

				if($_REQUEST['observacion3aclaracion'] != ''){

					$textoibs3aclaracion = $_REQUEST['observacion3aclaracion'];
					$textoibs3aclaracionline = str_replace("\n", "<br>",$textoibs3aclaracion);
					$datos['observacion3aclaracion'] = $textoibs3aclaracionline;


                }else{
                    $datos['observacion3aclaracion'] = '';
                }				

                if($_REQUEST['observaciones'] != ''){

                    $datos['observaciones'] = $_REQUEST['observaciones'];

                }else{
                    $datos['observaciones'] = 'Resuelve recurso de aclaración validación';
                }

                if(isset($_REQUEST['cboxmotivo1'])){

					$datos['nombresapellidos_errados'] = $_REQUEST['nombresapellidos_errados'];

                }else{
                    $datos['nombresapellidos_errados'] = NULL;
                }

                if(isset($_REQUEST['cboxmotivo2'])){

					$datos['nombre_profesionnerrado'] = $_REQUEST['nombre_profesionnerrado'];

                }else{
                    $datos['nombre_profesionnerrado'] = NULL;
                }
                
				if(isset($_REQUEST['cboxmotivo3'])){
					$descInstitucion = $this->login_model->institucionesdesc($_REQUEST['nombre_institucionerrado']);
					$datos['nombre_institucionerrado'] = $descInstitucion->nombre_institucion;

                }else{
                    $datos['nombre_institucionerrado'] = NULL;
                }

                if(isset($_REQUEST['cboxmotivo4'])){
					$descTipoIden = $this->login_model->tipo_identificaciondesc($_REQUEST['tipo_identificacionerrada']);
					$datos['tipo_identificacionerrada'] = strval($descTipoIden->Descripcion);

                }else{
                    $datos['tipo_identificacionerrada'] = NULL;
                }

				
                if(isset($_REQUEST['cboxmotivo5'])){

					$datos['fecha_termerrada'] = $_REQUEST['fecha_termerrada'];

                }else{
                    $datos['fecha_termerrada'] = NULL;
                }
				
                if(isset($_REQUEST['guardar'])){
                    $id_validacion = $this->validacion_model->registro_validacion($datos);
                }
                $datos['nume_resolucion'] = 'XXXXXX';

                switch(date('m')){
                    case '01':
                        $datos['mes'] = 'Enero';
                        break;
                    case '02':
                        $datos['mes'] = 'Febrero';
                        break;
                    case '03':
                        $datos['mes'] = 'Marzo';
                        break;
                    case '04':
                        $datos['mes'] = 'Abril';
                        break;
                    case '05':
                        $datos['mes'] = 'Mayo';
                        break;
                    case '06':
                        $datos['mes'] = 'Junio';
                        break;
                    case '07':
                        $datos['mes'] = 'Julio';
                        break;
                    case '08':
                        $datos['mes'] = 'Agosto';
                        break;
                    case '09':
                        $datos['mes'] = 'Septiembre';
                        break;
                    case '10':
                        $datos['mes'] = 'Octubre';
                        break;
                    case '11':
                        $datos['mes'] = 'Noviembre';
                        break;
                    case '12':
                        $datos['mes'] = 'Diciembre';
                        break;

                }
                $datos['datos_tramite'] = $this->validacion_model->datos_tramite($id_titulo);
				
				$datos['resolucion'] = $this->usuarios_model->consulta_resolucion($id_titulo);
				
				if(!empty($datos['resolucion'])){
				//var_dump($mis_tramites[$i]->id_titulo);
                $datos['resolucionarchivo'] = $this->usuarios_model->consultar_archivo_resolucion($datos['resolucion']->id_archivo);
				}
				
                $datos['firma'] = FALSE;
				//var_dump($datos);
				//exit;

                $mpdf->SetWatermarkText('Aclaración Validación');

                $mpdf->WriteHTML($this->load->view('validacion/resolucion_aclaracion', $datos, true));

            }else if($_REQUEST['resultado_validacion'] == '9'){

                //Recurso de reposicion

                $datos['id_titulo'] = $id_titulo;
                $datos['id_usuario'] = $this->session->userdata('id_usuario');
                $datos['id_estado_tramite'] = $_REQUEST['resultado_validacion'];
                $datos['fecha'] = date('Y-m-d H:i:s');
                if($_REQUEST['observaciones'] != ''){

                    $datos['observaciones'] = $_REQUEST['observaciones'];

                }else{
                    $datos['observaciones'] = 'Resuelve Recurso por usuario validador de documentos';
                }

                $datos['argumentos_recurrente'] = $_REQUEST['argumentos_recurrente'];
                $datos['consideraciones'] = $_REQUEST['consideraciones'];
				$datos['consideraciones2'] = $_REQUEST['consideraciones2'];
                $datos['articulos'] = $_REQUEST['articulos'];

                if(isset($_REQUEST['guardar'])){
                    $id_validacion = $this->validacion_model->registro_validacion($datos);
                }
                $datos['nume_resolucion'] = 'XXXXXX';

                switch(date('m')){
                    case '01':
                        $datos['mes'] = 'Enero';
                        break;
                    case '02':
                        $datos['mes'] = 'Febrero';
                        break;
                    case '03':
                        $datos['mes'] = 'Marzo';
                        break;
                    case '04':
                        $datos['mes'] = 'Abril';
                        break;
                    case '05':
                        $datos['mes'] = 'Mayo';
                        break;
                    case '06':
                        $datos['mes'] = 'Junio';
                        break;
                    case '07':
                        $datos['mes'] = 'Julio';
                        break;
                    case '08':
                        $datos['mes'] = 'Agosto';
                        break;
                    case '09':
                        $datos['mes'] = 'Septiembre';
                        break;
                    case '10':
                        $datos['mes'] = 'Octubre';
                        break;
                    case '11':
                        $datos['mes'] = 'Noviembre';
                        break;
                    case '12':
                        $datos['mes'] = 'Diciembre';
                        break;

                }
                $datos['datos_tramite'] = $this->validacion_model->datos_tramite($id_titulo);
				
				$datos['resolucion'] = $this->usuarios_model->consulta_resolucion($id_titulo);
				
				if(!empty($datos['resolucion'])){
				//var_dump($mis_tramites[$i]->id_titulo);
                $datos['resolucionarchivo'] = $this->usuarios_model->consultar_archivo_resolucion($datos['resolucion']->id_archivo);
				}
				
                $datos['firma'] = FALSE;

                $mpdf->SetWatermarkText('Reposición Validaci&oacute;n');

                $mpdf->WriteHTML($this->load->view('validacion/resolucion_recurso', $datos, true));
            }
            //exit;

            if(isset($_REQUEST['guardar'])){
                $salvar = $mpdf->Output($ruta_archivos.$nombre_archivo, "F");

                if(file_exists ( $ruta_archivos.$nombre_archivo )){
                    $datosAr['ruta'] = $ruta_archivos;
                    $datosAr['nombre'] = $nombre_archivo;
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoID = $this->validacion_model->insertarArchivo($datosAr);

                    if($resultadoID){

                        $dataArRe['id_validacion'] = $id_validacion;
                        $dataArRe['id_archivo'] = $resultadoID;
                        $actualizarNumeroArchivo = $this->validacion_model->actualizar_id_archivo($dataArRe);
                        //var_dump($datos['nume_resolucion']);exit;
                        $dataEsTi['id_titulo'] = $id_titulo;
                        $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                        $actualizarEstado = $this->validacion_model->actualizarEstado($dataEsTi);

                        $dataSeg['id_titulo'] = $id_titulo;
                        $consecutivo_actual = $this->validacion_model->consulta_consecutivo($id_titulo);
                        $consecutivo = $consecutivo_actual->max_cons + 1;
                        $dataSeg['id_consecutivo'] = $consecutivo;
                        $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                        $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
						$dataSeg['observaciones'] = $_REQUEST['observaciones'];
                        $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');

                        $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

                        $this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos</b>');
                        redirect(base_url('validacion/'), 'refresh');
                        exit;
                    }
                }else{
                    $this->session->set_flashdata('error', 'Error al generar el documento PDF</b>');
                    redirect(base_url('validacion/'), 'refresh');
                    exit;
                }
            }else{
                $mpdf->Output();
            }

        }

    }

    public function cambiarEstado()
    {
        $data['id_titulo'] = $this->input->post('id_titulo');
        $data['estado'] = $this->input->post('estado');
        $resultado = $this->validacion_model->actualizarEstado($data);

        if($resultado){
            echo 'OK';
        }else{
            echo 'ERROR';
        }
    }

     /*  
      Perfil funcionario
      Exhumacion
     */

     /*
     Visualizar PDF
     */

    public function visualizarPDF() {
        $id = $_POST['idl'];
        //var_dump($id);
		//exit;
        $ruta_archivos = "uploads/exhumacion/";
        $nombre_archivo = "validacion-" . $id . "-3-" . date('Ymd') . ".pdf";

        //load mPDF library
        $this->load->library('M_pdf');
        $mpdf = new mPDF('c', 'Letter');
        $mpdf->showWatermarkText = false;
        $datos['listadosolicitudesExh_vali'] = $this->mlicencia_exhumacion->exhumacionfetch($id);
        $datos['datos_oracle'] = $this->mlicencia_exhumacion->informacion_oracle($datos['listadosolicitudesExh_vali']);
        $datos['sesionU'] = $this->mlicencia_exhumacion->sesionU($this->session->userdata('id_usuario'));
        $datos['firma'] = FALSE;
        $datos['difunto'] = $_POST['difunto'];
        $datos['cementerio'] = $_POST['cementerio'];
        $datos['licenciai'] = $_POST['licenciai'];
        $datos['fecha'] = $_POST['fecha'];

        $mpdf->SetWatermarkImage(FCPATH . 'assets/imgs/escudobogota.png', 0.2, array(50, 60), array(80, 50));
        $mpdf->showWatermarkImage = true;

        //exit;
        $mpdf->WriteHTML($this->load->view('validacion/prevresolucion_licencia_exhumacion', $datos, true)); #pendiemte
        $salvar = $mpdf->Output($ruta_archivos . $nombre_archivo, "F");
    }

    /**
     * Validar si el numero y fecha de inhumacion existe antes de realizar la solicitud
     */
    public function validarExistelicenciaInh() {
        $numeroInh = $_POST['numeroInh'];
        $fechaI = $_POST['fechaI'];
        $data['datos_oracle'] = $this->mlicencia_exhumacion->validacionUsuario_oracle($numeroInh, $fechaI);
        $data['js'] = array(base_url('assets/js/exhumacion_Validacion.js'));
        $data['titulo'] = 'Perfil Funcionario';
        $data['contenido'] = 'usuario/exhumacion_view';
        $this->load->view('templates/layout_general', $data);
    }

    public function listar() {
        $data['listadosolicitudesExh_vali'] = $this->mlicencia_exhumacion->listadosolicitudesExh_vali($this->session->userdata('id_persona')); #exhumacion
        $data['titulo'] = 'Perfil Usuario';
        $data['contenido'] = 'validacion/tramites_pendientes_view';
        $this->load->view('templates/layout_general', $data);
    }

    public function validacion_exh($id) {
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
        $data['listadosolicitudesExh_vali'] = $this->mlicencia_exhumacion->exhumacionfetch($id);
		$data['tipo_identificacion'] = $this->login_model->tipo_identificacion();
        $data['documento_preliminar'] = $this->mlicencia_exhumacion->archivo_preliminar($id);
        $data['info_aprobacionL'] = $this->mlicencia_exhumacion->info_aprobacionL($data['listadosolicitudesExh_vali']);
		$data['tramites_seguimientos'] = $this->mlicencia_exhumacion->tramites_seguimientocompleto_id($id);
		//var_dump($data['info_aprobacionL']);
		//exit;
        $data['informacionLEXH_oracle'] = $this->mlicencia_exhumacion->informacionLEXH_oracle($data['listadosolicitudesExh_vali']);
        $data['datos_oracle'] = $this->mlicencia_exhumacion->informacion_oracle($data['listadosolicitudesExh_vali']);
        $data['js'] = array(base_url('assets/js/exhumacion_Validacion.js'));
        $data['titulo'] = 'Perfil Validador';
        $data['contenido'] = 'validacion/validar_solicitudExhumacion_view';
        $this->load->view('templates/layout_general', $data);
    }

    public function guardarAprobacionLicenciaExh($id) {
    
    date_default_timezone_set('America/Bogota');

	
		$datos_auditoria['id_persona'] = $_REQUEST['id_persona'];
		$datos_auditoria['id_usuario'] = $this->session->userdata('id_usuario');
        //Actualizar los datos personales
        $datos_personales['id_persona'] = $_REQUEST['id_persona'];
        $datos_personales['tipo_identificacion'] = $_REQUEST['tipo_identificacion'];
        //$datos_personales['nume_documento'] = $_REQUEST['nume_documento'];
        $datos_personales['p_nombre'] = $_REQUEST['p_nombre'];
		
		if(isset($_REQUEST['s_nombre'])){

			$datos_personales['s_nombre'] = $_REQUEST['s_nombre'];

		}else{
			$datos_personales['s_nombre']  = NULL;
		}
        
        $datos_personales['p_apellido'] = $_REQUEST['p_apellido'];
		
		if(isset($_REQUEST['s_apellido'])){

			$datos_personales['s_apellido'] = $_REQUEST['s_apellido'];

		}else{
			$datos_personales['s_apellido']  = NULL;
		}		

        $datos_personales['email'] = $_REQUEST['email'];
		
		if(isset($_REQUEST['telefono_fijo'])){

			$datos_personales['telefono_fijo'] = $_REQUEST['telefono_fijo'];

		}else{
			$datos_personales['telefono_fijo']  = NULL;
		}			

		
		$datos_personales['telefono_celular'] = $_REQUEST['telefono_celular'];
		
		$datos_auditoria['modificado'] = date('Y-m-d');
	
		$actualizarDatosPersona = $this->mlicencia_exhumacion->actualizarDatosPersona($datos_personales);
		$actualizarAuditoriaUsuario = $this->mlicencia_exhumacion->actualizarAuditoriaUsuario($datos_auditoria);

		
		$datos_licencia['idlicencia_exhumacion'] = $_REQUEST['idlicencia_exhumacion'];
        //$datos_licencia['numero_licencia'] = $_REQUEST['numero_licencia'];
        //$datos_licencia['numero_regdefuncion'] = $_REQUEST['registro_defuncion'];
		//$datos_licencia['numero_docfallecido'] = $_REQUEST['numero_docfallecido'];
		$parenOTRO= isset($_REQUEST['parentescoOTRO'])?$_REQUEST['parentescoOTRO']:'';
        $datos_licencia['parentesco']= $_REQUEST['parentesco'].'/ '.$parenOTRO;		
		
		$datos_licencia['interviene_medlegal'] = $_REQUEST['intervienemedlegal'];
		//$datos_licencia['fecha_inhumacion'] = $_REQUEST['fecha_inhumacion'];
		
		$actualizarDatosLicencia = $this->mlicencia_exhumacion->actualizarDatosLicencia($datos_licencia);		
	
        if ($_REQUEST['resultado_validacion'] == '2') {

            $datosTramite = $this->mlicencia_exhumacion->exhumacionfetch($id);

            require_once(APPPATH . 'libraries/PHPMailer_5.2.4/class.phpmailer.php');
            $mail = new PHPMailer(true);

            $mail->IsSMTP(); // set mailer to use SMTP
            $mail->Host = "172.16.0.238"; // specif smtp server
            $mail->SMTPSecure = ""; // Used instead of TLS when only POP mail is selected
            $mail->Port = 25; // Used instead of 587 when only POP mail is selected
            $mail->SMTPAuth = false;
            $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
            $mail->Password = "Colombia2018"; // SMTP password
            $mail->FromName = "Trámites en línea";
            $mail->From = "tramiteslinea@saludcapital.gov.co";
            $mail->AddAddress($datosTramite->email, $datosTramite->email);
            $mail->WordWrap = 50;
            $mail->CharSet = 'UTF-8';
            $mail->IsHTML(true); // set email format to HTML
            $mail->Subject = 'Solicitud de información - Ventanilla única de Trámites y Servicios Secretaría Distrital de Salud';

            $html = '
              <p>Señor(a)</p>
              <p><b>' . $datosTramite->p_nombre . ' ' . $datosTramite->p_apellido . ',</b></p>

              <p>Una vez realizado el proceso de validación de documentos del Trámite de Licencia de Exhumación se encontró la siguiente inconsistencia, por favor ingrese a la plataforma <a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> y realice los ajustes correspondientes para continuar con su trámite:</p>
              <p>
                ' . $_REQUEST['mensaje'] . '
              </p>
			  <p>Ante cualquier inquietud o novedad no dude consultar primeramente la documentación dispuesta en el portal de la Ventanilla Única de Trámites y Servicios o por medio del correo: contactenos@saludcapital.gov.co</p>
              <p><b>Secretaría Distrital de Salud.<br>
				    Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
					<a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Licencia de Exhumación<br>
					Cra 32 #12-81 Bogotá D.C, Colombia<br>
					Teléfono: (571) 3649090
				</p>';

            $mail->Body = nl2br($html, false);

            if ($mail->Send()) {

                $dataEsTi['idlicencia_exhumacion'] = $id;
                $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                $actualizarEstado = $this->mlicencia_exhumacion->actualizarEstado($dataEsTi);

                $dataSeg['idlicencia_exhumacion'] = $id;
                $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
                $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                $dataSeg['observaciones'] = $_REQUEST['mensaje'];

                $resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);

                $this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos y se remitio correo al usuario para complementar la información</b>');
                redirect(base_url('validacion/'), 'refresh');
                exit;
            } else {

                $this->session->set_flashdata('retorno_error', 'Error al remitir el correo electr&oacute;nico, por favor verifique que el correo del usuario este correcto.');
                redirect(base_url('validacion/'), 'refresh');
                exit;
            }
        } else if ($_REQUEST['resultado_validacion'] == '5') {

            $datosTramite = $this->mlicencia_exhumacion->exhumacionfetch($id);

            require_once(APPPATH . 'libraries/PHPMailer_5.2.4/class.phpmailer.php');
            $mail = new PHPMailer(true);

            $mail->IsSMTP(); // set mailer to use SMTP
            $mail->Host = "172.16.0.238"; // specif smtp server
            $mail->SMTPSecure = ""; // Used instead of TLS when only POP mail is selected
            $mail->Port = 25; // Used instead of 587 when only POP mail is selected
            $mail->SMTPAuth = false;
            $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
            $mail->Password = "Colombia2018"; // SMTP password
            $mail->FromName = "Trámites en línea";
            $mail->From = "tramiteslinea@saludcapital.gov.co";
            $mail->AddAddress($datosTramite->email, $datosTramite->email);
            $mail->WordWrap = 50;
            $mail->CharSet = 'UTF-8';
            $mail->IsHTML(true); // set email format to HTML
            $mail->Subject = 'Trámite Anulado con Observación - Ventanilla Única de Trámites y Servicios Secretaría Distrital de Salud';

            $html = '
              <p>Señor(a)</p>
              <p><b>' . $datosTramite->p_nombre . ' ' . $datosTramite->p_apellido . ',</b></p>

              <p>Una vez realizado el proceso de validación de documentos del Trámite de Licencia de Exhumación se encontró la siguiente inconsistencia.</p>
              <p>
                ' . $_REQUEST['mensajeR'] . '
              </p>
              <p>Ante cualquier inquietud o novedad no dude consultar primeramente la documentación dispuesta en el portal de la Ventanilla Única de Trámites y Servicios o por medio del correo: contactenos@saludcapital.gov.co</p>
              <p><b>Secretaría Distrital de Salud.<br>
				    Subdirección de Inspección Vigilancia y Control - Oficina de Registro</b><br>
					<a href="http://tramitesenlinea.saludcapital.gov.co/" target="_blank">tramitesenlinea.saludcapital.gov.co</a> - Trámite Licencia de Exhumación<br>
					Cra 32 #12-81 Bogotá D.C, Colombia<br>
					Teléfono: (571) 3649090
				</p>';

            $mail->Body = nl2br($html, false);

            if ($mail->Send()) {

                $dataEsTi['idlicencia_exhumacion'] = $id;
                $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                $actualizarEstado = $this->mlicencia_exhumacion->actualizarEstado($dataEsTi);

                $dataSeg['idlicencia_exhumacion'] = $id;
                $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
                $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                $dataSeg['observaciones'] = $_REQUEST['mensajeR'];

                $resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);

                $this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos y se remitio correo al usuario para complementar la información</b>');
                redirect(base_url('validacion/'), 'refresh');
                exit;
            } else {

                $this->session->set_flashdata('retorno_error', 'Error al remitir el correo electr&oacute;nico, por favor verifique que el correo del usuario este correcto.');
                redirect(base_url('validacion/'), 'refresh');
                exit;
            }
        } else {

            $ruta_archivos = "uploads/exhumacion/";
            $nombre_archivo = "validacion-" . $id . "-" . $_REQUEST['resultado_validacion'] . "-" . date('Ymd') . ".pdf";

            //load mPDF library
            $this->load->library('M_pdf');
            $mpdf = new mPDF('C', 'Letter');
            $mpdf->showWatermarkText = false;
			$mpdf->allow_output_buffering= true;
			
            /*  require_once APPPATH . 'libraries/vendor/autoload.php';
              $mpdf = new \Mpdf\Mpdf();
              // Define a Landscape page size/format by name
              $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);

              // Define a page size/format by array - page will be 190mm wide x 236mm height
              $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [190, 236]]);

              // Define a page using all default values except "L" for Landscape orientation
              $mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

              $mpdf->debug = true;
              $mpdf->showImageErrors = true;
              //$mpdf->showWatermarkText = true; */

            //$mpdf->SetHeader('PENDIENTE APROBACI&Oacute;N  |Contrato: '.$datos['info_contrato'][0]->nom_mpio.' - '.$datos['info_contrato'][0]->nume_contrato.'|{PAGENO}');
            //$mpdf->setFooter('P&aacute;gina {PAGENO} DE {nb}');

			if ($_REQUEST['resultado_validacion'] == '3'){
			//Aprobacion		
			
			$datospdf['idlicencia_exhumacion'] = $id;
			$datospdf['revisado'] = $this->session->userdata('id_usuario');
			$datospdf['fecha_aprob'] = date('Y-m-d H:i:s');
			$datospdf['nombre_difunto'] = strtoupper($_REQUEST['nombre_difunto']);
			$datospdf['cementerio'] = strtoupper($_REQUEST['cementerio']);
			$datospdf['num_licencia_inhumacion'] = $_REQUEST['num_licencia_inhumacion'];
			$datospdf['fecha_inhumacion'] = $_REQUEST['fecha_inh'];
			$datospdf['numero_verificacion'] = $id . $_REQUEST['num_licencia_inhumacion'];
			$datospdf['observaciones'] = $_REQUEST['observaciones'];
			$datospdf['estado_apro'] = $_REQUEST['resultado_validacion'];
			$datospdf['info_aprobacionlicencia'] = $this->mlicencia_exhumacion->info_licencia($id);
			//$datos['usuarios'] = $this->mlicencia_exhumacion->usuarios($id);
			$datospdf['usuariosReviso'] = $this->mlicencia_exhumacion->consultaUsuario($this->session->userdata('id_usuario'));
			$datospdf['firma'] = FALSE;
			//$mpdf->SetWatermarkImage(FCPATH.'assets/imgs/escudobogota.png',0.2, 'D',array(50,60));
			$mpdf->SetWatermarkImage(FCPATH . 'assets/imgs/escudobogota.png', 0.2, array(50, 60), array(80, 50));
			$mpdf->showWatermarkImage = true;
			//echo $this->load->view('validacion/resolucion_aprobacion', $datos, true);
			//exit;
			$mpdf->WriteHTML($this->load->view('validacion/resolucion_licencia_exhumacion', $datospdf, true)); #pendiemte
			//var_dump($datos);
			//exit;
            }
			

        if(isset($_REQUEST['guardar'])){
			
			$datos['idlicencia_exhumacion'] = $id;
			$datos['revisado'] = $this->session->userdata('id_usuario');
			$datos['fecha_aprob'] = date('Y-m-d H:i:s');
			$datos['nombre_difunto'] = strtoupper($_REQUEST['nombre_difunto']);
			$datos['num_docdifunto'] = $_REQUEST['num_docdifunto'];
			$datos['num_certdefunsion'] = $_REQUEST['num_certdefunsion'];
			$datos['cementerio'] = strtoupper($_REQUEST['cementerio']);
			$datos['num_licencia_inhumacion'] = $_REQUEST['num_licencia_inhumacion'];
			$datos['fecha_inhumacion'] = $_REQUEST['fecha_inh'];
			$datos['numero_verificacion'] = $id . $_REQUEST['num_licencia_inhumacion'];


			if(isset($_REQUEST['observaciones']) && $_REQUEST['observaciones'] != ''){

				$datos['observaciones'] = strtoupper($_REQUEST['observaciones']);

			}else{
				$datos_personales['observaciones']  = NULL;
			}
			
			$datos['estado_apro'] = $_REQUEST['resultado_validacion'];
			$idestado = $_REQUEST['idestado'];
			
			
            $salvar = $mpdf->Output($ruta_archivos . $nombre_archivo, "F");

            if (file_exists($ruta_archivos . $nombre_archivo)) {
                $datosAr['ruta'] = $ruta_archivos;
                $datosAr['nombre'] = $nombre_archivo;
                $datosAr['fecha'] = date('Y-m-d');
                $datosAr['tags'] = "";
                $datosAr['es_publico'] = 1;
                $datosAr['estado'] = 'AC';

                $resultadoID = $this->mlicencia_exhumacion->insertarArchivo($datosAr);

                if ($resultadoID) {

					
						if ($idestado == 1){
							$resultIdAprobacion = $this->mlicencia_exhumacion->registro_aprobacionlicencia($datos);
						}else{
							$resultIdAprobacion = $this->mlicencia_exhumacion->actualizarAprobacionLicencia($datos);
						}

						$dataArRe['idlicencia_exhumacion'] = $id;
						$dataArRe['id_archivo'] = $resultadoID;
						$actualizarNumeroArchivo = $this->mlicencia_exhumacion->actualizar_id_archivo($dataArRe);
						//var_dump($datos['nume_resolucion']);exit;
						$dataEsTi['idlicencia_exhumacion'] = $id;
						$dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
						$actualizarEstado = $this->mlicencia_exhumacion->actualizarEstado($dataEsTi);


						$dataSeg['idlicencia_exhumacion'] = $id;
						$dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
						$dataSeg['estado'] = $_REQUEST['resultado_validacion'];
						$dataSeg['id_usuario'] = $this->session->userdata('id_usuario');

						if(isset($_REQUEST['observacionesint']) && $_REQUEST['observacionesint'] != ''){
							$dataSeg['observaciones'] = $_REQUEST['observacionesint'];
						}else{
							$dataSeg['observaciones']  = NULL;
						}

						$resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);


						$this->session->set_flashdata('exito', 'Se realizo el registro de aprobación de licencia de exhumación</b>');
						redirect(base_url('validacion/'), 'refresh');
						exit;
					}
				} else {
					$this->session->set_flashdata('error', 'Error al generar el documento PDF</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}
			}else{
					$mpdf->Output();
			}
        }
    }

    public function actualizarLicenciaInh() {

		$datos_licencia['idlicencia_exhumacion'] = $_REQUEST['idlicencia_exhumacion'];
        $datos_licencia['numero_licencia'] = $_REQUEST['numero_licencia'];
		$datos_licencia['fecha_inhumacion'] = $_REQUEST['fecha_inhumacion'];
		$resultOracle = $this->mlicencia_exhumacion->validacionFuncionario_oracle($datos_licencia['numero_licencia'], $datos_licencia['fecha_inhumacion']);
		if ($resultOracle){
			$actualizarDatosLicencia = $this->mlicencia_exhumacion->actualizarNumFecInhuma($datos_licencia);
			$this->session->set_flashdata('exito', 'Se realizo la actualización del trámite de Licencia de Exhumación de forma correcta</b>');
			redirect(base_url('validacion/'), 'refresh');
		}else{
			$this->session->set_flashdata('error', 'Los datos ingresados no fuerón enconrados en Oracle, favor verifiqué e intente de nuevo.</b>');
			redirect(base_url('validacion/'), 'refresh');
		}	
		
		
    }


    public function exhumacion() {

		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
        $fechai= isset($_POST['fecha_i']) ? $_POST['fecha_i']:'';
        $fechaf= isset($_POST['fecha_f']) ? $_POST['fecha_f']:'';
        $data['listado_soli'] = $this->mlicencia_exhumacion->listarSolicitudesLE($fechai,$fechaf,0);
        $data['js'] = array(base_url('assets/js/listado_licenciaExh.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
        $data['contenido'] = 'validacion/tramites_licenciaExh_view';
        $this->load->view('templates/layout_general', $data);
    }
    
    public function generar_excel(){
	   $fechai= isset($_GET['fecha_i']) ? $_GET['fecha_i']:'';
	   $fechaf= isset($_GET['fecha_f']) ? $_GET['fecha_f']:'';//echo $fechaf;exit;

	   $fec=strtotime ('-31 day', strtotime($_GET['fecha_f']));
	   $fec2= date('Y-m-d', $fec);
		
	   if($fechai>$fechaf){
			$this->session->set_flashdata('error', 'La Fecha de Inicio es superior a la fecha Final. Favor ingrese nuevamente los datos</b>');
			redirect(base_url('validacion/exhumacion'), 'refresh');
			exit;
	   }elseif($fechai<$fec2){
			$this->session->set_flashdata('error', 'Estimado usuario ha elegido un rango de fecha superior a un mes. Favor ingrese nuevamente los datos</b>');
			redirect(base_url('validacion/exhumacion'), 'refresh');
			exit;
	   }
	   else{	
			$data['listado_soli']= $this->mlicencia_exhumacion->listarSolicitudesLE($fechai,$fechaf,0);
			$listado_soli= $this->mlicencia_exhumacion->listarSolicitudesLE($fechai,$fechaf,0);
			$data['titulo'] = 'Perfil Validaci&oacute;n';
			$data['contenido'] = 'validacion/tramites_licenciaExh_view';
			$this->load->view('templates/layout_general', $data);
			if(count($listado_soli) > 0){
				require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
				$this->excel = new PHPExcel();

				$this->excel->setActiveSheetIndex(0);
				$this->excel->getActiveSheet()->setTitle('Solicitudes');
				//Contador de filas
				$contador = 1;
				// ancho las columnas.
				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(40);
				$this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(30);
				$this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(50);
				$this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(30);
				$this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(20);
				// negrita a los títulos de la cabecera.
				$this->excel->getActiveSheet()->getStyle("A{$contador}:O{$contador}")->getFont()->setBold(true);
				#$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
				// títulos de la cabecera.
				$this->excel->getActiveSheet()->setCellValue("A{$contador}", 'ID Solicitud');
				$this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Número licencia Inhumación');
				$this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Fecha Inhumación');
				$this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Numero doc Solicitante');
				$this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Nombres Solicitante');
				$this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Número Licencia Exh');
				$this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Fecha Solicitud licencia');
				$this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Nombre Difunto');
				$this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Número Identificación difunto');
				$this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Cementerio');
				$this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Fecha Aprobación licencia');
				$this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Estado');
				$this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Notificaciones');
				$this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Funcionario');
				$this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Fecha Notificación');

				foreach($listado_soli as $l){
				   $contador++;
				   //Informacion de la consulta.
				   $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->idap);
				   $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->numero_licencia);
				   $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->fechaI);
				   $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->nume_identificacion);
				   $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->p_nombre." ".$l->s_nombre." ".$l->p_apellido." ".$l->s_apellido);
				   $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->numero_lic_exhu);
				   $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->fecha_solicitud);
				   $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->nombre_difunto);
				   $this->excel->getActiveSheet()->setCellValue("I{$contador}", $l->numero_docfallecido);
				   $this->excel->getActiveSheet()->setCellValue("J{$contador}", $l->cementerio);
				   $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->fecha_aprob);
				   if (isset($l->des_estado))
					   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->des_estado);
				   else
					   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->estadoNotif);

				   $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->obs);
				   $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->pnom_funcionario." ".$l->snom_funcionario." ".$l->pape_funcionario." ".$l->sape_funcionario);
				   $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_registro);
				}
				//nombre de archivo que se va a generar.
				$archivo = "Solicitudes_licenciaExhumacion.xls";
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$archivo.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				// salida al navegador con el archivo Excel.
				$objWriter->save('php://output');
			 }/*else{
				echo 'No se encontraron registros';
				exit;
			 }*/
		  }

	}
  
  /**
	RAYOS X
  **/

	
    public function validar_documentos_rx($id_tramite)
    {
        $data['id_tramite'] = $id_tramite;
		$data['tramite_info'] = $this->rx_model->tramite_info($id_tramite);
        $data['departamento'] = $this->rx_model->departamentos_col();
        $data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
        $data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();
        $data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
        $data['nivelAcademico'] = $this->rx_model->nivelAcademico();
        $data['programasAcademicos'] = $this->rx_model->programasAcademicos();
		
		//Verificar si tiene datos por cada paso
		$data['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
		$data['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
		$data['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
		$data['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
		$data['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
		$data['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
		$data['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
		$data['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
		$data['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
        
        $data['resultado_validacion'] = $this->rx_model->estado_tramite_validador_rx($this->session->userdata('perfil'));
        $data['tramites_pendientes'] = $this->rx_model->tramite_info_validacion($id_tramite);
        $data['tipo_identificacion'] = $this->login_model->tipo_identificacion_todos();
        $data['departamentos_col'] = $this->login_model->departamentos_col();
        $data['tramites_seguimientos'] = $this->rx_model->seguimiento_tramite($id_tramite);
		
        //$data['js'] = array(base_url('assets/js/rayosx.js'),base_url('assets/js/validacion_rx.js'));
        $data['js'] = array(base_url('assets/js/rayosx.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
		if($this->session->userdata('perfil') == 8){
			$data['contenido'] = 'validacion/validar_documentosrx_previa_view';	
		}else if($this->session->userdata('perfil') == 9){
			$data['contenido'] = 'validacion/validar_documentosrx_infra_view';
		}else if($this->session->userdata('perfil') == 3){
			$data['contenido'] = 'validacion/validar_documentosrx_view';
		}
        
        $this->load->view('templates/layout_rx',$data);

    }
	
	public function prueba_validar_documentos_rx($id_tramite)
    {
        $data['id_tramite'] = $id_tramite;
		$data['tramite_info'] = $this->rx_model->tramite_info($id_tramite);
        $data['departamento'] = $this->rx_model->departamentos_col();
        $data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
        $data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();
        $data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
        $data['nivelAcademico'] = $this->rx_model->nivelAcademico();
        $data['programasAcademicos'] = $this->rx_model->programasAcademicos();
		
		//Verificar si tiene datos por cada paso
		$data['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
		$data['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
		$data['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
		$data['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
		$data['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
		$data['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
		$data['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
		$data['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
		$data['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
        
        $data['resultado_validacion'] = $this->rx_model->estado_tramite_validador_rx($this->session->userdata('perfil'));
        $data['tramites_pendientes'] = $this->rx_model->tramite_info_validacion($id_tramite);
        $data['tipo_identificacion'] = $this->login_model->tipo_identificacion_todos();
        $data['departamentos_col'] = $this->login_model->departamentos_col();
        $data['tramites_seguimientos'] = $this->rx_model->seguimiento_tramite($id_tramite);
		
        //$data['js'] = array(base_url('assets/js/rayosx.js'),base_url('assets/js/validacion_rx.js'));
        $data['js'] = array(base_url('assets/js/rayosx.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
		if($this->session->userdata('perfil') == 8){
			$data['contenido'] = 'validacion/RX/validar_docprueba_rx';	
		}else if($this->session->userdata('perfil') == 9){
			$data['contenido'] = 'validacion/RX/validar_docprueba_rx';
		}else if($this->session->userdata('perfil') == 3){
			$data['contenido'] = 'validacion/RX/validar_docprueba_rx';
		}
        
        $this->load->view('templates/layout_rx',$data);

    }


	public function guardarEstadoPreviaRx($id_tramite)
    {		
		$datos_auditoria['id_persona'] = $_REQUEST['id_persona'];
		$datos_auditoria['id_usuario'] = $this->session->userdata('id_usuario');
        //Actualizar los datos personales
        $datos_personales['id_persona'] = $_REQUEST['id_persona'];
        $datos_personales['tipo_identificacion'] = $_REQUEST['tipo_identificacion'];
        //$datos_personales['nume_documento'] = $_REQUEST['nume_documento'];
		
		
		if(isset($_REQUEST['tipo_iden_rl'])){
			$datos_personales['tipo_iden_rl'] = $_REQUEST['tipo_iden_rl'];
		}else{
			$datos_personales['tipo_iden_rl'] = '';
		}
		
		if(isset($_REQUEST['nume_iden_rl'])){
			$datos_personales['nume_iden_rl'] = $_REQUEST['nume_iden_rl'];
		}else{
			$datos_personales['nume_iden_rl'] = '';
		}
		
		if(isset($_REQUEST['nombre_rs'])){
			$datos_personales['nombre_rs'] = $_REQUEST['nombre_rs'];
		}else{
			$datos_personales['nombre_rs'] = '';
		}
		
		if(isset($_REQUEST['fecha_nacimiento'])){
			$datos_personales['fecha_nacimiento'] = $_REQUEST['fecha_nacimiento'];
		}else{
			$datos_personales['fecha_nacimiento'] = '';
		}
		
		if(isset($_REQUEST['sexo'])){
			$datos_personales['sexo'] = $_REQUEST['sexo'];
		}else{
			$datos_personales['sexo'] = '';
		}
		
		if(isset($_REQUEST['genero'])){
			$datos_personales['genero'] = $_REQUEST['genero'];
		}else{
			$datos_personales['genero'] = '';
		}
		
		if(isset($_REQUEST['orientacion'])){
			$datos_personales['orientacion'] = $_REQUEST['orientacion'];
		}else{
			$datos_personales['orientacion'] = '';
		}

		if(isset($_REQUEST['etnia'])){
			$datos_personales['etnia'] = $_REQUEST['etnia'];
		}else{
			$datos_personales['etnia'] = '';
		}	
		
		if(isset($_REQUEST['estado_civil'])){
			$datos_personales['estado_civil'] = $_REQUEST['estado_civil'];
		}else{
			$datos_personales['estado_civil'] = '';
		}		

		if(isset($_REQUEST['nivel_educativo'])){
			$datos_personales['nivel_educativo'] = $_REQUEST['nivel_educativo'];
		}else{
			$datos_personales['nivel_educativo'] = '';
		}	
		
        	
		$datos_personales['p_nombre'] = $_REQUEST['p_nombre'];
		$datos_personales['s_nombre'] = $_REQUEST['s_nombre'];
		$datos_personales['p_apellido'] = $_REQUEST['p_apellido'];
		$datos_personales['s_apellido'] = $_REQUEST['s_apellido'];
		$datos_personales['email'] = $_REQUEST['email'];
		$datos_personales['telefono_fijo'] = $_REQUEST['telefono_fijo'];
        $datos_personales['telefono_celular'] = $_REQUEST['telefono_celular'];
		

        $actualizarDatosPersona = $this->rx_model->actualizarDatosPersona($datos_personales);
		
		
		
        if($_REQUEST['resultado_validacion'] == '3'){

            $dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "3";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$dataflujo['tramite_id'] = $id_tramite;				
				$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
				$dataflujo['id_estado'] = 3;
				$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
				$dataflujo['observaciones'] = $_REQUEST['observaciones'];
				
				$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
				
				if($resultadoCrearTramiteFlujo){
					$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}else{
					$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;					
				}
			
			}else{
				$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}

        }else if($_REQUEST['resultado_validacion'] == '15'){

            $dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "15";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$dataflujo['tramite_id'] = $id_tramite;				
				$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
				$dataflujo['id_estado'] = 15;
				$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
				$dataflujo['observaciones'] = $_REQUEST['observaciones'];
				
				$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
				
				if($resultadoCrearTramiteFlujo){
					$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}else{
					$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;					
				}
			
			}else{
				$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}

        }else if($_REQUEST['resultado_validacion'] == '24'){

            $dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "24";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$dataflujo['tramite_id'] = $id_tramite;				
				$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
				$dataflujo['id_estado'] = 24;
				$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
				$dataflujo['observaciones'] = $_REQUEST['observaciones'];
				
				$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
				
				if($resultadoCrearTramiteFlujo){
					$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}else{
					$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;					
				}
			
			}else{
				$this->session->set_flashdata('error', 'Error al realizar la validación de documentos tr&aacute;mite</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}

        }

    }
	
	public function guardarObservacionInfra(){
		
		for($i=1;$i<=15;$i++){
			
			$datos['id_tramite'] = $_REQUEST['id_tramite'];
			$datos['id_equipo'] = $_REQUEST['id_equipo'];	
			$datos['id_item'] = $i;	
			$datos['id_estado'] = $_REQUEST['estado'];	
			$datos['observaciones'] = $_REQUEST['item_'.$_REQUEST['id_equipo'].'_'.$i];	
			$datos['id_usuario'] = $this->session->userdata('id_usuario');	
			$datos['fecha_observacion'] = date('Y-m-d H:i:s');	
			
			$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($datos);
			
			if(count($resultadoEquipoInfra) > 0){
				$resultadoCrearEquipoInfra = $this->rx_model->actualiza_obs_infra($datos);		
			}else{
				$resultadoCrearEquipoInfra = $this->rx_model->crear_obs_infra($datos);		
			}			
		}

		if($resultadoCrearEquipoInfra)
		{
			$this->session->set_flashdata('exito', 'Se realizo el registro de las observaciones del equipo '.$_REQUEST['id_equipo'].'</b>');
			redirect(base_url('validacion/validar_documentos_rx/'.$_REQUEST['id_tramite']), 'refresh');
			exit;	
		}else{
			$this->session->set_flashdata('error', 'Error al realizar el registro de las observaciones del equipo '.$_REQUEST['id_equipo'].'</b>');
			redirect(base_url('validacion/validar_documentos_rx/'.$_REQUEST['id_tramite']), 'refresh');
			exit;
		}

		
	}
	
	public function guardarEstadoInfraRx($id_tramite)
    {
		$datos_auditoria['id_persona'] = $_REQUEST['id_persona'];
		$datos_auditoria['id_usuario'] = $this->session->userdata('id_usuario');
        		
        if($_REQUEST['resultado_validacion'] == '4' || $_REQUEST['resultado_validacion'] == '16' || $_REQUEST['resultado_validacion'] == '25'){

            $dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = $_REQUEST['resultado_validacion'];
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$dataflujo['tramite_id'] = $id_tramite;				
				$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
				$dataflujo['id_estado'] = $_REQUEST['resultado_validacion'];
				$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
				$dataflujo['observaciones'] = $_REQUEST['observaciones'];
				
				$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
				
				if($resultadoCrearTramiteFlujo){
					$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de equipos</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}else{
					$this->session->set_flashdata('error', 'Error al realizar la validación de equipos</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;					
				}
			
			}else{
				$this->session->set_flashdata('error', 'Error al realizar la validación de equipos</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}

        }

    }
	
    
    public function guardarEstadoRx($id_tramite)
    {
		$datos_auditoria['id_persona'] = $_REQUEST['id_persona'];
		$datos_auditoria['id_usuario'] = $this->session->userdata('id_usuario');
        //Actualizar los datos personales
        $datos_personales['id_persona'] = $_REQUEST['id_persona'];
        $datos_personales['tipo_identificacion'] = $_REQUEST['tipo_identificacion'];
        //$datos_personales['nume_documento'] = $_REQUEST['nume_documento'];
		
		
		if(isset($_REQUEST['tipo_iden_rl'])){
			$datos_personales['tipo_iden_rl'] = $_REQUEST['tipo_iden_rl'];
		}else{
			$datos_personales['tipo_iden_rl'] = '';
		}
		
		if(isset($_REQUEST['nume_iden_rl'])){
			$datos_personales['nume_iden_rl'] = $_REQUEST['nume_iden_rl'];
		}else{
			$datos_personales['nume_iden_rl'] = '';
		}
		
		if(isset($_REQUEST['nombre_rs'])){
			$datos_personales['nombre_rs'] = $_REQUEST['nombre_rs'];
		}else{
			$datos_personales['nombre_rs'] = '';
		}
		
		if(isset($_REQUEST['fecha_nacimiento'])){
			$datos_personales['fecha_nacimiento'] = $_REQUEST['fecha_nacimiento'];
		}else{
			$datos_personales['fecha_nacimiento'] = '';
		}
		
		if(isset($_REQUEST['sexo'])){
			$datos_personales['sexo'] = $_REQUEST['sexo'];
		}else{
			$datos_personales['sexo'] = '';
		}
		
		if(isset($_REQUEST['genero'])){
			$datos_personales['genero'] = $_REQUEST['genero'];
		}else{
			$datos_personales['genero'] = '';
		}
		
		if(isset($_REQUEST['orientacion'])){
			$datos_personales['orientacion'] = $_REQUEST['orientacion'];
		}else{
			$datos_personales['orientacion'] = '';
		}

		if(isset($_REQUEST['etnia'])){
			$datos_personales['etnia'] = $_REQUEST['etnia'];
		}else{
			$datos_personales['etnia'] = '';
		}	
		
		if(isset($_REQUEST['estado_civil'])){
			$datos_personales['estado_civil'] = $_REQUEST['estado_civil'];
		}else{
			$datos_personales['estado_civil'] = '';
		}		

		if(isset($_REQUEST['nivel_educativo'])){
			$datos_personales['nivel_educativo'] = $_REQUEST['nivel_educativo'];
		}else{
			$datos_personales['nivel_educativo'] = '';
		}	
		
        	
		$datos_personales['p_nombre'] = $_REQUEST['p_nombre'];
		$datos_personales['s_nombre'] = $_REQUEST['s_nombre'];
		$datos_personales['p_apellido'] = $_REQUEST['p_apellido'];
		$datos_personales['s_apellido'] = $_REQUEST['s_apellido'];
		$datos_personales['email'] = $_REQUEST['email'];
		$datos_personales['telefono_fijo'] = $_REQUEST['telefono_fijo'];
        $datos_personales['telefono_celular'] = $_REQUEST['telefono_celular'];
		

        $actualizarDatosPersona = $this->rx_model->actualizarDatosPersona($datos_personales);
		
		$estado_infra = $_REQUEST['estadoInfra'];
		$rayosxEquipo = $this->rx_model->rayosxEquipo($id_tramite);
		
		for($i=0;$i<count($rayosxEquipo);$i++){
			
			$id_equipo = $rayosxEquipo[$i]->id_equipo_rayosx;
			
			for($j=1;$j<=15;$j++){
				
				$itemCons['id_equipo'] = $id_equipo; 
				$itemCons['id_tramite'] = $id_tramite; 
				$itemCons['id_item'] = $j; 
				$itemCons['id_estado'] = $estado_infra; 
				$itemCons['observaciones'] = $_REQUEST['item_'.$id_equipo.'_'.$j]; 
				$itemCons['id_usuario'] = $this->session->userdata('id_usuario'); 
				$itemCons['fecha_observacion'] = date('Y-m-d H:i:s'); 

				$resultadoEquipoInfra = $this->rx_model->consulta_obs_infra($itemCons);
				
				if(count($resultadoEquipoInfra) > 0){
					//Actualizar observacion
					$this->rx_model->actualiza_obs_infra($itemCons);
				}else{
					//Insertar observacion
					$this->rx_model->crear_obs_infra($itemCons);
				}								
			}
			
		} 
		
		if($_REQUEST['resultado_validacion'] == '5' || $_REQUEST['resultado_validacion'] == '17' || $_REQUEST['resultado_validacion'] == '26'){
			//APROBACION
			if($_REQUEST['observaciones'] != ''){

				$dataSeg['observaciones'] = $_REQUEST['observaciones'];

			}else{
				$dataSeg['observaciones'] = 'Aprobación validación de documentos';
			}

			$datos['nume_resolucion'] = 'XXXXXX';
			switch(date('m')){
				case '01': $datos['mes'] = 'Enero';	break;
				case '02': $datos['mes'] = 'Febrero'; break;
				case '03': $datos['mes'] = 'Marzo'; break;
				case '04': $datos['mes'] = 'Abril';	break;
				case '05': $datos['mes'] = 'Mayo'; break;
				case '06': $datos['mes'] = 'Junio'; break;
				case '07': $datos['mes'] = 'Julio';	break;
				case '08': $datos['mes'] = 'Agosto'; break;
				case '09': $datos['mes'] = 'Septiembre'; break;
				case '10': $datos['mes'] = 'Octubre'; break;
				case '11': $datos['mes'] = 'Noviembre'; break;
				case '12': $datos['mes'] = 'Diciembre';	break;

			}
			
			$datos['datos_tramite'] = $this->rx_model->tramite_info_validacion($id_tramite);
			$datos['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
			$datos['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
			$datos['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
			$datos['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
			$datos['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
			$datos['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
			$datos['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
			$datos['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
			$datos['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);			
			
			$datos['obs_val1'] = $_REQUEST['observaciones_item1'];
			$datos['obs_val2'] = $_REQUEST['observaciones_item2'];
			$datos['obs_val3'] = $_REQUEST['observaciones_item3'];
			$datos['obs_val4'] = $_REQUEST['observaciones_item4'];
			$datos['obs_val5'] = $_REQUEST['observaciones_item5'];
			$datos['obs_val6'] = $_REQUEST['observaciones_item6'];
			
			$datos['firma'] = FALSE;
			
			$ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "validacionRX-".$_REQUEST['id_tramite']."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter', '', '', 20, 20, 40, 35, 5, 10 , 'P');

            //load mPDF library para windows
            //require_once APPPATH . 'libraries/vendor/autoload.php';
            //$mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = true;
			$mpdf->SetWatermarkText('Aprobación');//colocar texto preelilminar
			$imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
			$imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
            $mpdf->SetHTMLHeader("<table class='table ' width='100%'>
										<tr class=''> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('validacion/RX/resolucion_aprobacion_rx_1', $datos, true));		
			
			if($datos['datos_tramite']->tipo_identificacion == 5){
					$nombre_rs = $datos['datos_tramite']->nombre_rs;
					$nombre_rl = $datos['datos_tramite']->p_nombre." ".$datos['datos_tramite']->s_nombre." ".$datos['datos_tramite']->p_apellido." ".$datos['datos_tramite']->s_apellido;
					switch ($datos['datos_tramite']->tipo_iden_rl) {
						case 1:
							$tipo_iden_rl = "Cédula de ciudadanía";
							break;
						case 2:
							$tipo_iden_rl = "Cédula de extranjería";
							break;
						case 3:
							$tipo_iden_rl = "Tarjeta de identidad";
							break;
						case 4:
							$tipo_iden_rl = "Permiso especial de permanencia";
							break;
						case 5:
							$tipo_iden_rl = "NIT";
							break;
					}				
					$nume_iden_rl = $datos['datos_tramite']->nume_iden_rl;
				}else{
					$nombre_rs = $datos['datos_tramite']->p_nombre." ".$datos['datos_tramite']->s_nombre." ".$datos['datos_tramite']->p_apellido." ".$datos['datos_tramite']->s_apellido;
					$nombre_rl = $datos['datos_tramite']->p_nombre." ".$datos['datos_tramite']->s_nombre." ".$datos['datos_tramite']->p_apellido." ".$datos['datos_tramite']->s_apellido;
					
					$tipo_iden_rl = $datos['datos_tramite']->descTipoIden;
					$nume_iden_rl = $datos['datos_tramite']->nume_identificacion;
				}
						
			$mpdf->SetHTMLHeader("<table class='table' width='100%'>
										<tr class=''> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
										<tr>
											<td><p>continuación de la resolución No ".$datos['nume_resolucion']." de ".date('Y-m-d')." por la cual se concede licencia de práctica médica para ".$nombre_rs." - ".$datos['rayosxDireccion']->sede_entidad.".</p></td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			$mpdf->AddPage();						
			$mpdf->WriteHTML($this->load->view('validacion/RX/resolucion_aprobacion_rx_2', $datos, true));		
			
		}else if($_REQUEST['resultado_validacion'] == '6' || $_REQUEST['resultado_validacion'] == '18' || $_REQUEST['resultado_validacion'] == '27'){
			//NEGACION
			if($_REQUEST['observaciones'] != ''){

				$dataSeg['observaciones'] = $_REQUEST['observaciones'];

			}else{
				$dataSeg['observaciones'] = 'Negación validación de documentos';
			}

			$datos['nume_resolucion'] = 'XXXXXX';
			switch(date('m')){
				case '01': $datos['mes'] = 'Enero';	break;
				case '02': $datos['mes'] = 'Febrero'; break;
				case '03': $datos['mes'] = 'Marzo'; break;
				case '04': $datos['mes'] = 'Abril';	break;
				case '05': $datos['mes'] = 'Mayo'; break;
				case '06': $datos['mes'] = 'Junio'; break;
				case '07': $datos['mes'] = 'Julio';	break;
				case '08': $datos['mes'] = 'Agosto'; break;
				case '09': $datos['mes'] = 'Septiembre'; break;
				case '10': $datos['mes'] = 'Octubre'; break;
				case '11': $datos['mes'] = 'Noviembre'; break;
				case '12': $datos['mes'] = 'Diciembre';	break;

			}
			
			$datos['datos_tramite'] = $this->rx_model->tramite_info_validacion($id_tramite);
			$datos['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
			$datos['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
			$datos['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
			$datos['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
			$datos['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
			$datos['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
			$datos['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
			$datos['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
			$datos['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
			
			
			$datos['obs_val1'] = $_REQUEST['observaciones_item1'];
			$datos['obs_val2'] = $_REQUEST['observaciones_item2'];
			$datos['obs_val3'] = $_REQUEST['observaciones_item3'];
			$datos['obs_val4'] = $_REQUEST['observaciones_item4'];
			$datos['obs_val5'] = $_REQUEST['observaciones_item5'];
			$datos['obs_val6'] = $_REQUEST['observaciones_item6'];
			
			$datos['firma'] = FALSE;
			
			$ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "validacionRX-".$_REQUEST['id_tramite']."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter', '', '', 20, 20, 40, 35, 5, 10 , 'P');

            //load mPDF library para windows
            //require_once APPPATH . 'libraries/vendor/autoload.php';
            //$mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = true;
			$imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
			$imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
            $mpdf->SetHTMLHeader("<table class='sinborde centro' width='100%' border='0'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='sinborde centro' border='0' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			$mpdf->SetWatermarkText('Negación');
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('validacion/resolucion_negacion_rx', $datos, true));		
			
			
			
		}else if($_REQUEST['resultado_validacion'] == '7' || $_REQUEST['resultado_validacion'] == '21'){
			//SUBSANACION
			if($_REQUEST['observaciones'] != ''){

				$dataSeg['observaciones'] = $_REQUEST['observaciones'];

			}else{
				if($_REQUEST['resultado_validacion'] == '7'){
					$dataSeg['observaciones'] = 'Subsanación primera instancia';
				}else if($_REQUEST['resultado_validacion'] == '21'){
					$dataSeg['observaciones'] = 'Subsanación segunda instancia';
				}
				
			}

			$datos['nume_resolucion'] = 'XXXXXX';
			switch(date('m')){
				case '01': $datos['mes'] = 'Enero';	break;
				case '02': $datos['mes'] = 'Febrero'; break;
				case '03': $datos['mes'] = 'Marzo'; break;
				case '04': $datos['mes'] = 'Abril';	break;
				case '05': $datos['mes'] = 'Mayo'; break;
				case '06': $datos['mes'] = 'Junio'; break;
				case '07': $datos['mes'] = 'Julio';	break;
				case '08': $datos['mes'] = 'Agosto'; break;
				case '09': $datos['mes'] = 'Septiembre'; break;
				case '10': $datos['mes'] = 'Octubre'; break;
				case '11': $datos['mes'] = 'Noviembre'; break;
				case '12': $datos['mes'] = 'Diciembre';	break;

			}
			
			$datos['datos_tramite'] = $this->rx_model->tramite_info_validacion($id_tramite);
			$datos['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
			$datos['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
			$datos['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
			$datos['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
			$datos['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
			$datos['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
			$datos['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
			$datos['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
			$datos['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
			
			$datos['obs_val1'] = $_REQUEST['observaciones_item1'];
			$datos['obs_val2'] = $_REQUEST['observaciones_item2'];
			$datos['obs_val3'] = $_REQUEST['observaciones_item3'];
			$datos['obs_val4'] = $_REQUEST['observaciones_item4'];
			$datos['obs_val5'] = $_REQUEST['observaciones_item5'];
			$datos['obs_val6'] = $_REQUEST['observaciones_item6'];
			
			$datos['firma'] = FALSE;
			
			$ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "validacionRX-".$_REQUEST['id_tramite']."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter', '', '', 30, 20, 40, 35, 5, 10 , 'P');

            //load mPDF library para windows
            //require_once APPPATH . 'libraries/vendor/autoload.php';
            //$mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = true;
			$imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
			$imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
            $mpdf->SetHTMLHeader("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			if($_REQUEST['resultado_validacion'] == '7'){
				$mpdf->SetWatermarkText('Subsanación Primera Instancia');
			}else if($_REQUEST['resultado_validacion'] == '21'){
				$mpdf->SetWatermarkText('Subsanación Segunda Instancia');
			}						
			
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('validacion/resolucion_rx_subsanacion_view', $datos, true));	
			
			
		}else if($_REQUEST['resultado_validacion'] == '19'){
			//PROGRAMAR VISITA
												
			if($_REQUEST['observaciones'] != ''){

				$dataSeg['observaciones'] = $_REQUEST['observaciones'];

			}else{
				$dataSeg['observaciones'] = 'Programar visita';
			}

			$datos['nume_resolucion'] = 'XXXXXX';
			switch(date('m')){
				case '01': $datos['mes'] = 'Enero';	break;
				case '02': $datos['mes'] = 'Febrero'; break;
				case '03': $datos['mes'] = 'Marzo'; break;
				case '04': $datos['mes'] = 'Abril';	break;
				case '05': $datos['mes'] = 'Mayo'; break;
				case '06': $datos['mes'] = 'Junio'; break;
				case '07': $datos['mes'] = 'Julio';	break;
				case '08': $datos['mes'] = 'Agosto'; break;
				case '09': $datos['mes'] = 'Septiembre'; break;
				case '10': $datos['mes'] = 'Octubre'; break;
				case '11': $datos['mes'] = 'Noviembre'; break;
				case '12': $datos['mes'] = 'Diciembre';	break;

			}
			
			$datos['datos_tramite'] = $this->rx_model->tramite_info_validacion($id_tramite);
			$datos['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
			$datos['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
			$datos['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
			$datos['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
			$datos['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
			$datos['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
			$datos['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
			$datos['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
			$datos['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
			
			$datos['obs_val1'] = $_REQUEST['observaciones_item1'];
			$datos['obs_val2'] = $_REQUEST['observaciones_item2'];
			$datos['obs_val3'] = $_REQUEST['observaciones_item3'];
			$datos['obs_val4'] = $_REQUEST['observaciones_item4'];
			$datos['obs_val5'] = $_REQUEST['observaciones_item5'];
			$datos['obs_val6'] = $_REQUEST['observaciones_item6'];
			
			$datos['firma'] = FALSE;
			
			$ruta_archivos = "uploads/resoluciones/";
            $nombre_archivo = "visitaRX-".$_REQUEST['id_tramite']."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter', '', '', 30, 20, 40, 35, 5, 10 , 'P');

            //load mPDF library para windows
            //require_once APPPATH . 'libraries/vendor/autoload.php';
            //$mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = false;
			$imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
			$imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
            $mpdf->SetHTMLHeader("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			$mpdf->SetWatermarkText('Programar visita');
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('validacion/resolucion_rx_visita_view', $datos, true));	
			
			
		}else if($_REQUEST['resultado_validacion'] == '20' || $_REQUEST['resultado_validacion'] == '28'){
			//DESISTIMIENTO
			if($_REQUEST['observaciones'] != ''){

				$dataSeg['observaciones'] = $_REQUEST['observaciones'];

			}else{
				$dataSeg['observaciones'] = 'Desistimiento';
			}

			$datos['nume_resolucion'] = 'XXXXXX';
			switch(date('m')){
				case '01': $datos['mes'] = 'Enero';	break;
				case '02': $datos['mes'] = 'Febrero'; break;
				case '03': $datos['mes'] = 'Marzo'; break;
				case '04': $datos['mes'] = 'Abril';	break;
				case '05': $datos['mes'] = 'Mayo'; break;
				case '06': $datos['mes'] = 'Junio'; break;
				case '07': $datos['mes'] = 'Julio';	break;
				case '08': $datos['mes'] = 'Agosto'; break;
				case '09': $datos['mes'] = 'Septiembre'; break;
				case '10': $datos['mes'] = 'Octubre'; break;
				case '11': $datos['mes'] = 'Noviembre'; break;
				case '12': $datos['mes'] = 'Diciembre';	break;

			}
			
			$datos['datos_tramite'] = $this->rx_model->tramite_info_validacion($id_tramite);
			$datos['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
			$datos['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
			$datos['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
			$datos['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
			$datos['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
			$datos['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
			$datos['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
			$datos['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
			$datos['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
			
			$datos['obs_val1'] = $_REQUEST['observaciones_item1'];
			$datos['obs_val2'] = $_REQUEST['observaciones_item2'];
			$datos['obs_val3'] = $_REQUEST['observaciones_item3'];
			$datos['obs_val4'] = $_REQUEST['observaciones_item4'];
			$datos['obs_val5'] = $_REQUEST['observaciones_item5'];
			$datos['obs_val6'] = $_REQUEST['observaciones_item6'];
			
			$datos['firma'] = FALSE;
			
			$ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "validacionRX-".$_REQUEST['id_tramite']."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //load mPDF library para linux
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter', '', '', 30, 20, 40, 35, 5, 10 , 'P');

            //load mPDF library para windows
            //require_once APPPATH . 'libraries/vendor/autoload.php';
            //$mpdf = new \Mpdf\Mpdf();
            $mpdf->debug = true;
			$mpdf->allow_output_buffering= true;
            $mpdf->showImageErrors = true;
            $mpdf->showWatermarkText = true;
			$imagenHeader = FCPATH."assets/imgs/logo_pdf_alcaldia.png";
			$imagenFooter = FCPATH."assets/imgs/logo_pdf_footer.png";
            $mpdf->SetHTMLHeader("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenHeader."' width='250px'>
											</td>
										</tr>
									</table>","O");
            $mpdf->SetHTMLFooter("<table class='table centro' width='100%'>
										<tr class='centro'> 
											<td width='100%'>
												<img src='".$imagenFooter."' width='550px'>
											</td>
										</tr>
									</table>","O"); 
			$mpdf->SetWatermarkText('Desistimiento');
			$mpdf->AddPage();
			$mpdf->WriteHTML($this->load->view('validacion/resolucion_rx_desistimiento_view', $datos, true));	
			
		}
		

		if(isset($_REQUEST['guardar'])){
			
			if($_REQUEST['resultado_validacion'] != '8')
			{	
				if($datos['datos_tramite']->estado == 19 && ($_REQUEST['resultado_validacion'] == 17 || $_REQUEST['resultado_validacion'] == 18 || $_REQUEST['resultado_validacion'] == 20 || $_REQUEST['resultado_validacion'] == 21)){
					if (isset($_FILES['resultado_visita']) && $_FILES['resultado_visita']['size'] > 0) {

						$nombre_archivo = "resultado_visita-".$id_tramite."-".date('YmdHis');
						$config['upload_path'] = "uploads/rayosx/";
						$config['allowed_types'] = 'pdf';
						$config['max_size'] = '30000';
						$config['file_name'] = $nombre_archivo;
						/* Fin Configuracion parametros para carga de archivos */
						// Cargue libreria
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						
						if ($this->upload->do_upload('resultado_visita')) {
							$upload_data = $this->upload->data();
							$rutaFinal = array('rutaFinal' => $this->upload->data());					
						} else {
							$bandera = 0;
						}

						$datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
						$datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
						$datosAr['fecha'] = date('Y-m-d');
						$datosAr['tags'] = "";
						$datosAr['es_publico'] = 1;
						$datosAr['estado'] = 'AC';

						$resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);

						$dataAct['id_tramite'] = $id_tramite;
						$dataAct['modulo'] = "archivo_visita";
						$dataAct['estado'] = $resultadoIDDocumento;
						$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);

					}else{
						
						$this->session->set_flashdata('error', 'Error al cargar el archivo del resultado de la visita</b>');
						redirect(base_url('validacion/'), 'refresh');
						exit;
						
					}
					
					$bandera = 1;
					$documentos = array("visita_certificado","visita_prote_radio","visita_niveles","visita_elementos","visita_procedimientos",
									   "visita_resultados","visita_vigilancia_radio","visita_tecnovigilancia","visita_docu_prote");
					
					for($i=0;$i<count($documentos);$i++){
						if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {

							$nombre_archivo = $documentos[$i]."-".$_REQUEST['id_tramite_rayosx']."-".date('YmdHis');
							$config['upload_path'] = "uploads/rayosx/";
							$config['allowed_types'] = 'pdf';
							$config['max_size'] = '30000';
							$config['file_name'] = $nombre_archivo;
							/* Fin Configuracion parametros para carga de archivos */

							// Cargue libreria
							$this->load->library('upload', $config);
							$this->upload->initialize($config);
							
							if ($this->upload->do_upload($documentos[$i])) {
								$upload_data = $this->upload->data();
								$rutaFinal = array('rutaFinal' => $this->upload->data());
							} else {
								$bandera = 0;
							}

							$datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
							$datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
							$datosAr['fecha'] = date('Y-m-d');
							$datosAr['tags'] = "";
							$datosAr['es_publico'] = 1;
							$datosAr['estado'] = 'AC';

							$resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);

							$datosDoc['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
							$datosDoc['documento'] = $documentos[$i];
							$datosDoc['path'] = $rutaFinal['rutaFinal']['full_path'];
							$datosDoc['categoria'] = $_REQUEST['categoria_docs'];
							$datosDoc['id_archivo'] = $resultadoIDDocumento;
							$datosDoc['estado'] = 1;
							$datosDoc['created_at'] = date("Y-m-d H:i:s");
							$resultRayosxArc = $this->rx_model->inactivarDocumentoPrevios($datosDoc);
							$resultRayosxObjeto = $this->rx_model->insertDocumento($datosDoc);

						}
					}
					
					
					
				}
				
				$salvar = $mpdf->Output($ruta_archivos.$nombre_archivo, "F");

				if(file_exists ( $ruta_archivos.$nombre_archivo )){
					$datosAr['ruta'] = $ruta_archivos;
					$datosAr['nombre'] = $nombre_archivo;
					$datosAr['fecha'] = date('Y-m-d');
					$datosAr['tags'] = "";
					$datosAr['es_publico'] = 1;
					$datosAr['estado'] = 'AC';

					$resultadoID = $this->validacion_model->insertarArchivo($datosAr);
					
					$datosObs['id_tramite'] = $id_tramite;
					$datosObs['estado'] = $_REQUEST['resultado_validacion'];
					$datosObs['id_usuario'] = $this->session->userdata('id_usuario');
					
					if(trim($_REQUEST['observaciones_item1']) != ''){
						$datosObs['observaciones1'] = $_REQUEST['observaciones_item1'];
					}else{
						$datosObs['observaciones1'] = "";
					}
					
					if(trim($_REQUEST['observaciones_item2']) != ''){
						$datosObs['observaciones2'] = $_REQUEST['observaciones_item2'];
					}else{
						$datosObs['observaciones2'] = "";
					}
					
					if(trim($_REQUEST['observaciones_item3']) != ''){
						$datosObs['observaciones3'] = $_REQUEST['observaciones_item3'];
					}else{
						$datosObs['observaciones3'] = "";
					}
					
					if(trim($_REQUEST['observaciones_item4']) != ''){
						$datosObs['observaciones4'] = $_REQUEST['observaciones_item4'];
					}else{
						$datosObs['observaciones4'] = "";
					}
					
					if(trim($_REQUEST['observaciones_item5']) != ''){
						$datosObs['observaciones5'] = $_REQUEST['observaciones_item5'];
					}else{
						$datosObs['observaciones5'] = "";
					}
					
					if(trim($_REQUEST['observaciones_item6']) != ''){
						$datosObs['observaciones6'] = $_REQUEST['observaciones_item6'];
					}else{
						$datosObs['observaciones6'] = "";
					}
					
					$resultadoObservaciones = $this->rx_model->registrarObservacionValidacion($datosObs);	

					if($resultadoID){
						
						$dataAct['id_tramite'] = $id_tramite;
						$dataAct['modulo'] = "estado";
						$dataAct['estado'] = $_REQUEST['resultado_validacion'];
						$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
						
						if($resultRayosxEstado){
							$dataflujo['tramite_id'] = $id_tramite;				
							$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
							$dataflujo['id_estado'] = $_REQUEST['resultado_validacion'];
							$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
							$dataflujo['observaciones'] = $_REQUEST['observaciones'];
							$dataflujo['id_archivo'] = $resultadoID;
							
							$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
							
							if($resultadoCrearTramiteFlujo){
								
								if($datos['datos_tramite']->notificacion == 1){
									
									if($_REQUEST['resultado_validacion'] == '19'){
										$this->enviarCorreo($datos['datos_tramite']);	
									}
								}
								
								$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n del tramite</b>');
								redirect(base_url('validacion/'), 'refresh');
								exit;
							}else{
								$this->session->set_flashdata('error', 'Error al realizar la validación del tramite</b>');
								redirect(base_url('validacion/'), 'refresh');
								exit;					
							}
						
						}else{
							$this->session->set_flashdata('error', 'Error al realizar la validación de información</b>');
							redirect(base_url('validacion/'), 'refresh');
							exit;
						}
						
						$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de información</b>');
						redirect(base_url('validacion/'), 'refresh');
						exit;
					}
				}else{
					$this->session->set_flashdata('error', 'Error al generar el documento PDF</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}
			}else{
				$dataAct['id_tramite'] = $id_tramite;
				$dataAct['modulo'] = "estado";
				$dataAct['estado'] = $_REQUEST['resultado_validacion'];
				$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
				
				if($resultRayosxEstado){
					$dataflujo['tramite_id'] = $id_tramite;				
					$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
					$dataflujo['id_estado'] = $_REQUEST['resultado_validacion'];
					$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
					$dataflujo['observaciones'] = $_REQUEST['observaciones'];
					
					$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);	
					
					if($resultadoCrearTramiteFlujo){
						$this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n del tramite</b>');
						redirect(base_url('validacion/'), 'refresh');
						exit;
					}else{
						$this->session->set_flashdata('error', 'Error al realizar la validación del tramite</b>');
						redirect(base_url('validacion/'), 'refresh');
						exit;					
					}
				
				}else{
					$this->session->set_flashdata('error', 'Error al realizar la validación de información</b>');
					redirect(base_url('validacion/'), 'refresh');
					exit;
				}
			}
			
		}else{
			$mpdf->Output();
		}

    }
    
	public function enviarCorreo($datos_tramite){
		
		require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
		$mail = new PHPMailer(true);

		try {
				$mail->IsSMTP(); // set mailer to use SMTP
				$mail->Host = "172.16.0.238"; // specif smtp server
				$mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
				$mail->Port = 25; // Used instead of 587 when only POP mail is selected
				$mail->SMTPAuth = false;
				$mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
				$mail->Password = "Colombia2018"; // SMTP password
				$mail->FromName = "Tramites en línea - SDS";
				$mail->From = "hceu@saludcapital.gov.co";
				//$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
				$mail->AddAddress($datos_tramite->email, $datos_tramite->email);
				//$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
				$mail->WordWrap = 50;
				$mail->CharSet = 'UTF-8';
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
				$mail->IsHTML(true); // set email format to HTML
				$mail->Subject = 'Tramites en línea - SDS';
				
				$html = "
						<table border=\"0\" width=\"80% \" align= \"justify\"><tr align=\"justify\"><td style = \" border: inset 0pt\" >" .
						"<p align=\"justify\"><img src='cid:imagen'/></p>" .
						"</br><h3>Estimado(a): ".$datos_tramite->p_nombre." ".$datos_tramite->p_apellido."</h3> " .
						"<p>El estado del trámite ".$datos_tramite->id." ha cambiado de estado, puede ingresar a través del siguiente <a href='" . base_url() ."'  target='_blank'>link</a></p>" .
						"<p>Este estado corresponde a una comunicación enviada por la Secretaría Distrital de Salud y la cual debe ser revisada por el usuario</p>" .
						"<p align=\"justify\"><img src='cid:imagen2'/></p></table>";

				$mail->Body = nl2br ($html,false);

				$mail->Send();

		}catch (Exception $e){
			print_r($e->getMessage());
			exit;
		}
	}
	
	public function actualizarFechaNotificacion($id_tramite, $id_subsanacion, $fecha){
		
		if($id_tramite != '' && $id_subsanacion != '' && $fecha != ''){
			
			$exd = date_create($fecha);
			$fecha_subsana = date_format($exd,"Y-m-d H:i:s");
			
			$dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "fecha_subsanacion".$id_subsanacion;
			$dataAct['estado'] = $fecha_subsana;
			
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$this->session->set_flashdata('exito', 'Se realizo el registro de la fecha de notificación</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}else{
				$this->session->set_flashdata('exito', 'Error al registrar la fecha de notificación</b>');
				redirect(base_url('validacion/'), 'refresh');
				exit;
			}

			
		}
		
		
	}
	
	public function repor_consulta_rx(){
		
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
		$data['rayosx_pendientes'] = $this->rx_model->reporte_rayosx();
		//$data['js'] = array(base_url('assets/js/validacion.js'),base_url('assets/js/validacion_rx.js')); 
		$data['js'] = array(base_url('assets/js/validacion_rx.js')); 
		$data['titulo'] = 'Perfil Validaci&oacute;n';
		$data['contenido'] = 'validacion/reporte_rx_view';
		$this->load->view('templates/layout_rx',$data);
		
	}
	
	public function info_tramite_rx($id_tramite)
    {
		$datosAr1 = $this->session->userdata('username');
		$data['validacion'] = $this->login_model->info_usuariotramite($datosAr1);
        $data['id_tramite'] = $id_tramite;
		$data['tramite_info'] = $this->rx_model->tramite_info($id_tramite);
        $data['departamento'] = $this->rx_model->departamentos_col();
        $data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
        $data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();
        $data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
        $data['nivelAcademico'] = $this->rx_model->nivelAcademico();
        $data['programasAcademicos'] = $this->rx_model->programasAcademicos();
		
		//Verificar si tiene datos por cada paso
		$data['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
		$data['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
		$data['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
		$data['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
		$data['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
		$data['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
		$data['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
		$data['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
		$data['documentosTramite'] = $this->rx_model->rayosxDocumentosValidacion($id_tramite);
        
        $data['resultado_validacion'] = $this->rx_model->estado_tramite_validador_rx($this->session->userdata('perfil'));
        $data['tramites_pendientes'] = $this->rx_model->tramite_info_validacion($id_tramite);
        $data['tipo_identificacion'] = $this->login_model->tipo_identificacion_todos();
        $data['departamentos_col'] = $this->login_model->departamentos_col();
        $data['tramites_seguimientos'] = $this->rx_model->seguimiento_tramite($id_tramite);
		
        //$data['js'] = array(base_url('assets/js/rayosx.js'),base_url('assets/js/validacion_rx.js'));
        $data['js'] = array(base_url('assets/js/rayosx.js'));
        $data['titulo'] = 'Perfil Validaci&oacute;n';
		$data['contenido'] = 'validacion/info_tramite_rx_view';			
        
        $this->load->view('templates/layout_rx',$data);

    }
}

