<h2 class="text-blue"><b>Registro y autorización de Licencia de practica medica categoría I y II</b></h2>
<form class="form-inline" id="formInicial" name="form_tramite" action="<?php echo base_url('usuario/crearTramiteRayosx')?>" method="post">

   <div class="line"></div>

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div id="paso0" class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>Registro de Información - Crear Trámite Licencia de practica medica categoría I y II</b></h3>
         </div>
		 <div class="col-12 col-md-12 pl-4">
			<div class="alert alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<p align="justify">
				<b>Apreciado Ciudadano(a)</b><br><br>
				Este trámite es solo para licencia de practica medica categoría I Y II. En el caso de licencia de prácticas industriales veterinarias o de investigación contactar a: contactenos@saludcapital.gov.co
				<br><br>
				Este módulo permite crear la solicitud y será dirigido al módulo de mis tramites donde puede completar la información y enviar la solicitud.
				</p>
			</div>
        </div>

         <div class="form-group ">
            <span class="text-orange"  >•</span><label for="tipo_tramite">Tipo de trámite: </label>
            <select id="tipo_tramite" name="tipo_tramite" class="form-control validate[required]">
               <option value="0">Seleccione...</option>
               <option value="1">Nuevo</option>
               <!--<option value="2">Renovación</option>--> 
            </select>
         </div>
		 <div class="form-group" id="div_renovacion" style="display:none">
            <span class="text-orange"  >•</span><label for="tipo_tramite">N&uacute;mero de trámite a renovar (Recuerde que debe pertenecer al mismo usuario que hizo la solicitud inicial): </label>
            <input type="text" id="nume_tramite" name="nume_tramite" class="form-control validate[required]">
         </div>
       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200 collapse">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Crear Solicitud
            </button>
         </p>
       </div>
    </div>
   </div>
</form>
 <script languague="javascript">
  $(document).ready(function () {
        //Validaciónes para crear el tramite de rayos x
        $("#tipo_tramite").change(function(){          

           var tipoTramite=$("#tipo_tramite").val();
           if($("#tipo_tramite").val() == 1 || $("#tipo_tramite").val() == 2){
              $("#btnRegistrarSolicitud").show();
           }else{
              $("#btnRegistrarSolicitud").hide();
           }

			if($("#tipo_tramite").val() == 2){
              $("#div_renovacion").show();
           }else{
              $("#div_renovacion").hide();
           }

        });

  });
 </script>