<?php

    $retornoError = $this->session->flashdata('retorno_error');
    if ($retornoError) {
        ?>
	<br>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
    }

    $retornoExito = $this->session->flashdata('retorno_exito');
    if ($retornoExito) {
        ?>
		<br>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
    }

?>

<div class="row block right">
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Listado Mis Trámites.</b></h2>
					<h3>Menú Trámites Registrados</h3>
                </div>
            </div>
			
        <div class="col-12 col-md-12 pl-4">
			<div class="alert alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<p align="justify">
				<b>Apreciado Ciudadano(a)</b><br><br>
				A continuación, encontrara un menú de selección que brindara acceso a los listados específicos de cada trámite, en estos encontrará los trámites que están registrados en el sistema con su correspondiente estado de gestión.
				Recomendamos leer el manual correspondiente para cada trámite para conocer la descripción de cada estado y los tiempos de gestión que conlleva cada trámite. Todos los trámites una vez son completados genera una notificación automática vía correo electrónico de su correspondiente licencia y/o resolución, este documento podrá ser descargado en esta misma herramienta una vez sea concluido el trámite.
				Cada trámite es personal y reposa bajo la titularidad del usuario registrado en la Ventanilla Única de Trámites y Servicios.
				<br><br>
				Ante cualquier inquietud favor tener presente su id Trámite y categoría del mismo para ser escalada a los funcionarios de atención al ciudadano, puede contactar atención al ciudadano mediante el correo electrónico contactenos@saludcapital.gov.co, agradecemos leer la documentación disponible, se han realizado esfuerzos para aclarar todas las dudas en estos manuales, e instrucciones que cuenta cada trámite. 
				Favor leer la información disponible para evitar reprocesos y demoras de la misma gestión.
				</p>
			</div>
        </div>	
		<div class="col-12 col-md-4 pl-4 text-center">
		<button class="btn blue w-100 py-2" id="btn_atdiv"> Mis Trámites de Autorización de Títulos</button>
		<a href="<?php echo base_url('assets/docs/manual_aut_titulos.pdf')?>" target="_blank"><button class="btn-success">Ver Manual</button></a>
		</div>
		<div class="col-12 col-md-4 pl-4 text-center">
		<button class="btn blue w-100 py-2" id="btn_exhdiv"> Mis Trámites de Licencia de Exhumación</button>
		<a href="<?php echo base_url('assets/docs/manual_lic_exhum.pdf')?>" target="_blank"><button class="btn-success">Ver Manual</button></a>
		</div>
		<div class="col-12 col-md-4 pl-4 text-center">
		<button class="btn blue w-100 py-2" id="btn_rxdiv"> Mis Trámites de Licencia de Equipos RX</button>
		<a href="<?php echo base_url('assets/docs/manual_lic_rayosx.pdf')?>" target="_blank"><button class="btn-success">Ver Manual</button></a>
		</div>
</div>
		
		<div class="row block right" id="divat" style="width:100%;display:none;" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Mis Trámites.</b></h2>
					<h3>Autorización de Títulos en Área de Salud</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12 table-responsive">
					<table class="table" id="tabla_tramites33" style="font-size:small;">
                        <thead>
							<tr>
								<th>ID Trámite</th>
								<th>Tipo de título</th>
								<th>Fecha radicaci&oacute;n</th>
								<th>Instituci&oacute;n</th>
								<th>Programa</th>
								<th>Estado</th>
								<th colspan="3">Acciones</th>
							</tr>
                        </thead>
                        <tbody>
					<?php
					if(count($mis_tramites)>0){
						for($i=0;$i<count($mis_tramites);$i++){
                    ?>
                            <tr>
                                <td>
									<?php echo $mis_tramites[$i]->id_titulo?>
                                </td>
                                <td>
                                    <?php
                                        if($mis_tramites[$i]->tipo_titulo == '1'){
                                            echo "Nacional";
                                        }else{
                                            echo "Extranjero";
                                        }
                                    ?>
                                </td>                                
                                <td>
                                    <?php echo $mis_tramites[$i]->fecha_tramite?>
                                </td>
                                <td>
                                   <?php
                                        if($mis_tramites[$i]->tipo_titulo == '1'){
                                            echo $mis_tramites[$i]->nombre_institucion;
                                        }else{
                                            echo $mis_tramites[$i]->cod_universidad;
                                        }
                                    ?>
                                </td>
                                <td>
									<?php
                                        if($mis_tramites[$i]->tipo_titulo == '1'){
                                            echo $mis_tramites[$i]->nombre_programa;
                                        }else{
                                            echo $mis_tramites[$i]->programaequivalente;
                                        }
                                    ?>
                                </td>
								
								<td>
                                    <?php echo $mis_tramites[$i]->descripcion?>
                                </td>
                                <td>
                                 <?php
                                    
									
									if($mis_tramites[$i]->estado == '13'){
                                        ?>
										<td>
                                        <a href="<?php echo base_url('usuario/editarSolicitud/'.$mis_tramites[$i]->id_titulo)?>"><img src="<?php echo base_url('assets/imgs/editar.png')?>" width="40px"></a><br>Editar<br>Trámite
										</td>
										<td>
                                        <a href="#"><img src="<?php echo base_url('assets/imgs/audit.png')?>" width="40px"></a><br>Observaciones
										</td>
                                        <?php
                                    }else if($mis_tramites[$i]->estado == '16'){
										$resoluciones = $this->usuarios_model->consulta_resolucionreposicion($mis_tramites[$i]->id_titulo);

										foreach ($resoluciones as $resolucion){
											$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
									?>
										<td colspan="1">
											<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdf.png')?>"  width="40px"></a> <?php $coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); $coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); $coincidencia3 = strstr($pdfresolucion->nombre,'-11-');  $coincidencia4 = strstr($pdfresolucion->nombre,'-19-'); if (!empty($coincidencia1)) {echo "<br>Resolución Aprobación"; } elseif(!empty($coincidencia2)){echo "<br>Resolución Negación";}elseif(!empty($coincidencia3)){echo "<br>Resolución Reposición";}elseif(!empty($coincidencia4)){echo "<br>Resolución Aclaración";}?>
										</td>		
									<?php									
										}
									?>
										<td colspan="1">
                                        	<a id="btn_seguimiento" href="#" data-target="#modalseguimiento" data-toggle="modal" data-idaseg="<?php echo $mis_tramites[$i]->id_titulo?>"><img src="<?php echo base_url('assets/imgs/audit.png')?>" width="40px"><br>Observaciones</a>
										</td>
										

										
                                        <?php
                                    }else if($mis_tramites[$i]->estado == '14'){
											
											$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
											//var_dump($mis_tramites[$i]->id_titulo);
											$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
											
											?>
											<td colspan="1">
											<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfaprobacion.png')?>" width="40px"></a>
											<br>Resolución<br>Aprobación
											</td>

											<td colspan="1">
												<a href="#" data-target="#modalaclaracion" data-toggle="modal" class="modalaclaracionlink" data-ida="<?php echo $mis_tramites[$i]->id_titulo?>" data-fechatramitea="<?php echo $mis_tramites[$i]->fecha_tramite?>" data-tipotituloa="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo "Nacional";}else{ echo "Extranjero";}?>" data-instituciona="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo $mis_tramites[$i]->nombre_institucion;}else{echo $mis_tramites[$i]->cod_universidad;}?>" data-programaa="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo $mis_tramites[$i]->nombre_programa;}else{ echo $mis_tramites[$i]->programaequivalente;}?>"><img src="<?php echo base_url('assets/imgs/aclaracionpdf.png')?>" width="40px"><br>Solicitar<br>Aclaración</a>
											</td>
											<?php

									}
									else if($mis_tramites[$i]->estado == '7'){
										$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
										//var_dump($mis_tramites[$i]->id_titulo);
										$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
									?>
										<td colspan="1">
											<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfnegacion.png')?>" width="40px"></a><br>Resolución<br>Negación
										</td>									
									<?php
									
										$from = $pdfresolucion->fecha;
										$to = date('Y-m-d');
										$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
										$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

										$from = new DateTime($from);
										$to = new DateTime($to);
										$to->modify('+1 day');
										$interval = new DateInterval('P1D');
										$periods = new DatePeriod($from, $interval, $to);

										$days = 0;
										foreach ($periods as $period) {
											if (!in_array($period->format('N'), $workingDays)) continue;
											if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
											if (in_array($period->format('*-m-d'), $holidayDays)) continue;
											$days++;
										}

										$tiempo_validacion = 10;
								

										if($days <= 10){
											$faltan= $tiempo_validacion - $days;
									?>
										<td colspan="1">
											<a href="#" data-target="#modalreposicion" data-toggle="modal" class="modalreposicionlink" data-idr="<?php echo $mis_tramites[$i]->id_titulo?>" data-fechatramiter="<?php echo $mis_tramites[$i]->fecha_tramite?>" data-tipotitulor="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo "Nacional";}else{ echo "Extranjero";}?>" data-institucionr="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo $mis_tramites[$i]->nombre_institucion;}else{echo $mis_tramites[$i]->cod_universidad;}?>" data-programar="<?php if($mis_tramites[$i]->tipo_titulo == '1'){ echo $mis_tramites[$i]->nombre_programa;}else{ echo $mis_tramites[$i]->programaequivalente;}?>"><img src="<?php echo base_url('assets/imgs/reposicionpdf.png')?>" width="40px"><br>Solicitar<br>Reposición</a>
										</td>									
										<?php
										}
									}
									else if($mis_tramites[$i]->estado == '11' || $mis_tramites[$i]->estado == '19' ){
										$resoluciones = $this->usuarios_model->consulta_resolucionreposicion($mis_tramites[$i]->id_titulo);

										foreach ($resoluciones as $resolucion){
											$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
									?>
										<td colspan="1">
											<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdf.png')?>"></a> <?php $coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); $coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); $coincidencia3 = strstr($pdfresolucion->nombre,'-11-');  $coincidencia4 = strstr($pdfresolucion->nombre,'-19-'); if (!empty($coincidencia1)) {echo "<br>Resolución Aprobación"; } elseif(!empty($coincidencia2)){echo "<br>Resolución Negación";}elseif(!empty($coincidencia3)){echo "<br>Resolución Reposición";}elseif(!empty($coincidencia4)){echo "<br>Resolución Aclaración";}?>
										</td>		
									<?php									

										}
									}
									else if($mis_tramites[$i]->estado >= '17' or $mis_tramites[$i]->estado == '12'){
										
										$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
										//var_dump($mis_tramites[$i]->id_titulo);
										if(!empty($resolucion)){
										$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
										$coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); 
										$coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); 
										
										if (!empty($coincidencia1) || !empty($coincidencia2)) {
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfaprobacion.png')?>"></a>
										<br>Resolución<br>Aprobación
										</td>	
										<?php
										
										}
										}
									}
									else if($mis_tramites[$i]->estado >= '8' and $mis_tramites[$i]->estado <= '11'){
																			
										$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
										//var_dump($mis_tramites[$i]->id_titulo);
										if(!empty($resolucion)){
										$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
										
										$coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); 
										$coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); 
										
										if (!empty($coincidencia1) || !empty($coincidencia2)) {
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfnegacion.png')?>"></a>
										<br>Resolución<br>Negación
										</td>	
										<?php
										
										}
										}
									}
									else if ($mis_tramites[$i]->estado >= '16'){
										$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
										//var_dump($mis_tramites[$i]->id_titulo);
										if(!empty($resolucion)){
										$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
										
										$coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); 
										$coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); 
										
										if (!empty($coincidencia1)) {
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfaprobacion.png')?>"></a>
										<br>Resolución<br>Aprobación
										</td>	
										<?php

										}
										else{
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfnegacion.png')?>"></a>
										<br>Resolución<br>Negación
										</td>	
										<?php	
										}
										}
									}
									else if ($mis_tramites[$i]->estado == '15'){
										$resolucion = $this->usuarios_model->consulta_resolucion($mis_tramites[$i]->id_titulo);
										//var_dump($mis_tramites[$i]->id_titulo);
										if(!empty($resolucion)){
										$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($resolucion->id_archivo);
										
										$coincidencia1 = strstr($pdfresolucion->nombre,'-14-'); 
										$coincidencia2 = strstr($pdfresolucion->nombre,'-7-'); 
										
										if (!empty($coincidencia1)) {
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfaprobacion.png')?>"></a>
										<br>Resolución<br>Aprobación
										</td>	
										<?php

										}
										else{
										?>
										<td colspan="1">
										<a href="<?php echo base_url('uploads/resoluciones/'.$pdfresolucion->nombre)?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfnegacion.png')?>"></a>
										<br>Resolución<br>Negación
										</td>	
										<?php	
										}
										}
									}
									?>
                                </td>
                            </tr>
                    <?php
						}
					}
					?>
                        </tbody>
                    </table>
            </div>
		</div>	

		<div class="row block right" id="divexh" style="width:100%;display:none;" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Mis Trámites.</b></h2>
					<h3>Licencia Exhumación de Cadáveres</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12 table-responsive">
			<table class="table" id="tabla_tramites" style="font-size:small;">
				<thead>
					<tr>
						<th>ID Trámite</th>
						<th>Número Registro defunción</th>
						<th>Fecha radicaci&oacute;n</th>
						<th>Número Licencia</th>
						<th>Número documento Fallecido</th>
						<th>Estado</th>
						<th>Acciones</th>
					</tr>
				</thead>
			<?php
			if (count($mistramites_exh) > 0) {
				for ($i = 0; $i < count($mistramites_exh); $i++) {
					?>
						<tbody>
							<tr>
								<td>
								<?php echo $mistramites_exh[$i]->idlicencia_exhumacion?>
								</td>
								<td>
								<?php echo $mistramites_exh[$i]->numero_regdefuncion?>
								</td>
								<td>
								<?php echo $mistramites_exh[$i]->fecha_solicitud ?>
								
								</td>
								<td>
								<?php echo $mistramites_exh[$i]->numero_licencia?>
								</td>
								<td>
								<?php
								echo $mistramites_exh[$i]->numero_docfallecido
								?>
								</td>
								<td>
								<?php echo $mistramites_exh[$i]->des_estado?>
								</td>
								<td>
								<?php
								if ($mistramites_exh[$i]->estado == '2') {
								?>
									<a href="<?php echo base_url('usuario/editarLExh/' . $mistramites_exh[$i]->idlicencia_exhumacion) ?>">
									<img src="<?php echo base_url('assets/imgs/editar.png') ?>"></a><br>Editar<br>Trámite
								<?php
								}
								else if ($mistramites_exh[$i]->estado == '4') {
									$licencia = $this->mlicencia_exhumacion->info_aprobacionlicencia($mistramites_exh[$i]->idlicencia_exhumacion);
									$pdfresolucion = $this->usuarios_model->consultar_archivo_resolucion($licencia->id_archivo);
								?>
									<a href="<?php echo base_url('uploads/exhumacion/' . $pdfresolucion->nombre) ?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdfaprobacionexh.png') ?>" width="40px"></a><br>Licencia<br> Exhumación Aprobada
								<?php
								}
								?>
								</td>
							</tr>
						<?php
						#exhumacion
					}
				}
				?>
					</tbody>
				</table>
            </div>
		</div>	

		<div class="row block right" id="divrx" style="width:100%;display:none;" >
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Mis Trámites.</b></h2>
					<h3>Licencia Equipos Ionizantes Rayos RX</h3>
                </div>
            </div>
			
            <div class="col-12 col-md-12 table-responsive">
			<table class="table" id="tabla_tramites">
				<thead>
					<tr>
						<th> Id Trámite </th>
						<th> Tipo Trámite </th>
						<th> Inicio Trámite </th>
						<th> Estado </th>
						<th> Fecha Estado </th>                      
						<th> Categoría </th>                      
						<th>Editar Tr&aacute;mite</th> 
						<th>Ver PDF</th> 
						<th>Prorroga</th> 
						<th>Tiempo</th> 
					</tr>
				</thead>
			<?php
			if (count($mistramites_rx) > 0) {
				for ($i = 0; $i < count($mistramites_rx); $i++) {
					?>
						<tbody>
							<tr>
								<td><?php echo $mistramites_rx[$i]->id;?></td> 
								<td><?php echo $mistramites_rx[$i]->descripcion ?></td>
								<td><?php echo $mistramites_rx[$i]->created_at ?></td>                       
								<td><?php echo $mistramites_rx[$i]->estadoDesc ?></td>
								<td><?php echo $mistramites_rx[$i]->fecha_estado ?></td>                       
								<td><?php echo $mistramites_rx[$i]->categoria?></td>
													
								
								<?php
								if ($mistramites_rx[$i]->estado == '1') {
								?>
									<td>
										<a href="<?php echo base_url('usuario/rx_editarForm/' . $mistramites_rx[$i]->id) ?>">
										<img src="<?php echo base_url('assets/imgs/editar.png') ?>" width="25px"></a>
									</td>
									<td></td>
									<td></td>
								<?php
								}else if(($mistramites_rx[$i]->estado == '13') && $mistramites_rx[$i]->fecha_subsanacion1 != 'NULL'){
									
									$archivoTram = $this->rx_model->consultar_archivo_equipo($mistramites_rx[$i]->id_archivo);
									?>
									<td>
										<a href="<?php echo base_url('usuario/rx_editarForm/' . $mistramites_rx[$i]->id) ?>">
										<img src="<?php echo base_url('assets/imgs/editar.png') ?>" width="25px"></a>
										
									</td>
									<td>
										<a href="<?php echo base_url('uploads/resoluciones/' . $archivoTram->nombre) ?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdf.png') ?>"  width="25px"></a>
									</td>
									<?php
										if($mistramites_rx[$i]->estado == '13' && $mistramites_rx[$i]->solicita_prorroga != 1){											
											?>
											<td>
												<a class="btn blue" href="#" onClick="solicitaProrroga(<?php echo $mistramites_rx[$i]->id?>)">Solicitar Prorroga</a>
											</td>
											<?php											
										}else{
											?>
											<td></td>
											<?php
										}
										?>
										
										<td>
										
										<?php
										
										if($mistramites_rx[$i]->estado == '13')
										{											
											$exd = date_create($mistramites_rx[$i]->fecha_subsanacion1);
											$fecha_radicado = date_format($exd,"Y-m-d");//here you make mistake
											$from = $fecha_radicado;
											
											$to = date('Y-m-d');
											$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
											$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

											$from = new DateTime($from);
											
											if($mistramites_rx[$i]->solicita_prorroga == 1){
												
												$from->modify('+20 day');
												$tiempo_validacion = 40;
											}else{
												$tiempo_validacion = 20;
											}
											
											$to = new DateTime($to);
											$to->modify('+0 day');
											$interval = new DateInterval('P1D');
											$periods = new DatePeriod($from, $interval, $to);

											$days = 0;
											foreach ($periods as $period) {
												if (!in_array($period->format('N'), $workingDays)) continue;
												if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
												if (in_array($period->format('*-m-d'), $holidayDays)) continue;
												$days++;
											}

											
											if($tiempo_validacion == 40){
												if($days > $tiempo_validacion){
													echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
												}else if($days > 35 && $days <=40){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
												}else if($days > 11 && $days <=35){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
												}else if($days <= 10){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
												}
											}else if($tiempo_validacion == 20){
												if($days > $tiempo_validacion){
													echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
												}else if($days > 15 && $days <=20){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
												}else if($days > 11 && $days <=15){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
												}else if($days <= 10){
													$faltan= $tiempo_validacion - $days;
													echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
												}
											}
											
										}
										?>
										</td>
										<?php
								}else if(($mistramites_rx[$i]->estado == '22') && $mistramites_rx[$i]->fecha_subsanacion2 != 'NULL'){
									
									$archivoTram = $this->rx_model->consultar_archivo_equipo($mistramites_rx[$i]->id_archivo);
									?>
									<td>
										<a href="<?php echo base_url('usuario/rx_editarForm/' . $mistramites_rx[$i]->id) ?>">
										<img src="<?php echo base_url('assets/imgs/editar.png') ?>" width="25px"></a>
										
									</td>
									<td>
										<a href="<?php echo base_url('uploads/resoluciones/' . $archivoTram->nombre) ?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdf.png') ?>"  width="25px"></a>
									</td>
									<td></td>
									
										<td>
										
										<?php
										
										if($mistramites_rx[$i]->estado == '22')
										{											
											$from = $mistramites_rx[$i]->fecha_subsanacion2;
											
											
											$to = date('Y-m-d');
											$workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
											$holidayDays = ['*-12-25', '*-01-01', '*-05-01', '*-07-20', '*-08-07', '2019-08-19', '2019-03-25', '2019-04-18', '2019-04-19', '2019-06-03', '2019-06-24']; # variable and fixed holidays

											$from = new DateTime($from);
											
											$tiempo_validacion = 20;
											
											$to = new DateTime($to);
											$to->modify('+0 day');
											$interval = new DateInterval('P1D');
											$periods = new DatePeriod($from, $interval, $to);

											$days = 0;
											foreach ($periods as $period) {
												if (!in_array($period->format('N'), $workingDays)) continue;
												if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
												if (in_array($period->format('*-m-d'), $holidayDays)) continue;
												$days++;
											}

											
										
											if($days > $tiempo_validacion){
												echo '<div class="alert alert-danger" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> <br>El trámite se encuentra vencido. Días transcurridos '.$days.'</div>';
											}else if($days > 3 && $days <=5){
												$faltan= $tiempo_validacion - $days;
												echo '<div class="alert alert-warning" role="alert" style="font-size:10px;"><b>Atenci&oacute;n Usuario!</b> Trámite Proximo a vencerse. Quedan '.$faltan.' d&iacute;as.<br> Días transcurridos  '.$days.'</div>';
											}else if($days > 2 && $days <=3){
												$faltan= $tiempo_validacion - $days;
												echo '<div class="alert alert-success" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Estos son los días transcurridos '.$days.'</div>';
											}else if($days <= 2){
												$faltan= $tiempo_validacion - $days;
												echo '<div class="alert alert-info" role="alert" style="font-size:10px;">Quedan '.$faltan.' d&iacute;as. Días transcurridos '.$days.'</div>';
											}
										}
										?>
										</td>
										<?php
								}else if($mistramites_rx[$i]->estado == '10' || $mistramites_rx[$i]->estado == '12' || $mistramites_rx[$i]->estado == '13' || $mistramites_rx[$i]->estado == '22' || $mistramites_rx[$i]->estado == '34' || $mistramites_rx[$i]->estado == '35' || $mistramites_rx[$i]->estado == '36' || $mistramites_rx[$i]->estado == '37' || $mistramites_rx[$i]->estado == '38' || $mistramites_rx[$i]->estado == '40'  || $mistramites_rx[$i]->estado == '41'){
									
									$archivoTram = $this->rx_model->consultar_archivo_equipo($mistramites_rx[$i]->id_archivo);
									?>
									<td></td>
									<td>
										<a href="<?php echo base_url('uploads/resoluciones/' . $archivoTram->nombre) ?>" target="_blank"><img src="<?php echo base_url('assets/imgs/pdf.png') ?>"  width="25px"></a>
									</td>
									<td></td>
									<td></td>
									<?php
									
								}
								
								?>
							</tr>
						<?php
					}
				}
				?>
					</tbody>
				</table>
            </div>
		</div>	

		
<div class="modal fade" id="modalseguimiento" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog modal-lg" role="dialog">
    <div class="modal-content">
        <div class="modal-header" style="background:#3D5DA6;">
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Ventana de Seguimiento y Observaciones Ciudadano(a)</h4>		
        </div>
        <div class="modal-body">
		<legend>Tabla de Seguimiento</legend>
		<br>
			<table width="100%" border="1" id="observacionestabla">
				<thead>
					<tr>
						<th width="12%"><b>Fecha Observación</b></th>
						<th width="25%"><b>Observación</b></th>
					</tr>
				</thead>
				<tbody id="bodyobservacionestabla">
				<tr>
				</tr>
				</tbody>
			</table>	
		
        </div>
        <div class="modal-footer" align="center">
		<table width="100%">
		<tr>
			<td align="center">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</td>
		</tr>
		</table>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="modalreposicion" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
	<form  enctype="multipart/form-data" role="form" action="<?php echo base_url('usuario/generareposicion')?>" method="post">
    <div class="modal-content">
        <div class="modal-header" style="background:#3D5DA6;">
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Formulario de Solicitud Reposición Trámite Autorización de Títulos</h4>
			
        </div>
        <div class="modal-body">
		
			<div class="alert alert alert-warning" role="alert">
				<b>Apreciado Usuario!</b> Se ha generado una resolución de Negación de Autorización de Título en área de Salud. 
				Con el siguiente formulario tiene con la posibilidad de solicitar reposición y/o nueva revisión del trámite. 
				Agradezco argumentar los motivos por los cual considera que se debe realizar un cambio de la resolución generada.
            </div>
			
				<legend><b>Datos del Trámite</b></legend>
				<table style="width:100%;">
				<tr>
				<td>
				<label for="fechatramite"><b>Fecha Trámite</b></label>
				<input type="text" name="fechatramitemodalr" id="fechatramitemodalr" value="" size="68" readonly style="width:100%;"><br>
				<input type="hidden" name="fecha_reposicion" id="fecha_reposicion" value="<?php print_r($fecha_actual = date('Y-m-d')); ?>">
				</td>
				<td>
				<label for="tipo_titulo"><b>Id Trámite</b></label>
				<input type="text" name="id_titulomodalr" id="id_titulomodalr" value="" size="68" readonly style="width:100%;"><br>
				</td>
				<td>
				<label for="tipo_titulo"><b>Tipo Título</b></label>
				<input type="text" name="tipotitulomodalr" id="tipotitulomodalr" value="" size="68" readonly style="width:100%;"><br>
				</td>
				</tr>
				<tr>
				<td>
				<label for="tipo_titulo"><b>Institución</b></label>
				<input type="text" name="institucionmodalr" id="institucionmodalr" maxlength="30" size="68" value="" readonly style="width:100%;"><br>
				</td>
				<td colspan="2">
				<label for="tipo_titulo"><b>Programa</b></label>
				<input type="text" name="programamodalr" id="programamodalr" value="" maxlength="30" size="68" readonly style="width:100%;"><br>
				<input type="hidden" name="estador" id="estador" value="8" maxlength="30" size="68" readonly>
				</td>
				</tr>
				<tr>
				<td colspan="3">
				<label for="tipo_titulo"><b>Observación Motivo de Reposición</b></label><br>
				<textarea style="width:100%;" rows="4" cols="68" placeholder="Favor Ingresar los Motivos de la Reposición" name="observacionreposicion" id="observacionreposicion" required></textarea><br>
				</td>
				</tr>
				</table>

        </div>
        <div class="modal-footer">
			<button class="btn red w-100 py-2" type="button" data-dismiss="modal">Cancelar</button>
			<button class="btn green w-100 py-2" type="submit">Enviar</button>
        </div>
	</form>
    </div>
</div>
</div>



<div class="modal fade" id="modalaclaracion" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="modal-dialog" role="dialog">
	<form  enctype="multipart/form-data" role="form"  action="<?php echo base_url('usuario/generaaclaracion')?>" method="post" id="formAclaracion">
    <div class="modal-content">
        <div class="modal-header" style="background:#3D5DA6;">
            <h4 class="modal-title" id="myModalLabel" style="color:white;">Formulario de Solicitud Aclaración Trámite Autorización de Títulos</h4>
			
        </div>
        <div class="modal-body">
		
			<div class="alert alert alert-warning" role="alert">
				<b>Apreciado Usuario!</b> Se ha generado una resolución de Aprobación de Autorización de Título en área de Salud. 
				Con el siguiente formulario tiene con la posibilidad de solicitar aclaración y/o corrección de la anterior resolución. 
				Agradezco argumentar los motivos por los cual considera que se debe realizar una aclaración de la resolución generada.
            </div>

			<div class="alert alert alert-danger" role="alert">
				<b>Apreciado Usuario!</b> Si tiene novedades en relación con el registro de RETHUS (Registro Único Nacional del Talento Humano en Salud - MinSalud), 
				favor tener en cuenta que este registro se realiza dentro de los primero (5) cinco días hábiles del mes inmediatamente siguiente al de expedición de 
				la Resolución de Autorización de Títulos en Salud, si no encuentra su registro en la siguiente <a href="https://web.sispro.gov.co/THS/Cliente/ConsultasPublicas/ConsultaPublicaDeTHxIdentificacion.aspx" target="_blank"> link</a>, 
				favor notificar esta novedad al correo contactenos@saludcapital.gov.co. 
				Favor no continuar por esta opción si no aplica alguno de los tipos de aclaración disponibles. Gracias”
            </div>			
				<legend><b>Datos del Trámite</b></legend>
				<table style="width:100%;">
				<tr>
				<td>				
				<label for="fechatramite"><b>Fecha Trámite</b></label>
				<input type="text" name="fechatramitemodala" id="fechatramitemodala" value="" size="68" readonly style="width:100%;"><br>
				<input type="hidden" name="fecha_aclaracion" id="fecha_aclaracion" value="<?php print_r($fecha_actual = date('Y-m-d')); ?>">
				</td>
				<td>
				<label for="tipo_titulo"><b>Id Trámite</b></label>
				<input type="text" name="id_titulomodala" id="id_titulomodala" value="" size="68" readonly style="width:100%;"><br>
				</td>
				<td>			
				<label for="tipo_titulo"><b>Tipo Título</b></label>
				<input type="text" name="tipotitulomodala" id="tipotitulomodala" value="" size="68" readonly style="width:100%;"><br>
				</td>
				</tr>
				<tr>
				<td>
				<label for="tipo_titulo"><b>Institución</b></label>
				<input type="text" name="institucionmodala" id="institucionmodala" maxlength="30" size="68" value="" readonly style="width:100%;"><br>
				</td>
				<td colspan="2">            
				<label for="tipo_titulo"><b>Programa</b></label>
				<input type="text" name="programamodala" id="programamodala" value="" maxlength="30" size="68" readonly style="width:100%;"><br>
				<input type="hidden" name="estadoa" id="estadoa" value="12" maxlength="30" size="68" readonly>
				</td>
				</tr>
				<tr>
				<td colspan="3">
				<label for="tipo_titulo"><b>Tipo Motivo Aclaración</b></label>
				<select id="tipomotivoaclaracion" name="tipomotivoaclaracion" class="form-control select2 validate[required]" required>
				<option value="">Seleccione...</option>
				<option value="1">Error digitación Nombres y/o Apellidos</option>
				<option value="2">Error selección Titulación Profesión</option>
				<option value="3">Error selección Institución Educativa</option>
				<option value="4">Error selección Tipo de Documento</option>
				<option value="5">Error digitación Fecha de Grado</option>
				</select>
				</td>
				</tr>
				<tr>
				<td colspan="3">				
				<label for="tipo_titulo"><b>Observación Motivo de Aclaración</b></label><br>
				<textarea style="width:100%;" rows="4" cols="68" placeholder="Favor Ingresar los Motivos de la Aclaración" name="observacionaclaracion" id="observacionaclracion" required></textarea><br>
				</td>
				</tr>
				</table>

        </div>
        <div class="modal-footer">
			<button class="btn red w-100 py-2" type="button" data-dismiss="modal">Cancelar</button>
			<button class="btn green w-100 py-2" id="btn-submitAclaracion" type="submit">Enviar</button>
        </div>
	</form>
    </div>
</div>
</div>

            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>

<script type="text/javascript">

	$("#btn_seguimiento").click(function () {
		var id_tituloaseg = $(this).data('idaseg');
		console.log(id_tituloaseg);
		$('#bodyobservacionestabla').empty();
		//$(".modal-body #id_titulomodalseg" ).html(id_tituloaseg);
		$.ajax({
				url: base_url + "usuario/seguimientociudadano/",
				type:'POST',
				dataType: "json",
				data:{
					idt :  id_tituloaseg			
				},
				
				success:function(res){

					var res = jQuery.parseJSON(JSON.stringify(res));

							$.each(res, function(index, value){
								//console.log(value);
								$( "#bodyobservacionestabla" ).append("<td>" + value.fecha_registro + "</td>");
								$( "#bodyobservacionestabla" ).append("<td>" + value.observaciones + "</td>");
							});	
					$('#modalseguimiento').modal('show');
					//alertify.alert('Usuario creado', res.mensaje, function(){ location.reload(); alertify.success('Recargando...'); });
				
				},
				error:function(){
					alertify.alert('Usuario no creado', res.mensaje, function(){ location.reload(); alertify.success('Recargando...'); });
				}
			});	
			return false;
		

	});
	
	$("#btn_seguimiento2").click(function () {
		var id_tituloaseg2 = $(this).data('idaseg2');
		console.log(id_tituloaseg2);
		$('#bodyobservacionestabla').empty();
		//$(".modal-body #id_titulomodalseg" ).html(id_tituloaseg);
		$.ajax({
				url: base_url + "usuario/seguimientociudadano/",
				type:'POST',
				dataType: "json",
				data:{
					idt :  id_tituloaseg2			
				},
				
				success:function(res){

					var res = jQuery.parseJSON(JSON.stringify(res));
							$.each(res, function(index, value){
								//console.log(value);
								$( "#bodyobservacionestabla" ).append("<td>" + value.fecha_registro + "</td>");
								$( "#bodyobservacionestabla" ).append("<td>" + value.observaciones + "</td>");
							});	

					$('#modalseguimiento').modal('show');
					//alertify.alert('Usuario creado', res.mensaje, function(){ location.reload(); alertify.success('Recargando...'); });
				
				},
				error:function(){
					alertify.alert('Usuario no creado', res.mensaje, function(){ location.reload(); alertify.success('Recargando...'); });
				}
			});	
			return false;
		

	});	
	
	
	$("#btn_atdiv").click(function () {
		$('#divat').show('slow');
		$("#divexh").hide();
		$("#divrx").hide();
	});
	
	$("#btn_exhdiv").click(function () {
		$('#divat').hide();
		$("#divexh").show('slow');
		$("#divrx").hide();
	});

	$("#btn_rxdiv").click(function () {
		$('#divat').hide();
		$("#divexh").hide();
		$("#divrx").show('slow');
	});	

	$(".modalaclaracionlink").click(function () {
		var id_tituloa = $(this).data('ida');
		var fechatramitea = $(this).data('fechatramitea');
		var tipotituloa = $(this).data('tipotituloa');
		var instituciona = $(this).data('instituciona');
		var programaa = $(this).data('programaa');
		$(".modal-body #id_titulomodala").val(id_tituloa);
		$(".modal-body #fechatramitemodala").val(fechatramitea);
		$(".modal-body #tipotitulomodala").val(tipotituloa);
		$(".modal-body #institucionmodala").val(instituciona);
		$(".modal-body #programamodala").val(programaa);
		$('#modalaclaracion').modal('show');
    })
	

	$(".modalreposicionlink").click(function () {
		var id_titulor = $(this).data('idr');
		var fechatramiter = $(this).data('fechatramiter');
		var tipotitulor = $(this).data('tipotitulor');
		var institucionr = $(this).data('institucionr');
		var programar = $(this).data('programar');
		$(".modal-body #id_titulomodalr").val(id_titulor);
		$(".modal-body #fechatramitemodalr").val(fechatramiter);
		$(".modal-body #tipotitulomodalr").val(tipotitulor);
		$(".modal-body #institucionmodalr").val(institucionr);
		$(".modal-body #programamodalr").val(programar);
		$('#modalreposicion').modal('show');
	})

	$("#formAclaracion").submit(function(){
		event.preventDefault();
		alertify.confirm('Información Importante', 'Apreciado Ciudadano(a): La siguiente solicitud solo resuelve temas relacionados con novedades en digitación en los nombres, apellidos, tipo de documento, error en profesión y/o institución, error en fecha de grado ó error en el No. de identificación de la resolución de aprobación ya generada; si tiene dudas o inquietudes diferentes a los casos mencionados favor no continuar y remitir dicha inquietud al correo electrónico contactenos@saludcapital.gov.co. \r Esta seguro de continuar con esta solicitud.', 
			function(){ 
				$("#formAclaracion").submit();
			}
            , 
			function(){ 
				window.location = "<?php  echo site_url('/usuario/'); ?>";
			}
		).set('labels', {ok:'Enviar Solicitud', cancel:'Cancelar Solicitud'});
	});
		

</script>
