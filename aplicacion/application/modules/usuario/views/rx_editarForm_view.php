
<h3 class="text-blue"><b>Registro y autorización de Licencia de practica medica categoría I y II</b></h3>

<?php

    $retornoError = $this->session->flashdata('error');
    if ($retornoError) {
        ?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
    }

    $retornoExito = $this->session->flashdata('exito');
    if ($retornoExito) {
        ?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
    }

if($tramite_info->modulo1 == 0){
	$step1_class = "yellow";
	$step1_check = "display:none";
}else{
	$step1_class = "green";
	$step1_check = "display:block";
}

if($tramite_info->modulo2 == 0){
	$step2_class = "yellow";
	$step2_check = "display:none";
}else{
	$step2_class = "green";
	$step2_check = "display:block";
}

if($tramite_info->modulo3 == 0){
	$step3_class = "yellow";
	$step3_check = "display:none";
}else{
	$step3_class = "green";
	$step3_check = "display:block";
}

if($tramite_info->modulo4 == 0){
	$step4_class = "yellow";
	$step4_check = "display:none";
}else{
	$step4_class = "green";
	$step4_check = "display:block";
}

if($tramite_info->modulo5 == 0){
	$step5_class = "yellow";
	$step5_check = "display:none";
}else{
	$step5_class = "green";
	$step5_check = "display:block";
}

if($tramite_info->modulo1 == 1 && $tramite_info->modulo2 == 1 && $tramite_info->modulo3 == 1 && $tramite_info->modulo4 == 1 && $tramite_info->modulo5 == 1){
	$btn_fin_proceso = "1";
}else{
	$btn_fin_proceso = "0";
}
?>


<div id="divPasos" class="text-center">
   <div class="registro_triada">
      <button class="btn <?php echo $step1_class?>" onClick="step1();" id="cstep1"><span id="check_step1" style="<?php echo $step1_check?>"><i class="far fa-check-square"></i></span> Localizacíón Entidad</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn <?php echo $step2_class?>" onClick="step2();" id="cstep2"><span id="check_step2" style="<?php echo $step2_check?>"><i class="far fa-check-square"></i></span> Equipos Rayos X</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn <?php echo $step3_class?>" onClick="step3();" id="cstep3"><span id="check_step3" style="<?php echo $step3_check?>"><i class="far fa-check-square"></i></span> Trabajadores TOE</button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn <?php echo $step4_class?>" onClick="step4();" id="cstep4"><span id="check_step4" style="<?php echo $step4_check?>"><i class="far fa-check-square"></i></span> Talento Humano </button>
      <i class="fa fa-chevron-right"></i>
      <button class="btn <?php echo $step5_class?>" onClick="step5();" id="cstep5"><span id="check_step5" style="<?php echo $step5_check?>"><i class="far fa-check-square"></i></span> Documentos Adjuntos</button>	  
	  <?php 
		if($btn_fin_proceso == 1){
			
			if($tramite_info->estado == 13){
				?>
				<i class="fa fa-chevron-right"></i>
				<button class="btn yellow" onClick="step6();" id="cstep6"><span id="check_step6" style="<?php echo $step5_check?>"><i class="far fa-check-square"></i></span> Subsanar Trámite</button>	  	
				<?php
			}else if($tramite_info->estado == 22){
				?>
				<i class="fa fa-chevron-right"></i>
				<button class="btn yellow" onClick="step6();" id="cstep6"><span id="check_step6" style="<?php echo $step5_check?>"><i class="far fa-check-square"></i></span> Subsanar Trámite</button>	  	
				<?php
			}else{
				?>
				<i class="fa fa-chevron-right"></i>
				<button class="btn yellow" onClick="step6();" id="cstep6"><span id="check_step6" style="<?php echo $step5_check?>"><i class="far fa-check-square"></i></span> Finalizar Trámite</button>	  	
				<?php
			}			
		}
	  ?>	  
   </div>
</div>


<!-- localizacion Identidad -->
<form id="formSeccion1" name="form_tramite" action="<?php echo base_url('usuario/editarDireccionRX')?>" method="post" class="form-row">

   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso1" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Localizacíón Entidad:</b></h3>
        </div>

        <div class="row">
            <div class="col-md-6">
               <span class="text-orange">•</span><label for="depto_entidad">Departamento(*)</label>
               <input id="id_tramite" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">
               <input id="id_tramite" name="id_direccion_tramite" type="hidden" value="<?php if(isset($rayosxDireccion)&& $rayosxDireccion->id_direccion_rayosx != ''){ echo $rayosxDireccion->id_direccion_rayosx;} ?>">
               <select id="depto_entidad" name="depto_entidad" class="form-control validate[required]" required>
                  <option value=""> - Seleccione Departamento -</option>
                  <?php
				  for($i=0;$i<count($departamento);$i++){
					  ?>
						<option value="<?php echo $departamento[$i]->IdDepartamento?>"
						<?php
							if(isset($rayosxDireccion->depto_entidad) && $rayosxDireccion->depto_entidad == $departamento[$i]->IdDepartamento)
								{
									echo "selected";
								}else{ 
									echo "";
								} ?>>
							<?php echo $departamento[$i]->Descripcion?>				
						</option>
				  <?php
				  }
				  ?>
               </select>
            </div>
			<?php
			if(isset($rayosxDireccion->depto_entidad)){
				$municipios = $this->login_model->municipios_col($rayosxDireccion->depto_entidad);
			}
			
			?>
            <div class="col-md-6">
               <span class="text-orange">•</span><label for="mpio_entidad">Municipio(*)</label>
               <select id="mpio_entidad" name="mpio_entidad" class="form-control validate[required]" required>
				<option value="">Seleccione...</option>
				<?php 
					if(count($municipios) > 0){
						for($j=0;$j<count($municipios);$j++){
							?>
								<option value="<?php echo $municipios[$j]->CodigoDane?>" 
								<?php
									if(isset($rayosxDireccion->mpio_entidad) && $rayosxDireccion->mpio_entidad == $municipios[$j]->CodigoDane)
										{
											echo "selected";
										}?>>
									<?php echo $municipios[$j]->Descripcion?>				
								</option>
							<?php
						}
						
					}
				
				?>
               </select>
               <div class="col-md-2" style="visibility:hidden;"><span id="loader"><i class="fa fa-spinner fa-3x fa-spin"></i></span></div>
            </div>

            <!--div class="col">
               <span class="text-orange">•</span><label for="loc_entidad">Localidad</label>
               <select id="loc_entidad" name="loc_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Localidad...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="barrio_entidad">Barrio</label>
               <select id="barrio_entidad" name="barrio_entidad" class="form-control validate[required]">
                  <option value="">Seleccione Barrio...</option>
                  <option value = "Prueba">Prueba</option>
               </select>
            </div-->

        </div>

        <div class="form-group">
            <span class="text-orange">•</span><label for="tipo_tramite">Dirección de la Instalación(*)</label>
            <input id="dire_entidad" name="dire_entidad" class="form-control input-md validate[required, minSize[4], maxSize[200]]" type="text" required placeholder="Ingresar la Dirección Entidad" value="<?php if(isset($rayosxDireccion->dire_entidad)){echo $rayosxDireccion->dire_entidad;}?>"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="200" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 200 carácteres">
         </div>

         <div class="form-group">
            <span class="text-orange">•</span><label for="dire_entidad">Sede de la Instalación(*)</label>
            <input id="sede_entidad" name="sede_entidad" class="form-control input-md validate[required, minSize[4], maxSize[200]]" type="text" required placeholder="Ingresar el Nombre Distintivo de la sede que será sujeta a inspección" value="<?php if(isset($rayosxDireccion->sede_entidad)){echo $rayosxDireccion->sede_entidad;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"  minlength="4" maxlength="200"  title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 200 carácteres">
         </div>
         <!-- 2 campos en la fila --->
         <div class="row">
            <div class="col-md-6">
               <span class="text-orange">•</span><label for="email_entidad">Correo Electrónico(*)</label>
               <input id="email_entidad" name="email_entidad" class="form-control validate[required, custom[email]] input-md" type="email" required placeholder="Ingresar Correo Electrónico"  value="<?php if(isset($rayosxDireccion->email_entidad)){echo $rayosxDireccion->email_entidad;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"  minlength="4" maxlength="30"  title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col">
               <span class="text-orange">•</span><label for="celular_entidad">Número celular(*)</label>
               <input id="celular_entidad" name="celular_entidad" placeholder="Ingresar Número Celular de contacto en sede de la Instalación" class="form-control input-md validate[required,custom[number], minSize[10], maxSize[10]]" value="<?php if(isset($rayosxDireccion->celular_entidad)){echo $rayosxDireccion->celular_entidad;}?>"   minlength="10" maxlength="10"  title="Ingresar un Tamaño mínimo: 10 carácteres a un Tamaño máximo: 10 carácteres" onKeyPress="if(this.value.length==10) return false;">
            </div>
         </div>
         <div class="row">
            <div class="col-md-6">
               <span class="text-orange">•</span><label for="telefono_entidad">Número telefónico fijo</label>
               <input id="telefono_entidad" name="telefono_entidad" placeholder="Ingresar Número telefónico de contacto en sede de la Instalación" class="form-control input-md validate[required, custom[number], minSize[7], maxSize[10]]" value="<?php if(isset($rayosxDireccion->telefono_entidad)){echo $rayosxDireccion->telefono_entidad;}?>"   minlength="7" maxlength="10"  title="Ingresar un Tamaño mínimo: 7 carácteres a un Tamaño máximo: 10 carácteres" onKeyPress="if(this.value.length==10) return false;">
            </div>

            <div class="col-md-6">
               <span class="text-orange">•</span><label for="extension_entidad">Extensión</label>
               <input id="extension_entidad" name="extension_entidad" placeholder="Ingresar Extensión telefónica de contacto en sede" class="form-control input-md validate[custom[number], minSize[3], maxSize[5]]" value="<?php if(isset($rayosxDireccion->extension_entidad)){echo $rayosxDireccion->extension_entidad;}?>"   minlength="3" maxlength="5"  title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 5 carácteres" onKeyPress="if(this.value.length==5) return false;">
            </div>
         </div>

          <div id="btnRegistrarGuardarEntidad" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>
      </div>
   </div>
</form>


<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion2" name="formSeccion2" method="post" class="form-row collapse" action="<?php echo base_url('usuario/editarCategoriaRX')?>">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso2" class="row block w-100 newsletter ">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Equipos generadores de radiación ionizante:</b></h3>
        </div>
        <div class="row">
			<?php
			$verbtnCate = "block";
			?>
           <div class="col-md-12">
               <span class="text-orange">•</span><label for="categoria">Categoría</label>
               <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">
			   <input id="id_categoria_rayosx" name="id_categoria_rayosx" type="hidden" value="<?php if(isset($tramite_info)&& $tramite_info->categoria != ''){ echo $tramite_info->categoria;} ?>">
               <select id="categoria" name="categoria" class="form-control validate[required]" required <?php if(isset($tramite_info->categoria) && ($tramite_info->categoria == 1 || $tramite_info->categoria == 2)){echo "disabled";}?>>
                  <option value="">Seleccione...</option>
                  <option value="1"
                  <?php if(isset($tramite_info->categoria) && $tramite_info->categoria == 1){echo 'selected';$vercate1="block";$verbtnCate = "none";}else{$vercate1="none";} ?>>
                    Categoría  I
                  </option>
                  <option value="2"
                  <?php if(isset($tramite_info->categoria) && $tramite_info->categoria == 2){echo 'selected';$verbtnCate = "none";$vercate2="block";}else{$vercate2="none";}?>>
					Categoría  II
                  </option>
               </select>
            </div>
         </div>
		 <div id="resultado_seccion2"></div>
         <div class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow" id="btnGuardarCategoria" style="display:<?php echo $verbtnCate;?>">
                  Guardar y Continuar
               </button>
			   <?php 
			   if(isset($tramite_info->categoria) && ($tramite_info->categoria == 1 || $tramite_info->categoria == 2))
				{
					$btn_res = "display:block";
					
				}else{
					$btn_res = "display:none";
				}					
			   ?>
			   <button type="button" id="btn_rest_cat" class="btn red" style="<?php echo $btn_res?>">
					Restablecer categoría
				</button>
            </p>
		</div>
      </div>
   </div>
</form>

<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion2-1" name="formSeccion2-1" action="<?php echo base_url('usuario/editarEquipoRX')?>" method="post" class="form-row">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso2" class="row block w-100 newsletter ">
		<input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">

      <div class="w-100">
        <div class="subtitle">
            <h3><b>Descripción del equipo generador de radiación ionizante:</b></h3>
        </div>
          <div class="row">
            <div class="col-md-6" style="display:<?php echo $vercate1?>" id="div_categoria1">
                <span class="text-orange">•</span><label for="categoria1">Equipos generadores de radicaci&oacute;n ionizante</label>
                <div>
                    <select id="categoria1" name="categoria1" class="form-control validate[required]">
                        <option value="">Seleccione...</option>
                        <option value="1" >Radiolog&iacute;a odontol&oacute;gica periapical</option>
                        <option value="2" >Equipo de RX</option>
                    </select>
                </div>
            </div>
          </div>
          <div class="row">
			<div class="col-md-6" style="display:<?php echo $vercate2?>" id="div_categoria2">
				<label for="categoria2">Equipos generadores de radicaci&oacute;n ionizante</label>
				<div>
					<select id="categoria2" name="categoria2" class="form-control validate[required]">
						<option value="">Seleccione...</option>
						<option value="1" >Radioterapia</option>
						<option value="2" >Radio diagn&oacute;stico de alta complejidad</option>
						<option value="3" >Radio diagn&oacute;stico de media complejidad</option>
						<option value="4" >Radio diagn&oacute;stico de baja complejidad</option>
						<option value="5" >Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</option>
					</select>
				</div>
			</div>
			<div class="col-md-6" style="display:none" id="div_categoria1-1">
				<label for="categoria1-1">Radiolog&iacute;a odontol&oacute;gica periapical</label>
				<div>
					<select id="categoria1_1" name="categoria1_1" class="form-control validate[required]">
				<option value="">Seleccione...</option>
				<option value="1" >Equipo de RX odontol&oacute;gico periapical</option>
				<option value="2" >Equipo de RX odontol&oacute;gico periapical portat&iacute;l</option>
			</select>
				</div>
			</div>
			<div class="col-md-6" style="display:none" id="div_categoria1-2">
				<label for="categoria1-2">Equipo de RX</label>
				<div>
					<select id="categoria1_2" name="categoria1_2" class="form-control validate[required]">
				<option value="">Seleccione...</option>
				<option value="1" >Densit&oacute;metro &oacute;seo</option>
			</select>
				</div>
			</div>
			<div class="col-md-6" style="display:none" id="div_categoria2-1">
				<label for="categoria2_1">Equipo de RX</label>
				<div>
					<select id="categoria2_1" name="categoria2_1" class="form-control validate[required]">
				<option value="">Seleccione...</option>
				<option value="1">Equipo de RX convencional</option>
				<option value="2">Tomógrafo Odontológico</option>
				<option value="3">Tomógrafo</option>
				<option value="4">Equipo de RX Portátil</option>
				<option value="5">Equipo de RX Odontológico</option>
				<option value="6">Panorámico Cefálico</option>
				<option value="7">Fluoroscopio</option>
				<option value="8">SPECT-CT</option>
				<option value="9">Arco en C</option>
				<option value="10">Mamógrafo</option>
				<option value="11">Litotriptor</option>
				<option value="12">Angiógrafo</option>
				<option value="13">PET-CT</option>
				<option value="14">Acelerador lineal</option>
				<option value="15">Sistema de radiocirugia robótica</option>
				<option value="16">Otro</option>
			</select>
				</div>
			</div>
			<div class="col-md-6" style="display:none" id="div_categoria2-1-otro">
				<label for="otro_equipo">Otro equipo de RX</label>
				<input id="otro_equipo" name="otro_equipo" placeholder="Otro equipo" class="form-control input-md validate[minSize[4], maxSize[100]]"  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="100" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 100 carácteres">
				</div>
			
			<div class="col-md-6">
				<label for="tipo_visualizacion">Tipo de visualización de la imagen</label>
				<select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]" required>
				   <option value="1" >Digital</option>
				   <option value="2" >Digitalizado</option>
				   <option value="3" >Análogo</option>
				   <option value="4" >Revelado Automático</option>
				   <option value="5" >Revelado Manual</option>
				   <option value="6" >Monitor Análogo</option>
				   <option value="7" >No Aplica</option>
				</select>
			 </div>
			 </div>
		 </div>
		 <div class="row">
		 
			<div class="col-md-3">
               <label for="marca_equipo">Marca equipo</label>
               <input id="marca_equipo" name="marca_equipo" placeholder="Ingresar Marca equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="modelo_equipo">Modelo equipo</label>
               <input id="modelo_equipo" name="modelo_equipo" placeholder="Ingresar Modelo equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"   onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="serie_equipo">Serie equipo</label>
               <input id="serie_equipo" name="serie_equipo" placeholder="Ingresar Serie equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="marca_tubo_rx">Marca tubo RX</label>
               <input id="marca_tubo_rx" name="marca_tubo_rx" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="modelo_tubo_rx">Modelo tubo RX</label>
               <input id="modelo_tubo_rx" name="modelo_tubo_rx" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="serie_tubo_rx">Serie tubo RX</label>
               <input id="serie_tubo_rx" name="serie_tubo_rx" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="tension_tubo_rx">Tensión máxima tubo RX [kV]</label>
               <input id="tension_tubo_rx" name="tension_tubo_rx" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]"  required step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;" >
            </div>

            <div class="col-md-3">
               <label for="contiene_tubo_rx">Corriente Max del tubo RX [mA]</label>
               <input id="contiene_tubo_rx" name="contiene_tubo_rx" placeholder="Ingresar corriente máxima del tubo RX [mA]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]" required step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="energia_fotones">Energ&iacute;a de fotones [MeV]</label>
               <input id="energia_fotones" name="energia_fotones" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[3]]" required step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="energia_electrones">Energ&iacute;a de electrones [MeV]</label>
               <input id="energia_electrones" name="energia_electrones" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[3]]" required step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="carga_trabajo">Carga de trabajo [mA.min/semana]</label>
               <input id="carga_trabajo" name="carga_trabajo" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]" required step="any"    min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>
			<div class="col-md-4">
				<label for="ubicacion_equipo">Ubicación del equipo de la instalación</label>
				<input id="ubicacion_equipo" name="ubicacion_equipo" placeholder="Ingresar Ubicación del equipo de la instalación" class="form-control input-md validate[required, minSize[4], maxSize[100]]" required type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="100" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 100 carácteres">
			</div>
			<div class="col-md-4">
               <label for="anio_fabricacion">A&ntilde;o de fabricación del equipo</label>
               <input id="anio_fabricacion" name="anio_fabricacion" placeholder="Ingresar A&ntilde;o de fabricación del equipo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]" onKeyPress="if(this.value.length==4) return false;">
            </div>

            <div class="col-md-4">
               <label for="anio_fabricacion_tubo">A&ntilde;o de fabricación del tubo</label>
               <input id="anio_fabricacion_tubo" name="anio_fabricacion_tubo" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]]" onKeyPress="if(this.value.length==4) return false;">
            </div>

            <div class="col-md-4" id="div_numpermiso">
               <label for="numero_permiso">Número de permiso de comercialización</label>
               <input id="numero_permiso" name="numero_permiso" placeholder="Ingresar Número de permiso de comercialización" class="form-control input-md validate[minSize[4], maxSize[30]]" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>
		 </div>
		 <div class="row">
			<table class="table">
				<thead>
					<th>Descripción documento</th>
					<th>PDF</th>
				</thead>
				<tbody>
					<tr>
						<td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
						<td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
					</tr>
					<tr>
						<td>Informe sobre los resultados del control de calidad</td>
						<td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
					</tr>
					<tr>
						<td>Plano general de las instalaciones</td>
						<td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
					</tr>
					<?php
						if($tramite_info->categoria == 1){
							?>
							<tr>
								<td>Pruebas iniciales de caracterización de los equipos o licencia anterior</td>
								<td><input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="archivopdf validate[required]"></td>
							</tr>
							<?php
						}
					?>					
				</tbody>
			</table>
         </div>
		 <div class="row" id="div_info_tubo2" style="display:none">	
			<div class="subtitle">
				<h3><b>Descripción tubo Rx 2:</b></h3>
			</div>
            <div class="col-md-3">
               <label for="marca_tubo_rx2">Marca tubo RX</label>
               <input id="marca_tubo_rx2" name="marca_tubo_rx2" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="modelo_tubo_rx2">Modelo tubo RX</label>
               <input id="modelo_tubo_rx2" name="modelo_tubo_rx2" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"    type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="serie_tubo_rx2">Serie tubo RX</label>
               <input id="serie_tubo_rx2" name="serie_tubo_rx2" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
            </div>

            <div class="col-md-3">
               <label for="tension_tubo_rx2">Tensión máxima tubo RX [kV]</label>
               <input id="tension_tubo_rx2" name="tension_tubo_rx2" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[4]]"   step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;" >
            </div>

            <div class="col-md-3">
               <label for="contiene_tubo_rx2">Corriente Max del tubo RX [mA]</label>
               <input id="contiene_tubo_rx2" name="contiene_tubo_rx2" placeholder="Ingresar corriente máxima del tubo RX [mA]" class="form-control input-md validate[custom[number], minSize[1], maxSize[4]]"  step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="energia_fotones2">Energ&iacute;a de fotones [MeV]</label>
               <input id="energia_fotones2" name="energia_fotones2" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[3]]"  step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="energia_electrones2">Energ&iacute;a de electrones [MeV]</label>
               <input id="energia_electrones2" name="energia_electrones2" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[3]]"  step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>

            <div class="col-md-4">
               <label for="carga_trabajo2">Carga de trabajo [mA.min/semana]</label>
               <input id="carga_trabajo2" name="carga_trabajo2" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[custom[number],  minSize[1], maxSize[4]]"  step="any"    min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
            </div>
            <div class="col-md-4">
               <label for="anio_fabricacion_tubo2">A&ntilde;o de fabricación del tubo</label>
               <input id="anio_fabricacion_tubo2" name="anio_fabricacion_tubo2" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]]" onKeyPress="if(this.value.length==4) return false;">
            </div>
		 </div>
		 <div class="row" id="div_doc_tubo2" style="display:none">
			<table class="table">
				<div class="subtitle">
					<h3><b>Documentos tubo Rx 2:</b></h3>
				</div>
				<thead>
					<th>Descripción documento</th>
					<th>PDF</th>
				</thead>
				<tbody>
					<tr>
						<td>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje</td>
						<td><input id="fi_blindajes2" name="fi_blindajes2" type="file" class="archivopdf validate[]"></td>
					</tr>
					<tr>
						<td>Informe sobre los resultados del control de calidad</td>
						<td><input id="fi_control_calidad2" name="fi_control_calidad2" type="file" class="archivopdf validate[]"></td>
					</tr>
					<?php
						if($tramite_info->categoria == 1){
							?>
							<tr>
								<td>Pruebas iniciales de caracterización de los equipos o licencia anterior</td>
								<td><input id="fi_pruebas_caracterizacion2" name="fi_pruebas_caracterizacion2" type="file" class="archivopdf validate[required]"></td>
							</tr>
							<?php
						}
					?>	
				</tbody>
			</table>
         </div>
			
          <div id="btnGuardarEquipos" class="col-md-12 pt-200">
            <p align="center">
               <br/>
               <!-- Primer Collapsible - Localizacion Entidad -->
               <button type="submit" class="btn yellow">
                  Guardar y Continuar
               </button>
            </p>
          </div>		  
		  <div id="resultado_seccion2_1" class="col-12 col-md-12 table-responsive">
			<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Marca Equipo</th>
					<th>Modelo Equipo</th>
					<th>Serie Equipo</th>										
					<th>Ver más</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
			<?php
			for($i=0;$i<count($rayosxEquipo);$i++){
				?>
				<tr>
					<td><?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></td>
					<td><?php echo $rayosxEquipo[$i]->marca_equipo?></td>
					<td><?php echo $rayosxEquipo[$i]->modelo_equipo?></td>
					<td><?php echo $rayosxEquipo[$i]->serie_equipo?></td>										
					<td>
						<a class="btn green" onClick="abrirModal('Equipo <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>','#ex<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>')">Ver más...</a>
					</td>
					<td>
						<a class="btn red" href="#" onClick="eliminarEquipo(<?php echo $rayosxEquipo[$i]->id_tramite_rayosx?>,<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">Eliminar</a>
					</td>
				</tr>
				<div id="ex<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="modal">
				  <p><h2><b>Equipo ID:<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></b></h2></p>
					<ul>
                        <?php
                                    if($rayosxEquipo[$i]->categoria1 != 0 || $rayosxEquipo[$i]->categoria1 != NULL){
                                        if($rayosxEquipo[$i]->categoria1 == 1){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiolog&iacute;a odontol&oacute;gica periapical</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria1 == 2){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Equipo de RX</li>
                                            <?php
                                        }
                                    }
                               
                                    if($rayosxEquipo[$i]->categoria2 != 0 || $rayosxEquipo[$i]->categoria2 != NULL){
                                        if($rayosxEquipo[$i]->categoria2 == 1){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radioterapia</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2 == 2){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de alta complejidad</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2 == 3){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de media complejidad</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2 == 4){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de baja complejidad</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2 == 5){
                                            ?>
                                            <li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</li>
                                            <?php
                                        }
                                    }
                        
                                    if($rayosxEquipo[$i]->categoria1_1 != 0 || $rayosxEquipo[$i]->categoria1_1 != NULL){
                                        if($rayosxEquipo[$i]->categoria1_1 == 1){
                                            ?>
                                            <li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria1_1 == 2){
                                            ?>
                                            <li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical portat&iacute;l</li>
                                            <?php
                                        }
                                    }
                        
                                    if($rayosxEquipo[$i]->categoria1_2 != 0 || $rayosxEquipo[$i]->categoria1_2 != NULL){
                                        if($rayosxEquipo[$i]->categoria1_2 == 1){
                                            ?>
                                            <li><b>Equipo de RX: </b>Densit&oacute;metro &oacute;seo</li>
                                            <?php
                                        }
                                    }
									
									if($rayosxEquipo[$i]->categoria2_1 != 0 || $rayosxEquipo[$i]->categoria2_1 != NULL){
                                        if($rayosxEquipo[$i]->categoria2_1 == 1){
                                            ?>
                                            <li><b>Equipo de RX: </b>Equipo de RX convencional</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 2){
                                            ?>
                                            <li><b>Equipo de RX: </b>Tomógrafo Odontológico</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 3){
                                            ?>
                                            <li><b>Equipo de RX: </b>Tomógrafo</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 4){
                                            ?>
                                            <li><b>Equipo de RX: </b>Equipo de RX Portátil</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 5){
                                            ?>
                                            <li><b>Equipo de RX: </b>Equipo de RX Odontológico</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 6){
                                            ?>
                                            <li><b>Equipo de RX: </b>Panorámico Cefálico</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 7){
                                            ?>
                                            <li><b>Equipo de RX: </b>Fluoroscopio</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 8){
                                            ?>
                                            <li><b>Equipo de RX: </b>SPECT-CT</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 9){
                                            ?>
                                            <li><b>Equipo de RX: </b>Arco en C</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 10){
                                            ?>
                                            <li><b>Equipo de RX: </b>Mamógrafo</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 11){
                                            ?>
                                            <li><b>Equipo de RX: </b>Litotriptor</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 12){
                                            ?>
                                            <li><b>Equipo de RX: </b>Angiógrafo</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 13){
                                            ?>
                                            <li><b>Equipo de RX: </b>PET-CT</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 14){
                                            ?>
                                            <li><b>Equipo de RX: </b>Acelerador lineal</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 15){
                                            ?>
                                            <li><b>Equipo de RX: </b>Sistema de radiocirugia robótica</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->categoria2_1 == 16){
                                            ?>
                                            <li><b>Equipo de RX: </b><?php echo $rayosxEquipo[$i]->otro_equipo?></li>
                                            <?php
                                        }
                                    }					
                        
                                    if($rayosxEquipo[$i]->tipo_visualizacion != 0 || $rayosxEquipo[$i]->tipo_visualizacion != NULL){
                                        if($rayosxEquipo[$i]->tipo_visualizacion == 1){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Digital</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 2){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Digitalizado</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 3){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Análogo</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 4){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Revelado Automático</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 5){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Revelado Manual</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 6){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>Monitor Análogo</li>
                                            <?php
                                        }else if($rayosxEquipo[$i]->tipo_visualizacion == 7){
                                            ?>
                                            <li><b>Tipo de visualización de la imagen: </b>No Aplica</li>
                                            <?php
                                        }
                                    }
                                ?>
						<li><b>Marca Equipo: </b><?php echo $rayosxEquipo[$i]->marca_equipo?></li>
						<li><b>Modelo Equipo: </b><?php echo $rayosxEquipo[$i]->modelo_equipo?></li>
						<li><b>Serie Equipo: </b><?php echo $rayosxEquipo[$i]->serie_equipo?></li>
						<li><b>Marca Tubo RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx?></li>
						<li><b>Modelo Tubo RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx?></li>
						<li><b>Serie Tubo RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx?></li>
						<li><b>Tensión máxima tubo RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx?></li>
						<li><b>Cont. Max del tubo RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx?></li>
						<li><b>Energía de fotones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones?></li>
						<li><b>Energía de electrones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones?></li>
						<li><b>Carga de trabajo [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo?></li>
						<li><b>Ubicación del equipo de la instalación: </b><?php echo $rayosxEquipo[$i]->ubicacion_equipo?></li>
						<li><b>Año de fabricación del equipo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion?></li>
						<li><b>Año de fabricación del tubo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo?></li>
						
						<?php
							
							if($rayosxEquipo[$i]->fi_blindajes != ''){
									$fi_blindajes = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes);
									
									if($fi_blindajes){
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
										<?php
									}		
								}else{
									?>
									<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
									<?php
								}
								
								
								
								if($rayosxEquipo[$i]->fi_control_calidad != ''){
									$fi_control_calidad = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad);
									
									if($fi_control_calidad){
										?>
										<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
									<?php
								}	

								if($rayosxEquipo[$i]->fi_plano != ''){
									$fi_plano = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_plano);
									
									if($fi_plano){
										?>
										<li><b>Plano general de las instalaciones: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_plano->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
									<?php
								}		
								
								if($rayosxEquipo[$i]->fi_pruebas_caracterizacion != ''){
									$fi_pruebas_caracterizacion = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion);
									
									if($fi_pruebas_caracterizacion){
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion->nombre);?>" target="_blank">Ver archivo</a></li>		
										<?php
									}else{
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
										<?php
									}
								}else{
									?>
									<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
									<?php
								}		
						
							if($rayosxEquipo[$i]->categoria2_1 == 16){
								
								?>
								<h3>Información Tubo RX 2</h3>
								
								<li><b>Marca Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx2?></li>
								<li><b>Modelo Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx2?></li>
								<li><b>Serie Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx2?></li>
								<li><b>Tensión máxima tubo 2 RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx2?></li>
								<li><b>Cont. Max del tubo 2 RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx2?></li>
								<li><b>Energía de fotones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones2?></li>
								<li><b>Energía de electrones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones2?></li>
								<li><b>Carga de trabajo 2 [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo2?></li>
								<li><b>Año de fabricación del tubo2: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo2?></li>
								<?php
																
								if($rayosxEquipo[$i]->fi_blindajes2 != ''){
										$fi_blindajes2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes2);
										
										if($fi_blindajes2){
											?>
											<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
											<?php
										}		
									}else{
										?>
										<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
										<?php
									}
									
									if($rayosxEquipo[$i]->fi_control_calidad2 != ''){
										$fi_control_calidad2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad2);
										
										if($fi_control_calidad2){
											?>
											<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
											<?php
										}
									}else{
										?>
										<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
										<?php
									}	

									
									if($rayosxEquipo[$i]->fi_pruebas_caracterizacion2 != ''){
										$fi_pruebas_caracterizacion2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion2);
										
										if($fi_pruebas_caracterizacion2){
											?>
											<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion2->nombre);?>" target="_blank">Ver archivo</a></li>		
											<?php
										}else{
											?>
											<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
											<?php
										}
									}else{
										?>
										<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
										<?php
									}
							
								
							}
						?>
					</ul>					  
				</div>
				<?php
			}
			?>								
			</tbody>
		</table>
		  </div>
      </div>
   </div>
</form>

      <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
      <!-- Equipos generadores de radiación ionizante -->
		<form id="formSeccion3-1" name="formSeccion3-1" action="<?php echo base_url('usuario/editarOficialToeRX')?>" method="post" >
              <div id="paso3-1" class="row block w-100 newsletter">
                 <div class="w-100">
                   <div class="subtitle">
                       <h3><b>Trabajadores ocupacionalmente expuestos - TOE:</b></h3>
                   </div>
                   <div class="row">
						<input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">
						<input id="id_encargado_rayosx" name="id_encargado_rayosx" type="hidden" value="<?php if(isset($rayosxOficialToe)&& $rayosxOficialToe->id_encargado_rayosx != ''){ echo $rayosxOficialToe->id_encargado_rayosx;} ?>">
                      <div class="col">
                       <h4><b><span class="text-orange">•</span>Oficial de protección radiológica/Encargado de protección Radiológica</b></h4>
                      </div>
                   </div>
                       <div class="row">

                           <div class="col-md-3">
                              <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
                              <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[3], maxSize[30]]" required type="text" value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_pnombre;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
                           </div>

                           <div class="col-md-3">
                              <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
                              <input id="encargado_snombre" name="encargado_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[3], maxSize[30]]"  type="text" value="<?php if(isset($rayosxOficialToe)) {echo $rayosxOficialToe->encargado_snombre ;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
                           </div>

                           <div class="col-md-3">
                              <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
                              <input id="encargado_papellido" name="encargado_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[3], maxSize[30]]" required type="text" value="<?php if(isset($rayosxOficialToe)) {echo $rayosxOficialToe->encargado_papellido;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
                           </div>
                           <div class="col-md-3">
                              <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
                              <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[3], maxSize[30]]" type="text" value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_sapellido; }?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-4">
                              <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
                              <select id="encargado_tdocumento" name="encargado_tdocumento" class="form-control validate[required]" required>
                                 <option value=""> - Seleccione Tipo Documento -</option>
								 <?php
								 for($i=0;$i<count($tipo_identificacion_natural);$i++){
									 ?>
									 <option value="<?php echo $tipo_identificacion_natural[$i]->IdTipoIdentificacion?>" 
									 <?php if(isset($rayosxOficialToe->encargado_tdocumento) && $rayosxOficialToe->encargado_tdocumento == $tipo_identificacion_natural[$i]->IdTipoIdentificacion ){ echo 'selected';}?>>
                                     <?php echo $tipo_identificacion_natural[$i]->Descripcion?>
									 </option>
									 <?php
								 }
								 ?>
                                 
                              </select>
                           </div>

                           <div class="col-md-4">
                              <span class="text-orange">•</span><label for="encargado_ndocumento">Número Documento</label>
                              <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[custom[number], required, minSize[4], maxSize[15]]" required value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_ndocumento; }?>" onKeyPress="if(this.value.length==15) return false;">
                           </div>

                           <div class="col-md-4">
                              <span class="text-orange">•</span><label for="encargado_lexpedicion">Lugar Expedición</label>
                              <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Ingresar Lugar Expedición" required class="form-control input-md validate[required, minSize[4], maxSize[30]]" type="text" value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_lexpedicion; }?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
                           </div>

                        </div>

                        <div class="form-group">
                           <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
                           <input id="encargado_correo" name="encargado_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_correo; }?>" required>
                        </div>
						
                        <div class="row">
                           <div class="col-md-6">
                              <span class="text-orange">•</span><label for="encargado_nivel">Nivel Académico</label>
                              <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]" required>
                                 <option value="">Seleccione...</option>
								 <?php
								 for($i=0;$i<count($nivelAcademico);$i++){
									 ?>
									 <option value="<?php echo $nivelAcademico[$i]->IdNivelEducativo?>" 
									 <?php if(isset($rayosxOficialToe->encargado_nivel) && $rayosxOficialToe->encargado_nivel == $nivelAcademico[$i]->IdNivelEducativo ){ echo 'selected';}?>>
                                     <?php echo $nivelAcademico[$i]->Nombre?>
									 </option>
									 <?php
								 }
								 ?>                                 
                              </select>
                           </div>

                           <div class="col-md-6">
                              <span class="text-orange">•</span><label for="encargado_profesion">Profesión</label>
							  <input id="encargado_profesion" name="encargado_profesion" placeholder="Ingresar Profesión" class="form-control validate[required]" type="text" value="<?php if(isset($rayosxOficialToe)){echo $rayosxOficialToe->encargado_profesion; }?>" required>
                           </div>
                        </div>
						<br>
						<div class="row">
							<div class="alert alert alert-success" role="alert">
								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									Si el oficial o encargado también es TOE deberá relacionarlo en el módulo TOE - Trabajadores ocupacionalmente expuestos
							</div>
						</div>
							
						<div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
						   <p align="center">
							  <br/>
							  <!-- Primer Collapsible - Localizacion Entidad -->
							  <button type="submit" class="btn yellow">
								 Guardar Encargado TOE
							  </button>
						   </p>
						 </div>
						 <div id="resultado3_1">
						 </div>
           </div>
        </div>
        </form>

         <!-- Equipos generadores de radiación ionizante -->
         <form id="formSeccion3-2" name="formSeccion3-2" action="<?php echo base_url('usuario/editarTrabajadorToeRX')?>"  method="post" >
         <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->

            <div id="paso3-2" class="row block w-100 newsletter ">

            <div class="w-100">
                   <h4><b><span class="text-orange">•</span>TOE - Trabajadores ocupacionalmente  Expuestos</b></h4>             
            <div id="seccion3-2" >

            <div class="row">

               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_pnombre">Primer Nombre</label>
                  <input id="toe_pnombre" name="toe_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[3], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_snombre">Segundo Nombre</label>
                  <input id="toe_snombre" name="toe_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[3], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>

               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_papellido">Primer Apellido</label>
                  <input id="toe_papellido" name="toe_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[3], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>
               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_sapellido">Segundo Apellido</label>
                  <input id="toe_sapellido" name="toe_sapellido" placeholder="Ingresar Segundo Apellido" class="form-control input-md validate[minSize[3], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>
            </div>

            <div class="row">
               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_correo">Correo Electrónico</label>
                  <input id="toe_correo" name="toe_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]]" type="email" required>
               </div>
               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="encargado_tdocumento">Tipo Documento</label>
                  <select id="toe_tdocumento" name="toe_tdocumento" class="form-control validate[required]" required>
					  <option value=""> - Seleccione Tipo Documento -</option>
					 <?php
					 for($i=0;$i<count($tipo_identificacion_natural);$i++){
						 ?>
						 <option value="<?php echo $tipo_identificacion_natural[$i]->IdTipoIdentificacion?>">
						 <?php echo $tipo_identificacion_natural[$i]->Descripcion?>
						 </option>
						 <?php
					 }
					 ?>                    
                  </select>
               </div>

               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="toe_ndocumento">Número Documento</label>
                  <input id="toe_ndocumento" name="toe_ndocumento" placeholder="Ingresar Número Documento" class="form-control input-md validate[custom[number], required, minSize[4], maxSize[15]]"  onKeyPress="if(this.value.length==15) return false;">
               </div>

               <div class="col-md-3">
                  <span class="text-orange">•</span><label for="toe_lexpedicion">Lugar Expedición</label>
                  <input id="toe_lexpedicion" name="toe_lexpedicion" placeholder="Ingresar Lugar Expedición" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
               </div>
            </div>

            <div class="row">
               <div class="col-md-6">
                  <span class="text-orange">•</span><label for="toe_nivel">Nivel Académico</label>
                  <select id="toe_nivel" name="toe_nivel" class="form-control validate[required]" required>
                     <option value="">Seleccione...</option>
					 <?php
					 for($i=0;$i<count($nivelAcademico);$i++){
						 ?>
						 <option value="<?php echo $nivelAcademico[$i]->IdNivelEducativo?>">
						 <?php echo $nivelAcademico[$i]->Nombre?>
						 </option>
						 <?php
					 }
					 ?> 
                  </select>
               </div>
               <div class="col-md-6">
                  <span class="text-orange">•</span><label for="toe_profesion">Profesión</label>
				  <input id="toe_profesion" name="toe_profesion" placeholder="Ingresar Profesión" class="form-control validate[required]" type="text" required>                  
               </div>
            </div>

            <div class="row">
               <div class="col-md-6">
                  <span class="text-orange">•</span><label for="toe_ult_entrenamiento">Fecha del último entrenamiento en protección radiológica</label>
                  <input id="toe_ult_entrenamiento" name="toe_ult_entrenamiento" placeholder="Ingresar Fecha del último entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date"  max="<?php echo date('Y-m-d')?>" required >
               </div>
               <div class="col-md-6">
                  <span class="text-orange">•</span><label for="toe_pro_entrenamiento">Fecha del próximo entrenamiento en protección radiológica</label>
                  <input id="toe_pro_entrenamiento" name="toe_pro_entrenamiento" placeholder="Ingresar Fecha del próximo entrenamiento en protección radiológica" class="form-control validate[required] input-md" type="date" >
               </div>               
            </div>
			<div class="row">
				<div class="col-md-6">
                  <span class="text-orange">•</span><label for="toe_registro">Número del registro profesional de salud</label>
                  <input id="toe_registro" name="toe_registro" placeholder="Ingresar Número del registro profesional de salud" class="form-control input-md validate[custom[number], required, minSize[4], maxSize[15]]" onKeyPress="if(this.value.length==15) return false;">
               </div>
			</div>


            <div id="btnGuardarOficialTOE" class="col-md-12 pt-200">
               <p align="center">
                  <br/>
                  <!-- Primer Collapsible - Localizacion Entidad -->
                  <button type="submit" class="btn yellow">
                     Guardar TOE
                  </button>
               </p>
             </div>

         </form>
       </div>

		<div id="resultado3_2" class="col-12 col-md-12 table-responsive">
			<table class="display nowrap table table-hover">
			   <thead>
				  <tr>
					 <th>ID</th>
					 <th>Número Identificación</th>
					 <th>Nombres y Apellidos </th>
					 <th>Ver Más</th>
					 <th>Eliminar</th>
				  </tr>
			   </thead>

			<tbody>
			<?php 
			if(isset($rayosxTemporalToe)){
				for($i=0;$i<count($rayosxTemporalToe);$i++){
					?>
					<tr>
						<td><?php echo $rayosxTemporalToe[$i]->id_toe_rayosx;?></td>
						<td><?php echo $rayosxTemporalToe[$i]->toe_ndocumento;?></td>
						<td><?php echo $rayosxTemporalToe[$i]->toe_pnombre;?> <?php echo $rayosxTemporalToe[$i]->toe_snombre;?> <?php echo $rayosxTemporalToe[$i]->toe_papellido;?> <?php echo $rayosxTemporalToe[$i]->toe_sapellido;?></td>
						<td>
							<a class="btn green"  onClick="abrirModal('TOE ID: <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>','#modalToe<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>')">Ver más...</a> 
						</td>
						<td>
							<a class="btn red" href="#" onClick="eliminarTOE(<?php echo $rayosxTemporalToe[$i]->id_tramite_rayosx?>,<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>)">Eliminar</a>
						</td>
					</tr>
					<div id="modalToe<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>" class="modal">
					  <p><b>TOE ID:<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?></b></p>
						<ul>							
							<li><b>Primer Nombre: </b><?php echo $rayosxTemporalToe[$i]->toe_pnombre?></li>
							<li><b>Segundo Nombre: </b><?php echo $rayosxTemporalToe[$i]->toe_snombre?></li>
							<li><b>Primer Apellido: </b><?php echo $rayosxTemporalToe[$i]->toe_papellido?></li>
							<li><b>Segundo Apellido: </b><?php echo $rayosxTemporalToe[$i]->toe_sapellido?></li>
							<li><b>Número de identificación: </b><?php echo $rayosxTemporalToe[$i]->toe_ndocumento?></li>
							<li><b>Lugar Expedición: </b><?php echo $rayosxTemporalToe[$i]->toe_lexpedicion?></li>
							<li><b>Correo: </b><?php echo $rayosxTemporalToe[$i]->toe_correo?></li>
							<li><b>Profesión: </b><?php echo $rayosxTemporalToe[$i]->toe_profesion?></li>
							<li><b>Nivel Académico: </b><?php echo $rayosxTemporalToe[$i]->toe_nivel?></li>
							<li><b>Fecha del último entrenamiento en protección radiológica: </b><?php echo $rayosxTemporalToe[$i]->toe_ult_entrenamiento?></li>
							<li><b>Fecha del próximo entrenamiento en protección radiológica: </b><?php echo $rayosxTemporalToe[$i]->toe_pro_entrenamiento?></li>
							<li><b>Número del registro profesional de salud: </b><?php echo $rayosxTemporalToe[$i]->toe_registro?></li>
						</ul>					  
					</div>
					<?php	
				}	
			}else{
				?>
				<tr>
					<td colspan="6" scope="col">No Existen TOE Registrados</td>
				</tr>	
				<?php
			}
			?>            
			</tbody>
			</table>
		</div>
	</div>
</div>


<?php 

	if($tramite_info->visita_previa == 1){
		
		$visible_director = "display:block";
		$visible_objetos = "display:block";
		$btnGuardarIps = "display:block";
		$opcionSi = "selected";
		$opcionNo = "";
	}else if($tramite_info->visita_previa == 2){
		$visible_director = "display:none";
		$visible_objetos = "display:none";
		$btnGuardarIps = "display:none";
		$opcionSi = "";
		$opcionNo = "selected";
	}else{
		$visible_director = "display:none";
		$visible_objetos = "display:none";
		$btnGuardarIps = "display:none";
		$opcionSi = "";
		$opcionNo = "";
	}
		
?>



	<!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
	<!-- Equipos generadores de radiación ionizante -->
	<form id="formSeccion4-1" name="formSeccion4-1" action="<?php echo base_url('usuario/editarDirector')?>" method="post">
		<div id="paso4-1" class="row block w-100 newsletter ">
		<input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
		<input id="id_director_rayosx" name="id_director_rayosx" type="hidden" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->id_director_rayosx;}?>">
		<div class="w-100">
			<div class="subtitle">
				<h3><b>Talento Humano:</b></h3>
			</div>
		<div class="col-md-12">
			<span class="text-orange">•</span><label for="tipo_titulo">La IPS cuenta con el talento humano estipulado en el artículo 6 y 7, numeral 7.1?</label>
			<select id="visita_previa" name="visita_previa_th" class="form-control validate[required]">
				<option value="">Seleccione...</option>
				<option value="1" <?php echo $opcionSi;?>>SI</option>
				<option value="2" <?php echo $opcionNo;?>>NO</option>
			</select>				 
			<div id="btnGuardarIps" class="col-md-12 pt-200" style="<?php echo $btnGuardarIps?>">
				<p align="center">
				   <br/>
				   <button type="submit" class="btn yellow">
					  Guardar
				   </button>
				</p>
			</div>	
		</div>
		<div id="div_talentohumano" style="<?php echo $visible_director?>">
			<h4><b><span class="text-orange">•</span>Director Técnico</b></h4>

			<div class="row">

				<div class="col-md-3">
				   <span class="text-orange">•</span><label for="talento_pnombre">Primer Nombre</label>				   
				   <input id="talento_pnombre" name="talento_pnombre" placeholder="Ingresar Primer Nombre" class="form-control input-md validate[required, minSize[3], maxSize[30]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_pnombre;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-3">
				   <span class="text-orange">•</span><label for="talento_snombre">Segundo Nombre</label>
				   <input id="talento_snombre" name="talento_snombre" placeholder="Ingresar Segundo Nombre" class="form-control input-md validate[minSize[3], maxSize[30]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_snombre;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-3">
				   <span class="text-orange">•</span><label for="talento_papellido">Primer Apellido</label>
				   <input id="talento_papellido" name="talento_papellido" placeholder="Ingresar Primer Apellido" class="form-control input-md validate[required, minSize[3], maxSize[30]]" type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_papellido;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-3">
				   <span class="text-orange">•</span><label for="talento_sapellido">Segundo Apellido</label>
				   <input id="talento_sapellido" name="talento_sapellido" placeholder="Ingresar Segundo Apellido"  class="form-control input-md validate[minSize[3], maxSize[30]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_sapellido;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="3" maxlength="30" title="Ingresar un Tamaño mínimo: 3 carácteres a un Tamaño máximo: 30 carácteres">
				</div>
			</div>

			<div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_tdocumento">Tipo Documento</label>
				   <select id="talento_tdocumento" name="talento_tdocumento" class="form-control validate[required]" >
					  <option value=""> - Seleccione Tipo Documento -</option>
					  <?php
						 for($i=0;$i<count($tipo_identificacion_natural);$i++){
							 ?>
							 <option value="<?php echo $tipo_identificacion_natural[$i]->IdTipoIdentificacion?>" 
							 <?php if(isset($rayosxTalento->talento_tdocumento) && $rayosxTalento->talento_tdocumento == $tipo_identificacion_natural[$i]->IdTipoIdentificacion ){ echo 'selected';}?>>
							 <?php echo $tipo_identificacion_natural[$i]->Descripcion?>
							 </option>
							 <?php
						 }
						 ?>   
					  </select>
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_ndocumento">Número Documento</label>
				   <input id="talento_ndocumento" name="talento_ndocumento" placeholder="Ingresar Número Documento"  class="form-control input-md validate[custom[number], required, minSize[4], maxSize[15]]" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_ndocumento;}?>" onKeyPress="if(this.value.length==15) return false;">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_lexpedicion">Lugar Expedición</label>
				   <input id="talento_lexpedicion" name="talento_lexpedicion" placeholder="Ingresar Lugar Expedición"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"   type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_lexpedicion;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

			

			 <div class="col-md-12">
				<span class="text-orange">•</span><label for="talento_correo">Correo Electrónico</label>
				<input id="talento_correo" name="talento_correo" placeholder="Ingresar Correo Electrónico" class="form-control validate[required, custom[email]] input-md" type="email"  value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_correo;}?>" >
			 </div>
			</div> 
			 <br/>

			 <h4><b><span class="text-orange">•</span>Idoneidad Profesional</b></h4>
			 <div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_titulo">Título de pregrado obtenido </label>
				   <input id="talento_titulo" name="talento_titulo" placeholder="Título de pregrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"   type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_titulo;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_universidad">Universidad que otorgó el título de pregrado</label>
				   <input id="talento_universidad" name="talento_universidad" placeholder="Universidad que otorgó el titulo de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"   value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_universidad;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_libro">Libro del diploma de pregrado</label>
				   <input id="talento_libro" name="talento_libro" placeholder="Libro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]" type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_libro;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
				</div>

			 </div>

			 <div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_registro">Registro del diploma de pregrado</label>
				   <input id="talento_registro" name="talento_registro" placeholder="Registro del diploma de pregrado"  class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_registro;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_fecha_diploma">Fecha diploma de pregrado</label>
				   <input id="talento_fecha_diploma" name="talento_fecha_diploma" placeholder="Fecha diploma de pregrado" class="form-control input-md validate[required]"  max="<?php echo date('Y-m-d')?>"  type="date" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_fecha_diploma;}?>">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación título pregrado</label>
				   <input id="talento_resolucion" name="talento_resolucion" placeholder="Resolución convalidación título pregrado"  class="form-control input-md validate[minSize[4], maxSize[30]]"   type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_resolucion;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

			 </div>

			 <div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_fecha_convalida">Fecha convalidación título de pregrado</label>
				   <input id="talento_fecha_convalida" name="talento_fecha_convalida" placeholder="Fecha convalidación título de pregrado" class="form-control input-md" type="date"  max="<?php echo date('Y-m-d')?>" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_fecha_convalida;}?>">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_nivel">Nivel Académico último posgrado</label>
				   <select id="talento_nivel" name="talento_nivel" class="form-control validate[required]">
					  <option value="">Seleccione...</option>
					  <?php
						 for($i=0;$i<count($nivelAcademico);$i++){
							 if($nivelAcademico[$i]->IdNivelEducativo == 6){
							 ?>
								 <option value="<?php echo $nivelAcademico[$i]->IdNivelEducativo?>" 
								 <?php if(isset($rayosxTalento->talento_nivel) && $rayosxTalento->talento_nivel == $nivelAcademico[$i]->IdNivelEducativo ){ echo 'selected';}?>>
								 <?php echo $nivelAcademico[$i]->Nombre?>
								 </option>
								 <?php
							 }
						 }
						?>   
				   </select>
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_titulo_pos">Título de posgrado obtenido</label>
				   <input id="talento_titulo_pos" name="talento_titulo_pos" placeholder="Título de posgrado obtenido"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_titulo_pos;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

			 </div>

			 <div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_universidad_pos">Universidad que otorgó el título de posgrado</label>
				   <input id="talento_universidad_pos" name="talento_universidad_pos" placeholder="Universidad que otorgó el título de posgrado"  class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_universidad_pos;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_libro_pos">Libro del diploma de posgrado</label>
				   <input id="talento_libro_pos" name="talento_libro_pos" placeholder="Libro del diploma de posgrado"class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_libro_pos;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_registro_pos">Registro del diploma de posgrado</label>
				   <input id="talento_registro_pos" name="talento_registro_pos" placeholder="Registro del diploma de posgrado" class="form-control input-md validate[required, minSize[4], maxSize[15]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_registro_pos;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
				</div>
			 </div>

			 <div class="row">

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_fecha_diploma_pos">Fecha diploma de posgrado</label>
				   <input id="talento_fecha_diploma_pos" name="talento_fecha_diploma_pos" placeholder="Fecha diploma de posgrado" class="form-control  validate[required]" type="date" max="<?php echo date('Y-m-d')?>" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_fecha_diploma_pos;}?>" >
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_resolucion">Resolución convalidación título posgrado</label>
				   <input id="talento_resolucion_pos" name="talento_resolucion_pos" placeholder="Resolución convalidación título posgrado" class="form-control input-md validate[minSize[4], maxSize[15]]"  type="text" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_resolucion_pos;}?>" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="15" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 15 carácteres">
				</div>

				<div class="col-md-4">
				   <span class="text-orange">•</span><label for="talento_fecha_convalida_pos">Fecha convalidación título de posgrado</label>
				   <input id="talento_fecha_convalida_pos" name="talento_fecha_convalida_pos" placeholder="Fecha convalidación título de posgrado" class="form-control input-md" type="date" max="<?php echo date('Y-m-d')?>" value="<?php if(isset($rayosxTalento)){echo $rayosxTalento->talento_fecha_convalida_pos;}?>">
				</div>
			 </div>
			 <div id="btnGuardarDirector" class="col-md-12 pt-200">
				<p align="center">
				   <br/>
				   <button type="submit" class="btn yellow">
					  Guardar Talento Humano
				   </button>
				</p>
			  </div>

			</div>
		</div>	
        </div>          
	</form>
	<div id="div_objetos" style="<?php echo $visible_objetos?>">
	<form id="formSeccion4-2"  name="formSeccion4-2" action="<?php echo base_url('usuario/editarObjetosPrueba')?>" method="post" style="display:none">
		
	  <div id="paso4-2" class="row block w-100 newsletter ">

	  <div class="w-100">
			 <h4><b><span class="text-orange">•</span>Equipos u objetos de prueba</b></h4>		
	  <div id="seccion4-2">
		<div class="row">
		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_nombre">Nombre del Equipo:</label>
			  <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
			  <input id="obj_nombre" name="obj_nombre" placeholder="Nombre del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"   type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_marca">Marca del equipo</label>
			  <input id="obj_marca" name="obj_marca" placeholder="Marca del equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_modelo">Modelo del equipo</label>
			  <input id="obj_modelo" name="obj_modelo" placeholder="Modelo del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
		   </div>
		</div>

		<div class="row">

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_serie">Serie del equipo</label>
			  <input id="obj_serie" name="obj_serie" placeholder="Serie del Equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_calibracion">Calibración</label>
			  <input id="obj_calibracion" name="obj_calibracion" placeholder="Calibración" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_vigencia">Vigencia de calibración</label>
			  <select id="obj_vigencia" name="obj_vigencia" class="form-control validate[required]" >
				 <option value="">Seleccione...</option>
				 <option value="1">Un (1) A&ntilde;o</option>
				 <option value="2">Dos (2) A&ntilde;os</option>
				 <option value="3">Otra, definida por el fabricante</option>
			  </select>
		   </div>
		</div>

		<div class="row">

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_fecha">Fecha de calibración</label>
			  <input id="obj_fecha" name="obj_fecha" placeholder="Fecha de la calibracion" class="form-control validate[required] input-md" type="date"  max="<?php echo date('Y-m-d')?>">
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_manual">Manual Técnico y ficha Técnica</label>
			  <select id="obj_manual" name="obj_manual" class="form-control validate[required]" >
				 <option value="">Seleccione...</option>
				 <option value="1">Posee manual técnico</option>
				 <option value="2">Posee ficha técnica</option>
			  </select>
		   </div>

		   <div class="col-md-4">
			  <span class="text-orange">•</span><label for="obj_uso">Usos</label>
			  <textarea id="obj_uso" name="obj_uso" class="form-control validate[required] input-md" ></textarea>
		   </div>
		</div>

		<div id="btnGuardarEquipoPrueba" class="col-md-12 pt-200">
		   <p align="center">
			  <br/>
			  <!-- Primer Collapsible - Localizacion Entidad -->
			  <button type="submit" class="btn yellow">
				 Guardar Equipo/Objeto Prueba
			  </button>
		   </p>
		 </div>
	 </form>
	 <div id="respuesta_4_2" class="col-12 col-md-12 table-responsive">
		<table class="display nowrap table table-hover">
			   <thead>
				  <tr>
					  <th>ID</th>
					  <th>Nombre del equipo</th>
					  <th>Marca del equipo</th>
					  <th>Modelo del equipo</th>
					  <th>Ver Más</th>
					  <th>Eliminar</th>
				  </tr>
			   </thead>

			<tbody>
			<?php 
			if(isset($rayosxObjprueba)){
				for($i=0;$i<count($rayosxObjprueba);$i++){
					?>
					<tr>
						<td><?php echo $rayosxObjprueba[$i]->id_obj_rayosx;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_nombre;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_marca;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_modelo;?></td>
						<td>
							<a class="btn green" onClick="abrirModal('Equipo Objeto de prueba ID: <?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>','#modalObj<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>')">Ver más...</a>
						</td>
						<td>
							<a class="btn red" href="#" onClick="eliminarObj(<?php echo $rayosxObjprueba[$i]->id_tramite_rayosx?>,<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>)">Eliminar</a>
						</td>
					</tr>
					<div id="modalObj<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>" class="modal">
					  <p><b>Equipo Objeto de prueba ID:<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?></b></p>
						<ul>							
							<li><b>Nombre del Equipo: </b><?php echo $rayosxObjprueba[$i]->obj_nombre?></li>
							<li><b>Marca del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_marca?></li>
							<li><b>Modelo del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_modelo?></li>
							<li><b>Serie del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_serie?></li>
							<li><b>Calibración: </b><?php echo $rayosxObjprueba[$i]->obj_calibracion?></li>
							<li><b>Vigencia de calibración: </b><?php echo $rayosxObjprueba[$i]->obj_vigencia?></li>
							<li><b>Fecha de calibración: </b><?php echo $rayosxObjprueba[$i]->obj_fecha?></li>
							<li><b>Manual Técnico y ficha Técnica: </b><?php echo $rayosxObjprueba[$i]->obj_manual?></li>
							<li><b>Usos: </b><?php echo $rayosxObjprueba[$i]->obj_uso?></li>							
						</ul>					  
					</div>
					<?php	
				}	
			}else{
				?>
				<tr>
					<td colspan="6" scope="col">No Existen Objetos de prueba Registrados</td>
				</tr>	
				<?php
			}
			?>            
			</tbody>
			</table>
	 </div>
	 
	 </div>
</div>


         
              

</div>
</div>

<?php
if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 1))
{	
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_1 = "display:block";
	$display_form4_2 = "display:none";
}else if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 2)){
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_2 = "display:block";
	$display_form4_1 = "display:none";
}else{
	$categoria_form = 1;
	$display_form4_1 = "display:none";
	$display_form4_2 = "display:none";
}

if($this->session->userdata('tipo_identificacion') == 1){
	$display_cedula = "display:block";
	$display_rut = "display:none";
}else{
	$display_rut = "display:block";
	$display_cedula = "display:none";
}

if($tramite_info->visita_previa && $tramite_info->visita_previa == 2){
	$display_docTalento = "display:none";
}else{
	$display_docTalento = "display:block";
}
?>

<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-1" name="formSeccion5" action="<?php echo base_url('usuario/editarDocumentos1')?>" method="post" class="form-row" enctype="multipart/form-data" style="<?php echo $display_form4_1?>">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso5-1" class="row block w-100 newsletter ">

     <div class="w-100">
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría I</b></h4>
         <input id="categoria_docs" name="categoria_docs" type="hidden" value="<?php echo $categoria_form?>">
		<div class="col-12 col-md-12 table-responsive">
		   <table class="table table-hover">
			   <thead>
				  <tr>
					 <th>Descripción</th>
					 <th>Cargar Documento</th>
				  </tr>
			   </thead>
			   <tbody>
			   <?php
			   
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr>
						 <td>Fotocopia documento de identificación</td>
						 <td>
						   <input id="pn_doc" name="pn_doc" type="file" class="archivopdf validate[required]">                   
						   </td>
					   </tr>
					   <?php
				   }
				   
				   
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Fotocopia del Registro Unico Tributario - RUT</td>
						 <td>
						   <input id="pj_doc" name="pj_doc" type="file" class="archivopdf validate[required]"></td>
					   </tr>
					   <?php
				   }
				   
				   if($display_rut == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Registro Camara y comercio o certificado de representación legal</td>
						 <td>
						   <input id="pj_cyc" name="pj_cyc" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					   <?php
				   }
				   ?>
				  				  
				  <tr>
					 <td>Copia documento identificación del encargado de protección radiológica</td>
					 <td>
					   <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
					   <input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Copia del diploma del encargado de protección radiológica</td>
					 <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  
				  <tr>
					 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
					 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
					 <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf validate[required]"></td>
				  </tr>				  
				  <tr>
					 <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
					 <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Programa de capacitación en protección radiológica</td>
					 <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
					 <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf validate[required]"></td>
				  </tr>				  
				  <tr>
					 <td>Programa de Tecno vigilancia</td>
					 <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Programa de protección radiológica</td>
					 <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <?php
			   
				   if($display_docTalento == 'display:block'){
					   ?>
					   <tr class="docu_talento">
						 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo 1 del artículo 21</td>
						 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
						 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
						 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
						 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
						 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
						 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
						 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					   <?php
				   }
				   
				   ?>
				  
				  
			   </tbody>
			</table>
		</div>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
     </div>
  </div>
</form>

<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-2" name="formSeccion5" action="<?php echo base_url('usuario/editarDocumentos2')?>" method="post" class="form-row" enctype="multipart/form-data"  style="<?php echo $display_form4_2?>">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   <div id="paso5-2" class="row block w-100 newsletter ">

     <div class="w-100">
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>
        <input id="categoria_docs" name="categoria_docs" type="hidden" value="<?php echo $categoria_form?>">     
		<div class="col-12 col-md-12 table-responsive">		
			<table class="table table-hover">
			   <thead>
				  <tr>
					 <th>Descripción</th>
					 <th>Documento</th>
				  </tr>
			   </thead>
			   <tbody>
				 <?php
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr>
						 <td>Fotocopia documento de identificación</td>
						 <td>
						   <input id="pn_doc" name="pn_doc" type="file" class="archivopdf validate[required]">                   
						   </td>
					   </tr>
					   <?php
				   }
				   
				   
				   if($display_rut == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Fotocopia del Registro Unico Tributario - RUT</td>
						 <td>
						   <input id="pj_doc" name="pj_doc" type="file" class="archivopdf validate[required]"></td>
					   </tr>
					   <?php
				   }
				   
				   if($display_rut == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Registro Camara y comercio</td>
						 <td>
						   <input id="pj_cyc" name="pj_cyc" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					   <?php
				   }
				   ?>
				  <tr>
					 <td>Copia documento identificación del oficial de protección radiológica</td>
					 <td>
					   <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
					   <input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf validate[required]"></td>
				  </tr>
				  <tr>
					 <td>Copia del diploma del oficial de protección radiológica</td>
					 <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf validate[required]"></td>
				  </tr>				  
				  <tr>
					 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosímetro</td>
					 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
				  </tr>				  
				  <?php
				   if($display_docTalento == 'display:block'){
					   ?>
					   <tr class="docu_talento">
						 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo del articulo 23</td>
						 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]" ></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
						 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
						 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
						 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
						 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
						 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
						 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
					  </tr>
					   <?php
				   }
				   ?>
				  
			   </tbody>
			</table>
		</div>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
     </div>
  </div>
</form>

<?php
	
if($tramite_info->modulo1 == 1 && $tramite_info->modulo2 == 1 && $tramite_info->modulo3 == 1 && $tramite_info->modulo4 == 1 && $tramite_info->modulo5 == 1)
{
?>
<form class="form-inline" id="formfinal" name="formfinal" action="<?php echo base_url('usuario/completarTramiteRayosx')?>" method="post">

   <div class="line"></div>
   <?php
	if($tramite_info->estado == 13){
		$nombreBoton = "Subsanar Trámite";
	}else if($tramite_info->estado == 22){
		$nombreBoton = "Subsanar Trámite";
	}else{
		$nombreBoton = "Finalizar Trámite";
	} 
   
   ?>

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div id="paso6" class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>El trámite Licencia de practica medica categoría I y II está completo</b></h3>
         </div>

         <div class="form-group ">
            <label for="tipo_tramite">¡Apreciado Usuario! Una vez se presione el botón de <?php echo $nombreBoton?>, este no podrá ser editado y será gestionado por los Funcionarios de la Secretaría de Salud.</label>
			<input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>">
			<input type="hidden" value="2" name="estado_tramite">
         </div>
		 <br>
		 <p>
			Autoriza a la Secretaria Distrital de Salud de conformidad con lo establecido en la ley 1437 del 2011 a ser notificado y comunicado de cualquier decisión o requerimiento por medio de correo electrónico?			
		 </p>
		 <div class="form-group">
            <label for="notificacion">Notificación electrónica: </label>
			<select id="notificacion" name="notificacion" class="form-control validate[required]">
				<option value="">Seleccione...</option>
				<option value="1">Si</option>
				<option value="2">No</option>
			</select>
         </div>
		 <div class="form-group" id="div_correo_noti" style="display:none">
            <label for="correo_notificacion">Correo electrónico de notificación: </label>
			<input type="email" id="correo_notificacion" name="correo_notificacion" class="form-control validate[required, custom[email]]" placeholder="Correo electrónico de notificación">
         </div>

       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               <?php echo $nombreBoton?>
            </button>
         </p>
       </div>
    </div>
   </div>
</form>
<?php
	}
?>