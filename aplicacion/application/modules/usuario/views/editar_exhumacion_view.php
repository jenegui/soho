<style type="text/css">
    .btn-primary{width: 100%;padding-top: 6px;padding-bottom: 6px;}
    .btn-warning{width: 100%;padding-top: 6px;padding-bottom: 6px;}
    .main-section .row {
        border-top: 0px;
    }
</style>

<form method="post" action="<?php echo base_url('usuario/EditarTramiteExhumacion/'.$exhumacionfetch->idlicencia_exhumacion)?>" enctype="multipart/form-data">
<!--<input type="hidden" name="action" value="subir_doc">-->
<br>
<div class="col-12 col-md-12">
	<div class="alert alert-danger" role="alert">
	<b>Señor Ciudadano(a):</b><br>
	Si le solicitaron cambiar uno de los documentos, por favor adjuntar solo el documento que desea cambiar, los demás dejelos vacíos. Recuerde que solo se deben adjuntar documentos en pdf.
	</div>
</div>		
		
		<div class="row block right">
            <div class="col-12 col-md-12 pl-4 alert alert-info">
                <div class="subtitle">
                    <h2><b>Solicitud Licencia de Exhumación</b></h2>
					<h3>Documentos PDF de soporte para realizar el trámite.</h3>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">
					  <b>Señor Ciudadano(a):</b>
					  <br><br>
					  Con el fin de dar una pronta respuesta a su solicitud a continuación, se encuentra información importante sobre los documentos que puede adjuntar para acreditar parentesco.
                      <br><br>
					  <b>Padres:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido.
                      <br>
					  <b>Hijos:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Solicitante.
					  <br>
					  <b>Hermanos:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido y Solicitante.
					  <br>
					  <b>Abuelos:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Fallecido y Registro civil de Nacimiento del Padre o Madre del Fallecido con el que tiene línea directa.
					  <br>
					  <b>Nietos:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Solicitante.
					  <br>
					  <b>Suegros:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Matrimonio del Fallecido y Registro Civil de Nacimiento de Conyugue del Fallecido 
					  <br>
					  <b>Hijos del cónyuge:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Solicitante y Registro civil de Matrimonio del Fallecido con  Padre o Madre del Solicitante.
					  <br>
					  <b>Adoptante:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido en el cual conste la adopción.
					  <br>
					  <b>Adoptado:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido en el cual conste la adopción.
					  <br>
					  <b>Matrimonio:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro Civil de Matrimonio.
					  <br>
					  <b>Unión marital de hecho:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Escritura Pública de declaración de Unión marital de hecho ó extrajuicio.<
					  <br>
					  <b>Contrato Civil (mismo sexo):</b> Documento de identidad del solicitante, Documento Carta Cementerio y Contrato Solemne suscrito entre el solicitante y el fallecido.
                      <br><br>
                      <b>Nota Importante:</b> Si el fallecido tuvo intervención de medicina legal por favor anexar carta de autorización de exhumación y 
					  cremación de la Fiscalía General de la Nación. El registro civil de matrimonio ó de nacimiento debe no ser superior a 30 días en el momento de la solicitud.
					</p>                    
                </div>
                <div class="registro_triada">
                </div>
            </div>
        </div>
		

	<div class="row block right"> 
		<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Trámite Licencia de Exhumación de Cadaveres.</b></h2>
					<h3>Actualizaci&oacute;n de Informaci&oacute;n</h3>
                </div>
		</div>

		  <!---******************************************************************************
			1	parentezco
		***********************************************************************************-->
		<div class="col-lg-5 col-md-12">
			<label for="parentesco"><b>Seleccione Parentesco con el difunto</b> <font color="orange">(*)</font></label>			
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<select name="parentesco"  class="validate[required] form-control" id="parentesco" required>
				<option value="<?php print $exhumacionfetch->parentesco?>"><?php print $exhumacionfetch->parentesco ?></option>
				<option value="Conyugue">Conyugue/compañero(a) permanente</option>
				<option value="Padre/Madre">Padre/Madre</option>
				<option value="Hermano(a)">Hermano(a)</option>
				<option value="Hijo(a)">Hijo(a)</option>
				<option value="Abuelo(a)">Abuelo(a)</option>
				<option value="Nieto(a)">Nieto(a)</option>
				<option value="Otro">Otro</option>
			</select>
		</div>
		
            <!---******************************************************************************
                1	Fotocopia Cédula de Cudadania Solicitante
            ***********************************************************************************-->
		
		<div class="col-lg-5 col-md-12">
			<br>
			<label for="pdf_cedulasolicitante"><b>Fotocopia Cédula de Ciudadanía Solicitante</b> <font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12">
			<br>
			<input class="btn btn-primary" type="file"  title="Seleccione un PDF por favor!" name="pdf_cedulasolicitante" id="pdf_cedulasolicitante" accept=".pdf"/>
		</div>		

            <!---******************************************************************************
                1	Fotocopia Documento adicional en caso de otro parensco
            ***********************************************************************************-->
		<div class="col-lg-12 col-md-12" id="parentescootro1" style="display: none"> 
			<font style="font-size:10px;" color="grey">Por favor digite el tipo de vínculo existente con el fallecido y adjunté los documentos solicitados según el caso, si no cuenta con la documentación requerida, por favor adjunte la documentación disponible para efectos de analizar su solicitud. En caso de requerir información adicional, le sera comunicado de acuerdo a lo establecido en el artículo 17 de la Ley 1437 de 2011, sustituida por el artículo 1 de la Ley 1755 de 2015</font>
		</div>	
		
		<div class="col-lg-5 col-md-12" id="parentescootro" style="display: none"> 
			<br>
			<label for=""><b>Parentesco</b></label>
		</div>	

		<div class="col-lg-7 col-md-12" id="parentescootro2" style="display: none">
			<br>
			<input type="text" name="parentescoOTRO" class="form-control" id="parent" placeholder="Digite el parentesco con el difunto">
		</div>	

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_otro"><b>Fotocopia de los Documento que acredite parentesco</b> <font color="orange">(*)</font><br> <font style="font-size:10px;" color="grey">(Registros civiles de nacimiento, registro civil de matrimonio, escritura pública y/o contrato de unión según el caso que aplique. Consolidados en un único PDF)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input class="btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_otro" id="pdf_otro" accept=".pdf"/>  
		</div>

			<!---******************************************************************************
                Adicion campos Muerte Medicina Legal Mario Beltran 30-07-2019 Requerimiento Nuevo
            ***********************************************************************************-->

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_otro"><b>Favor Indicar si Medicina Legal interviene en la Licencia Exhumación</b> <font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<select id="intervienemedlegal" name="intervienemedlegal" class="validate[required] form-control" required>
			  <option value="" selected="true" disabled>Seleccione una opción</option>
			  <option value="0" <?php if($exhumacionfetch->interviene_medlegal==0) echo "selected"?>>No</option>
			  <option value="1" <?php if($exhumacionfetch->interviene_medlegal==1) echo "selected"?>>SI</option>
			</select>
		</div>
		
		<?php
			if($exhumacionfetch->interviene_medlegal == '1'){
			$display_medicina = '';
			}
			else{
			$display_medicina = 'style="display:none"';
			}
		?>		
		
		<div class="col-lg-5 col-md-12" id="docmedicinalegal" <?php echo $display_medicina?>> 
			<br>
			<label for=""><b>Fotocopia Documento emitido por Medicina Legal</b> <font color="orange">(*)</font></label>
		</div>	

		<div class="col-lg-7 col-md-12" id="docmedicinalegal2" <?php echo $display_medicina?>>
			<br>
			<input class="btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_autorizacionfiscal" id="pdf_autorizacionfiscal" accept=".pdf">  
		</div>	



            <!---******************************************************************************
                2	Fotocopia Registro Defunción o Licencia de Inhumacion
            ***********************************************************************************-->
				
		<!--
		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="numero_regdefuncion"><b>No. Registro de Defunción</b></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<?php 
				//print' <input name="numero_regdefuncion" class="form-control input-md validate[maxSize[11], custom[number]]" id="numero_regdefuncion" value="'.$exhumacionfetch->numero_regdefuncion.'" >';
			?>
		</div>		
		-->
            <!---******************************************************************************
                2	No Licencia Inhumcion
            ***********************************************************************************-->
				

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="numero_licencia"><b>No. Licencia Inhumación</b></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input name="numero_licencia" class="form-control input-md validate[required, maxSize[11], custom[number]]" id="numero_licencia"  readonly="true" value="<?php print $exhumacionfetch->numero_licencia ?>">
		</div>	

            <!---******************************************************************************
                2	fecha Licencia Inhumcion
            ***********************************************************************************-->
				

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="fecha"><b>Fecha Inhumación</b> <font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input type="text" name="fechaInh" class="validate[required] form-control" id="fechaIn" readonly="true" value="<?php print $exhumacionfetch->fecha_inhumacion?>">
		</div>	

            <!---******************************************************************************
                3	No identificacion Fallecido
            ***********************************************************************************-->
				
		<!--
		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="numero_docfallecido"><b>No. Identificación del fallecido</b></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<?php 
			//	print' <input type="number" name="numero_docfallecido" class="form-control" id="numero_docfallecido" value="'. $exhumacionfetch->numero_docfallecido .'">';
			?>
		</div>	
		-->
            <!---******************************************************************************
                3	Certificado de Cementerio, donde se encuentra sepultado el cadáver
            ***********************************************************************************-->

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_certificadocementerio"><b>Certificado del Cementerio en donde se encuentra sepultado el cadáver</b> <font color="orange">(*)</font><br> <font style="font-size:10px;" color="grey">(Carta para exhumación del cementerio donde se encuentra sepultado el cadáver.)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input class="btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_certificadocementerio" id="pdf_certificadocementerio" accept=".pdf"/>  
		</div>	

	

		<div class="col-12 col-md-12 pl-4" align="center">
			<div class="paragraph">
				<br><br>
				<button class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button">Actualizar y Terminar</button>
				<br><br>
				<a class="btn red w-100 py-2" href="<?php echo base_url()?>" class="btn btn-primary" role="button">Regresar</a>
			</div>
		</div>
		
	</div>
    </form>			

    <div class="col-xl-2"></div>
    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #009CDF;">
                    <h5 class="modal-title" style=" color: #ffffff">Atención!!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert">
                        <p><ion-icon name="add-circle"></ion-icon><b><strong>Información importante</strong></b> 
                        <div id="respuestaMensaje"></div>
                        </p>
                    </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #003e65; color: #ffffff;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="resultadoAjax"></div>
</div>


            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>
	
	
<script type="text/javascript">
    $("#mayor").click(function () {
        $("#mayor7").show();
        $("#menor7").hide();
    })
    $("#menor").click(function () {
        $("#menor7").show();
        $("#mayor7").hide();
    })

    $("#parentesco").change(function () {
        if ($("#parentesco").val() === "Otro"){
            $("#parentescootro").show();
			$("#parentescootro1").show();
			$("#parentescootro2").show();
            $("#parentescoPDF").show();
        }
        else if ($("#parentesco").val() === "Conyugue"){
            $("#parentescoPDF").show();
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else if ($("#parentesco").val() === "Abuelo(a)"){
            $("#parentescoPDF").show();
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else if ($("#parentesco").val() === "Nieto(a)"){
            $("#parentescoPDF").show();        
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else{
            $("#parentescootro").hide();
            $("#parentescoPDF").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
    })
	
	$("#intervienemedlegal").change(function () {
        if ($("#intervienemedlegal").val() === "1"){
            $("#docmedicinalegal").show();
			$("#docmedicinalegal2").show();
			$("#pdf_autorizacionfiscal").attr('required',true);
        }
        else {
            $("#docmedicinalegal").hide();
			$("#docmedicinalegal2").hide();
			$("#pdf_autorizacionfiscal").attr('required',false);
        }
    })
	
</script>