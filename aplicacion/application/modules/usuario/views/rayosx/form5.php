
<?php
if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 1))
{	
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_1 = "display:block";
	$display_form4_2 = "display:none";
}else if((isset($rayosxCategoria->categoria) && $rayosxCategoria->categoria == 2)){
    $categoria_form = $rayosxCategoria->categoria;
	$display_form4_2 = "display:block";
	$display_form4_1 = "display:none";
}else{
	$categoria_form = 1;
	$display_form4_1 = "display:none";
	$display_form4_2 = "display:none";
}

if($this->session->userdata('tipo_identificacion') == 1){
	$display_cedula = "display:block";
	$display_rut = "display:none";
}else{
	$display_rut = "display:block";
	$display_cedula = "display:none";
}

if($tramite_info->visita_previa && $tramite_info->visita_previa == 2){
	$display_docTalento = "display:none";
}else{
	$display_docTalento = "display:block";
}

?>
<div id="paso5" class="row-center shadow-lg p-3 mb-5 bg-white rounded ">
<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-1" name="formSeccion5" action="<?php echo base_url('usuario/editarDocumentos1')?>" method="post" class="form-row" enctype="multipart/form-data" style="<?php echo $display_form4_1?>">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->

       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría I</b></h4>
         <input id="categoria_docs" name="categoria_docs" type="hidden" value="<?php echo $categoria_form?>">
		<div class="col-12 col-md-12 table-responsive">
		   <table class="table table-hover">
			   <thead>
				  <tr>
					 <th>Descripción</th>
					 <th>Cargar Documento</th>
					 <th>Documento Cargado</th>
				  </tr>
			   </thead>
			   <tbody>
			   <?php
			   
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr>
						 <td>Fotocopia documento de identificación</td>
						 <td>
						   <input id="pn_doc" name="pn_doc" type="file" class="archivopdf ">                   
						   </td>
						   <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pn_doc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					   </tr>
					   <?php
				   }
				   
				   
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Fotocopia del Registro Unico Tributario - RUT</td>
						 <td>
						   <input id="pj_doc" name="pj_doc" type="file" class="archivopdf "></td>
						<td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pj_doc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>   
					   </tr>
					   <?php
				   }
				   
				   if($display_rut == 'display:block'){
					   ?>
					   <tr style="<?php echo $display_rut?>">
						 <td>Registro Camara y comercio o certificado de representación legal</td>
						 <td>
						   <input id="pj_cyc" name="pj_cyc" type="file" class="archivopdf "></td>
						<td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pj_cyc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>   
					  </tr>
					   <?php
				   }
				   ?>
				  				  
				  <tr>
					 <td>Copia documento identificación del encargado de protección radiológica</td>
					 <td>
					   <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
					   <input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf "></td>
					   <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_doc_encargado");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  <tr>
					 <td>Copia del diploma del encargado de protección radiológica</td>
					 <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_encargado");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  
				  <tr>
					 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos</td>
					 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_registro_dosimetrico");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  <tr>
					 <td>Registro del cumplimiento de los niveles de referencia para diagnóstico</td>
					 <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_registro_niveles");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>				  
				  <tr>
					 <td>Certificado de la capacitación en protección radiológica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
					 <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_certificado_capacitacion");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  <tr>
					 <td>Programa de capacitación en protección radiológica</td>
					 <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_programa_capacitacion");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  <tr>
					 <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
					 <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_procedimiento_mantenimiento");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>				  
				  <tr>
					 <td>Programa de Tecno vigilancia</td>
					 <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf "></td>
					 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_programa_tecno");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
				  </tr>
				  <tr>
					 <td>Programa de protección radiológica</td>
					 <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf "></td>
					 <td>
						<?php
							$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_programa_proteccion");
							
							if(count($resultado_archivo)>0){
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<?php
							}
						?>
					</td>
				  </tr>
				  <?php
			   
				   if($display_docTalento == 'display:block'){
					   ?>
					   <tr class="docu_talento">
						 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo 1 del artículo 21</td>
						 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_soporte_talento");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
						 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_director");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
						 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_res_convalida_director");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
						 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_pos_profe");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
						 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_res_convalida_profe");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
						 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_cert_calibracion");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
						 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_declaraciones");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					   <?php
				   }
				   
				   ?>
				  
				  
			   </tbody>
			</table>
		</div>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
</form>

<!-- Equipos generadores de radiación ionizante -->
<form id="formSeccion5-2" name="formSeccion5" action="<?php echo base_url('usuario/editarDocumentos2')?>" method="post" class="form-row" enctype="multipart/form-data"  style="<?php echo $display_form4_2?>">
   <!-- PRIMERA PARTE DEL REGISTRO DE LA SOLICITUD - LOCALIZACION tipoTramite  -->
   
       <div class="subtitle">
           <h3><b>Documentos Adjuntos:</b></h3>
       </div>
       <h4><b><span class="text-orange">•</span>Documentos Categoría II</b></h4>
        <input id="categoria_docs" name="categoria_docs" type="hidden" value="<?php echo $categoria_form?>">     
		<div class="col-12 col-md-12 table-responsive">		
			<table class="table table-hover">
			   <thead>
				  <tr>
					 <th width="50%">Descripción</th>
					 <th width="30%">Documento</th>
					 <th width="20%">Documento Cargado</th>
				  </tr>
			   </thead>
			   <tbody>
				 <?php
				   if($display_cedula == 'display:block'){
					   ?>
					   <tr>
						 <td>Fotocopia documento de identificación</td>
						 <td>
						   <input id="pn_doc" name="pn_doc" type="file" class="archivopdf ">                   
						   </td>
						   <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pn_doc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
									<?php
								}
							?>
						   </td>
					   </tr>
					   <?php
				   }
				   
				   
				   if($display_rut == 'display:block'){
					   ?>
						<tr>
							<td>Fotocopia del Registro Unico Tributario - RUT</td>
							<td>
							   <input id="pj_doc" name="pj_doc" type="file" class="archivopdf ">
							</td>
							<td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pj_doc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
									<?php
								}
							?>
						   </td>
						</tr>
					   <?php
				   }
				   
				   if($display_rut == 'display:block'){
					   ?>
					   <tr>
						 <td>Registro Camara y comercio</td>
						 <td>
						   <input id="pj_cyc" name="pj_cyc" type="file" class="archivopdf ">
							</td>
							<td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "pj_cyc");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
                                        <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                    </a>
									<?php
								}
							?>
						   </td>
					  </tr>
					   <?php
				   }
				   ?>
				  <tr>
					 <td>Copia documento identificación del oficial de protección radiológica</td>
					 <td>
					   <input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php if(isset($tramite_info)){echo $tramite_info->id;}?>">
					   <input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf "></td>
					   <td>
						<?php
							$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_doc_oficial");
							
							if(count($resultado_archivo)>0){
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<?php
							}
						?>
					   </td>
				  </tr>
				  <tr>
					 <td>Copia del diploma del oficial de protección radiológica</td>
					 <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf "></td>
					 <td>
						<?php
							$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_oficial");
							
							if(count($resultado_archivo)>0){
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<?php
							}
						?>
					   </td>
				  </tr>				  
				  <tr>
					 <td>Registros dosimétricos del último periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosímetro</td>
					 <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf "></td>
					 <td>
						<?php
							$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_registro_dosimetrico");
							
							if(count($resultado_archivo)>0){
								?>
								<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
									<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
								</a>
								<?php
							}
						?>
					   </td>
				  </tr>				  
				  <?php
				   if($display_docTalento == 'display:block'){
					   ?>
					   <tr class="docu_talento">
						 <td>Documentación de soporte de talento humano e infraestructura técnica. En el evento contemplado en el parágrafo del articulo 23</td>
						 <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf " ></td>
						<td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_soporte_talento");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de Posgrado del Director Técnico</td>
						 <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_director");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del Director Técnico  </td>
						 <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_res_convalida_director");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
						 <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_diploma_pos_profe");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Fotocopia de la Resolución de convalidación del t&iacute;tulo ante el Ministerio de Educación Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
						 <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_res_convalida_profe");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Certificados de calibración con una vigencia superior a seis (6) meses por cada equipo reportado</td>
						 <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_cert_calibracion");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					  <tr class="docu_talento">
						 <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
						 <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf "></td>
						 <td>
							<?php
								$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "fi_declaraciones");
								
								if(count($resultado_archivo)>0){
									?>
									<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
										<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
									</a>
									<?php
								}
							?>
						</td>
					  </tr>
					   <?php
				   }
				   
				   if($tramite_info->estado == 22){
					   ?>
					   
						<tr class="text-center">
							<td colspan="2"><b>Documentos Subsanación segunda instancia</b></td>
						</tr>
						
						<tr>
							 <td>Certificado expedido por una institución de educación superior o por una institución de Educación para el Trabajo y el Desarrollo Humano, en el que se acredite la capacitación en materira de protección radiológica de los trabajadores ocupacionalmente expuestos</td>
							 <td><input id="visita_certificado" name="visita_certificado" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_certificado");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Programa de capacitación en protección radiológica</td>
							 <td><input id="visita_prote_radio" name="visita_prote_radio" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_prote_radio");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Registro de los niveles de referencia para diagnóstico, respecto de los procedimientos más comunes</td>
							 <td><input id="visita_niveles" name="visita_niveles" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_niveles");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
					    <tr>
							 <td>Descripción de los elementos, sistemas y componenetes necesarios en la práctica médica categoria II que se realice</td>
							 <td><input id="visita_elementos" name="visita_elementos" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_elementos");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Procedimientos de mantenimiento de los equipos generadores de radicación ionizante, de conformidad con lo establecido por el fabricante</td>
							 <td><input id="visita_procedimientos" name="visita_procedimientos" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_procedimientos");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Documentos suministrado por el instalador del equipo o equipos, que contenga los resultados de las pruebas iniciales de caracterización y puesta en marcha de dicho equipo o equipos</td>
							 <td><input id="visita_resultados" name="visita_resultados" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_resultados");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Documento que contenga el programa de vigilancia radiológica</td>
							 <td><input id="visita_vigilancia_radio" name="visita_vigilancia_radio" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_vigilancia_radio");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Documento que contenga el programa institucional de tecnovigilancia</td>
							 <td><input id="visita_tecnovigilancia" name="visita_tecnovigilancia" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_tecnovigilancia");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
						<tr>
							 <td>Documento que contenga un programa de protección radiológica</td>
							 <td><input id="visita_docu_prote" name="visita_docu_prote" type="file" class="archivopdf "></td>
							 <td>
								<?php
									$resultado_archivo = $this->rx_model->consultar_archivo($tramite_info->id, "visita_docu_prote");
									
									if(count($resultado_archivo)>0){
										?>
										<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
											<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
										</a>
										<?php
									}
								?>
							</td>
						</tr>
					   
					   <?php
					   
					   
					   
				   }
				   
				   
				   ?>					
			   </tbody>
			</table>
		</div>
        <div id="btnGuardarDocumentos" class="col-md-12 pt-200">
           <p align="center">
              <br/>
              <!-- Primer Collapsible - Localizacion Entidad -->
              <button type="submit" class="btn yellow">
                 Guardar Documento
              </button>
           </p>
         </div>
    
</form>
</div>