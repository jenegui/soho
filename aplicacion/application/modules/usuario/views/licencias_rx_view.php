<?php

    $retornoError = $this->session->flashdata('error');
    if ($retornoError) {
        ?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
    }

    $retornoExito = $this->session->flashdata('exito');
    if ($retornoExito) {
        ?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
    }

?>
            <style>
                select {
                    width: 100%;
                }

            </style>
            <form class="form-inline" id="form_tramite" name="form_tramite" action="<?php echo base_url('usuario/guardarTramiteRayosX')?>" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Registro y autorizaci&oacute;n de licencias de Rayos X</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Registro de Informaci&oacute;n</legend>
                                <!-- Select Basic -->
                                <div class="form-group  col-lg-12">
                                    <label for="tipo_titulo">Tipo de tr&aacute;mite</label>
                                    <div>
                                        <select id="tipo_tramite" name="tipo_tramite" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Nuevo</option>
                                            <option value="2">Renovaci&oacute;n</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row" id="div_nuevo" style="display:none">
                        <div class="row-section">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#direccion" aria-controls="direccion" role="tab" data-toggle="tab">Direcci&oacute;n de la entidad</a></li>
                                <li role="presentation"><a href="#equipos" aria-controls="equipos" role="tab" data-toggle="tab">Equipos generadores de radiaci&oacute;n ionizante</a></li>
                                <li role="presentation"><a href="#toe" aria-controls="toe" role="tab" data-toggle="tab">Trabajadores ocupacionalmente expuestos - TOE</a></li>
                                <li role="presentation"><a href="#talento" aria-controls="talento" role="tab" data-toggle="tab">Talento Humano</a></li>
                                <li role="presentation"><a href="#documentos" aria-controls="documentos" role="tab" data-toggle="tab">Documentos Adjuntos</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane active" id="direccion">
                                    <div class="row-section">
                                        <div class="form-group  col-lg-6">
                                            <label for="dire_entidad">Direcci&oacute;n de la entidad</label>
                                            <div>
                                                <input id="dire_entidad" name="dire_entidad" placeholder="Direcci&oacute;n de la entidad" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-6">
                                            <label for="dire_entidad">Sede</label>
                                            <div>
                                                <input id="sede_entidad" name="sede_entidad" placeholder="Sede de la entidad" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-section">
                                        <div class="form-group  col-lg-3">
                                            <label for="email_entidad">Correo Electr&oacute;nico</label>
                                            <div>
                                                <input id="email_entidad" name="email_entidad" placeholder="Correo Electr&oacute;nico" class="form-control validate[required, custom[email]] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="depto_entidad">Departamento</label>
                                            <div>
                                                <select id="depto_entidad" name="depto_entidad" class="form-control validate[required]">
                                        <option value="">Seleccione...</option>
                                            <?php

                                            for($i=0;$i<count($departamento);$i++){
                                                echo "<option value='".$departamento[$i]->IdDepartamento."'>".$departamento[$i]->Descripcion."</option>";
                                            }

                                            ?>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="mpio_entidad">Municipio</label>
                                            <div>
                                                <select id="mpio_entidad" name="mpio_entidad" class="form-control validate[required]">
                                        <option value="">Seleccione Departamento...</option>

                                        </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-section">
                                        <div class="form-group  col-lg-3">
                                            <label for="celular_entidad">N&uacute;mero celular</label>
                                            <div>
                                                <input id="celular_entidad" name="celular_entidad" placeholder="N&uacute;mero celular" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="indicativo_entidad">Indicativo</label>
                                            <div>
                                                <input id="indicativo_entidad" name="indicativo_entidad" placeholder="Indicativo" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="telefono_entidad">N&uacute;mero telef&oacute;nico fijo</label>
                                            <div>
                                                <input id="telefono_entidad" name="telefono_entidad" placeholder="N&uacute;mero telef&oacute;nico fijo" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="extension_entidad">Extensi&oacute;n</label>
                                            <div>
                                                <input id="extension_entidad" name="extension_entidad" placeholder="Extensi&oacute;n" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <button id="guardar1">Guardar</button>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="equipos">
                                    <div class="col-md-12">
                                        <div class="form-group col-lg-4">
                                            <label for="categoria">Categoria</label>
                                            <div>
                                                <select id="categoria" name="categoria" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Categoria I</option>
                                            <option value="2">Categoria II</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-4" style="display:none" id="div_categoria1">
                                            <label for="categoria1">Equipos generadores de radicaci&oacute;n ionizante</label>
                                            <div>
                                                <select id="categoria1" name="categoria1" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Radiolog&iacute;a odontol&oacute;gica periapical</option>
                                            <option value="2">Equipo de RX</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-4" style="display:none" id="div_categoria2">
                                            <label for="categoria2">Equipos generadores de radicaci&oacute;n ionizante</label>
                                            <div>
                                                <select id="categoria2" name="categoria2" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Radioterapia</option>
                                            <option value="2">Radio diagn&oacute;stico de alta complejidad</option>
                                            <option value="3">Radio diagn&oacute;stico de media complejidad</option>
                                            <option value="4">Radio diagn&oacute;stico de baja complejidad</option>
                                            <option value="5">Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-4" style="display:none" id="div_categoria1-1">
                                            <label for="categoria1-1">Radiolog&iacute;a odontol&oacute;gica periapical</label>
                                            <div>
                                                <select id="categoria1_1" name="categoria1_1" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Equipo de RX odontol&oacute;gico periapical</option>
                                            <option value="2">Equipo de RX odontol&oacute;gico periapical portat&iacute;l</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-4" style="display:none" id="div_categoria1-2">
                                            <label for="categoria1-2">Equipo de RX</label>
                                            <div>
                                                <select id="categoria1_2" name="categoria1_2" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Densit&oacute;metro &oacute;seo</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Tipo de equipo generador de radiaci&oacute;n ionizante</label>
                                               <div>
                                                    <select id="equipo_generador" name="equipo_generador" class="form-control validate[required]">
                                                        <option value="">Seleccione...</option>
                                                        <?php

                                                        for($i=0;$i<count($equipos_radiacion);$i++){
                                                            echo "<option value='".$equipos_radiacion[$i]->id_equipo."'>".$equipos_radiacion[$i]->desc_equipo."</option>";
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Tipo de visualizaci&oacute;n de la imagen</label>
                                               <div>
                                                    <select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]">
                                                        <option value="">Seleccione...</option>
                                                        <?php

                                                        for($i=0;$i<count($tipo_visualizacion);$i++){
                                                            echo "<option value='".$tipo_visualizacion[$i]->id_tipo_visualizacion."'>".$tipo_visualizacion[$i]->desc_tipo_visualizacion."</option>";
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Marca equipo</label>
                                               <div>
                                                    <input id="marca_equipo" name="marca_equipo" placeholder="Marca equipo" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Modelo equipo</label>
                                               <div>
                                                    <input id="modelo_equipo" name="modelo_equipo" placeholder="Modelo equipo" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Serie equipo</label>
                                               <div>
                                                    <input id="serie_equipo" name="serie_equipo" placeholder="Serie equipo" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Marca tubo RX</label>
                                               <div>
                                                    <input id="marca_tubo_rx" name="marca_tubo_rx" placeholder="Marca tubo RX" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Modelo tubo RX</label>
                                               <div>
                                                    <input id="modelo_tubo_rx" name="modelo_tubo_rx" placeholder="Modelo tubo RX" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Serie tubo RX</label>
                                               <div>
                                                    <input id="serie_tubo_rx" name="serie_tubo_rx" placeholder="Serie tubo RX" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Tensi&oacute;n m&aacute;xima tubo RX [kV]</label>
                                               <div>
                                                    <input id="tension_tubo_rx" name="tension_tubo_rx" placeholder="Tensi&oacute;n m&aacute;xima tubo RX [kV]" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Contiene m&aacute;xima del tubo RX [mA]</label>
                                               <div>
                                                    <input id="contiene_tubo_rx" name="contiene_tubo_rx" placeholder="Contiene m&aacute;xima del tubo RX [mA]" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Energ&iacute;a de fotones [MeV]</label>
                                               <div>
                                                    <input id="energia_fotones" name="energia_fotones" placeholder="Energ&iacute;a de fotones [MeV]" class="form-control input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Energ&iacute;a de electrones [MeV]</label>
                                               <div>
                                                    <input id="energia_electrones" name="energia_electrones" placeholder="Energ&iacute;a de electrones [MeV]" class="form-control input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Carga de trabajo [mA.min/semana]</label>
                                               <div>
                                                    <input id="carga_trabajo" name="carga_trabajo" placeholder="Carga de trabajo [mA.min/semana]" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">Ubicaci&oacute;n del equipo de la instalaci&oacute;n</label>
                                               <div>
                                                    <input id="ubicacion_equipo" name="ubicacion_equipo" placeholder="Ubicaci&oacute;n del equipo de la instalaci&oacute;n" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">N&uacute;mero de permiso de comercializaci&oacute;n</label>
                                               <div>
                                                    <input id="numero_permiso" name="numero_permiso" placeholder="N&uacute;mero de permiso de comercializaci&oacute;n" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">A&ntilde;o de fabricaci&oacute;n del equipo</label>
                                               <div>
                                                    <input id="anio_fabricacion_equipo" name="anio_fabricacion" placeholder="A&ntilde;o de fabricaci&oacute;n del equipo" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4" >
                                               <label for="categoria1-2">A&ntilde;o de fabricaci&oacute;n del tubo</label>
                                               <div>
                                                    <input id="anio_fabricacion_tubo" name="anio_fabricacion_tubo" placeholder="A&ntilde;o de fabricaci&oacute;n del tubo" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div role="tabpanel" class="tab-pane" id="toe">
                                    <div class="col-md-12">
                                        <h3>Oficial de protecci&oacute;n radiol&oacute;gica/Encargado de protecci&oacute;n Radiol&oacute;gica</h3>
                                        <div class="form-group  col-lg-3">
                                            <label for="encargado_pnombre">Primer Nombre</label>
                                            <div>
                                                <input id="encargado_pnombre" name="encargado_pnombre" placeholder="Primer Nombre" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="encargado_snombre">Segundo Nombre</label>
                                            <div>
                                                <input id="encargado_snombre" name="encargado_snombre" placeholder="Segundo Nombre" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="encargado_papellido">Primer Apellido</label>
                                            <div>
                                                <input id="encargado_papellido" name="encargado_papellido" placeholder="Primer Apellido" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-3">
                                            <label for="encargado_sapellido">Segundo Apellido</label>
                                            <div>
                                                <input id="encargado_sapellido" name="encargado_sapellido" placeholder="Segundo Apellido" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_tdocumento">Tipo Documento</label>
                                            <div>
                                                <select id="encargado_tdocumento" name="encargado_tdocumento[]" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <?php

                                            for($i=0;$i<count($tipo_identificacion_natural);$i++){
                                                echo "<option value='".$tipo_identificacion_natural[$i]->IdTipoIdentificacion."'>".$tipo_identificacion_natural[$i]->Descripcion."</option>";
                                            }

                                            ?>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_ndocumento">N&uacute;mero Documento</label>
                                            <div>
                                                <input id="encargado_ndocumento" name="encargado_ndocumento" placeholder="N&uacute;mero Documento" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_lexpedicion">Lugar Expedici&oacute;n</label>
                                            <div>
                                                <input id="encargado_lexpedicion" name="encargado_lexpedicion" placeholder="Lugar Expedici&oacute;n" class="form-control validate[required] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_correo">Correo Electr&oacute;nico</label>
                                            <div>
                                                <input id="encargado_correo" name="encargado_correo" placeholder="Correo Electr&oacute;nico" class="form-control validate[required, custom[email]] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_profesion">Profesi&oacute;n</label>
                                            <div>
                                                <input id="encargado_profesion" name="encargado_profesion" placeholder="Profesi&oacute;n" class="form-control validate[required, custom[email]] input-md" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group  col-lg-4">
                                            <label for="encargado_nivel">Nivel Acad&eacute;mico</label>
                                            <div>
                                                <select id="encargado_nivel" name="encargado_nivel" class="form-control validate[required]">
                                            <option value="">Seleccione...</option>
                                            <?php

                                            for($i=0;$i<count($nivelAcademico);$i++){
                                                echo "<option value='".$nivelAcademico[$i]->IdNivelAcademico."'>".$nivelAcademico[$i]->Nombre."</option>";
                                            }

                                            ?>
                                        </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h3>TOE - Trabajadores Ocacionalmente Expuestos</h3>
                                        <div class="col-md-12 text-center">
                                            <button class="btn-sm btn-primary" id="clonar_form_toe">Agregar TOE</button>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="display nowrap" id="tabla_toe" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Primer Nombre</th>
                                                        <th>Segundo Nombre</th>
                                                        <th>Primer Apellido</th>
                                                        <th>Segundo Apellido</th>
                                                        <th>Tipo Documento</th>
                                                        <th>N&uacute;mero Documento</th>
                                                        <th>Lugar Expedici&oacute;n</th>
                                                        <th>Profesi&oacute;n</th>
                                                        <th>Nivel Acad&eacute;mico</th>
                                                        <th>Fecha del &uacute;ltimo entrenamiento en protecci&oacute;n radiol&oacute;gica </th>
                                                        <th>Fecha del pr&oacute;ximo entrenamiento en protecci&oacute;n radiol&oacute;gica</th>
                                                        <th>N&uacute;mero del registro profesional de salud</th>
                                                    </tr>
                                                </thead>

                                                <tr>
                                                    <td>
                                                        <input id="toe_pnombre" name="toe_pnombre[]" placeholder="Primer Nombre" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_snombre" name="toe_snombre[]" placeholder="Segundo Nombre" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_papellido" name="toe_papellido[]" placeholder="Primer Apellido" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_sapellido" name="toe_sapellido[]" placeholder="Segundo Apellido" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <select id="toe_tdocumento" name="toe_tdocumento[]" class="form-control validate[required]">
                                                            <option value="">Seleccione...</option>
                                                            <?php

                                                            for($i=0;$i<count($tipo_identificacion_natural);$i++){
                                                                echo "<option value='".$tipo_identificacion_natural[$i]->IdTipoIdentificacion."'>".$tipo_identificacion_natural[$i]->Descripcion."</option>";
                                                            }

                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input id="toe_ndocumento" name="toe_ndocumento[]" placeholder="N&uacute;mero Documento" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_lexpedicion" name="toe_lexpedicion[]" placeholder="Lugar Expedici&oacute;n" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_profesion" name="toe_profesion[]" placeholder="Profesi&oacute;n" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <select id="toe_nivel" name="toe_nivel[]" class="form-control validate[required]">
                                                            <option value="">Seleccione...</option>
                                                            <?php

                                                            for($i=0;$i<count($nivelAcademico);$i++){
                                                                echo "<option value='".$nivelAcademico[$i]->IdNivelAcademico."'>".$nivelAcademico[$i]->Nombre."</option>";
                                                            }

                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input id="toe_ult_entrenamiento" name="toe_ult_entrenamiento[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_pro_entrenamiento" name="toe_pro_entrenamiento[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                    <td>
                                                        <input id="toe_registro" name="toe_registro[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>

                                </div>

                                <div role="tabpanel" class="tab-pane" id="talento">
                                    <div class="form-group  col-lg-12">
                                        <label for="tipo_titulo">La IPS cuenta con el talento humano estipulado en el articulo 6 y 7, numeral 7.1?</label>
                                        <div>
                                            <select id="visita_previa" name="visita_previa" class="form-control validate[required]">
                                                <option value="">Seleccione...</option>
                                                <option value="1">SI</option>
                                                <option value="2">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="div_talento" style="display:none">

                                      <fieldset>
                                          <legend>Director T&eacute;cnico</legend>
                                          <div class="form-group  col-lg-3">
                                                <label for="talento_pnombre">Primer Nombre</label>
                                                <div>
                                                    <input id="talento_pnombre" name="talento_pnombre" placeholder="Primer Nombre" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_snombre">Segundo Nombre</label>
                                                <div>
                                                    <input id="talento_snombre" name="talento_snombre" placeholder="Segundo Nombre" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_papellido">Primer Apellido</label>
                                                <div>
                                                    <input id="talento_papellido" name="talento_papellido" placeholder="Primer Apellido" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_sapellido">Segundo Apellido</label>
                                                <div>
                                                    <input id="talento_sapellido" name="talento_sapellido" placeholder="Segundo Apellido" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_tdocumento">Tipo Documento</label>
                                                <div>
                                                    <select id="talento_tdocumento" name="talento_tdocumento[]" class="form-control validate[required]">
                                                <option value="">Seleccione...</option>
                                                <?php

                                                for($i=0;$i<count($tipo_identificacion_natural);$i++){
                                                    echo "<option value='".$tipo_identificacion_natural[$i]->IdTipoIdentificacion."'>".$tipo_identificacion_natural[$i]->Descripcion."</option>";
                                                }

                                                ?>
                                            </select>
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_ndocumento">N&uacute;mero Documento</label>
                                                <div>
                                                    <input id="talento_ndocumento" name="talento_ndocumento" placeholder="N&uacute;mero Documento" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_lexpedicion">Lugar Expedici&oacute;n</label>
                                                <div>
                                                    <input id="talento_lexpedicion" name="talento_lexpedicion" placeholder="Lugar Expedici&oacute;n" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_correo">Correo Electr&oacute;nico</label>
                                                <div>
                                                    <input id="talento_correo" name="talento_correo" placeholder="Correo Electr&oacute;nico" class="form-control validate[required, custom[email]] input-md" type="text">
                                                </div>
                                            </div>
                                      </fieldset>
                                      <fieldset>
                                          <legend>Idoneidad Profesional</legend>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_titulo">Titulo de pregrado obtenido</label>
                                                <div>
                                                    <input id="talento_titulo" name="talento_titulo" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_universidad">Universidad que otorg&oacute; el titulo de pregrado</label>
                                                <div>
                                                    <input id="talento_universidad" name="talento_universidad" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_libro">Libro del diploma de pregrado</label>
                                                <div>
                                                    <input id="talento_libro" name="talento_libro" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_registro">Registro del diploma de pregrado</label>
                                                <div>
                                                    <input id="talento_registro" name="talento_registro" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_fecha_diploma">Fecha diploma de pregrado</label>
                                                <div>
                                                    <input id="talento_fecha_diploma" name="talento_fecha_diploma" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_resolucion">Resoluci&oacute;n convalidaci&oacute;n titulo pregrado</label>
                                                <div>
                                                    <input id="talento_resolucion" name="talento_resolucion" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_fecha_convalida">Fecha convalidaci&oacute;n titulo de pregrado</label>
                                                <div>
                                                    <input id="talento_fecha_convalida" name="talento_fecha_convalida" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_nivel">Nivel Acad&eacute;mico &uacute;ltimo posgrado</label>
                                                <div>
                                                    <select id="talento_nivel" name="talento_nivel" class="form-control validate[required]">
                                                <option value="">Seleccione...</option>
                                                <?php

                                                for($i=0;$i<count($nivelAcademico);$i++){
                                                    echo "<option value='".$nivelAcademico[$i]->IdNivelAcademico."'>".$nivelAcademico[$i]->Nombre."</option>";
                                                }

                                                ?>
                                            </select>
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_titulo_pos">Titulo de posgrado obtenido</label>
                                                <div>
                                                    <input id="talento_titulo_pos" name="talento_titulo_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_universidad_pos">Universidad que otorg&oacute; el titulo de posgrado</label>
                                                <div>
                                                    <input id="talento_universidad_pos" name="talento_universidad_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_libro_pos">Libro del diploma de posgrado</label>
                                                <div>
                                                    <input id="talento_libro_pos" name="talento_libro_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_registro_pos">Registro del diploma de posgrado</label>
                                                <div>
                                                    <input id="talento_registro_pos" name="talento_registro_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_fecha_diploma_pos">Fecha diploma de posgrado</label>
                                                <div>
                                                    <input id="talento_fecha_diploma_pos" name="talento_fecha_diploma_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_resolucion">Resoluci&oacute;n convalidaci&oacute;n titulo posgrado</label>
                                                <div>
                                                    <input id="talento_resolucion_pos" name="talento_resolucion_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group  col-lg-3">
                                                <label for="talento_fecha_convalida_pos">Fecha convalidaci&oacute;n titulo de posgrado</label>
                                                <div>
                                                    <input id="talento_fecha_convalida_pos" name="talento_fecha_convalida_pos" placeholder="" class="form-control validate[required] input-md" type="text">
                                                </div>
                                            </div>
                                      </fieldset>
                                      <fieldset>
                                          <legend>Equipos u objetos de prueba</legend>

                                            <div class="col-md-12 text-center">
                                                <button class="btn-sm btn-primary" id="clonar_form_equiprueba">Agregar Equipo</button>
                                            </div>
                                            <div class="col-md-12">
                                                <table class="display nowrap" id="tabla_equiprueba" style="width:120%">
                                                    <thead>
                                                        <tr>
                                                            <th>Nombre del equipo</th>
                                                            <th>Marca del equipo</th>
                                                            <th>Modelo del equipo</th>
                                                            <th>Serie del equipo</th>
                                                            <th>Calibraci&oacute;n</th>
                                                            <th>Vigencia de calibraci&oacute;n</th>
                                                            <th>Fecha de calibraci&oacute;n</th>
                                                            <th>Manual T&eacute;cnico y ficha T&eacute;cnica</th>
                                                            <th>Usos</th>
                                                        </tr>
                                                    </thead>

                                                    <tr>
                                                        <td>
                                                            <input id="obj_nombre" name="obj_nombre[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <input id="obj_marca" name="obj_marca[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <input id="obj_modelo" name="obj_modelo[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <input id="obj_serie" name="obj_serie[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <input id="obj_calibracion" name="obj_calibracion[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <select id="obj_vigencia" name="obj_vigencia[]" class="form-control validate[required]">
                                                            <option value="">Seleccione...</option>
                                                            <option value="1">Un (1) A&ntilde;o</option>
                                                            <option value="2">Dos (2) A&ntilde;os</option>
                                                            <option value="3">Otra, definida por el fabricante</option>

                                                        </select>
                                                        </td>
                                                        <td>
                                                            <input id="obj_fecha" name="obj_fecha[]" placeholder="" class="form-control validate[required] input-md" type="text">
                                                        </td>
                                                        <td>
                                                            <select id="obj_manual" name="obj_manual[]" class="form-control validate[required]">
                                                                <option value="">Seleccione...</option>
                                                                <option value="1">Posee manual t&eacute;cnico</option>
                                                                <option value="2">Pose ficha t&eacute;cnica</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <textarea id="obj_uso" name="obj_uso[]" class="form-control validate[required] input-md"></textarea>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>
                                      </fieldset>
                                    </div>
                                </div>


                                <div role="tabpanel" class="tab-pane" id="documentos">
                                    <div class="col-md-12" id="div_doc_cat1" style="display:none">
                                        <fieldset>
                                            <legend>Documentos Categor&iacute;a I</legend>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Descripci&oacute;n</th>
                                                        <th>Documento</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Copia documento identificaci&oacute;n del encargado de protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_doc_encargado" name="fi_doc_encargado" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Copia del diploma del encargado de protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_diploma_encargado" name="fi_diploma_encargado" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Descripci&oacute;n de los blindajes estructurales o port&aacute;tiles y el c&aacute;lculo del blindaje</td>
                                                        <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Informe sobre los resultados del control de calidad</td>
                                                        <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Registros dosim&eacute;tricos del &uacute;ltimo periodo de los trabajadores ocupacionalmente expuestos</td>
                                                        <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Registro del cumplimiento de los niveles de referencia para diagn&oacute;stico</td>
                                                        <td><input id="fi_registro_niveles" name="fi_registro_niveles" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Plano general de las instalaciones</td>
                                                        <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Certificado de la capacitaci&oacute;n en protecci&oacute;n radiol&oacute;gica de cada trabajador ocupacionalmente expuesto reportado en el formulario</td>
                                                        <td><input id="fi_certificado_capacitacion" name="fi_certificado_capacitacion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Programa de capacitaci&oacute;n en protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_programa_capacitacion" name="fi_programa_capacitacion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Procedimientos de mantenimiento de conformidad a lo establecido por el fabricante</td>
                                                        <td><input id="fi_procedimiento_mantenimiento" name="fi_procedimiento_mantenimiento" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pruebas iniciales de caracterizaci&oacute;n de los equipos</td>
                                                        <td><input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Programa de Tecnovigilancia</td>
                                                        <td><input id="fi_programa_tecno" name="fi_programa_tecno" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Programa de protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_programa_proteccion" name="fi_programa_proteccion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Documentaci&oacute;n de soporte de talento humano e infraestructura t&eacute;cnica. En el evento contemplado en el paragrafo 1 del articulo 21</td>
                                                        <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de Diploma de Posgrado del Director T&eacute;cnico</td>
                                                        <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de la Resoluci&oacute;n de convalidaci&oacute;n del t&iacute;tulo ante el Ministerio de Educaci&oacute;n Nacional - MEN del Director T&eacute;cnico  </td>
                                                        <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                                                        <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de la Resoluci&oacute;n de convalidaci&oacute;n del t&iacute;tulo ante el Ministerio de Educaci&oacute;n Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                                                        <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Certificados de calibraci&oacute;n con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                                                        <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                                                        <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="col-md-12 text-center">
                                                <a href="<?php echo base_url()?>" class="btn-danger btn-lg">Regresar</a>
                                                <button id="Guardar" type="submit" class="btn-primary submit  btn-lg">Guardar</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12" id="div_doc_cat2" style="display:none">
                                        <fieldset>
                                            <legend>Documentos Categor&iacute;a II</legend>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Descripci&oacute;n</th>
                                                        <th>Documento</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Copia documento identificaci&oacute;n del oficial de protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_doc_oficial" name="fi_doc_oficial" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Copia del diploma del oficial de protecci&oacute;n radiol&oacute;gica</td>
                                                        <td><input id="fi_diploma_oficial" name="fi_diploma_oficial" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Descripci&oacute;n de los blindajes estructurales o port&aacute;tiles y el c&aacute;lculo del blindaje</td>
                                                        <td><input id="fi_blindajes" name="fi_blindajes" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Informe sobre los resultados del control de calidad</td>
                                                        <td><input id="fi_control_calidad" name="fi_control_calidad" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Registros dosim&eacute;tricos del &uacute;ltimo periodo de los trabajadores ocupacionalmente expuestos. Para alta complejidad, registros del segundo dosimetro</td>
                                                        <td><input id="fi_registro_dosimetrico" name="fi_registro_dosimetrico" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Plano general de las instalaciones</td>
                                                        <td><input id="fi_plano" name="fi_plano" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Documentaci&oacute;n de soporte de talento humano e infraestructura t&eacute;cnica. En el evento contemplado en el paragrafo del articulo 23</td>
                                                        <td><input id="fi_soporte_talento" name="fi_soporte_talento" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de Diploma de Posgrado del Director T&eacute;cnico</td>
                                                        <td><input id="fi_diploma_director" name="fi_diploma_director" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de la Resoluci&oacute;n de convalidaci&oacute;n del t&iacute;tulo ante el Ministerio de Educaci&oacute;n Nacional - MEN del Director T&eacute;cnico  </td>
                                                        <td><input id="fi_res_convalida_director" name="fi_res_convalida_director" type="file" class="archivopdf "></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de Diploma de posgrado del (los) profesional(es) que realiza(n) control de calidad</td>
                                                        <td><input id="fi_diploma_pos_profe" name="fi_diploma_pos_profe" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fotocopia de la Resoluci&oacute;n de convalidaci&oacute;n del t&iacute;tulo ante el Ministerio de Educaci&oacute;n Nacional - MEN del (los) profesional(es) que realiza(n) control de calidad  </td>
                                                        <td><input id="fi_res_convalida_profe" name="fi_res_convalida_profe" type="file" class="archivopdf "></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Certificados de calibraci&oacute;n con una vigencia superior a seis (6) meses por cada equipo reportado</td>
                                                        <td><input id="fi_cert_calibracion" name="fi_cert_calibracion" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Declaraciones de primera parte por cada objeto de prueba reportado</td>
                                                        <td><input id="fi_declaraciones" name="fi_declaraciones" type="file" class="archivopdf validate[required]"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="col-md-8 col-offset-2 text-center">
                                                <a href="<?php echo base_url()?>" class="btn-danger btn-lg">Regresar</a>
                                                <button id="Guardar" type="submit" class="btn-success submit  btn-lg">Guardar</button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </fieldset>
            </form>
