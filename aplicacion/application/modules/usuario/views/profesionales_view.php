<?php

    $retornoError = $this->session->flashdata('retorno_error');
    if ($retornoError) {
        ?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
    }

    $retornoExito = $this->session->flashdata('retorno_exito');
    if ($retornoExito) {
        ?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
    }

?>

						<!--Author: Mario Beltrán mebeltran@saludcapital.gov.co Since: 08072019
						Actualizacion Mensaje Incial Instruccioness-->

		<div class="row block right" style="background-color: #f8d7da;" id="2termino">
            <div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Registro y autorizaci&oacute;n de t&iacute;tulos en el &aacute;rea de la salud.</b></h2>
					<h3>Declaración Juramentada</h3>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">
					  <b>Señor Ciudadano(a):</b>
					  <br><br>
					  El siguiente Trámite se denomina "<b>Registro y Autorización de Títulos en el Área de la Salud</b>,
                      para las tecnologías, ocupaciones u oficios en el área salud y para las profesiones de Psicología y Gerontología".
                      <br><br>
                      Los documentos requeridos (Cédula, Diploma, Acta de Grado, Tarjeta Profesional (para Psicólogos), Resolución de Convalidación (cuando se requiere)),
                      deben ser cargados en el orden y espacio establecido para tal fin, y no deben ser cargados documentos diferentes. En caso de ser cargados en desorden
                      o que no cumplan con los requisitos exigidos, será devuelta la inscripción.
                      <br><br>
					  Los documentos adjuntados están sujetos a verificación antes las autoridades que los emitieron.
					  <br><br>
                      Si ya usted cuenta con esta autorización para el Ejercicio de la Profesión u Ocupación emitida anteriormente por alguna Secretaría de Salud o Colegio
                      para la misma Profesión, Ocupación u Oficio, no deberá realizarlo nuevamente.
                      <br><br>
                      De acuerdo a la normatividad vigente, usted no debe solicitar otra resolución para la misma profesión, si ya cuenta con ella.
                      <br><br>
                      Acorde con lo anterior, manifiesto expresamente y Bajo la Gravedad de Juramento, que no he sido autorizado ni he iniciado el trámite para ser autorizado
                      para ejercer mi profesión, ocupación u oficio en salud por ningún ente territorial en el país o colegio de profesionales, así como que los documentos
                      cargados en esta plataforma a título personal son auténticos.
					</p>                    
                </div>
                <div class="registro_triada">
                    <a class="btn green w-100 py-2" href="#" id="btn_acep2term" class="btn btn-primary" role="button">Aceptar</a>
					<a class="btn red w-100 py-2" href="#" id="btn_nacep2term" class="btn btn-primary" role="button">No Aceptar</a>

                </div>
            </div>
        </div>


<form class="form-inline" enctype="multipart/form-data" role="form" id="form_tramite" name="form_tramite" action="<?php echo base_url('usuario/guardarTramite')?>" method="post">
<div class="row block right" style="width:100%;display:none"  id="formreg">

                        <!-- Form Name -->

						<!--Author: Mario Beltrán mebeltran@saludcapital.gov.co Since: 12062019
						Validación Oracle lado Ciudadano existencia de Resoluciones-->
						<?php
						if(count($tramitesoracle)>0){
						?>
						<br>
                        <div class="alert alert-success" style="width:100%;">
                          <b>Apreciado Ciudadano!</b>
							<p>El sistema ha identificado su número de identificación ya cuenta con profesiones registradas en nuestras bases de datos existentes.<br>
							Agradecemos validar la siguiente información, si la profesión a realizar el trámite se encuentra a continuación. Favor abstenerse de generar el trámite.
							Por medio del siguiente correo <b>(contactenos@saludcapital.gov.co; archivo_sds@saludcapital.gov.co)</b> podra obtener más información de como solicitar una copia de la resolución relacionada.<br>
							</p>
							<br>
							<?php
									for($i=0;$i<1;$i++){
									  echo "<center><b>Número de Identificación:</b> ".$tramitesoracle[$i]->NROIDENT." - <b>Nombres y Apellidos:</b> ".$tramitesoracle[$i]->NOMBRES." ".$tramitesoracle[$i]->APELLIDOS."</center>";
									}
							?>
							<table style="width:100%;border: 1px solid green;" border="1">
							  <thead>
								<tr>
								  <th><b>Nombre Profesión</b></th>
								  <th><b>Nombre Institución</b></th>
								  <th><b>Fecha y No Resolución</b></th>
								</tr>
							  </thead>
							  <tbody>
								  <?php
									for($i=0;$i<count($tramitesoracle);$i++){
									  echo "<td>".$tramitesoracle[$i]->NOMBRE_PROFESION."</td>";
									  echo "<td>".$tramitesoracle[$i]->NOMBRE_INSTIT."</td>";
									  echo "<td>".$tramitesoracle[$i]->FECHA_RESOLUCION." - No Resolución: ".$tramitesoracle[$i]->NUMERO_RESOLUCION."</td></tr>";
									}
								  ?>
							  </tbody>
							</table>
                        </div>

						<?php
						}
						?>


			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Registro de Informaci&oacute;n.</b></h2>
                </div>
            </div>
			
            <div class="col-12 col-md-12 text-center ">
					<label for="tipo_titulo"><b>Tipo de título obtenido</b> <font color="orange">(*)</font></label>
                    <select id="tipo_titulo" name="tipo_titulo" class="form-control validate[required]" required style="width:100%;">
						<option value="">Seleccione...</option>
						<option value="1">Nacional</option>
						<option value="2">Extranjero</option>
					</select>
            </div>
		
	
            <div class="col-12 col-md-6 pl-4" id="div_nacional" style="display:none">
                <div class="paragraph">
					<br><br>
                    <label for="institucion_educativa"><b>Instituci&oacute;n educativa</b> <font color="orange">(*)</font></label>
                    <select id="institucion_educativa" name="institucion_educativa" class="form-control select2 validate[required]">
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($instituciones);$i++){
							echo "<option value='".$instituciones[$i]->id_institucion."'>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
						}
						?>
					</select>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional2" style="display:none">
                <div class="paragraph">
					<br><br>
                    <label for="profesion"><b>Profesión</b> <font color="orange">(*)</font></label>
                    <select id="profesion" name="profesion" class="form-control select2 validate[required]">
						<option value="">Seleccione...</option>
					</select>                    
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional3" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="diploma"><b>Diploma No.</b></label>
                    <input id="diploma" name="diploma" placeholder="Diploma No." class="form-control input-md" type="text" onkeypress="if(this.value.length==20) return false;" autocomplete="off" style="width:100%;">                 
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional4" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="acta"><b>Acta de grado</b></label>
                    <input id="acta" name="acta" placeholder="Acta de grado" class="form-control input-md validate[custom[number],minSize[1], maxSize[20]]" onkeypress="if(this.value.length==20) return false;" autocomplete="off" style="width:100%;">               
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional5" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="acta"><b>Fecha terminación</b> <font color="orange">(*)</font></label>
                    <input id="fecha_term" name="fecha_term" placeholder="Fecha terminación" class="form-control input-md validate[required]" autocomplete="off" readonly="readonly" style="width:100%;"> 
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional6" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="libro"><b>Libro</b></label>
					<input id="libro" name="libro" placeholder="Libro" class="form-control input-md validate[custom[number],minSize[1], maxSize[6]]" autocomplete="off" onkeypress="if(this.value.length==6) return false;" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional7" style="display:none">
                <div class="paragraph">
					<br><br>
                    <label for="folio"><b>Folio</b></label>
                    <input id="folio" name="folio" placeholder="Folio" class="form-control input-md validate[custom[number], minSize[1], maxSize[6]]" autocomplete="off" onkeypress="if(this.value.length==6) return false;" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_nacional8" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="anio"><b>A&ntilde;o Título</b> <font color="orange">(*)</font></label>
					<input id="anio" name="anio" placeholder="A&ntilde;o" class="form-control input-md validate[custom[number], required, minSize[4], maxSize[4]]" autocomplete="off" onkeypress="if(this.value.length==4) return false;" style="width:100%;">
				</div>
            </div>
			
			<div class="col-12 col-md-6 pl-4" id="div_extranjero" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="cod_universidad"><b>Nombre Universidad internacional</b> <font color="orange">(*)</font></label>
                    <input id="cod_universidad" name="cod_universidad" placeholder="Nombre Universidad internacional" class="form-control input-md validate[required]" autocomplete="off" type="text" onkeyup="javascript:this.value.toUpperCase();" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero2" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="titulo_equivalente"><b>Título equivalente</b> <font color="orange">(*)</font></label>
                    <select id="titulo_equivalente" name="titulo_equivalente" class="form-control validate[required]" style="width:100%;">
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($profesionesequi);$i++){
							echo "<option value='".$profesionesequi[$i]->id_programaequi."'>".$profesionesequi[$i]->nombre_programa." - ".$profesionesequi[$i]->tipo_prog."</option>";
						}
		                ?>
					</select>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero3" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="titulo_equivalente"><b>País Origen T&iacute;tulo</b> <font color="orange">(*)</font></label>
					<select id="pais_tituloequi" name="pais_tituloequi" class="form-control validate[required]" style="width:100%;">
						<option value="">Seleccione...</option>
						<?php
						for($i=0;$i<count($paises);$i++){
							echo "<option value='".$paises[$i]->IdPais."'>".$paises[$i]->Nombre."</option>";
						}
						?>
					</select>
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero4" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="resolucion"><b>N&uacute;mero de resoluci&oacute;n de convalidaci&oacute;n</b> <font color="orange">(*)</font></label>
                    <input id="resolucion" name="resolucion" placeholder="Numero de resoluci&oacute;n de convalidaci&oacute;n" class="form-control input-md validate[custom[number],required]" autocomplete="off" onkeypress="if(this.value.length==15) return false;" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero5" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="fecha_resolucion"><b>Fecha de resoluci&oacute;n</b> <font color="orange">(*)</font></label>
                    <input id="fecha_resolucion" name="fecha_resolucion" placeholder="Fecha de resoluci&oacute;n" class="form-control input-md validate[required]" type="text" autocomplete="off" readonly="readonly" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero6" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="entidad"><b>Entidad</b> <font color="orange">(*)</font></label>
                    <select id="entidad" name="entidad" class="form-control validate[required]" style="width:100%;">
						<option value="">Seleccione...</option>
                        <option value="1">Ministerio de Educaci&oacute;n</option>
                        <option value="2">ICFES</option>
                    </select>
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero7" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="acta"><b>Fecha terminación</b> <font color="orange">(*)</font></label>
                    <input id="fecha_term_ext" name="fecha_term_ext" placeholder="Fecha terminación" class="form-control input-md validate[required]" type="text" autocomplete="off" readonly="readonly" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_extranjero8" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="anio"><b>A&ntilde;o Título</b> <font color="orange">(*)</font></label>
					<input id="anio2" name="anio2" placeholder="A&ntilde;o" class="form-control input-md validate[custom[number],required, minSize[4], maxSize[4]]" autocomplete="off" onkeypress="if(this.value.length==4) return false;" style="width:100%;">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_tarjeta" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="tarjeta"><b>Tarjeta Profesional</b> <font color="orange">(*)</font></label>
					<input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md validate[custom[number],minSize[1], maxSize[15]]" autocomplete="off" onkeypress="if(this.value.length==15) return false;" style="width:100%;">
				</div>
            </div>			
			

	
</div>
<div class="row block right" style="width:100%;display:none" id="formreg2">
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Documentos Adjuntos.</b></h2>
                </div>
            </div>
			
            <div class="col-12 col-md-12 text-center ">
				<div class="alert alert-warning" role="alert">Los documentos adjuntos deben ser en formato PDF y su tama&ntilde;o inferior a 3Mb. Para los documentos (documento de identificación y tarjeta profesional), debe estar escaneados por las dos caras los documentos.</div>
            </div>
		
            <div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
                    <label for="pdf_documento" style="justify-content:left;"><b>Documento de identificaci&oacute;n</b></label>
					<input type="file" name="pdf_documento" id="pdf_documento" class="validate[required]" required>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
                    <label for="pdf_titulo" style="justify-content:left;"><b>Título (Diploma de grado)</b></label>
					<input type="file" name="pdf_titulo" id="pdf_titulo" class="validate[required]" required>                    
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
					<label for="pdf_acta" style="justify-content:left;"><b>Acta de grado</b></label>
					<input type="file" name="pdf_acta" id="pdf_acta" class="validate[required]" required>
                </div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_doctarjeta" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="pdf_tarjeta" style="justify-content:left;"><b>Tarjeta Profesional</b></label>
					<input type="file" name="pdf_tarjeta" id="pdf_tarjeta" class="validate[required]">
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4" id="div_archivos_extranjero" style="display:none">
                <div class="paragraph">
					<br><br>
					<label for="pdf_resolucion" style="justify-content:left;"><b>Resoluci&oacute;n de convalidaci&oacute;n</b></label>
					<input type="file" name="pdf_resolucion" id="pdf_resolucion" class="validate[required]">
				</div>
            </div>
			<div class="col-12 col-md-12 pl-4" align="center">
                <div class="paragraph">
					<br><br>
					<button class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button">Guardar y Terminar</button>
					<br><br>
					<a class="btn red w-100 py-2" href="<?php echo base_url()?>" class="btn btn-primary" role="button">Regresar</a>

				</div>
            </div>
</div>		
</form>			



<!--Author: Mario E Beltran mebeltran@saludcapital.gov.co Since: 30052019
//Modal Infografia y Listado PDF-->
	  <!-- Modal -->
	  <div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Información Importante</h4>
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  
			</div>
			<div class="modal-body">
			<center>
			 <img style="max-width: 100%;height: auto" src="<?php echo base_url('assets/imgs/infografia.jpg')?>">
			</center>
			</div>
			<div class="modal-footer">
			<center>
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <a href="<?php echo base_url('assets/docs/listado.pdf')?>" class="btn btn-success" target="_blank">Listado PDF</a>
			  <a href="<?php echo base_url('assets/docs/manual_aut_titulos.pdf')?>" class="btn btn-success" target="_blank">Ver Manual del Trámite</a>
			</center>
			</div>
		  </div>

		</div>
	  </div>
</div>

            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>
	
	
	<script type="text/javascript">
		//$(window).on('load',function(){
		//	$('#myModal').modal('show');
		//});
	</script>
