
		<div class="row block right" style="background-color: #f8d7da;">
            <div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>TERMINOS Y CONDICIONES</b></h2>
					<h3>Declaración Juramentada</h3>
                </div>
                <div class="paragraph">
					<p>1. Los datos personales proporcionados a la Secretaria Distrital de Salud, en adelante SDS son objeto de tratamiento (recolección, almacenamiento, uso, circulación o supresión), con el fin de darles la finalidad específica para la que fueron suministrados y el cumplimiento de las funciones constitucionales y legales de la Entidad, según la reglamentación de la respectiva función o del servicio en virtud del cual el titular proporcionó dichos datos.</p>
					<p>2.  El tratamiento de los datos personales se realiza como la Entidad rectora en salud en Bogotá D.C, con la finalidad de garantizar el derecho a la salud a través de un modelo de atención integral e integrado y la gobernanza, para contribuir al mejoramiento de la calidad de vida de la población del Distrito Capital.</p>
					<p>3.  La modalidad de divulgación del dato personal ocurrirá de manera legítima, cuando la motivación de la solicitud de información esté basada en una clara y específica competencia funcional de la SDS.</p>
					<p>4.  El titular tiene derecho a optar por no suministrar cualquier información sensible solicitada por la SDS, relacionada, entre otros, con datos sobre su origen racial o étnico; la pertenencia a sindicatos, organizaciones sociales o de derechos humanos; convicciones políticas, religiosas, de la vida sexual, datos biométricos o datos de salud.</p>
					<p>5.  El suministro de los datos personales de menores de edad es facultativo y debe realizarse con autorización del padre, la madre o del representante legal del menor.</p>
					<p>6.  La SDS vela por el uso adecuado de los datos personales de niñas, niños y adolescentes, y respetará en su tratamiento el interés superior de aquellos, asegurando la protección de sus derechos fundamentales.</p>
					<p>7.  Una vez la SDS accede al dato personal, se convierte en responsable y encargada del tratamiento del dato, con el deber de garantizar los derechos fundamentales del titular de la información, previstos en la Constitución Política, y en consecuencia debe:</p>
					<p>7.1 Conservar con las debidas seguridades la información recibida para impedir su deterioro, pérdida, alteración, uso no autorizado o fraudulento.</p>
					<p>7.2 Guardar reserva de la información que le sea suministrada por el titular, en los términos señalados en el ordenamiento jurídico vigente aplicable a la materia.</p>
					<p>7.3 Utilizar los datos personales únicamente para los fines que justificaron la entrega, esto es, aquellos relacionados con la competencia funcional específica que motivó la solicitud de suministro del dato personal.</p>
					<p>7.4 Informar a los titulares del dato el uso que le esté dando al mismo, en caso de requerirlo el titular.</p>
					<p>7.5 Cumplir con las instrucciones que imparta la autoridad de control en relación con el cumplimiento de la legislación estatutaria.</p>
					<p>8. El usuario acepta que cualquier notificación relacionada con el trámite realizado, se haga al correo electrónico suministrado, en virtud de lo establecido en el artículo 56 de la Ley 1437 de 2011, para lo cual se entenderá que la hora y fecha de acceso del usuario al acto administrativo es la misma hora y fecha de envío de la notificación correspondiente desde el correo o aplicativo institucional hasta el correo suministrado por el usuario. </p>                   
                </div>
                <div class="registro_triada">
                    <a class="btn green w-100 py-2" href="<?php echo base_url('Usuario/aceptarTerminos')?>" class="btn btn-primary" role="button">Aceptar</a>
					<a class="btn red w-100 py-2" href="<?php echo base_url('login/logout_ci')?>" class="btn btn-primary" role="button">No Aceptar</a>
                </div>
            </div>
		</div>
		
		            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.nomasfilas.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>
