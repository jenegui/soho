<?php

    $retornoError = $this->session->flashdata('retorno_error');
    if ($retornoError) {
        ?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
    }

    $retornoExito = $this->session->flashdata('retorno_exito');
    if ($retornoExito) {
        ?>
        <div class="alert alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <?php echo $retornoExito ?>
        </div>
        <?php
    }

?>

<form class="form-inline" enctype="multipart/form-data" role="form" id="form_tramite" name="form_tramite" action="<?php echo base_url('usuario/actualizarTramite/'.$datos_tramite->id_titulo)?>" method="post">
		<div class="row block right" style="width:100%;">
            <div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Registro y autorizaci&oacute;n de títulos en el &aacute;rea de la salud.</b></h2>
					<h3>Actualizaci&oacute;n de Informaci&oacute;n</h3>
                </div>
            </div>
        </div>

	                    <input type="hidden" name="tipo_titulo" id="tipo_titulo" value="<?php echo $datos_tramite->tipo_titulo?>">
					<input type="hidden" name="fecha_editado" id="fecha_editado" value="<?php print_r($fecha_actual = date('Y-m-d')); ?>">
	<fieldset>		
		
		<?php
		if($datos_tramite->tipo_titulo == 1){
			$display_nacional = '';
			$display_extranjero = 'style="display:none"';
		}else{
			$display_nacional = 'style="display:none"';
			$display_extranjero = '';
		}
		?>
		
		<fieldset>
		<div class="row block right" <?php echo $display_nacional?>>
			<div class="subtitle">
				<h2><b>Formulario Titulación Nacional</b></h2>
			</div>
		
            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="institucion_educativa"><b>Instituci&oacute;n educativa</b></label>
					<select id="institucion_educativa" name="institucion_educativa" class="form-control select2 validate[required]">
						<option value="">Seleccione...</option>
							<?php
							for($i=0;$i<count($instituciones);$i++){

								if($instituciones[$i]->id_institucion == $datos_tramite->ins_titulo){
									echo "<option value='".$instituciones[$i]->id_institucion."' selected>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
								}else{
									echo "<option value='".$instituciones[$i]->id_institucion."'>".$instituciones[$i]->nombre_institucion." - ".$instituciones[$i]->sede."</option>";
								}
							}
							?>
					</select>
				</div>
            </div>
			<?php
				if($datos_tramite->profesion != ''){
					$programas = $this->usuarios_model->programasInstitucion($datos_tramite->ins_titulo);
				}
			?>			
            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="profesion"><b>Profesión</b></label>
					<select id="profesion2" name="profesion2" class="form-control select2 validate[required]">
						<option value="">Seleccione...</option>
						<?php

						if(isset($programas) && count($programas) > 0){
							for($i=0;$i<count($programas);$i++){
								if($programas[$i]->id_programa == $datos_tramite->profesion){
									echo "<option value='".$programas[$i]->id_programa."' selected>".$programas[$i]->nombre_programa."-".$programas[$i]->id_programa."-".$programas[$i]->tipo_prog. "</option>";
								}else{
									echo "<option value='".$programas[$i]->id_programa."'>".$programas[$i]->nombre_programa."</option>";
								}

							}
						}

						?>
					</select>					
				</div>
            </div>			

			<?php
				if($datos_tramite->tarjeta != ''){
				$display_tarjeta = '';				
			?>

            <div class="col-12 col-md-6 pl-4" id="div_tarjeta">
				<div class="paragraph">
					<br><br>
					<label for="tarjeta"><b>Tarjeta Profesional</b></label>
					<input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md validate[custom[number], minSize[1], maxSize[30]]" value="<?php echo $datos_tramite->tarjeta?>" style="width:100%;">					
				</div>
            </div>
			<?php										
				}
			else 
			{
				$display_tarjeta = 'style="display:none"';
			?>
			<div class="col-12 col-md-6 pl-4" id="div_tarjeta" style="display:none">
				<div class="paragraph">
					<br><br>
					<label for="tarjeta">Tarjeta Profesional</label>
					<div>
						<input id="tarjeta" name="tarjeta" placeholder="Tarjeta Profesional" class="form-control input-md validate[custom[number],minSize[1], maxSize[30]]" style="width:100%;">
					</div>
				</div>
			</div>										
			<?php
			}
			?>				
			
            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="diploma"><b>Diploma No.</b></label>
					<input id="diploma" name="diploma" placeholder="Diploma No." class="form-control input-md " type="text" value="<?php echo $datos_tramite->diploma?>" style="width:100%;">
				</div>
            </div>

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="acta"><b>Acta de grado</b></label>
					<input id="acta" name="acta" placeholder="Acta de grado" class="form-control input-md validate[custom[number], minSize[1], maxSize[30]]" value="<?php echo $datos_tramite->acta?>" style="width:100%;">
				</div>
            </div>	

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="acta"><b>Fecha terminación</b></label>
					<input id="fecha_term" name="fecha_term" placeholder="Fecha terminación" class="form-control input-md validate[required]" type="text" value="<?php echo $datos_tramite->fecha_term?>" style="width:100%;">
				</div>
            </div>

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="libro"><b>Libro</b></label>
					<input id="libro" name="libro" placeholder="Libro" class="form-control input-md validate[custom[number], minSize[1], maxSize[6]]" value="<?php echo $datos_tramite->libro?>" style="width:100%;">
				</div>
            </div>	

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="folio"><b>Folio</b></label>
					<input id="folio" name="folio" placeholder="Folio" class="form-control input-md validate[custom[number], minSize[1], maxSize[6]]" value="<?php echo $datos_tramite->folio?>" style="width:100%;">
				</div>
            </div>	

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="anio"><b>A&ntilde;o</b></label>
					<input id="anio" name="anio" placeholder="A&ntilde;o" class="form-control input-md validate[custom[number], required, minSize[4], maxSize[4]]" value="<?php echo $datos_tramite->anio?>" style="width:100%;">
				</div>
            </div>				
        </div>
		
		<div class="row block right" <?php echo $display_extranjero?>>
			<div class="subtitle">
				<h2><b>Formulario Titulación Extranjera</b></h2>
			</div>
		
            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="cod_universidad"><b>C&oacute;digo Universidad internacional</b></label>
					<input id="cod_universidad" name="cod_universidad" placeholder="C&oacute;digo Universidad internacional" class="form-control input-md validate[required]" type="text" value="<?php echo $datos_tramite->cod_universidad?>" style="width:100%;">
				</div>
            </div>
            
			<div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="resolucion"><b>Número de resoluci&oacute;n de convalidaci&oacute;n</b></label>
					<input id="resolucion" name="resolucion" placeholder="No de resoluci&oacute;n de convalidaci&oacute;n" class="form-control input-md validate[custom[number],required]" type="text" value="<?php echo $datos_tramite->resolucion?>" style="width:100%;">
				</div>
            </div>			


			<?php
				if($datos_tramite->tarjeta != ''){
			?>
			
            <div class="col-12 col-md-6 pl-4" id="div_tarjeta2">
				<div class="paragraph">
					<br><br>
					<label for="tarjeta"><b>Tarjeta Profesional</b></label>
					<input id="tarjeta" name="tarjeta2" placeholder="Tarjeta Profesional" class="form-control input-md validate[custom[number],minSize[1], maxSize[30]]" type="text" value="<?php echo $datos_tramite->tarjeta?>" style="width:100%;">					
				</div>
            </div>
			<?php										
				}
			else 
			{
			?>			
            <div class="col-12 col-md-6 pl-4" id="div_tarjeta2" style="display:none">
				<div class="paragraph">
					<br><br>
					<label for="tarjeta"><b>Tarjeta Profesional</b></label>
					<input id="tarjeta" name="tarjeta2" placeholder="Tarjeta Profesional" class="form-control input-md validate[custom[number],minSize[1], maxSize[30]]" type="text" value="<?php echo $datos_tramite->tarjeta?>" style="width:100%;">					
				</div>
            </div>
			<?php
			}
			?>	

			
            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>
					<label for="fecha_resolucion"><b>Fecha de resoluci&oacute;n</b></label>
					<input id="fecha_resolucion" name="fecha_resolucion" placeholder="Fecha de resoluci&oacute;n" class="form-control input-md validate[required]" type="text" autocomplete="off" readonly="readonly" value="<?php echo $datos_tramite->fecha_resolucion?>" style="width:100%;">
				</div>
            </div>

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="acta"><b>Fecha terminación</b></label>
					<input id="fecha_term_ext" name="fecha_term_ext" placeholder="Fecha terminaci&oacute;n" class="form-control input-md validate[required]" type="text" autocomplete="off" readonly="readonly" value="<?php echo $datos_tramite->fecha_term_ext?>" style="width:100%;">
				</div>
            </div>

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="entidad"><b>Entidad</b></label>
					<select id="entidad" name="entidad" class="form-control validate[required]">
						<option value="">Seleccione...</option>
						<option value="1" <?php if($datos_tramite->entidad == 1){ echo "selected";}?>>Ministerio de Educaci&oacute;n</option>
						<option value="2" <?php if($datos_tramite->entidad == 2){ echo "selected";}?>>ICFES</option>
					</select>
				</div>
            </div>	

			<?php
				if($datos_tramite->titulo_equivalente != ''){
					$profesionesequi = $this->login_model->profesionesequi();
				}
			?>


            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="titulo_equivalente"><b>Título equivalente</b></label>
					<select id="titulo_equivalente2" name="titulo_equivalente2" class="form-control select2 validate[required]" >
						<option value="">Seleccione...</option>
						<?php
						if(isset($profesionesequi) && count($profesionesequi) > 0){
							for($i=0;$i<count($profesionesequi);$i++){
								if($profesionesequi[$i]->id_programaequi == $datos_tramite->titulo_equivalente){
									echo "<option value='".$profesionesequi[$i]->id_programaequi."' selected>".$profesionesequi[$i]->nombre_programa." - ".$profesionesequi[$i]->tipo_prog."</option>";
								}else{
									echo "<option value='".$profesionesequi[$i]->id_programaequi."'>".$profesionesequi[$i]->nombre_programa." - ".$profesionesequi[$i]->tipo_prog."</option>";
								}

							}
						}
						?>
					</select>					
				</div>
            </div>

			<?php

				if($datos_tramite->pais_tituloequi != ''){
					$paisequivalante = $this->login_model->paises();
				}

			?>			

            <div class="col-12 col-md-6 pl-4">
				<div class="paragraph">
					<br><br>			
					<label for="titulo_equivalente"><b>País Origen T&iacute;tulo</b></label>
					<select id="pais_tituloequi" name="pais_tituloequi" class="form-control validate[required]">
						<option value="">Seleccione...</option>
						<?php

						if(isset($paisequivalante) && count($paisequivalante) > 0 ){
							for($i=0;$i<count($paisequivalante);$i++){
								if($paisequivalante[$i]->IdPais == $datos_tramite->pais_tituloequi){
									echo "<option value='".$paisequivalante[$i]->IdPais."' selected>".$paisequivalante[$i]->Nombre."</option>";
								}else{
									echo "<option value='".$paisequivalante[$i]->IdPais."'>".$paisequivalante[$i]->Nombre."</option>";
								}

							}
						}
						?>
					</select>				
				</div>
            </div>
			
        </div>
		</fieldset>
		<fieldset>
		<div class="row block right">
			
			<div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Documentos Adjuntos.</b></h2>
                </div>
            </div>

            <div class="col-12 col-md-12">
				<div class="alert alert-danger" role="alert">Si en la solicitud de mas informaci&oacute;n solicitaron cambiar uno de los documentos, por favor adjuntar solo el documento que desea cambiar, los dem&aacute;s dejar vac&iacute;os.</div>
            </div>			
			
            <div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>			
					<label for="pdf_documento" style="justify-content:left;"><b>Documento de identificaci&oacute;n</b></label>
					<input type="file" name="pdf_documento" id="pdf_documento" >
				</div>
            </div>
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
					<label for="pdf_titulo" style="justify-content:left;"><b>Título (Diploma de grado)</b></label>
					<input type="file" name="pdf_titulo" id="pdf_titulo">
				</div>
            </div>			
			
			<div class="col-12 col-md-6 pl-4">
                <div class="paragraph">
					<br><br>
					<label for="pdf_acta" style="justify-content:left;"><b>Acta de grado</b></label>
					<input type="file" name="pdf_acta" id="pdf_acta">
				</div>
            </div>



            <div class="col-12 col-md-6 pl-4" id="div_doctarjeta" <?php echo $display_tarjeta?>>
                <div class="paragraph">
					<br><br>			
					<label for="pdf_tarjeta" style="justify-content:left;"><b>Tarjeta Profesional</b></label>
					<input type="file" name="pdf_tarjeta" id="pdf_tarjeta">
				</div>
            </div>

            <div class="col-12 col-md-6 pl-4" <?php echo $display_extranjero?>>
                <div class="paragraph">
					<br><br>			
					<label for="pdf_resolucion" style="justify-content:left;"><b>Resoluci&oacute;n de convalidaci&oacute;n</b></label>
					<input type="file" name="pdf_resolucion" id="pdf_resolucion">
				</div>
            </div>

			<div class="col-12 col-md-12 pl-4" align="center">
                <div class="paragraph">
					<br><br>
					<button class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button">Actualizar Trámite</button>
					<br><br>
					<a class="btn red w-100 py-2" href="<?php echo base_url()?>" class="btn btn-primary" role="button">Regresar</a>

				</div>
            </div>			
        </div>
		</fieldset>
    </fieldset>

</form>

            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>