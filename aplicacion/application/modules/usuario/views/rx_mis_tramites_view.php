<h2 class="text-blue"><b>Registro y autorización de licencias de Rayos X</b></h2>

   <div class="line"></div>

   <!-- PRIMER PASO CREACION DEL TRAMITE Y ASIGNACION DEL PRIMER ESTADO -->
   <div class="row block w-100 newsletter">
      <div class="w-100">
         <div class="subtitle">
            <h3><b>Mis Trámites Licencia de practica medica categoría I y II</b></h3>
         </div>

         <div class="box-body">
           <div class="row">
             <div class="col-md-12">
               <a href="<?php echo base_url('usuario/tramite_rayosx')?>" class="btn blue w-100 py-2"> Crear Nuevo Trámite</a>
             </div>
               <div class="col-md-12">
                 <table class="table table-bordered">
                   <tbody>
                     <tr>
                      <th> Id Trámite </th>
                      <th> Tipo Trámite </th>
                      <th> Inicio Trámite </th>
                      <th> Actividad </th>
                      <th> Fecha Inicio Actividad </th>                      
                      <th> Observación </th>                      
                      <th><center> Ver Más</center></th>  
						<!--
                      <th><center> Resolucion Aprobación</center></th>
                      <th><center> Resolucion Negación</center></th>
                      <th><center> Resolucion Desistimiento</center></th>
						-->
                     </tr>
                   <!-- Conditional x  Variable Listado de Usuarios-->
                   <?php 
				   if(isset($tramitesrx)){
                   foreach($tramitesrx as $tramiterx)
				   {
					?>
                     <tr>
                       <td><?php echo $tramiterx->id?></td> 
                       <td><?php echo $tramiterx->descripcion ?></td>
                       <td><?php echo $tramiterx->created_at ?></td>                       
                       <td><?php echo $tramiterx->flujo_actividad_inicial ?></td>
                       <td><?php echo $tramiterx->update_at ?>}</td>                       
                       <td><?php echo $tramiterx->observaciones?></td>                       
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/tramite/'.$tramiterx->id)}}"><i class="fa fa-file"></i></a></td>
                      <!--              
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion1/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion2/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       <td align="center"><a href="{{url('/ventanilla/tramites/solicitud_rayosx/resolucion3/'.$tramiterx->id)}}" target="_blank"><i class="fa fa-file"></i></a></td>
                       -->
                     </tr>
					<?php
				   }
				   }else{
					   ?>
					   <tr>
						   <td colspan="6" scope="col">No Existen Trámites Registrados</td>
						 </tr>
					   <?php
				   }
					 ?>
                   </tbody></table>
                   <!-- Conditional Pagination Bootstrap -->
                   <div class="box-footer clearfix">
                     @if(count($tramitesrx))
                       <div class="mt-2 mx-auto">
                         {{ $tramitesrx->links('pagination::bootstrap-4') }}
                       </div>
                     @endif
                   </div>
                   @endif

              </div>
            </div>
         </div>

       <div id="btnRegistrarSolicitud" class="col-md-12 pt-200 collapse">
         <p align="center">
            <br/>
            <!-- Primer Collapsible - Localizacion Entidad -->
            <button type="submit" class="btn yellow">
               Crear Solicitud
            </button>
         </p>
       </div>
   </div>
@endsection

@section ("scripts")

 <script languague="javascript">

 </script>

@endsection
