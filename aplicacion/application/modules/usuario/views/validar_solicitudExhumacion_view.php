<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Documento Preliminar</h4> 
      </div>
      <div class="modal-body">
        <div style="text-align: center;">
            <embed src="<?php echo base_url("uploads/preliminares/".$documento_preliminar->nombre)?>)?>" width="100%" height="600" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">	
		</div>
      </div>
      <div class="modal-footer">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>


        <div class="form-registro">
            <form class="form-horizontal" id="form_tramite" name="form_tramite" action="<?php echo base_url('usuario/guardarAprobacionLicenciaExh/'.$listadosolicitudesExh->idlicencia_exhumacion)?>" method="post">
                <fieldset class="header-form">
                    <legend>Tr&aacute;mite
                        <?php echo $listadosolicitudesExh->idlicencia_exhumacion?>
                        <br>Estado:
                        <?php echo $listadosolicitudesExh->estado?>
                        <br> <?php if(isset($documento_preliminar->nombre)){?>
						<button class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
						  Ver Documento Preliminar
						</button>
                        <!--<a href="<?php echo base_url("uploads/preliminares/".$documento_preliminar->nombre)?>" target="_blank">Ver Documento</a>-->
                        <?php } ?>
                    </legend>
                    <div class="col-md-1">
                        <i class="fas fa-user fa-8x"></i>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <?php echo $listadosolicitudesExh->nume_identificacion?>
                        </p>
                        <p><label for="">Nombre:</label>
                            <?php echo $listadosolicitudesExh->p_nombre." ".$listadosolicitudesExh->s_nombre." ".$listadosolicitudesExh->p_apellido." ".$listadosolicitudesExh->s_apellido?>
                        </p>
                        <p><label for="">Correo:</label>
                            <?php echo $listadosolicitudesExh->email?>
                        </p>
                        <p><label for="">Tel&eacute;fono fijo:</label>
                            <?php echo $listadosolicitudesExh->telefono_fijo?>
                        </p>
                        <p><label for="">Celular:</label>
                            <?php echo $listadosolicitudesExh->telefono_celular?>
                        </p>
                    </div>
                </fieldset>
                <fieldset class="header-form">
                    <div class="col-md-1">
                        <label>Informaci&oacute;n Solicitud</label>
                        <i class="fas fa-file fa-8x"></i>
                    </div>
                    <div class="col-md-5">
                        <div class="inp-form">
                            <label for="">Número Licencia Inhumación:</label>
                            <?php
                             echo $listadosolicitudesExh->numero_licencia
                            ?>
                        </div>
                        <div class="inp-form">
                            <label for="">Registro Defunción:</label>
                            <?php
                               echo $listadosolicitudesExh->numero_regdefuncion
                            ?>
                        </div>
                        <div class="inp-form">
                            <label for="">Número Documento Fallecido:</label>
                            <?php
                                echo $listadosolicitudesExh->numero_docfallecido
                            ?>
                        </div>
                       

                    </div>
                    <div class="col-md-1">
                        <label>Documentos Adjuntos</label>
                        <i class="fas fa-file-pdf fa-8x"></i>
                    </div>
                    <div class="col-md-5">

                        <table class="table">
                            <tr>
                                <th>Descripci&oacute;n</th>
                                <th>Ver</th>
                            </tr>

                            <?php
                            if($listadosolicitudesExh->pdf_cedulasolicitante != 0){
                                $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh->pdf_cedulasolicitante);
                                ?>
                                <tr>
                                    <td>Documento Solicitante</td>
                                    <td>
                                        <a href="<?php echo base_url('uploads/exhumacion/'.$resultado_archivo->nombre)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }

                            if($listadosolicitudesExh->pdf_certificadocementerio != 0){
                                $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh->pdf_certificadocementerio);
                                ?>
                                    <tr>
                                        <td>Certificado cementerio</td>
                                        <td>
                                            <a href="<?php echo base_url('uploads/exhumacion/'.$resultado_archivo->nombre)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                        </a>
                                        </td>
                                    </tr>
                                    <?php
                            }

                            if($listadosolicitudesExh->pdf_certificado_per4 != 0){
                                $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh->pdf_certificado_per4);
                                ?>
                                        <tr class="inp-form">
                                            <td>Certificado permanencia mínimo 4 años</td>
                                            <td>
                                                <a href="<?php echo base_url('uploads/exhumacion/'.$resultado_archivo->nombre)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                        </a>
                                            </td>
                                        </tr>
                                        <?php
                            }

                            if($listadosolicitudesExh->pdf_certificado_per3 != 0){
                                $resultado_archivo = $this->mlicencia_exhumacion->consultar_archivo($listadosolicitudesExh->pdf_certificado_per3);
                                ?>
                                            <tr class="inp-form">
                                                <td>Certificado permanencia mínimo 3 años </td>
                                                <td>
                                                    <a href="<?php echo base_url('uploads/exhumacion/'.$resultado_archivo->nombre)?>" target="_blank">
                                            <img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
                                        </a>
                                                </td>
                                            </tr>
                                            <?php
                            }
                            ?>
                        </table>
                    </div>

                </fieldset>
<?php
                
$aprobacion_preliminar = $this->mlicencia_exhumacion->info_aprobacionlicencia($listadosolicitudesExh->idlicencia_exhumacion);                
?>
                <fieldset class="header-form">
                    <div class="col-md-1">
                        <label>Resultado de la validaci&oacute;n</label>
                        <i class="fas fa-check-double fa-8x"></i>
                    </div>
                    <div class="col-md-11">

                        <select id="resultado_validacion" name="resultado_validacion" class="form-control validate[required]">
                            <option value="">Seleccione...</option>
                            <option value="2">Solicitar más información</option>
                            <option value="3">Aprobado</option>
             
                        </select>
                        <br><br>
                        <div id="div_resuelve" style="display:none">
                           
                            
                            <div id="aprobado" style="display:none">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="nombre_difunto">Nombre completo difunto</label>
                                    <div class="col-md-8">
                                        <input  type="text" name="nombre_difunto" id="nombre_difunto" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="cementerio">Cementerio </label>
                                    <div class="col-md-8">
                                        <input  type="text" name="cementerio" id="cementerio" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="">Número Lic. Inhumación</label>
                                    <div class="col-md-3">
                                        <input type="number" name="num_licencia_inhumacion" id="" class="form-control" value="<?php if($listadosolicitudesExh->numero_licencia != ''){ echo $listadosolicitudesExh->numero_licencia;}?>">
                                    </div>
                                    <label class="col-md-2 control-label" for="">Fecha Inhumación</label>
                                    <div class="col-md-3">
                                        <input type="date" name="fecha_inh" id="fecha_inh" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="num_lic_exhumacion">Número Licencia Exhumación</label>
                                    <div class="col-md-8">
                                        <input  type="number" name="num_lic_exhumacion" id="num_lic_exhumacion" class="form-control">
                                    </div>
                                </div>
                               
                            </div><!-- -->
                            <div id="div_masinformacion" style="display:none">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="mensaje">Digite la informaci&oacute;n  que desea que adjunte o ajuste el usuario:</label>
                                    <div class="col-md-8">
                                        <textarea name="mensaje" id="mensaje" class="form-control validate[required]"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="observaciones">Observaciones</label>
                                    <div class="col-md-8">
                                        <textarea name="observaciones" id="observaciones" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <center>
                                            <button name="guardar" id="guardar" type="submit" class="btn btn-lg btn-success">Guardar</button>
                                        </center>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </fieldset>
            </form>
        </div>
<script>
 $("#resultado_validacion").change(function(){
        if($("#resultado_validacion").val() != ''){
            $("#div_resuelve").show();

            if($("#resultado_validacion").val() == 2){
                $("#div_masinformacion").show();
                $("#aprobado").hide();
            }else if($("#resultado_validacion").val() == 3){
                $("#aprobado").show();
                $("#div_masinformacion").hide();
            }

        }
    });
   
</script>