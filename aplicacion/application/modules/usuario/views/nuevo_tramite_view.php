
	<br>
	    <div class="col-12 col-md-12 pl-4">
			<div class="alert alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				<p align="justify">
				<b>Apreciado Ciudadano(a)</b><br><br>
				A continuación encontrará un listado de los trámites disponibles para su registro y solicitud.
				<br>
				Ante cualquier inquietud favor leer primeramente el manual e instrucciones disponibles que cuenta cada trámite. 
				Leer esta información puede ayudarle a evitar reprocesos y demoras de la misma gestión, registrando correctamente el trámite.
				</p>
			</div>
        </div>	
	<br>
	<div class="scroll">
	
	<?php
    if($this->session->userdata('tipo_identificacion') < 5){
    ?>
	
		<div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="<?php echo base_url('assets/imgs/foto_registro.png')?>" alt="establecimientos" title="establecimientos">
					<table style="width:100%;">
						<tr>
						<td><br>
							<a href="http://visor.suit.gov.co/VisorSUIT/index.jsf?FI=11260" target="_blank"><button class="btn-info btn-sm">Ver Requisitos</button></a>
						</td>
						<td><br>
							<a href="<?php echo base_url('assets/docs/manual_aut_titulos.pdf')?>" target="_blank"><button class="btn-success btn-sm">Ver Manual</button></a>
						</td>
						</tr>
					</table>
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2><b>Registro y autorizaci&oacute;n de t&iacute;tulos en el &aacute;rea de la salud.</b></h2>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">El propósito de este trámite es obtener autorización para el ejercicio de la profesión y ocupación, válida para todo el territorio nacional, en el área de la salud como técnico, tecnólogo, o universitario, de los títulos obtenidos en la Ciudad de Bogotá D.C, excepto convalidaciones de titulación Extranjera. Nota:  Las profesiones exceptuadas son: Medicina, Jefe de Enfermería, bacteriología, químicos farmacéuticos, fisioterapia, fonoaudiología, instrumentadores quirúrgicos, optometría, terapia respiratoria, odontología, nutrición y dietética y terapia ocupacional. Lo anterior teniendo en cuenta que el registro y autorización del título de dichas profesiones, fue delegada a sus respectivos cuerpos colegiados.</p>                    
                </div>
                <div class="registro_triada text-center">
                    <a class="btn yellow w-100 py-2" href="<?php echo base_url('usuario/tramite_profesionales')?>" class="btn btn-primary" role="button">Registrar Solicitud</a>
                </div>
            </div>
        </div>

		<div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="<?php echo base_url('assets/imgs/foto_registro2.png')?>" alt="establecimientos" title="establecimientos">
					<table style="width:100%;">
						<tr>
						<td><br>
							<a href="http://visor.suit.gov.co/VisorSUIT/index.jsf?FI=4783" target="_blank"><button class="btn-info btn-sm">Ver Requisitos</button></a>
						</td>
						<td><br>
							<a href="<?php echo base_url('assets/docs/manual_lic_exhum.pdf')?>" target="_blank"><button class="btn-success btn-sm">Ver Manual</button></a>
						</td>
						</tr>
					</table>
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2><b>Licencia de Exhumaci&oacute;n para Cadáveres.</b></h2>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">Autorización para extraer cadáveres, restos humanos y restos óseos del lugar de inhumación, previa orden judicial o administrativa para los efectos funerarios o legales una vez se haya cumplido el tiempo mínimo de permanencia establecido por la Ley.  Nota:  El trámite está dirigido para las exhumaciones de muertes ocurridas en la ciudad de Bogotá D.C. Cualquier inquietud en relación al trámite agradezco comunicarse al correo electrónico contactenos@saludcapital.gov.co.</p>                    
                </div>
                <div class="registro_triada text-center">
                    <a class="btn yellow w-100 py-2" href="<?php echo base_url('usuario/tramite_exhumacion')?>" class="btn btn-primary" role="button">Registrar Solicitud</a>
                </div>
            </div>
        </div>			

		<div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="<?php echo base_url('assets/imgs/foto_registro3.png')?>" alt="establecimientos" title="establecimientos">
					<table style="width:100%;">
						<tr>
						<td><br>
							<a href="http://visor.suit.gov.co/VisorSUIT/index.jsf?FI=71904" target="_blank"><button class="btn-info btn-sm">Ver Requisitos</button></a>
						</td>
						<td><br>
							<a href="<?php echo base_url('assets/docs/manual_lic_rayosx.pdf')?>" target="_blank"><button class="btn-success btn-sm">Ver Manual</button></a>
						</td>
						</tr>
					</table>				
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2><b>Licencia de Rayos RX para Prestadores de Servicios de Salud.</b></h2>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">Obtener la expedición, renovación o modificación de la licencia de prácticas médicas, a través de la cual se faculta al prestador de servicios de salud a hacer uso de equipos generadores de radiación ionizante, móviles o fijos, durante un período determinado. Cualquier inquietud en relación al trámite agradezco comunicarse telefónicamente al número 3649090 Ext 9801 ó al correo electrónico contactenos@saludcapital.gov.co.</p>                    
                </div>
                <div class="registro_triada text-center">
                    <a class="btn yellow w-100 py-2" href="<?php echo base_url('usuario/tramite_rayosx')?>" class="btn btn-primary" role="button">Registrar Solicitud</a>
                </div>
            </div>
        </div>	
        <?php
    }else if ($this->session->userdata('tipo_identificacion') == 5) {

    ?>	
	
		<div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="<?php echo base_url('assets/imgs/foto_registro3.png')?>" alt="establecimientos" title="establecimientos">
					<table style="width:100%;">
						<tr>
						<td><br>
							<a href="http://visor.suit.gov.co/VisorSUIT/index.jsf?FI=71904" target="_blank"><button class="btn-info btn-sm">Ver Requisitos</button></a>
						</td>
						<td><br>
							<a href="<?php echo base_url('assets/docs/manual_lic_rayosx.pdf')?>" target="_blank"><button class="btn-success btn-sm">Ver Manual</button></a>
						</td>
						</tr>
					</table>				
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2><b>Licencia de Equipos Ionizantes Rayos RX.</b></h2>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">Obtener la expedición, renovación o modificación de la licencia de prácticas médicas, a través de la cual se faculta al prestador de servicios de salud a hacer uso de equipos generadores de radiación ionizante, móviles o fijos, durante un período determinado. Cualquier inquietud en relación al trámite agradezco comunicarse telefónicamente al número 3649090 Ext 9801 ó al correo electrónico contactenos@saludcapital.gov.co.</p>                    
                </div>
                <div class="registro_triada text-center">
                    <a class="btn yellow w-100 py-2" href="<?php echo base_url('usuario/tramite_rayosx')?>" class="btn btn-primary" role="button">Registrar Solicitud</a>
                </div>
            </div>
        </div>	
	<?php
	}
	?>
	<div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="<?php echo base_url('assets/imgs/foto_registro3.png')?>" alt="establecimientos" title="establecimientos">
					<table style="width:100%;">
						<tr>
						<td><br>
							<a href="http://visor.suit.gov.co/VisorSUIT/index.jsf?FI=71904" target="_blank"><button class="btn-info btn-sm">Ver Requisitos</button></a>
						</td>
						<td><br>
							<a href="<?php echo base_url('assets/docs/manual_lic_rayosx.pdf')?>" target="_blank"><button class="btn-success btn-sm">Ver Manual</button></a>
						</td>
						</tr>
					</table>				
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2><b>Licencia de Expendedores de Droga.</b></h2>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">Obtener la credencial de expendedor de drogas, el cuál autoriza al ciudadano para dirigir el establecimeinto llamado DROGUERÍA. Cualquier inquietud en relación al trámite agradezco comunicarse telefónicamente al número 3649090 Ext 9801 ó al correo electrónico contactenos@saludcapital.gov.co.</p>                    
                </div>
                <div class="registro_triada text-center">
                    <a class="btn yellow w-100 py-2" href="<?php echo base_url('expendedor_droga/')?>" class="btn btn-primary" role="button">Registrar Solicitud</a>
                </div>
            </div>
        </div>
	</div>
	
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>
	<!--
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url('assets/imgs/enfermera.jpg')?>" alt="...">
                <div class="caption">
                    <h5><b>Licencia de Exhumaci&oacute;n para Cadáveres</b></h5>
                    <p>Autorización para extraer cadáveres, restos humanos y restos óseos del lugar de inhumación, previa orden judicial o administrativa para los efectos funerarios o legales una vez se haya cumplido el tiempo mínimo de permanencia establecido por la Ley.  Nota:  El trámite se encuentra orientado a la Licencia de Exhumación de muertes Naturales generadas en Bogotá D.C. Cualquier inquietud en relación al trámite agradezco comunicarse telefónicamente al número 3649090 Ext 9801 ó al correo electrónico contactenos@saludcapital.gov.co.</</p>
                    <p><a href="<?php echo base_url('usuario/tramite_exhumacion')?>" class="btn btn-primary" role="button">Registrar Solicitud</a></p>
                </div>
            </div>
        </div>
    ->


    <!--<div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img src="<?php echo base_url('assets/imgs/rayosx.jpg')?>" alt="...">
            <div class="caption">
                <h5><b>Licencia De Funcionamiento De Equipos De Rayos X Y Otras Fuentes Emisoras De Radiaciones Ionizantes</b></h5>
                <p>El prop&oacute;sito de este tr&aacute;mite es obtener la licencia que autoriza el funcionamiento de los equipos o fuentes emisoras de radiaciones ionizantes, para los servicios de radiolog&iacute;a e im&aacute;genes diagn&oacute;sticas y/o medicina nuclear que se ofrecen en la ciudad de Bogot&aacute;, D.C</p>
                <h5><b>Renovaci&oacute;n De La Licencia De Funcionamiento De Equipos De Rayos X Y Otras Fuentes Emisoras De Radiaciones Ionizantes</b></h5>
                <p>El prop&oacute;sito de este tr&aacute;mite es obtener la renovaci&oacute;n de la licencia que autoriza el funcionamiento de los equipos o fuentes emisoras de radiaciones ionizantes, para los servicios de radiolog&iacute;a e im&aacute;genes diagn&oacute;sticas y/o medicina nuclear que se ofrecen en la ciudad de Bogot&aacute;, D.C</p>
                <p><a href="<?php echo base_url('usuario/tramite_rayosx')?>" class="btn btn-primary" role="button">Registrar Solicitud</a></p>
            </div>
        </div>
    </div>-->

