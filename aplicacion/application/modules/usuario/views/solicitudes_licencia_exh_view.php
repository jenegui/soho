<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <!--Perfil Funcionario -->
             <div class="row">
            <h1>Solicitudes Licencias</h1>
            <table class="table" id="tabla_tramites">
                <thead>
                    <tr>
                        <th>ID Solicitud</th>
                        <th>Identificaci&oacute;n</th>
                        <th>Parentesco</th>
                        <th>Documento Fallecido</th>
                        <th>Licencia Inhumacón</th>
                        <th>Fecha radicaci&oacute;n</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if(count($listadosolicitudesExh)>0){
                        for($i=0;$i<count($listadosolicitudesExh);$i++){
                        ?>
                    <tr>
                        <td>
                            <?php echo $listadosolicitudesExh[$i]->idlicencia_exhumacion;?>
                        </td>
                        <td>
                            <?php echo $listadosolicitudesExh[$i]->nume_identificacion ?>
                        </td>
                        <td>
                            <?php echo $listadosolicitudesExh[$i]->parentesco?>
                        </td>
                        <td>
                            <?php echo $listadosolicitudesExh[$i]->numero_docfallecido?>
                        </td>
                          <td>
                            <?php echo $listadosolicitudesExh[$i]->numero_licencia ?>
                        </td>
                        <td>
                            <?php $listadosolicitudesExh[$i]->fecha_solicitud ?>
                        </td>
                      
                        <td>
                            <?php 
                            if($listadosolicitudesExh[$i]->estado=='1'){echo 'Solicitud realizada por el usuario';}
                            elseif($listadosolicitudesExh[$i]->estado=='2'){echo 'El Usuario debe actualizar información';}
                            else{echo 'Solicitud Aprobada';}
                             ?>
                        </td>
                        <td>
                            <center>
        
                              <a href="<?php echo base_url('usuario/validacion_exh/'.$listadosolicitudesExh[$i]->idlicencia_exhumacion)?>">
                                <img src="<?php echo base_url('assets/imgs/aprobar.png')?>" width="20px">
                                <br>Validar Solicitud
                                </a>
                            </center>
                        </td>
                    </tr>
                    <?php
    }
}


?>
                </tbody>
            </table>
        </div>