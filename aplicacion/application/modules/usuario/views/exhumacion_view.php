<style type="text/css">
    .btn-primary{width: 100%;padding-top: 6px;padding-bottom: 6px;}
    .btn-warning{width: 100%;padding-top: 6px;padding-bottom: 6px;}
    .main-section .row {
        border-top: 0px;
    }
</style>

        <form method="post" id="solicitudLiEXH" action="<?php echo base_url('usuario/guardarTramiteExhumacion'); ?>" enctype="multipart/form-data">
            <!--<input type="hidden" name="action" value="subir_doc">-->
		<div class="row block rightalert alert-warning" role="alert">
            <div class="col-12 col-md-12 pl-4">
                <div class="subtitle">
                    <h2><b>Solicitud Licencia de Exhumación</b></h2>
					<h3>Documentos PDF de soporte para realizar el trámite.</h3>
                </div>
                <div class="paragraph">
                    <p style="text-align: justify;">
					  <b>Señor Ciudadano(a):</b>
					  <br><br>
					  Con el fin de dar una pronta respuesta a su solicitud a continuación, se encuentra información importante sobre los documentos que puede adjuntar para acreditar parentesco.
                      <br><br>
					  <b>Padres:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido.
                      <br>
					  <b>Hijos:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Solicitante.
					  <br>
					  <b>Hermanos:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido y Solicitante.
					  <br>
					  <b>Abuelos:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Fallecido y Registro civil de Nacimiento del Padre o Madre del Fallecido con el que tiene línea directa.
					  <br>
					  <b>Nietos:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Solicitante.
					  <br>
					  <b>Suegros:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Matrimonio del Fallecido y Registro Civil de Nacimiento de Conyugue del Fallecido 
					  <br>
					  <b>Hijos del cónyuge:</b> Documento de identidad del solicitante, Documento Carta Cementerio, Registro civil de Nacimiento del Solicitante y Registro civil de Matrimonio del Fallecido con  Padre o Madre del Solicitante.
					  <br>
					  <b>Adoptante:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido en el cual conste la adopción.
					  <br>
					  <b>Adoptado:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro civil de Nacimiento del Fallecido en el cual conste la adopción.
					  <br>
					  <b>Matrimonio:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Registro Civil de Matrimonio.
					  <br>
					  <b>Unión marital de hecho:</b> Documento de identidad del solicitante, Documento Carta Cementerio y Escritura Pública de declaración de Unión marital de hecho ó extrajuicio.
					  <br>
					  <b>Contrato Civil (mismo sexo):</b> Documento de identidad del solicitante, Documento Carta Cementerio y Contrato Solemne suscrito entre el solicitante y el fallecido.
                      <br><br>
                      <b>Nota Importante:</b> Si el fallecido tuvo intervención de medicina legal por favor anexar carta de autorización de exhumación y 
					  cremación de la Fiscalía General de la Nación. El registro civil de matrimonio ó de nacimiento debe no ser superior a 30 días en el momento de la solicitud.
					</p>                    
                </div>
                <div class="registro_triada">
                </div>
            </div>
        </div>
		
              <!---******************************************************************************
                1	Fotocopia Numero de licencia inhumacion y fecha de inhumacion
            ***********************************************************************************-->

	<div class="row block right" id="consulta"> 
		<div class="col-12 col-md-12 pl-4">
			<div class="subtitle">
				<h2><b>Pre-Consulta Requisito Trámite Licencia de Exhumación.</b></h2>
			</div>
		</div>
		
		<div class="col-12 col-md-12">
			<p align="justify">Favor ingresar el Número de Licencia de Inhumación con su correspondiente Fecha del Documento, que fue generado por el Cementerio. Si desconoce esta información, no dude en contactar al siguiente correo electrónico contactenos@saludcapital.gov.co solicitando orientación sobre los datos que se encuentran registrados, para continuar con el trámite. Gracias por su comprensión.</p>	
		</div>
		
		<div class="col-lg-3 col-md-12" >
			<label for="numero_licencia"><b>No. Licencia Inhumación</b> <font color="orange">(*)</font></label>
		</div>
		<div class="col-lg-2 col-md-12" >
			<input name="numero_licencia" class="validate[custom[number], required] form-control" id="numero_licencia" min="0" onkeypress="return nro(event)" required>
		</div>
		<div class="col-lg-2 col-md-12" >
			<label for="fecha"><b>Fecha Inhumación</b> <font color="orange">(*)</font></label>
		</div>
		<div class="col-lg-3 col-md-12" >
			<input name="fechaInh" id="fechaInh" class="form-control input-md validate[required]" type="text" autocomplete="off" readonly="readonly" style="width:100%;">
		</div>
		<div class="col-lg-2 col-md-12" >
			<input type="button" name="consultarlicencia" class="btn btn-primary" value="Consultar" id="consultarlicencia">
		</div>
		
		<div class="col-12 col-md-12 pl-4" align="center">
			<div class="paragraph">
				<br><br>
				<a class="btn red w-100 py-2" href="<?php echo base_url()?>" class="btn btn-primary" role="button">Regresar</a>

			</div>
		</div>		
	</div>



	<div class="row block right" id="ocultar" style="display:none"> 
		<div class="col-12 col-md-12 pl-4">
			<div class="subtitle">
				<h2><b>Formulario Trámite Licencia de Exhumación.</b></h2>
			</div>
		</div>

		  <!---******************************************************************************
			1	parentezco
		***********************************************************************************-->
		
		<div class="col-lg-5 col-md-12">
			<label for="parentesco"><b>Seleccione Parentesco con el difunto</b> <font color="orange">(*)</font></label>			
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<select name="parentesco"  class="validate[required] form-control" id="parentesco" required>
				<option selected="true" disabled></option>
				<option value="Conyugue">Conyugue/compañero(a) permanente</option>
				<option value="Padre/Madre">Padre/Madre</option>
				<option value="Hermano(a)">Hermano(a)</option>
				<option value="Hijo(a)">Hijo(a)</option>
				<option value="Abuelo(a)">Abuelo(a)</option>
				<option value="Nieto(a)">Nieto(a)</option>
				<option value="Otro">Otro</option>
			</select>
		</div>
		
            <!---******************************************************************************
                1	Fotocopia Cédula de Cudadania Solicitante
            ***********************************************************************************-->
		
		<div class="col-lg-5 col-md-12">
			<br>
			<label for="pdf_cedulasolicitante"><b>Fotocopia Cédula de Ciudadanía Solicitante</b> <font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12">
			<br>
			<input class="validate[required] btn btn-primary" type="file"  title="Seleccione un PDF por favor!" name="pdf_cedulasolicitante" id="pdf_cedulasolicitante" accept=".pdf" class="validate[required]" required="true"/>
		</div>		

            <!---******************************************************************************
                1	Fotocopia Documento adicional en caso de otro parensco
            ***********************************************************************************-->
		
		<div class="col-lg-12 col-md-12" id="parentescootro1" style="display: none"> 
			<font style="font-size:10px;" color="grey">Por favor digite el tipo de vínculo existente con el fallecido y adjunté los documentos solicitados según el caso, si no cuenta con la documentación requerida, por favor adjunte la documentación disponible para efectos de analizar su solicitud. En caso de requerir información adicional, le sera comunicado de acuerdo a lo establecido en el artículo 17 de la Ley 1437 de 2011, sustituida por el artículo 1 de la Ley 1755 de 2015</font>
		</div>			
		
		<div class="col-lg-5 col-md-12" id="parentescootro" style="display: none"> 
			<br>
			<label for=""><b>Parentesco</b></label>
		</div>	

		<div class="col-lg-7 col-md-12" id="parentescootro2" style="display: none">
			<br>
			<input type="text" name="parentescoOTRO" class="validate[required],custom[onlyLetterNumber] form-control" id="parent" placeholder="Digite el parentesco con el difunto">
		</div>	

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_otro"><b>Fotocopia de los Documento que acredite parentesco</b> <font color="orange">(*)</font><br> <font style="font-size:10px;" color="grey">(Registros civiles de nacimiento, registro civil de matrimonio, escritura pública y/o contrato de unión según el caso que aplique. Consolidados en un único PDF)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input class="validate[required]  btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_otro" id="pdf_otro" accept=".pdf" required="true">  
		</div>

			<!---******************************************************************************
                Adicion campos Muerte Medicina Legal Mario Beltran 30-07-2019 Requerimiento Nuevo
            ***********************************************************************************-->

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_otro"><b>Favor Indicar si Medicina Legal interviene en la Licencia Exhumación</b> <font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<select id="intervienemedlegal" name="intervienemedlegal" class="validate[required] form-control" required>
			  <option value="" selected="true" disabled>Seleccione una opción</option>
			  <option value="0">No</option>
			  <option value="1">SI</option>
			</select>
		</div>
		
		<div class="col-lg-5 col-md-12" id="docmedicinalegal"  style="display: none"> 
			<br>
			<label for=""><b>Fotocopia Documento emitido por Medicina Legal</b> <font color="orange">(*)</font></label>
		</div>	

		<div class="col-lg-7 col-md-12" id="docmedicinalegal2" style="display: none">
			<br>
			<input class="validate[required]  btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_autorizacionfiscal" id="pdf_autorizacionfiscal" accept=".pdf">  
		</div>	

            <!---******************************************************************************
                2	Fotocopia Registro Defunción o Licencia de Inhumacion
            ***********************************************************************************-->
				
		<!--
		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="numero_regdefuncion"><b>No. Registro de Defunción</b></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input name="numero_regdefuncion" class="form-control input-md validate[maxSize[11], custom[number]]" id="numero_regdefuncion" min="0" onkeypress="return nro(event)">
		</div>		
		-->	
		
            <!---******************************************************************************
                2	No Identificacion del Fallecido
            ***********************************************************************************-->
				
		<!--
		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="numero_docfallecido"><b>No. Identificación del fallecido</b></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input name="numero_docfallecido" class="form-control input-md validate[maxSize[11], custom[number]]" id="numero_docfallecido">
		</div>	
		-->
            <!---******************************************************************************
                3	Certificado de Cementerio, donde se encuentra sepultado el cadáver
            ***********************************************************************************-->

		<div class="col-lg-5 col-md-12" >
			<br>
			<label for="pdf_certificadocementerio"><b>Certificado del Cementerio en donde se encuentra sepultado el cadáver</b> <font color="orange">(*)</font> <br> <font style="font-size:10px;" color="grey">(Carta para exhumación del cementerio donde se encuentra sepultado el cadáver.)</font></label>
		</div>
		
		<div class="col-lg-7 col-md-12" >
			<br>
			<input class="validate[required] btn btn-primary" type="file" title="Seleccione un PDF por favor!"  name="pdf_certificadocementerio" id="pdf_certificadocementerio" accept=".pdf" required="true"/>  
		</div>	

		<!-- **************************** declaracion bajo juramento *****************************************-->
		
		<div class="col-lg-11 col-md-12" >
			<br>
			<label for="declaracion"><b>DECLARO BAJO GRAVEDAD DEL JURAMENTO, QUE TODOS LOS DATOS SUMINISTRADOS SON TOTALMENTE CIERTOS</b>  
			<font color="orange">(*)</font></label>
		</div>
		
		<div class="col-lg-1 col-md-12" >
			<br>
			<input class="validate[required] " type="checkbox" name="declaracion" id="declaracion" value="Si" />  
		</div>	

			<!---******************************************************************************
                4	Orden Jurídica
            ***********************************************************************************-->

            <!--   <div class="row">
                  <div class="col-md-12">
                      <div class="well-sm" style="background-color: #003E65; color: #ffffff;"><strong>En caso de cadáveres no identificados y/o cuando no se haya cumplido con el tiempo minimo de permanencia :: </strong></div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-5">
                      <label for="pdf_ordenjudicial">Orden Judicial</label>
                  </div>
                  <div class="col-md-7">
                      <div class="form-group">
                        <input class="btn btn-primary" type="file" name="pdf_ordenjudicial" id="pdf_ordenjudicial" accept=".pdf" />  
                      </div>
                  </div>
              </div> -->
            <!---******************************************************************************
                5	Autorización del Fiscal Correspondiente
            ***********************************************************************************-->
            <!--  <div class="row">
                 <div class="col-md-12">
                     <div class="well-sm" style="background-color: #003E65; color: #ffffff;"><strong>En caso de muerte violenta :: </strong></div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-5">
                     <label for="pdf_autorizacionfiscal">Autorizacion fiscal</label>
                 </div>
                 <div class="col-md-7">
                     <div class="form-group">
                       <input class="btn btn-primary" type="file" name="pdf_autorizacionfiscal" id="pdf_autorizacionfiscal" accept=".pdf" />  
                     </div>
                 </div>
             </div> -->
            <!---******************************************************************************
              seleccione una opcion
           ***********************************************************************************-->
          <!--  <div class="row">
                <div class="col-md-12">
                    <div class="well-sm" style="background-color: #003E65; color: #ffffff;"><strong>seleccione la opcion según corresponda  </strong></div>
                </div>
            </div>  
            <div class="row">
                <div class="col-md-7">
                    <label>Fallecido mayor de 7 años &nbsp;</label><input type="radio" id="mayor" name="opcion" value="mayor"> 
                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fallecido menor de 7 años&nbsp;</label><input type="radio" name="opcion" id="menor" value="menor"> 
                </div>
            </div> 

            <!---******************************************************************************
                6	Certificado permanencia mínimo 4 años a partir de la fecha imhumanicación establecida en los registros del cementerio
            ***********************************************************************************-->
       <!--     <div style="display: none;" id="mayor7">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well-sm" style="background-color: #003E65; color: #ffffff;"><strong>En caso que el fallecido sea mayor de 7 años  </strong></div>
                    </div>
                </div>	
                <div class="row">
                    <div class="col-md-5">
                        <label for="pdf_certificado_per4">Certificado permanencia mínimo 4 años a partir de la fecha imhumanicación establecida en los registros del cementerio</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input class="btn btn-primary" type="file" name="pdf_certificado_per4" id="pdf_certificado_per4" accept=".pdf" />  
                        </div>
                    </div>
                </div>
            </div><!--<7-->
            <!---******************************************************************************
                7	Certificado permanencia mínimo 3 años a partir de la fecha imhumanicación establecida en los registros del cementerio
            ***********************************************************************************-->
         <!--   <div style="display: none;" id="menor7">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well-sm" style="background-color: #003E65; color: #ffffff;"><strong>En caso que el fallecido sea menor de 7 años </strong></div>
                    </div>
                </div>	
                <div class="row">
                    <div class="col-md-5">
                        <label for="pdf_certificado_per3">Certificado permanencia mínimo 3 años a partir de la fecha imhumanicación establecida en los registros del cementerio</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input class="btn btn-primary" type="file" name="pdf_certificado_per3" id="pdf_certificado_per3" accept=".pdf" />  
                        </div>
                    </div>
                </div>
            </div><!--menor7-->

		<div class="col-12 col-md-12 pl-4" align="center">
			<div class="paragraph">
				<br><br>
				<button class="btn green w-100 py-2" type="submit" class="btn btn-primary" role="button">Guardar y Terminar</button>
				<br><br>
				<a class="btn red w-100 py-2" href="<?php echo base_url()?>" class="btn btn-primary" role="button">Regresar</a>

			</div>
		</div>
		
	</div>
    </form>			

    <div class="col-xl-2"></div>
    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #009CDF;">
                    <h5 class="modal-title" style=" color: #ffffff">Atención!!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" role="alert">
                        <p><ion-icon name="add-circle"></ion-icon><b><strong>Información importante</strong></b> 
                        <div id="respuestaMensaje"></div>
                        </p>
                    </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #003e65; color: #ffffff;">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="resultadoAjax"></div>
</div>

			<!--Author: Mario E Beltran mebeltran@saludcapital.gov.co Since: 30052019
			//Modal Infografia y Listado PDF-->
				  <!-- Modal -->
				  <div class="modal fade" id="myModalAdvertencia" role="dialog">
					<div class="modal-dialog">

					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <h4 class="modal-title">Información Importante</h4>
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
						      <div class="alert alert-danger" role="alert">
								<p>
								 <b>Señor Ciudadano!</b><br>
								 Antes de diligenciar el formulario tenga en cuenta lo siguiente:
								 <!--<br> Si cuenta con una orden judicial o autorización fiscal debe dirigirse a la Secretaría Distrital de Salud directamente.-->
								 <br>En caso de que el fallecido sea mayor de 7 años, la permanencia mínima es de 4 años a partir de la fecha inhumación establecida en los registros del cementerio.
								 <br>En caso de que el fallecido sea menor de 7 años, la permanencia mínima es de 3 años a partir de la fecha inhumación establecida en los registros del cementerio.
								 <br><br><b><i>Para las muertes correspondientes a los años anteriores de 1998 y fallecimientos ocurridos fuera de Bogotá, favor diríjase personalmente a la Secretaría Distrital de Salud.</i></b>
								 <br><br>Todo campo con <b>(*)</b> será de carácter obligatorio en el diligenciamiento del formulario. Solo se deben adjuntar documentos en pdf, no superior a 5 megas.
								</p>
							  </div>
						</div>
						<div class="modal-footer">
						<center>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						  <a href="<?php echo base_url('assets/docs/manual_lic_exhum.pdf')?>" class="btn btn-success" target="_blank">Ver Manual del Trámite</a>
						</center>
						</div>
					  </div>

					</div>
				  </div>
            </div>

            
	<div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>			
			
			
<script type="text/javascript">
	//$(window).on('load',function(){
	//	$('#myModalAdvertencia').modal('show');
	//});
</script>
<script type="text/javascript">
    $("#consultarlicencia").click(function (){
    var numeroINH=$("#numero_licencia").val();
    var fechaINH=$("#fechaInh").val();
    var send = $.post("<?php echo base_url('Validacion/validarExistelicenciaInh')?>", {numeroInh: numeroINH,fechaI:fechaINH});

    send.done(function (data) {
        if(data>=1){ 
             $('#consulta').hide();
             $('#ocultar').show();

            }else{
                $('#respuestaMensaje').html("No se encontraron registros con la información ingresada. Por favor verifique que la información este correcta e intente nuevamente. Favor validar esta información con el Cementerio ó comunicarse con la Secretaría Distrital de Salud al teléfono 3649090 Ext 9049 - 9120");
                $('#myModal').modal('show');
                $('#ocultar').hide();
            }
            
            
    });

    send.error(function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Error");
        console.log(XMLHttpRequest,textStatus, errorThrown);

    });
    
    
  });
    
    
    
    
    $("#mayor").click(function () {
        $("#mayor7").show();
        $("#menor7").hide();
    })
    $("#menor").click(function () {
        $("#menor7").show();
        $("#mayor7").hide();
    })

    $("#parentesco").change(function () {
        if ($("#parentesco").val() === "Otro"){
            $("#parentescootro").show();
			$("#parentescootro1").show();
			$("#parentescootro2").show();
        }
        else if ($("#parentesco").val() === "Conyugue"){
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else if ($("#parentesco").val() === "Abuelo(a)"){
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else if ($("#parentesco").val() === "Nieto(a)"){
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
        else{
            $("#parentescootro").hide();
			$("#parentescootro1").hide();
			$("#parentescootro2").hide();
        }
    })
	
	$("#intervienemedlegal").change(function () {
        if ($("#intervienemedlegal").val() === "1"){
            $("#docmedicinalegal").show();
			$("#docmedicinalegal2").show();
			$("#pdf_autorizacionfiscal").attr('required',true);
        }
        else {
            $("#docmedicinalegal").hide();
			$("#docmedicinalegal2").hide();
			$("#pdf_autorizacionfiscal").attr('required',false);
        }
    })
	
	
    function nro(e) {
              var k;
              document.all ? k = e.keyCode : k = e.which;
              if ((e.keyCode == 101) || (e.keyCode == 69) || (e.keyCode == 46) || (e.keyCode == 43) || (e.keyCode == 45))
                   return false;
              return true;

}
 
</script>
