<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('America/Bogota');
/**
 *
 */
class Usuario extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('usuarios_model');
        $this->load->model('login_model');		
        $this->load->database('default');
		$this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form'));
		
		if(!$this->session->userdata('id_usuario') || $this->session->userdata('id_usuario') == ''){
			$this->session->sess_destroy();
			redirect(base_url());
		}
    }

    public function index()
    {

        if($this->session->userdata('fecha_terminos') == ''){
            $data['titulo'] = 'Terminos y Condiciones';
            $data['menu'] = 'NO';
            $data['contenido'] = 'usuario/terminos_view';
            $this->load->view('templates/layout_general',$data);
        }else{
            $data['mis_tramites'] = $this->usuarios_model->mis_tramites($this->session->userdata('id_persona'));
			$data['mistramites_exh'] = $this->mlicencia_exhumacion->mistramites_exh($this->session->userdata('id_persona'));#exhumacion 
			$data['mistramites_rx'] = $this->rx_model->mis_tramites_rx();#rx
			//var_dump($data['mistramites_rx']);
			//exit;
            $data['titulo'] = 'Perfil Usuario';
			$data['js'] = array(base_url('assets/js/usuario.js'));
            $data['contenido'] = 'usuario/perfil_view';
            $this->load->view('templates/layout_general',$data);
        }


    }

    public function seguimientociudadano()
    {
		$id_titulo = $this->input->post('idt');

    	$resultado = $this->usuarios_model->tramites_seguimientociudadanano_id($id_titulo);
			
		echo json_encode($resultado);			


    }

    public function seguimientociudadanoexh()
    {
		$id_licenciaexh = $this->input->post('idlexh');

    	$resultado = $this->mlicencia_exhumacion->licenciaexh_seguimientociudadanano_id($id_licenciaexh);
			
		echo json_encode($resultado);			


    }		

    public function nuevo_tramite()
    {
        if($this->session->userdata('fecha_terminos') == ''){
            $data['titulo'] = 'Terminos y Condiciones';
            $data['menu'] = 'NO';
            $data['contenido'] = 'usuario/terminos_view';
            $this->load->view('templates/layout_tramites',$data);
        }else{
            $data['instituciones'] = $this->login_model->instituciones();
            $data['js'] = array(base_url('assets/js/usuario.js'));
            $data['titulo'] = 'Nuevo Tramite';
            $data['contenido'] = 'usuario/nuevo_tramite_view';
            $this->load->view('templates/layout_tramites',$data);
        }

    }

    public function editarSolicitud($id_titulo)
    {
            $data['datos_tramite'] = $this->usuarios_model->datos_tramite($id_titulo);
            $data['instituciones'] = $this->login_model->instituciones();
            $data['js'] = array(base_url('assets/js/usuario.js'));
            $data['titulo'] = 'Editar Tramite';
            $data['contenido'] = 'usuario/editar_tramite_view';
            $this->load->view('templates/layout_general',$data);

    }

    public function tramite_profesionales()
    {
        $data['instituciones'] = $this->login_model->instituciones();
		$data['profesionesequi'] = $this->login_model->profesionesequi();
		$data['paises'] = $this->login_model->paises();
        $data['js'] = array(base_url('assets/js/usuario.js'));
        $data['titulo'] = 'Licencias profesionales de la salud';
		//Author Mario Beltran mebeltran@saludcapital.gov.co
		//Consulta Tramites generados Oracle since 11062019
		$datosAr2['nume_identificacion'] = $this->session->userdata('nume_identificacion');
		$data['tramitesoracle'] = $this->login_model->validacionUsuario_oracle($datosAr2);
        $data['contenido'] = 'usuario/profesionales_view';
        $this->load->view('templates/layout_general',$data);
    }


    public function guardarTramite(){

		//Author Mario Beltran mebeltran@saludcapital.gov.co since 11062019
		//Ajuste validacion MYSQL no duplicados

		$datosAr2['institucion_educativa'] = $_REQUEST['institucion_educativa'];
		$datosAr2['profesion'] = $_REQUEST['profesion'];
		$datosAr2['titulo_equivalente'] = $_REQUEST['titulo_equivalente'];

		$datosAr2['id_persona'] = $this->session->userdata('id_persona');
		$validacionmsql = $this->usuarios_model->validartramitemsql($datosAr2);

		if(count($validacionmsql)>0){
		    $this->session->set_flashdata('retorno_error', 'Se ha encontrado que se tiene ya un trámite de la misma profesión e institución registrado en el sistema. Si presenta novedades con el trámite ya existente favor comunicarse al correo contactenos@saludcapital.gov.co ');
            redirect(base_url('usuario/'), 'refresh');
            exit;
		}else{


        if (isset($_FILES['pdf_documento']) && $_FILES['pdf_documento']['size'] > 0) {

            $nombre_archivo = "Documento-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_documento')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo 1, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $pdf_documento_existe = $_REQUEST['pdf_documento_existe'];
            $resultadoIDDocumento = $pdf_documento_existe;
        }


        if (isset($_FILES['pdf_titulo']) && $_FILES['pdf_titulo']['size'] > 0) {

            $nombre_archivo = "Titulo-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_titulo')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo 2(".print_r($this->upload->data())."), por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTitulo = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTitulo = 0;
        }


        if (isset($_FILES['pdf_acta']) && $_FILES['pdf_acta']['size'] > 0) {

            $nombre_archivo = "Acta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_acta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo 3, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDActa = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDActa = 0;
        }


        if (isset($_FILES['pdf_tarjeta']) && $_FILES['pdf_tarjeta']['size'] > 0) {

            $nombre_archivo = "Tarjeta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_tarjeta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo 4, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTarjeta = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTarjeta = 0;
        }


        if (isset($_FILES['pdf_resolucion']) && $_FILES['pdf_resolucion']['size'] > 0) {

            $nombre_archivo = "Resolucion-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_resolucion')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo 5, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDResolucion = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDResolucion = 0;
        }


        $data['id_persona'] = $this->session->userdata('id_persona');
        $data['fecha_tramite'] = date('Y-m-d H:i:s');
        $data['tipo_titulo'] = $_REQUEST['tipo_titulo'];
        $data['institucion_educativa'] = strtoupper($_REQUEST['institucion_educativa']);
        $data['profesion'] = strtoupper($_REQUEST['profesion']);
        $data['fecha_term'] = $_REQUEST['fecha_term'];
        $data['tarjeta'] = $_REQUEST['tarjeta'];
        $data['diploma'] = $_REQUEST['diploma'];
        $data['acta'] = $_REQUEST['acta'];
        $data['libro'] = $_REQUEST['libro'];
        $data['folio'] = $_REQUEST['folio'];
		if($data['tipo_titulo']== 1){
			$data['anio'] = $_REQUEST['anio'];
		}
		else{
			$data['anio'] = $_REQUEST['anio2'];
		}
        $data['cod_universidad'] = strtoupper($_REQUEST['cod_universidad']);
        $data['resolucion'] = $_REQUEST['resolucion'];
        $data['fecha_term_ext'] = $_REQUEST['fecha_term_ext'];
        $data['fecha_resolucion'] = $_REQUEST['fecha_resolucion'];
        $data['entidad'] = $_REQUEST['entidad'];
        $data['titulo_equivalente'] = strtoupper($_REQUEST['titulo_equivalente']);
		$data['pais_tituloequi'] = strtoupper($_REQUEST['pais_tituloequi']);
        $data['pdf_documento'] = $resultadoIDDocumento;
        $data['pdf_titulo'] = $resultadoIDTitulo;
        $data['pdf_acta'] = $resultadoIDActa;
        $data['pdf_tarjeta'] = $resultadoIDTarjeta;
        $data['pdf_resolucion'] = $resultadoIDResolucion;
        $data['estado'] = 1;
        $data['terminos'] = 1;

        $resultadoIDRegistroTitulo = $this->usuarios_model->registrarTitulo($data);

        if($resultadoIDRegistroTitulo){

            $dataSeg['id_titulo'] = $resultadoIDRegistroTitulo;
            $dataSeg['id_consecutivo'] = 1;
            $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
            $dataSeg['estado'] = 1;
            $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');

            $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

            if($resultadoIDRegistroSeguimiento){

                $this->session->set_flashdata('retorno_exito', 'Su solicitud se ha registrado con &eacute;xito en el sistema con n&uacute;mero de radicado: <b>'.$resultadoIDRegistroTitulo.'</b>. <br><br>Recuerde que su resoluci&oacute;n estar&aacute; en un plazo m&aacute;ximo de 30 d&iacute;as h&aacute;biles a partir del d&iacute;a de hoy');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }

        }else{

            $this->session->set_flashdata('retorno_error', 'Error al registrar la solicitud, por favor contacte al administrador del sistema');
            redirect(base_url('usuario/'), 'refresh');
            exit;

        }

		}
    }

    public function actualizarTramite($id_titulo){


        if (isset($_FILES['pdf_documento']) && $_FILES['pdf_documento']['size'] > 0) {

            $nombre_archivo = "Documento-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_documento')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDDocumento = 0;
        }



        if (isset($_FILES['pdf_titulo']) && $_FILES['pdf_titulo']['size'] > 0) {

            $nombre_archivo = "Titulo-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_titulo')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTitulo = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTitulo = 0;
        }


        if (isset($_FILES['pdf_acta']) && $_FILES['pdf_acta']['size'] > 0) {

            $nombre_archivo = "Acta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_acta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDActa = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDActa = 0;
        }


        if (isset($_FILES['pdf_tarjeta']) && $_FILES['pdf_tarjeta']['size'] > 0) {

            $nombre_archivo = "Tarjeta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_tarjeta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTarjeta = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTarjeta = 0;
        }


        if (isset($_FILES['pdf_resolucion']) && $_FILES['pdf_resolucion']['size'] > 0) {

            $nombre_archivo = "Resolucion-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pdf_resolucion')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDResolucion = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDResolucion = 0;
        }

        $data['id_titulo'] = $id_titulo;
        $data['id_persona'] = $this->session->userdata('id_persona');
        $data['fecha_tramite'] = date('Y-m-d H:i:s');
        $data['tipo_titulo'] = $_REQUEST['tipo_titulo'];
		if($data['tipo_titulo'] ==1){
			$data['tarjeta'] = $_REQUEST['tarjeta'];
		}
		else{
			$data['tarjeta'] = $_REQUEST['tarjeta2'];
		}
        $data['institucion_educativa'] = $_REQUEST['institucion_educativa'];
        $data['profesion'] = $_REQUEST['profesion2'];
        $data['fecha_term'] = $_REQUEST['fecha_term'];
        $data['tarjeta'] = $_REQUEST['tarjeta'];
        $data['diploma'] = $_REQUEST['diploma'];
        $data['acta'] = $_REQUEST['acta'];
        $data['libro'] = $_REQUEST['libro'];
        $data['folio'] = $_REQUEST['folio'];
        $data['anio'] = $_REQUEST['anio'];
        $data['cod_universidad'] = $_REQUEST['cod_universidad'];
        $data['fecha_term_ext'] = $_REQUEST['fecha_term_ext'];
        $data['resolucion'] = $_REQUEST['resolucion'];
        $data['fecha_resolucion'] = $_REQUEST['fecha_resolucion'];
        $data['entidad'] = $_REQUEST['entidad'];
        $data['titulo_equivalente'] = $_REQUEST['titulo_equivalente2'];
		$data['pais_tituloequi'] = strtoupper($_REQUEST['pais_tituloequi']);
        $data['pdf_documento'] = $resultadoIDDocumento;
        $data['pdf_titulo'] = $resultadoIDTitulo;
        $data['pdf_acta'] = $resultadoIDActa;
        $data['pdf_tarjeta'] = $resultadoIDTarjeta;
        $data['pdf_resolucion'] = $resultadoIDResolucion;
        $data['estado'] = 1;
		$data['fecha_editado'] = $_REQUEST['fecha_editado'];

        $resultadoIDRegistroTitulo = $this->usuarios_model->actualizarTitulo($data);

        if($resultadoIDRegistroTitulo){

            $dataSeg['id_titulo'] = $id_titulo;
            $consecutivo_actual = $this->usuarios_model->consulta_consecutivo($id_titulo);
            $consecutivo = $consecutivo_actual->max_cons + 1;
            $dataSeg['id_consecutivo'] = $consecutivo;
            $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
            $dataSeg['estado'] = 1;
            $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
            $dataSeg['observaciones'] = "Actualización de información por parte del usuario";

            $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

            if($resultadoIDRegistroSeguimiento){

                $this->session->set_flashdata('retorno_exito', 'Su solicitud se ha actualizado con &eacute;xito');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }

        }else{

            $this->session->set_flashdata('retorno_error', 'Error al registrar la solicitud, por favor contacte al administrador del sistema');
            redirect(base_url('usuario/'), 'refresh');
            exit;

        }


    }


	 public function actualizarTramitePrueba($id_titulo){


        if (isset($_FILES['pdf_documento']) && $_FILES['pdf_documento']['size'] > 0) {

            $nombre_archivo = "Documento-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ($this->upload->do_upload('pdf_documento')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDDocumento = 0;
        }



        if (isset($_FILES['pdf_titulo']) && $_FILES['pdf_titulo']['size'] > 0) {

            $nombre_archivo = "Titulo-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ($this->upload->do_upload('pdf_titulo')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTitulo = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTitulo = 0;
        }


        if (isset($_FILES['pdf_acta']) && $_FILES['pdf_acta']['size'] > 0) {

            $nombre_archivo = "Acta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ($this->upload->do_upload('pdf_acta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDActa = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDActa = 0;
        }


        if (isset($_FILES['pdf_tarjeta']) && $_FILES['pdf_tarjeta']['size'] > 0) {

            $nombre_archivo = "Tarjeta-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ($this->upload->do_upload('pdf_tarjeta')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDTarjeta = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDTarjeta = 0;
        }


        if (isset($_FILES['pdf_resolucion']) && $_FILES['pdf_resolucion']['size'] > 0) {

            $nombre_archivo = "Resolucion-".$this->session->userdata('id_persona')."-".date('YmdHis');
            $config['upload_path'] = "uploads/documentos/";
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '30000';
            $config['file_name'] = $nombre_archivo;
            /* Fin Configuracion parametros para carga de archivos */

            // Cargue libreria
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

            if ($this->upload->do_upload('pdf_resolucion')) {
                $upload_data = $this->upload->data();
                $rutaFinal = array('rutaFinal' => $this->upload->data());
            } else {
                $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }

            $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
            $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
            $datosAr['fecha'] = date('Y-m-d');
            $datosAr['tags'] = "";
            $datosAr['es_publico'] = 1;
            $datosAr['estado'] = 'AC';

            $resultadoIDResolucion = $this->usuarios_model->insertarArchivo($datosAr);
        }else{
            $resultadoIDResolucion = 0;
        }

        $data['id_titulo'] = $id_titulo;
        $data['id_persona'] = $this->session->userdata('id_persona');
        $data['fecha_tramite'] = date('Y-m-d H:i:s');
        $data['tipo_titulo'] = $_REQUEST['tipo_titulo'];
		if($data['tipo_titulo'] ==1){
			$data['tarjeta'] = $_REQUEST['tarjeta'];
		}
		else{
			$data['tarjeta'] = $_REQUEST['tarjeta2'];
		}
		
		
        $data['institucion_educativa'] = $_REQUEST['institucion_educativa'];
        $data['profesion'] = $_REQUEST['profesion2'];
        $data['fecha_term'] = $_REQUEST['fecha_term'];
        $data['tarjeta'] = $_REQUEST['tarjeta'];
        $data['diploma'] = $_REQUEST['diploma'];
        $data['acta'] = $_REQUEST['acta'];
        $data['libro'] = $_REQUEST['libro'];
        $data['folio'] = $_REQUEST['folio'];
        $data['anio'] = $_REQUEST['anio'];
        $data['cod_universidad'] = $_REQUEST['cod_universidad'];
        $data['fecha_term_ext'] = $_REQUEST['fecha_term_ext'];
        $data['resolucion'] = $_REQUEST['resolucion'];
        $data['fecha_resolucion'] = $_REQUEST['fecha_resolucion'];
        $data['entidad'] = $_REQUEST['entidad'];
        $data['titulo_equivalente'] = $_REQUEST['titulo_equivalente2'];
		$data['pais_tituloequi'] = strtoupper($_REQUEST['pais_tituloequi']);
        $data['pdf_documento'] = $resultadoIDDocumento;
        $data['pdf_titulo'] = $resultadoIDTitulo;
        $data['pdf_acta'] = $resultadoIDActa;
        $data['pdf_tarjeta'] = $resultadoIDTarjeta;
        $data['pdf_resolucion'] = $resultadoIDResolucion;
        $data['estado'] = 1;
		$data['fecha_editado'] = $_REQUEST['fecha_editado'];

        $resultadoIDRegistroTitulo = $this->usuarios_model->actualizarTitulo($data);
		
        if($resultadoIDRegistroTitulo){

            $dataSeg['id_titulo'] = $id_titulo;
            $consecutivo_actual = $this->usuarios_model->consulta_consecutivo($id_titulo);
            $consecutivo = $consecutivo_actual->max_cons + 1;
            $dataSeg['id_consecutivo'] = $consecutivo;
            $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
            $dataSeg['estado'] = 1;
            $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
            $dataSeg['observaciones'] = "Actualización de información por parte del usuario";

            $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

            if($resultadoIDRegistroSeguimiento){

                $this->session->set_flashdata('retorno_exito', 'Su solicitud se ha actualizado con &eacute;xito');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }

        }else{

            $this->session->set_flashdata('retorno_error', 'Error al registrar la solicitud, por favor contacte al administrador del sistema');
            redirect(base_url('usuario/'), 'refresh');
            exit;

        }


    }


    public function generareposicion(){
        
		//var_dump($_REQUEST['id_titulo']);
		//exit;
		
        $data['id_titulo'] = $_REQUEST['id_titulomodalr'];
        $data['estado'] = 8;
		$data['fecha_reposicion'] = $_REQUEST['fecha_reposicion'];

        $resultadoIDRegistroTitulo = $this->usuarios_model->actualizarEstadoReposicion($data);

        if($resultadoIDRegistroTitulo){

            $dataSeg['id_titulo'] = $_REQUEST['id_titulomodalr'];
            $consecutivo_actual = $this->usuarios_model->consulta_consecutivo($_REQUEST['id_titulomodalr']);
            $consecutivo = $consecutivo_actual->max_cons + 1;
            $dataSeg['id_consecutivo'] = $consecutivo;
            $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
            $dataSeg['estado'] = 8;
            $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
            $dataSeg['observaciones'] = $_REQUEST['observacionreposicion'];

            $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

            if($resultadoIDRegistroSeguimiento){

                $this->session->set_flashdata('retorno_exito', 'Su solicitud de Reposición se ha generado con &eacute;xito');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }

        }else{

            $this->session->set_flashdata('retorno_error', 'Error al registrar la solicitud, por favor contacte al administrador del sistema');
            redirect(base_url('usuario/'), 'refresh');
            exit;

        }


    }



    public function generaaclaracion(){
        
		//var_dump($_REQUEST['id_titulo']);
		//exit;
		
        $data['id_titulo'] = $_REQUEST['id_titulomodala'];
        $data['estado'] = 12;
		$data['fecha_aclaracion'] = $_REQUEST['fecha_aclaracion'];


        $resultadoIDRegistroTitulo = $this->usuarios_model->actualizarEstadoAclaracion($data);

        if($resultadoIDRegistroTitulo){

            $dataSeg['id_titulo'] = $_REQUEST['id_titulomodala'];
            $consecutivo_actual = $this->usuarios_model->consulta_consecutivo($_REQUEST['id_titulomodala']);
            $consecutivo = $consecutivo_actual->max_cons + 1;
            $dataSeg['id_consecutivo'] = $consecutivo;
            $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
            $dataSeg['estado'] = 12;
            $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
            $dataSeg['observaciones'] = $_REQUEST['observacionaclaracion'];
			$dataSeg['tipomotivoaclaracion'] = $_REQUEST['tipomotivoaclaracion'];

            $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);

            if($resultadoIDRegistroSeguimiento){

                $this->session->set_flashdata('retorno_exito', 'Su solicitud de Aclaración se ha generado con &eacute;xito');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }

        }else{

            $this->session->set_flashdata('retorno_error', 'Error al registrar la solicitud, por favor contacte al administrador del sistema');
            redirect(base_url('usuario/'), 'refresh');
            exit;

        }


    }



    public function aceptarTerminos(){

        $acepta = $this->usuarios_model->aceptarTerminos($this->session->userdata('id_persona'));

        $this->session->set_userdata('fecha_terminos',date('Y-m-d'));

        $this->session->set_flashdata('retorno_exito', 'Se aceptaron los términos y condiciones. Ahora puede solicitar el trámite de su interés en la sección de Registrar Trámite.');
        redirect(base_url('usuario/'), 'refresh');
        exit;
    }

    public function cargaMunicipio() {
        if ($this->input->post('departamento')) {
            $departamento = $this->input->post('departamento');
            $ciudades = $this->login_model->municipios_col($departamento);
            ?>
            <option value="">Seleccione...</option>
            <?php
            foreach ($ciudades as $fila) {
                ?>
                <option value="<?php echo $fila->CodigoDane ?>"><?php echo $fila->Descripcion ?></option>
                <?php
            }
        }
    }

    public function guardarTramiteRayosX(){

        var_dump($_REQUEST);


    }
   /**
      * Exhumacion 
      * @ura
      */
    public function tramite_exhumacion()
    {
        $data['js'] = array(base_url('assets/js/exhumacion.js'));
        $data['titulo'] = 'Exhumacion';
        $data['contenido'] = 'usuario/exhumacion_view';
        $this->load->view('templates/layout_general',$data);
    }

     /**
      * Licencia Exhumacion 
      * @ura
      */   
    public function guardarTramiteExhumacion()
    {
		//Author Mario Beltran mebeltran@saludcapital.gov.co since 30012020
		//Ajuste validacion MYSQL no duplicados

		$datosAr2['numero_licencia'] = $_REQUEST['numero_licencia'];
		$datosAr2['fechaInh'] = $_REQUEST['fechaInh'];
		$datosAr2['id_persona'] = $this->session->userdata('id_persona');
		$validacionmsqlExh = $this->usuarios_model->validartramitemsqlExh($datosAr2);
		//var_dump($datosAr2);
		//exit;
		$session = $this->session->userdata('id_usuario');
		
		
		if(count($validacionmsqlExh)>0){
		    $this->session->set_flashdata('retorno_error', 'Se ha encontrado que se tiene ya un trámite de Licencia de Exhumación bajo los mismos criterios de No de Innumación y Fecha de Innumación registrado en el sistema. Si presenta novedades con el trámite ya existente favor comunicarse al correo contactenos@saludcapital.gov.co ');
            redirect(base_url('usuario/'), 'refresh');
            exit;
		}else if (!empty($session)){

            #$error=0;
            #subir cedula solicitante
            if (isset($_FILES['pdf_cedulasolicitante']) && $_FILES['pdf_cedulasolicitante']['size'] > 0) {

                    $nombre_archivo = 'Cedula-'.$this->session->userdata('id_persona')."-".date('YmdHis');
                    #$nombre_archivo="cedula";
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload');
                   $this->upload->initialize($config);

                    if ($this->upload->do_upload('pdf_cedulasolicitante')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                         redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                         //echo $this->upload->display_errors();
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCedula = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCedula = 0;
                }
                
                #subir documento parentesco = otro
            if (isset($_FILES['pdf_otro']) && $_FILES['pdf_otro']['size'] > 0) {

                    $nombre_archivo = "Otro-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    #$nombre_archivo="Otro";
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                    $this->load->library('upload');
                   $this->upload->initialize($config);
                    if ($this->upload->do_upload('pdf_otro')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                         redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                         //echo $this->upload->display_errors();
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDOtro = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDOtro = 0;
                }
                #subir Certificado del Cementerio en donde se encuentra sepultado el cadáver 
                if (isset($_FILES['pdf_certificadocementerio']) && $_FILES['pdf_certificadocementerio']['size'] > 0) {

                    $nombre_archivo1 = 'CertificadoCementerio-'.$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo1;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload');
                   $this->upload->initialize($config);
                    if ($this->upload->do_upload('pdf_certificadocementerio')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }
                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCertificadoCementerio = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCertificadoCementerio = 0;
                }



                
                #Autorización del Fiscal Correspondiente
                if (isset($_FILES['pdf_autorizacionfiscal']) && $_FILES['pdf_autorizacionfiscal']['size'] > 0) {

                    $nombre_archivo3 = "AutorizacionFiscal-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo3;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload', $config);

                    if ($this->upload->do_upload('pdf_autorizacionfiscal')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDAutorizacionFiscal = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDAutorizacionFiscal = 0;
                }

               if($resultadoIDCedula >0 && $resultadoIDCertificadoCementerio > 0){
                $data['id_persona'] = $this->session->userdata('id_persona');
                $data['fecha_solicitud'] = date('Y-m-d H:i:s');
                //$data['fecha_solicitud'] = date('Y-m-d');
                //$data['numero_regdefuncion'] = !empty($_REQUEST['numero_regdefuncion'])? $_REQUEST['numero_regdefuncion']:'';
                #$data['numero_licencia'] = $this->input->post('numero_regdefuncion');
                $data['numero_licencia'] = !empty($_REQUEST['numero_licencia'])? $_REQUEST['numero_licencia']:'';
                //$data['numero_docfallecido'] = isset($_REQUEST['numero_docfallecido'])? $_REQUEST['numero_docfallecido']:'';
                $data['pdf_cedulasolicitante'] = $resultadoIDCedula;
                $data['pdf_certificadocementerio'] = $resultadoIDCertificadoCementerio;
                //$data['pdf_ordenjudicial'] = $resultadoIDOrdenJudicial;
                $data['pdf_autorizacionfiscal'] = $resultadoIDAutorizacionFiscal;
                //$data['pdf_certificado_per4'] = $resultadoIDCertificadoPerm4a;
                //$data['pdf_certificado_per3'] = $resultadoIDCertificadoPermanencia3;
                $data['pdf_otro'] = $resultadoIDOtro;
                $data['estado'] = 1;
                $parenOTRO= isset($_REQUEST['parentescoOTRO'])?$_REQUEST['parentescoOTRO']:'';
                $data['parentesco']= $_REQUEST['parentesco'].'/ '.$parenOTRO;
                $data['fecha_inhumacion']=$_REQUEST['fechaInh'];
				
				$data['interviene_medlegal']=$_REQUEST['intervienemedlegal'];
                $data['declaracion_juramentada']=$_REQUEST['declaracion'];

                $resultadoIDLicenciaExhumacion = $this->mlicencia_exhumacion->registrarSolicitudLicencia($data);
                
                if($resultadoIDLicenciaExhumacion){
                    $dataSeg['idlicencia_exhumacion'] = $resultadoIDLicenciaExhumacion;
                    $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                    $dataSeg['estado'] = 1;
                    $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                    $dataSeg['observaciones'] = "Se realiza solicitud de licencia de exhumación y se adjunta documentación solicitada. -";
                    $resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);

                   $this->session->set_flashdata('retorno_exito', 'Su solicitud se ha registrado con &eacute;xito en el sistema con n&uacute;mero de radicado: <b>'.$resultadoIDLicenciaExhumacion.'</b>. <br><br>Recuerde que su Licencia estar&aacute; en un plazo m&aacute;ximo de 3 d&iacute;as h&aacute;biles a partir del d&iacute;a siguiente del registro.');
                    redirect(base_url('usuario/'), 'refresh');
                    exit;
                }
            }else{
               $this->session->set_flashdata('retorno_error', 'Su solicitud no se ha podido enviar, intentelo nuevamente');
                    redirect(base_url('usuario/'), 'refresh');
                    exit; 
            }
    }

	else{
		$this->session->set_flashdata('retorno_error', 'Su solicitud no se ha podido enviar, Debido a que su sesión a caducado. Ingrese nuevamente al sistema con todos los datos necesarios para el trámite de Licencia de Exhumación para su pronto guardado y envio.');
        redirect(base_url());
	}
	}
	
    
    /***
     * 
     */
    
    public function editarLExh($id_licencia)
    {
            $data['exhumacionfetch'] = $this->mlicencia_exhumacion->exhumacionfetch($id_licencia);
            $data['js'] = array(base_url('assets/js/exhumacion.js'));
            $data['titulo'] = 'Editar Solicitud';
            $data['contenido'] = 'usuario/editar_exhumacion_view';
            $this->load->view('templates/layout_general',$data);

    }
    
    
	public function EditarTramiteExhumacion($id)
    {
            #$error=0;
            #subir cedula solicitante
            if (isset($_FILES['pdf_cedulasolicitante']) && $_FILES['pdf_cedulasolicitante']['size'] > 0) {

                    $nombre_archivo = "Cedula-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    #$nombre_archivo="cedula";
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   //$this->load->library('upload', $config);
                    $this->load->library('upload');
                   $this->upload->initialize($config);

                    if ($this->upload->do_upload('pdf_cedulasolicitante')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                         redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                         //echo $this->upload->display_errors();
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCedula = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCedula = 0;
                }
            #subir documento acreditar parentesco
            if (isset($_FILES['pdf_otro']) && $_FILES['pdf_otro']['size'] > 0) {

                    $nombre_archivo = "Otro-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    #$nombre_archivo="cedula";
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload');
                   $this->upload->initialize($config);
                   
                    if ($this->upload->do_upload('pdf_otro')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                         redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                         //echo $this->upload->display_errors();
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDOtro = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDOtro = 0;
                }
                #subir Certificado del Cementerio en donde se encuentra sepultado el cadáver 
                if (isset($_FILES['pdf_certificadocementerio']) && $_FILES['pdf_certificadocementerio']['size'] > 0) {

                    $nombre_archivo1 = "CertificadoCementerio-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo1;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload');
                   $this->upload->initialize($config);

                    if ($this->upload->do_upload('pdf_certificadocementerio')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }
                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCertificadoCementerio = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCertificadoCementerio = 0;
                }

                   #En caso de cadáveres no identificados y/o cuando no se haya cumplido con el tiempo minimo de permanencia
                if (isset($_FILES['pdf_ordenjudicial']) && $_FILES['pdf_ordenjudicial']['size'] > 0) {

                    $nombre_archivo2 = "Ordenjudicial-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo2;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload', $config);

                    if ($this->upload->do_upload('pdf_ordenjudicial')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDOrdenJudicial = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDOrdenJudicial = 0;
                }

                
                #Autorización del Fiscal Correspondiente
                if (isset($_FILES['pdf_autorizacionfiscal']) && $_FILES['pdf_autorizacionfiscal']['size'] > 0) {

                    $nombre_archivo3 = "AutorizacionFiscal-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo3;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload', $config);

                    if ($this->upload->do_upload('pdf_autorizacionfiscal')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDAutorizacionFiscal = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDAutorizacionFiscal = 0;
                }               

                #subir Certificado permanencia mínimo 4 años a partir de la fecha imhumanicación establecida en los registros del cementerio
                if (isset($_FILES['pdf_certificado_per4']) && $_FILES['pdf_certificado_per4']['size'] > 0) {

                    $nombre_archivo4 = "CertificadoPermanencia4años-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo4;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload', $config);

                    if ($this->upload->do_upload('pdf_certificado_per4')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCertificadoPerm4a = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCertificadoPerm4a = 0;
                }
                
                #Certificado permanencia mínimo 3 años a partir de la fecha imhumanicación establecida en los registros del cementerio
                if (isset($_FILES['pdf_certificado_per3']) && $_FILES['pdf_certificado_per3']['size'] > 0) {

                    $nombre_archivo5 = "CertificadoPermanencia3años-".$this->session->userdata('id_persona')."-".date('YmdHis');
                    $config['upload_path'] = "uploads/exhumacion/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '50000';
                    $config['file_name'] = $nombre_archivo5;
                    /* Fin Configuracion parametros para carga de archivos */

                    // Cargue libreria
                   $this->load->library('upload', $config);

                    if ($this->upload->do_upload('pdf_certificado_per3')) {
                        $upload_data = $this->upload->data();
                        $rutaFinal = array('rutaFinal' => $this->upload->data());
                    } else {
                        $this->session->set_flashdata('retorno_error', "Error al cargar el archivo, por favor contacte al administrador");
                        redirect(base_url('usuario/tramite_exhumacion'), 'refresh');
                        exit;
                    }

                    $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                    $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                    $datosAr['fecha'] = date('Y-m-d');
                    $datosAr['tags'] = "";
                    $datosAr['es_publico'] = 1;
                    $datosAr['estado'] = 'AC';

                    $resultadoIDCertificadoPermanencia3 = $this->usuarios_model->insertarArchivo($datosAr);
                }else{
                    $resultadoIDCertificadoPermanencia3 = 0;
                }

                $data['idlicencia_exhumacion'] = $id;
                $data['id_persona'] = $this->session->userdata('id_persona');
                $data['fecha_solicitud'] = date('Y-m-d H:i:s');
                //$data['fecha_solicitud'] = date('Y-m-d');
                //$data['numero_regdefuncion'] = !empty($_REQUEST['numero_regdefuncion'])?$_REQUEST['numero_regdefuncion']:'';
                #$data['numero_licencia'] = $this->input->post('numero_regdefuncion');
                $data['numero_licencia'] = $_REQUEST['numero_licencia'];
                //$data['numero_docfallecido'] = $_REQUEST['numero_docfallecido'];
                $data['pdf_cedulasolicitante'] = $resultadoIDCedula;
                $data['pdf_certificadocementerio'] = $resultadoIDCertificadoCementerio;
				$data['pdf_ordenjudicial'] = $resultadoIDOrdenJudicial;
                $data['pdf_autorizacionfiscal'] = $resultadoIDAutorizacionFiscal;
                $data['pdf_certificado_per4'] = $resultadoIDCertificadoPerm4a;
                $data['pdf_certificado_per3'] = $resultadoIDCertificadoPermanencia3;
                $data['pdf_otro'] = $resultadoIDOtro;
                $data['estado'] = 1;
                $parenOTRO = isset($_REQUEST['parentescoOTRO'])?$_REQUEST['parentescoOTRO']:'';
                $data['parentesco']= $_REQUEST['parentesco'].' '.$parenOTRO;
                $data['fecha_inhumacion']=$_REQUEST['fechaInh'];
				$data['interviene_medlegal']=$_REQUEST['intervienemedlegal'];

                $resultadoIDLicenciaExhumacion = $this->mlicencia_exhumacion->actualizarTramiteExh($data);
                if($resultadoIDLicenciaExhumacion){
                    $dataSeg['idlicencia_exhumacion'] = $id;
                    $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                    $dataSeg['estado'] = 1;
                    $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                    $dataSeg['observaciones'] = "El ciudadano realiza la actualización de documentos  solicitados en el email enviado por el funcionario.";
                    $resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);

                    
                   $this->session->set_flashdata('retorno_exito', 'Su solicitud se ha Actualizado correctamente <br><br>Recuerde que su tr&aacute;mite estar&aacute; en un plazo m&aacute;ximo de 3 d&iacute;as h&aacute;biles a partir del d&iacute;a de hoy');
                    redirect(base_url('usuario/'), 'refresh');
                    exit;
                }
            else{
               $this->session->set_flashdata('retorno_error', 'El registro no se actualizo, intentelo nuevamente');
                    redirect(base_url('usuario/'), 'refresh');
                    exit; 
            }
    }
    
    /***
     * Perfil funcionario
     */
    
    public function listar(){
            $data['listadosolicitudesExh'] = $this->mlicencia_exhumacion->listadosolicitudesExh($this->session->userdata('id_persona'));#exhumacion
            $data['titulo'] = 'Perfil Usuario';
            $data['contenido'] = 'usuario/solicitudes_licencia_exh_view';
            $this->load->view('templates/layout_general',$data);
    }
    
    public function validacion_exh($id)
    {
        #$data['resultado_validacion'] = $this->coordinacion_model->estado_tramite_coordinador();
        #$data['causales_negacion'] = $this->coordinacion_model->causales_negacion();
        $data['listadosolicitudesExh'] = $this->mlicencia_exhumacion->exhumacionfetch($id);
        $data['documento_preliminar'] = $this->mlicencia_exhumacion->archivo_preliminar($id);
        #$data['documento_preliminar'] = $this->coordinacion_model->archivo_preliminar($id_titulo, $data['tramites_pendientes']->estado);
       
        $data['js'] = array(base_url('assets/js/validacion_coordinador.js'));
        $data['titulo'] = 'Perfil Funcionario';
        $data['contenido'] = 'usuario/validar_solicitudExhumacion_view';
        $this->load->view('templates/layout_general',$data);
    }
    
    
    
    public function guardarAprobacionLicenciaExh($id)
    {
        
        if($_REQUEST['resultado_validacion'] == '2'){
            
            $datosTramite = $this->mlicencia_exhumacion->exhumacionfetch($id);
            
            require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
            $mail = new PHPMailer(true);
            
            $mail->IsSMTP(); // set mailer to use SMTP
            $mail->Host = "172.16.0.238"; // specif smtp server
            $mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
            $mail->Port = 25; // Used instead of 587 when only POP mail is selected
            $mail->SMTPAuth = false;
            $mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
            $mail->Password = "Colombia2018"; // SMTP password
            $mail->FromName = "Trámites en línea";
            $mail->From = "tramiteslinea@saludcapital.gov.co";
            //$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
            $mail->AddAddress($datosTramite->email, $datosTramite->email);
            //$mail->AddAddress("esanchez1988@gmail.com", "Prueba Correo Gmail");
            //$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
            $mail->WordWrap = 50;
            $mail->CharSet = 'UTF-8';
            $mail->IsHTML(true); // set email format to HTML
            $mail->Subject = 'Solicitud de información - Ventanilla unica de tramites y servicios SDS';

            $html = '
              <p>Señor(a)</p>
              <p><b>'.$datosTramite->p_nombre.' '.$datosTramite->p_apellido.',</b></p>

              <p>Una vez realizado el proceso de validación de documentos se encontró la siguiente inconsistencia, por favor ingrese a la plataforma y realice los ajustes correspondientes para continuar con su tramite: 
              <p>
                '.$_REQUEST['mensaje'].'
              </p>
              <p>SECRETARIA DISTRITAL DE SALUD.</p>';

            $mail->Body = nl2br ($html,false);

            if($mail->Send()) {

                $dataEsTi['idlicencia_exhumacion'] = $id;
                $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                $actualizarEstado = $this->mlicencia_exhumacion->actualizarEstado($dataEsTi);
                
                $dataSeg['idlicencia_exhumacion'] = $id;
                $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
                $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');
                $dataSeg['observaciones'] = $_REQUEST['mensaje'];

                $resultadoIDRegistroSeguimiento = $this->mlicencia_exhumacion->registrarNotificacion($dataSeg);
                
                $this->session->set_flashdata('exito', 'Se realizo el registro de la validaci&oacute;n de documentos y se remitio correo al usuario para complementar la información</b>');
                redirect(base_url('usuario/'), 'refresh');
                exit;

            }else{

                $this->session->set_flashdata('retorno_error', 'Error al remitir el correo electr&oacute;nico, por favor verifique que el correo del usuario este correcto.');
                redirect(base_url(), 'refresh');
                exit;

            }
            
        }else{
            
            $ruta_archivos = "uploads/preliminares/";
            $nombre_archivo = "coordinacion-".$id."-".$_REQUEST['resultado_validacion']."-".date('YmdHis').".pdf";

            //var_dump($this->session->userdata());exit;

            //load mPDF library
            $this->load->library('M_pdf');
            $mpdf = new mPDF('c', 'Letter');
            $mpdf->showWatermarkText = false;
            //$mpdf->SetHeader('PENDIENTE APROBACI&Oacute;N  |Contrato: '.$datos['info_contrato'][0]->nom_mpio.' - '.$datos['info_contrato'][0]->nume_contrato.'|{PAGENO}');
            //$mpdf->setFooter('P&aacute;gina {PAGENO} DE {nb}');

            if($_REQUEST['resultado_validacion'] == '3'){
                //Aprobacion
                $datos['idlicencia_exhumacion'] = $id;
                $datos['aprobado'] = $this->session->userdata('id_usuario');
                $datos['numero_lic_exhu'] = $_REQUEST['num_lic_exhumacion'];
                $datos['fecha_aprob'] = date('Y-m-d H:i:s');
                $datos['nombre_difunto'] = $_REQUEST['nombre_difunto'];
                $datos['cementerio'] = $_REQUEST['cementerio'];
                $datos['num_licencia_inhumacion'] = $_REQUEST['num_licencia_inhumacion'];
                $datos['fecha_inhumacion'] = $_REQUEST['fecha_inh'];
                $datos['numero_verificacion'] = $id.$_REQUEST['num_lic_exhumacion'];
                #$datos['id_estado_tramite'] = $_REQUEST['resultado_validacion'];
                $datos['observaciones'] = $_REQUEST['observaciones'];
            
                 $resultIdAprobacion=$this->mlicencia_exhumacion->registro_aprobacionlicencia($datos);
                
                 $datos['info_aprobacionlicencia'] = $this->mlicencia_exhumacion->info_aprobacionlicencia($id);
                 $datos['usuarios'] = $this->mlicencia_exhumacion->usuarios($id);
                 #$datos['firma'] = FALSE;
                 $datos['firma'] = TRUE;

                $mpdf->SetWatermarkText('Aprobado');
                //echo $this->load->view('validacion/resolucion_aprobacion', $datos, true);
                //exit;
                $mpdf->WriteHTML($this->load->view('validacion/resolucion_licencia_exhumacion', $datos, true)); #pendiemte


            }
            $salvar = $mpdf->Output($ruta_archivos.$nombre_archivo, "F");

            if(file_exists ( $ruta_archivos.$nombre_archivo )){
                $datosAr['ruta'] = $ruta_archivos;
                $datosAr['nombre'] = $nombre_archivo;
                $datosAr['fecha'] = date('Y-m-d');
                $datosAr['tags'] = "";
                $datosAr['es_publico'] = 1;
                $datosAr['estado'] = 'AC';

                $resultadoID = $this->mlicencia_exhumacion->insertarArchivo($datosAr);

                if($resultadoID){

                    $dataArRe['idlicencia_exhumacion'] = $id;
                    $dataArRe['id_archivo'] = $resultadoID;
                    $actualizarNumeroArchivo = $this->mlicencia_exhumacion->actualizar_id_archivo($dataArRe);
                    //var_dump($datos['nume_resolucion']);exit;
                    $dataEsTi['idlicencia_exhumacion'] = $id;
                    $dataEsTi['estado'] = $_REQUEST['resultado_validacion'];
                    $actualizarEstado = $this->mlicencia_exhumacion->actualizarEstado($dataEsTi);

                    /*$dataSeg['idlicencia_exhumacion'] = $id;
                    #$consecutivo_actual = $this->coordinacion_model->consulta_consecutivo($id_titulo);
                    #$consecutivo = $consecutivo_actual->id_conse + 1;
                    #$dataSeg['id_consecutivo'] = $consecutivo;
                    $dataSeg['fecha_registro'] = date('Y-m-d H:i:s');
                    $dataSeg['estado'] = $_REQUEST['resultado_validacion'];
                    $dataSeg['id_usuario'] = $this->session->userdata('id_usuario');

                    $resultadoIDRegistroSeguimiento = $this->usuarios_model->registrarSeguimiento($dataSeg);*/

                    $this->session->set_flashdata('exito', 'Se realizo el registro de aprobación de licencia de exhumación</b>');
                    redirect(base_url('usuario/'), 'refresh');
                    exit;
                }
            }else{
                $this->session->set_flashdata('error', 'Error al generar el documento PDF</b>');
                redirect(base_url('usuario/'), 'refresh');
                exit;
            }
            
        }
        
    }

    

	/**
	** RAYOS X
	***/
	
    public function tramite_rayosx()
    {
        $data['departamento'] = $this->rx_model->departamentos_col();
        $data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
        $data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();
        $data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
        $data['nivelAcademico'] = $this->login_model->nivelAcademico();
        $data['js'] = array(base_url('assets/js/rayosx.js'));
        $data['titulo'] = 'Licencias Rayos X';
        //$data['contenido'] = 'usuario/licencias_rx_view';
        $data['contenido'] = 'usuario/rx_crear_tramite';
        $this->load->view('templates/layout_general',$data);		
    }
	
	public function crearTramiteRayosx()
     {		 
		if($_REQUEST['tipo_tramite'] == 1){
			
			$data['user'] = $this->session->userdata('id_usuario');
			 $data['tipo_tramite'] = $_REQUEST['tipo_tramite'];
			 $data['estado'] = 1;
			 $data['created_at'] = date("Y-m-d H:i:s");
			 
			 $resultadoCrearTramite = $this->rx_model->crear_tramite($data);
			 
			 if($resultadoCrearTramite){
				
				$data['departamento'] = $this->rx_model->departamentos_col();
				$data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
				$data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
				$data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();			
				$data['nivelAcademico'] = $this->login_model->nivelAcademico();	
				$data['pr_programas_univ'] = $this->login_model->programas_todos();	
				
				$dataflujo['tramite_id'] = $resultadoCrearTramite;				
				$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
				$dataflujo['id_estado'] = 1;
				$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
				$dataflujo['observaciones'] = "Creación del trámite RAYOS X";
				
				$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo); 
				
				if($resultadoCrearTramiteFlujo){
					$this->session->set_flashdata('retorno_exito', 'Se creo el trámite con exito. Recuerde que su número de trámite es el '.$resultadoCrearTramite.'. Puede iniciar el diligenciamiento de su tramite a traves de la opción "Mis Trámites de Licencia de Equipos RX".</b>');
				}
			}else{
				$this->session->set_flashdata('retorno_error', 'Error al crear el trámite</b>');	
			}
			redirect(base_url('usuario/'), 'refresh');
			exit;
		}else if($_REQUEST['tipo_tramite'] == 2){
			
			if($_REQUEST['nume_tramite'] && $_REQUEST['nume_tramite'] != ''){
				
				$resultadoRenovacion = $this->rx_model->tramite_info($_REQUEST['nume_tramite']); 
				
				if(count($resultadoRenovacion)>0){
					
					if($resultadoRenovacion->estado == 1 || $resultadoRenovacion->estado == 2){
						$this->session->set_flashdata('retorno_error', 'El trámite digitado esta en proceso de creación y/o validación, no es posible crear tramite de renovación</b>');	
					}
					
				}else{
					$this->session->set_flashdata('retorno_error', 'El trámite digitado no existe en el sistema</b>');
				}
				
			}else{
				$this->session->set_flashdata('retorno_error', 'Número de trámite no valido</b>');
			}
			redirect(base_url('usuario/'), 'refresh');
			exit;
		}
	 
		 

     }

    public function rx_editarForm($id_tramite){

        $data['id_tramite'] = $id_tramite;
		$data['tramite_info'] = $this->rx_model->tramite_info($id_tramite);
        $data['departamento'] = $this->rx_model->departamentos_col();
        $data['equipos_radiacion'] = $this->rx_model->equipos_radiacion();
        $data['tipo_visualizacion'] = $this->rx_model->tipo_visualizacion();
        $data['tipo_identificacion_natural'] = $this->login_model->tipo_identificacion();
        $data['nivelAcademico'] = $this->rx_model->nivelAcademico();
        $data['programasAcademicos'] = $this->rx_model->programasAcademicos();
		
		//Verificar si tiene datos por cada paso
		$data['rayosxDireccion'] = $this->rx_model->rayosxDireccion($id_tramite);
		$data['rayosxCategoria'] = $this->rx_model->rayosxCategoria($id_tramite);
		$data['rayosxEquipo'] = $this->rx_model->rayosxEquipo($id_tramite);
		$data['rayosxOficialToe'] = $this->rx_model->rayosxOficialToe($id_tramite);
		$data['rayosxTemporalToe'] = $this->rx_model->rayosxTemporalToe($id_tramite);
		$data['rayosxTalento'] = $this->rx_model->rayosxTalento($id_tramite);
		$data['rayosxObjprueba'] = $this->rx_model->rayosxObjprueba($id_tramite);
		$data['rayosxDocumentos'] = $this->rx_model->rayosxDocumentos($id_tramite);
		
		
        $data['js'] = array(base_url('assets/js/rayosx.js?n='.date('is')));
        $data['titulo'] = 'Licencias Rayos X';
        //$data['contenido'] = 'usuario/licencias_rx_view';
        //$data['contenido'] = 'usuario/rx_editarForm_view';
        $data['contenido'] = 'usuario/rx_editarForm2_view';
        $this->load->view('templates/layout_rx',$data);


    }
	
	public function editarDireccionRX() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
				$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
                redirect(base_url('usuario/tramite_rayosx'), 'refresh');
                exit;
         }else {
			 
			 $rayosxDireccion['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
			 $rayosxDireccion['depto_entidad'] = $_REQUEST['depto_entidad'];
			 $rayosxDireccion['mpio_entidad'] = $_REQUEST['mpio_entidad'];
			 $rayosxDireccion['dire_entidad'] = $_REQUEST['dire_entidad'];
			 $rayosxDireccion['sede_entidad'] = $_REQUEST['sede_entidad'];
			 $rayosxDireccion['email_entidad'] = $_REQUEST['email_entidad'];
			 $rayosxDireccion['celular_entidad'] = $_REQUEST['celular_entidad'];
			 $rayosxDireccion['telefono_entidad'] = $_REQUEST['telefono_entidad'];
			 $rayosxDireccion['extension_entidad'] = $_REQUEST['extension_entidad'];
			 
			 if($_REQUEST['id_direccion_tramite'] != ''){
				 
				 $rayosxDireccion['id_direccion_tramite'] = $_REQUEST['id_direccion_tramite'];
				 $rayosxDireccion['updated_at'] = date("Y-m-d H:i:s");
				 $resultRayosxDireccion = $this->rx_model->updateRayosxDireccion($rayosxDireccion);
			 }else{
				 $rayosxDireccion['created_at'] = date("Y-m-d H:i:s");
				 $resultRayosxDireccion = $this->rx_model->insertRayosxDireccion($rayosxDireccion);
			 }

				if($resultRayosxDireccion){
					$this->session->set_flashdata('exito', 'Se realizo la actualización con exito</b>');

					$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
					$dataAct['modulo'] = "modulo1";
					$dataAct['estado'] = "1";
					$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
					
				}else{
					$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');	
				}				
                redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
                exit;
				 
	
		 }
	}
		
	public function editarCategoriaRX() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
			 
			$respuestaServicio['estado'] = "ERROR";
			$respuestaServicio['mensaje'] = "Error al actualizar el registro";
			$respuestaServicio['btn_menu'] = 0;
			
         }else {
			 
             $dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
             $dataAct['modulo'] = "categoria";
             $dataAct['estado'] = $_REQUEST['categoria'];
             $resultRayosxCategoria = $this->rx_model->actualizarModulo($dataAct);
			 
			 if($resultRayosxCategoria){
										
					$rayosxCategoria = $this->rx_model->rayosxCategoria($_REQUEST['id_tramite_rayosx']);
					$rayosxEquipo = $this->rx_model->rayosxEquipo($_REQUEST['id_tramite_rayosx']);
					
					if(($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2) && count($rayosxEquipo) > 0){
						
						$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
						$dataAct['modulo'] = "modulo2";
						$dataAct['estado'] = "1";
						$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
						
						$respuestaServicio['btn_menu'] = 1;
					}else{
						$respuestaServicio['btn_menu'] = 0;
					}
					
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Actualización exitosa";					
					
                }else{
                    $respuestaServicio['estado'] = "OK";
                    $respuestaServicio['mensaje'] = "Actualización exitosa";
                    $respuestaServicio['btn_menu'] = 0;
                }					 
	
		 }
		 
		 echo json_encode($respuestaServicio);
	}
    
	public function editarEquipoRX() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
				$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
				redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
                exit;
         }else {
			 
			 $rayosxCategoria = $this->rx_model->rayosxCategoria($_REQUEST['id_tramite_rayosx']);
             
             if($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2){
                 $rayosxEquipo['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
                 $rayosxEquipo['categoria1'] = $_REQUEST['categoria1'];
                 $rayosxEquipo['categoria2'] = $_REQUEST['categoria2'];
                 $rayosxEquipo['categoria1_1'] = $_REQUEST['categoria1_1'];
                 $rayosxEquipo['categoria1_2'] = $_REQUEST['categoria1_2'];
                 $rayosxEquipo['categoria2_1'] = $_REQUEST['categoria2_1'];
                 $rayosxEquipo['otro_equipo'] = $_REQUEST['otro_equipo'];
                 $rayosxEquipo['tipo_visualizacion'] = $_REQUEST['tipo_visualizacion'];
                 $rayosxEquipo['marca_equipo'] = $_REQUEST['marca_equipo'];
                 $rayosxEquipo['modelo_equipo'] = $_REQUEST['modelo_equipo'];
                 $rayosxEquipo['serie_equipo'] = $_REQUEST['serie_equipo'];
                 $rayosxEquipo['marca_tubo_rx'] = $_REQUEST['marca_tubo_rx'];
                 $rayosxEquipo['modelo_tubo_rx'] = $_REQUEST['modelo_tubo_rx'];
                 $rayosxEquipo['serie_tubo_rx'] = $_REQUEST['serie_tubo_rx'];
                 $rayosxEquipo['tension_tubo_rx'] = $_REQUEST['tension_tubo_rx'];
                 $rayosxEquipo['contiene_tubo_rx'] = $_REQUEST['contiene_tubo_rx'];
                 $rayosxEquipo['energia_fotones'] = $_REQUEST['energia_fotones'];
                 $rayosxEquipo['energia_electrones'] = $_REQUEST['energia_electrones'];
                 $rayosxEquipo['carga_trabajo'] = $_REQUEST['carga_trabajo'];
                 $rayosxEquipo['ubicacion_equipo'] = $_REQUEST['ubicacion_equipo'];
                 $rayosxEquipo['anio_fabricacion'] = $_REQUEST['anio_fabricacion'];
                 $rayosxEquipo['anio_fabricacion_tubo'] = $_REQUEST['anio_fabricacion_tubo'];
                 $rayosxEquipo['numero_permiso'] = $_REQUEST['numero_permiso'];				 
				 $rayosxEquipo['marca_tubo_rx2'] = $_REQUEST['marca_tubo_rx2'];
                 $rayosxEquipo['modelo_tubo_rx2'] = $_REQUEST['modelo_tubo_rx2'];
                 $rayosxEquipo['serie_tubo_rx2'] = $_REQUEST['serie_tubo_rx2'];
                 $rayosxEquipo['tension_tubo_rx2'] = $_REQUEST['tension_tubo_rx2'];
                 $rayosxEquipo['contiene_tubo_rx2'] = $_REQUEST['contiene_tubo_rx2'];
                 $rayosxEquipo['energia_fotones2'] = $_REQUEST['energia_fotones2'];
                 $rayosxEquipo['energia_electrones2'] = $_REQUEST['energia_electrones2'];
                 $rayosxEquipo['carga_trabajo2'] = $_REQUEST['carga_trabajo2'];
				 $rayosxEquipo['anio_fabricacion_tubo2'] = $_REQUEST['anio_fabricacion_tubo2'];				 
                 $rayosxEquipo['estado'] = 1;
                 
				 $rayosxEquipo['created_at'] = date("Y-m-d H:i:s");
				 $resultRayosxEquipo = $this->rx_model->insertRayosxEquipo($rayosxEquipo);

					if($resultRayosxEquipo){
						
						
						/**
						Cargar archivos del equipo
						*/						
						$bandera = 1;
						$documentos = array("fi_blindajes","fi_control_calidad", "fi_pruebas_caracterizacion", "fi_plano", "fi_blindajes2","fi_control_calidad2", "fi_pruebas_caracterizacion2");
						
						for($i=0;$i<count($documentos);$i++){
							if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {

								$nombre_archivo = $documentos[$i]."-".$_REQUEST['id_tramite_rayosx']."-".$resultRayosxEquipo."-".date('YmdHis');
								$config['upload_path'] = "uploads/rayosx/";
								$config['allowed_types'] = 'pdf';
								$config['max_size'] = '30000';
								$config['file_name'] = $nombre_archivo;
								/* Fin Configuracion parametros para carga de archivos */

								// Cargue libreria
								$this->load->library('upload', $config);
								$this->upload->initialize($config);

								if ($this->upload->do_upload($documentos[$i])) {
									$upload_data = $this->upload->data();
									$rutaFinal = array('rutaFinal' => $this->upload->data());
								} else {
									$bandera = 0;
								}

								$datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
								$datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
								$datosAr['fecha'] = date('Y-m-d');
								$datosAr['tags'] = "";
								$datosAr['es_publico'] = 1;
								$datosAr['estado'] = 'AC';

								$resultadoIDDocumentoArc = $this->usuarios_model->insertarArchivo($datosAr);
								
								if($resultadoIDDocumentoArc){
									
									$datosDoc['id_equipo_rayosx'] = $resultRayosxEquipo;
									$datosDoc['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
									$datosDoc['documento'] = $documentos[$i];
									$datosDoc['id_archivo'] = $resultadoIDDocumentoArc;
									
									$resultadoIDDocumentoCarga = $this->rx_model->actualizarArchivoEquipo($datosDoc);
									
								}
							}
						}

						$rayosxEquipo = $this->rx_model->rayosxEquipo($_REQUEST['id_tramite_rayosx']);

						if(($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2) && count($rayosxEquipo) > 0){

							$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
							$dataAct['modulo'] = "modulo2";
							$dataAct['estado'] = "1";
							$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);

							$respuestaServicio['btn_menu'] = 1;
						}else{
							$respuestaServicio['btn_menu'] = 0;
						}

						$this->session->set_flashdata('exito', 'Se realizo la actualización con exito</b>');
						redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
						exit;					

					}else{
						$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
						redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
						exit;
					}
             }else{
				$this->session->set_flashdata('error', 'Debe guardar primero la categoria a la que pertenecen los equipos</b>');
				redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
				exit;
             } 	
		 }

	}
		
	public function actualizarEquipoRX() {
		
		if($_REQUEST['id_tramite_rayosx'] == NULL || $_REQUEST['id_equipo_rayosx'] == NULL) {
			
				$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
				redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
                exit;
         }else {
			 
			 $rayosxCategoria = $this->rx_model->rayosxCategoria($_REQUEST['id_tramite_rayosx']);
             
             if($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2){
                 $rayosxEquipo['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
                 $rayosxEquipo['id_equipo_rayosx'] = $_REQUEST['id_equipo_rayosx'];
                 $rayosxEquipo['categoria1'] = $_REQUEST['categoria1_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['categoria2'] = $_REQUEST['categoria2_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['categoria1_1'] = $_REQUEST['categoria1_1_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['categoria1_2'] = $_REQUEST['categoria1_2_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['categoria2_1'] = $_REQUEST['categoria2_1_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['otro_equipo'] = $_REQUEST['otro_equipo_eq'.$_REQUEST['id_equipo_rayosx']];
                 $rayosxEquipo['tipo_visualizacion'] = $_REQUEST['tipo_visualizacion'];
                 $rayosxEquipo['marca_equipo'] = $_REQUEST['marca_equipo'];
                 $rayosxEquipo['modelo_equipo'] = $_REQUEST['modelo_equipo'];
                 $rayosxEquipo['serie_equipo'] = $_REQUEST['serie_equipo'];
                 $rayosxEquipo['marca_tubo_rx'] = $_REQUEST['marca_tubo_rx'];
                 $rayosxEquipo['modelo_tubo_rx'] = $_REQUEST['modelo_tubo_rx'];
                 $rayosxEquipo['serie_tubo_rx'] = $_REQUEST['serie_tubo_rx'];
                 $rayosxEquipo['tension_tubo_rx'] = $_REQUEST['tension_tubo_rx'];
                 $rayosxEquipo['contiene_tubo_rx'] = $_REQUEST['contiene_tubo_rx'];
                 $rayosxEquipo['energia_fotones'] = $_REQUEST['energia_fotones'];
                 $rayosxEquipo['energia_electrones'] = $_REQUEST['energia_electrones'];
                 $rayosxEquipo['carga_trabajo'] = $_REQUEST['carga_trabajo'];
                 $rayosxEquipo['ubicacion_equipo'] = $_REQUEST['ubicacion_equipo'];
                 $rayosxEquipo['anio_fabricacion'] = $_REQUEST['anio_fabricacion'];
                 $rayosxEquipo['anio_fabricacion_tubo'] = $_REQUEST['anio_fabricacion_tubo'];
                 $rayosxEquipo['numero_permiso'] = $_REQUEST['numero_permiso'];				 
				 $rayosxEquipo['marca_tubo_rx2'] = $_REQUEST['marca_tubo_rx2'];
                 $rayosxEquipo['modelo_tubo_rx2'] = $_REQUEST['modelo_tubo_rx2'];
                 $rayosxEquipo['serie_tubo_rx2'] = $_REQUEST['serie_tubo_rx2'];
                 $rayosxEquipo['tension_tubo_rx2'] = $_REQUEST['tension_tubo_rx2'];
                 $rayosxEquipo['contiene_tubo_rx2'] = $_REQUEST['contiene_tubo_rx2'];
                 $rayosxEquipo['energia_fotones2'] = $_REQUEST['energia_fotones2'];
                 $rayosxEquipo['energia_electrones2'] = $_REQUEST['energia_electrones2'];
                 $rayosxEquipo['carga_trabajo2'] = $_REQUEST['carga_trabajo2'];
				 $rayosxEquipo['anio_fabricacion_tubo2'] = $_REQUEST['anio_fabricacion_tubo2'];				 
                 $rayosxEquipo['estado'] = 1;                 
				 $rayosxEquipo['updated_at'] = date("Y-m-d H:i:s");
				 $resultRayosxEquipo = $this->rx_model->updateRayosxEquipo($rayosxEquipo);
					
					if($resultRayosxEquipo){
						
						/**
						Cargar archivos del equipo
						*/						
						$bandera = 1;
						$documentos = array("fi_blindajes","fi_control_calidad", "fi_pruebas_caracterizacion", "fi_plano", "fi_blindajes2","fi_control_calidad2", "fi_pruebas_caracterizacion2");
						
						for($i=0;$i<count($documentos);$i++){
							
							if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {
								
								$nombre_archivo = $documentos[$i]."-".$_REQUEST['id_tramite_rayosx']."-".$_REQUEST['id_equipo_rayosx']."-".date('YmdHis');
								$config['upload_path'] = "uploads/rayosx/";
								$config['allowed_types'] = 'pdf';
								$config['max_size'] = '30000';
								$config['file_name'] = $nombre_archivo;
								/* Fin Configuracion parametros para carga de archivos */

								// Cargue libreria
								$this->load->library('upload', $config);
								$this->upload->initialize($config);

								if ($this->upload->do_upload($documentos[$i])) {
									$upload_data = $this->upload->data();
									$rutaFinal = array('rutaFinal' => $this->upload->data());
								} else {
									$bandera = 0;
								}
								
								$datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
								$datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
								$datosAr['fecha'] = date('Y-m-d');
								$datosAr['tags'] = "";
								$datosAr['es_publico'] = 1;
								$datosAr['estado'] = 'AC';
								
								$resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);
								
								if($resultadoIDDocumento){
									
									$datosDoc['id_equipo_rayosx'] = $_REQUEST['id_equipo_rayosx'];
									$datosDoc['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
									$datosDoc['documento'] = $documentos[$i];
									$datosDoc['id_archivo'] = $resultadoIDDocumento;
									
									$resultadoIDDocumentoCarga = $this->rx_model->actualizarArchivoEquipo($datosDoc);
									
								}
							}
						}
						
						$rayosxEquipo = $this->rx_model->rayosxEquipo($_REQUEST['id_tramite_rayosx']);

						if(($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2) && count($rayosxEquipo) > 0){

							$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
							$dataAct['modulo'] = "modulo2";
							$dataAct['estado'] = "1";
							$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);

							$respuestaServicio['btn_menu'] = 1;
						}else{
							$respuestaServicio['btn_menu'] = 0;
						}

						$this->session->set_flashdata('exito', 'Se realizo la actualización con exito</b>');
						redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
						exit;

					}else{
						$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
						redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
						exit;
					}
             }else{
				$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
				redirect(base_url('usuario/rx_editarForm/'.$_REQUEST['id_tramite_rayosx']), 'refresh');
				exit;
             } 	
		 }
	}	
		
	public function listarEquipos(){
		
		$rayosxEquipo = $this->rx_model->rayosxEquipos($_REQUEST['id_tramite_rayosx']);
		$tramite_info = $this->rx_model->tramite_info($_REQUEST['id_tramite_rayosx']);
					
		if($rayosxEquipo){
			?>
			<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Marca Equipo</th>
					<th>Modelo Equipo</th>
					<th>Serie Equipo</th>										
					<th>Ver más</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
			<?php
			for($i=0;$i<count($rayosxEquipo);$i++){
				?>
				<tr>
					<td><?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></td>
					<td><?php echo $rayosxEquipo[$i]->marca_equipo?></td>
					<td><?php echo $rayosxEquipo[$i]->modelo_equipo?></td>
					<td><?php echo $rayosxEquipo[$i]->serie_equipo?></td>										
					<td>
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#verEquipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
						  Ver mas...
						</button>
					</td>
					<td>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editarEquipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
						  Editar
						</button>	
					</td>
					<td>
						<a class="btn btn-danger" href="#" onClick="eliminarEquipo(<?php echo $rayosxEquipo[$i]->id_tramite_rayosx?>,<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">Eliminar</a>
					</td>
				</tr>

				<!-- Modal -->
				<div class="modal fade" id="editarEquipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">	
							<form id="formActualizar<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="formActualizar<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" action="<?php echo base_url('usuario/actualizarEquipoRX')?>" method="post" class="text-left border border-light formActEquipo" onsubmit="validarFormEquipoAct(<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)" enctype="multipart/form-data">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Editar equipo <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<?php
								
								if($tramite_info->categoria == 1){
									$vercateact1 = "block";
									$vercateact2 = "none";
								}else if($tramite_info->categoria == 2){
									$vercateact1 = "none";
									$vercateact2 = "block";
								}
								
								?>
								<input id="id_tramite_rayosx" name="id_tramite_rayosx" type="hidden" value="<?php echo $id_tramite ?>"/>
								<input id="id_equipo_rayosx" name="id_equipo_rayosx" type="hidden" value="<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>"/>
									<div class="row">
										<div class="col" style="display:<?php echo $vercateact1?>" id="div_categoria1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<span class="text-orange">•</span><label for="categoria1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Equipos generadores de radicaci&oacute;n ionizante</label>
											<select id="categoria1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="categoria1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="form-control validate[required]" onchange="act_cambiacat1(<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">
												<option value="">Seleccione...</option>
												<option value="1" <?php if($rayosxEquipo[$i]->categoria1 == 1){echo "selected";}?>>Radiolog&iacute;a odontol&oacute;gica periapical</option>
												<option value="2" <?php if($rayosxEquipo[$i]->categoria1 == 2){echo "selected";}?>>Equipo de RX</option>
											</select>
										</div>
										<div class="col" style="display:<?php echo $vercateact2?>" id="div_categoria2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<label for="categoria2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Equipos generadores de radicaci&oacute;n ionizante</label>
											<select id="categoria2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="categoria2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="form-control validate[required]"  onchange="act_cambiacat2(<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">
												<option value="">Seleccione...</option>
												<option value="1" <?php if($rayosxEquipo[$i]->categoria2 == 1){echo "selected";}?>>Radioterapia</option>
												<option value="2" <?php if($rayosxEquipo[$i]->categoria2 == 2){echo "selected";}?>>Radio diagn&oacute;stico de alta complejidad</option>
												<option value="3" <?php if($rayosxEquipo[$i]->categoria2 == 3){echo "selected";}?>>Radio diagn&oacute;stico de media complejidad</option>
												<option value="4" <?php if($rayosxEquipo[$i]->categoria2 == 4){echo "selected";}?>>Radio diagn&oacute;stico de baja complejidad</option>
												<option value="5" <?php if($rayosxEquipo[$i]->categoria2 == 5){echo "selected";}?>>Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</option>
											</select>
										</div>
									</div>
									<div class="row">			
										<div class="col" style="display:<?php echo $vercateact1?>" id="div_categoria1-1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<label for="categoria1-1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Radiolog&iacute;a odontol&oacute;gica periapical</label>
											<select id="categoria1_1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="categoria1_1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="form-control validate[required]">
												<option value="">Seleccione...</option>
												<option value="1" <?php if($rayosxEquipo[$i]->categoria1_1 == 1){echo "selected";}?>>Equipo de RX odontol&oacute;gico periapical</option>
												<option value="2" <?php if($rayosxEquipo[$i]->categoria1_1 == 2){echo "selected";}?>>Equipo de RX odontol&oacute;gico periapical portat&iacute;l</option>
											</select>
										</div>
										<div class="col" style="display:<?php echo $vercateact1?>" id="div_categoria1-2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<label for="categoria1-2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Equipo de RX</label>
											<select id="categoria1_2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="categoria1_2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="form-control validate[required]">
												<option value="">Seleccione...</option>
												<option value="1" <?php if($rayosxEquipo[$i]->categoria1_2 == 1){echo "selected";}?>>Densit&oacute;metro &oacute;seo</option>
											</select>
										</div>
										<div class="col" style="display:<?php echo $vercateact2?>" id="div_categoria2-1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<label for="categoria2_1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Equipo de RX</label>
											<select id="categoria2_1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="categoria2_1_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="form-control validate[required]" onchange="act_cambiacat2_1(<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)">
												<option value="">Seleccione...</option>
												<option value="1" <?php if($rayosxEquipo[$i]->categoria2_1 == 1){echo "selected";}?>>Equipo de RX convencional</option>
												<option value="2" <?php if($rayosxEquipo[$i]->categoria2_1 == 2){echo "selected";}?>>Tomógrafo Odontológico</option>
												<option value="3" <?php if($rayosxEquipo[$i]->categoria2_1 == 3){echo "selected";}?>>Tomógrafo</option>
												<option value="4" <?php if($rayosxEquipo[$i]->categoria2_1 == 4){echo "selected";}?>>Equipo de RX Portátil</option>
												<option value="5" <?php if($rayosxEquipo[$i]->categoria2_1 == 5){echo "selected";}?>>Equipo de RX Odontológico</option>
												<option value="6" <?php if($rayosxEquipo[$i]->categoria2_1 == 6){echo "selected";}?>>Panorámico Cefálico</option>
												<option value="7" <?php if($rayosxEquipo[$i]->categoria2_1 == 7){echo "selected";}?>>Fluoroscopio</option>
												<option value="8" <?php if($rayosxEquipo[$i]->categoria2_1 == 8){echo "selected";}?>>SPECT-CT</option>
												<option value="9" <?php if($rayosxEquipo[$i]->categoria2_1 == 9){echo "selected";}?>>Arco en C</option>
												<option value="10" <?php if($rayosxEquipo[$i]->categoria2_1 == 10){echo "selected";}?>>Mamógrafo</option>
												<option value="11" <?php if($rayosxEquipo[$i]->categoria2_1 == 11){echo "selected";}?>>Litotriptor</option>
												<option value="12" <?php if($rayosxEquipo[$i]->categoria2_1 == 12){echo "selected";}?>>Angiógrafo</option>
												<option value="13" <?php if($rayosxEquipo[$i]->categoria2_1 == 13){echo "selected";}?>>PET-CT</option>
												<option value="14" <?php if($rayosxEquipo[$i]->categoria2_1 == 14){echo "selected";}?>>Acelerador lineal</option>
												<option value="15" <?php if($rayosxEquipo[$i]->categoria2_1 == 15){echo "selected";}?>>Sistema de radiocirugia robótica</option>
												<option value="16" <?php if($rayosxEquipo[$i]->categoria2_1 == 16){echo "selected";}?>>Otro</option>
											</select>
										</div>
										<div class="col" style="display:none" id="div_categoria2-1-otro_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">
											<label for="otro_equipo_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>">Otro equipo de RX</label>
											<input id="otro_equipo_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="otro_equipo_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" placeholder="Otro equipo" class="form-control input-md validate[minSize[4], maxSize[100]]"  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="100" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 100 carácteres">
										</div>	
									</div>
									<div class="row">
										<div class="col">
											<label for="tipo_visualizacion">Tipo de visualización de la imagen</label>
											<select id="tipo_visualizacion" name="tipo_visualizacion" class="form-control validate[required]" required>
											   <option value="1" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 1){echo "selected";}?>>Digital</option>
											   <option value="2" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 2){echo "selected";}?>>Digitalizado</option>
											   <option value="3" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 3){echo "selected";}?>>Análogo</option>
											   <option value="4" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 4){echo "selected";}?>>Revelado Automático</option>
											   <option value="5" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 5){echo "selected";}?>>Revelado Manual</option>
											   <option value="6" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 6){echo "selected";}?>>Monitor Análogo</option>
											   <option value="7" <?php if($rayosxEquipo[$i]->tipo_visualizacion == 7){echo "selected";}?>>No Aplica</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col">
										   <label for="marca_equipo">Marca equipo</label>
										   <input id="marca_equipo" value="<?php if($rayosxEquipo[$i]->marca_equipo != ''){echo $rayosxEquipo[$i]->marca_equipo;}?>" name="marca_equipo" placeholder="Ingresar Marca equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>
										<div class="col">
										   <label for="modelo_equipo">Modelo equipo</label>
										   <input id="modelo_equipo" value="<?php if($rayosxEquipo[$i]->modelo_equipo != ''){echo $rayosxEquipo[$i]->modelo_equipo;}?>" name="modelo_equipo" placeholder="Ingresar Modelo equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"   onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>
										<div class="col">
										   <label for="serie_equipo">Serie equipo</label>
										   <input id="serie_equipo" value="<?php if($rayosxEquipo[$i]->serie_equipo != ''){echo $rayosxEquipo[$i]->serie_equipo;}?>" name="serie_equipo" placeholder="Ingresar Serie equipo" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>			
									</div>
									<div class="row">
										<div class="col">
										   <label for="marca_tubo_rx">Marca tubo RX</label>
										   <input id="marca_tubo_rx" value="<?php if($rayosxEquipo[$i]->marca_tubo_rx != ''){echo $rayosxEquipo[$i]->marca_tubo_rx;}?>" name="marca_tubo_rx" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]" required type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>

										<div class="col">
										   <label for="modelo_tubo_rx">Modelo tubo RX</label>
										   <input id="modelo_tubo_rx" value="<?php if($rayosxEquipo[$i]->modelo_tubo_rx != ''){echo $rayosxEquipo[$i]->modelo_tubo_rx;}?>" name="modelo_tubo_rx" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>

										<div class="col">
										   <label for="serie_tubo_rx">Serie tubo RX</label>
										   <input id="serie_tubo_rx" value="<?php if($rayosxEquipo[$i]->serie_tubo_rx != ''){echo $rayosxEquipo[$i]->serie_tubo_rx;}?>" name="serie_tubo_rx" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[required, minSize[4], maxSize[30]]"  required  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>
									</div>
									<div class="row">
										<div class="col">
										   <label for="tension_tubo_rx">Tensión máxima tubo RX [kV]</label>
										   <input id="tension_tubo_rx" value="<?php if($rayosxEquipo[$i]->tension_tubo_rx != ''){echo $rayosxEquipo[$i]->tension_tubo_rx;}?>" name="tension_tubo_rx" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]"  required step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;" >
										</div>
										<div class="col">
										   <label for="contiene_tubo_rx">Corriente Max del tubo RX [mA]</label>
										   <input id="contiene_tubo_rx" value="<?php if($rayosxEquipo[$i]->contiene_tubo_rx != ''){echo $rayosxEquipo[$i]->contiene_tubo_rx;}?>" name="contiene_tubo_rx" placeholder="Ingresar corriente máxima del tubo RX [mA]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]" required step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
										</div>
										<div class="col">
										   <label for="energia_fotones">Energ&iacute;a de fotones [MeV]</label>
										   <input id="energia_fotones" value="<?php if($rayosxEquipo[$i]->energia_fotones != ''){echo $rayosxEquipo[$i]->energia_fotones;}?>" name="energia_fotones" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[3]]" required step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
										</div>
									</div>
									<div class="row">
										<div class="col">
										   <label for="energia_electrones">Energ&iacute;a de electrones [MeV]</label>
										   <input id="energia_electrones" value="<?php if($rayosxEquipo[$i]->energia_electrones != ''){echo $rayosxEquipo[$i]->energia_electrones;}?>" name="energia_electrones" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[3]]" required step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
										</div>
										<div class="col">
										   <label for="carga_trabajo">Carga de trabajo [mA.min/semana]</label>
										   <input id="carga_trabajo" value="<?php if($rayosxEquipo[$i]->carga_trabajo != ''){echo $rayosxEquipo[$i]->carga_trabajo;}?>" name="carga_trabajo" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[custom[number], required, minSize[1], maxSize[4]]" required step="any"    min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
										</div>
										<div class="col">
											<label for="ubicacion_equipo">Ubicación del equipo de la instalación</label>
											<input id="ubicacion_equipo" value="<?php if($rayosxEquipo[$i]->ubicacion_equipo != ''){echo $rayosxEquipo[$i]->ubicacion_equipo;}?>" name="ubicacion_equipo" placeholder="Ingresar Ubicación del equipo de la instalación" class="form-control input-md validate[required, minSize[4], maxSize[100]]" required type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="100" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 100 carácteres">
										</div>
									</div>
									<div class="row">
										<div class="col">
										   <label for="anio_fabricacion">A&ntilde;o de fabricación del equipo</label>
										   <input id="anio_fabricacion" value="<?php if($rayosxEquipo[$i]->anio_fabricacion != ''){echo $rayosxEquipo[$i]->anio_fabricacion;}?>" name="anio_fabricacion" placeholder="Ingresar A&ntilde;o de fabricación del equipo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]" onKeyPress="if(this.value.length==4) return false;">
										</div>
										<div class="col">
										   <label for="anio_fabricacion_tubo">A&ntilde;o de fabricación del tubo</label>
										   <input id="anio_fabricacion_tubo" value="<?php if($rayosxEquipo[$i]->anio_fabricacion_tubo != ''){echo $rayosxEquipo[$i]->anio_fabricacion_tubo;}?>" name="anio_fabricacion_tubo" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]]" onKeyPress="if(this.value.length==4) return false;">
										</div>
										<div class="col" id="div_numpermiso">
										   <label for="numero_permiso">Número de permiso de comercialización</label>
										   <input id="numero_permiso" value="<?php if($rayosxEquipo[$i]->numero_permiso != ''){echo $rayosxEquipo[$i]->numero_permiso;}?>" name="numero_permiso" placeholder="Ingresar Número de permiso de comercialización" class="form-control input-md validate[minSize[4], maxSize[30]]" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
										</div>
									</div>
									<div class="row justify-content-md-center">
										<div class="col-md-10">
											<div class="row bg-light text-dark">
												<div class="col-md-4">
													Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_blindajes != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_blindajes);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_blindajes" name="fi_blindajes" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													Informe sobre los resultados del control de calidad
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_control_calidad != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_control_calidad);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_control_calidad" name="fi_control_calidad" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<div class="row bg-light text-dark">
												<div class="col-md-4">
													Plano general de las instalaciones
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_plano != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_plano);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_plano" name="fi_plano" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<?php
											if($tramite_info->categoria == 1){
											?>
											<div class="row">
												<div class="col-md-4">
													Pruebas iniciales de caracterización de los equipos o licencia anterior
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_pruebas_caracterizacion != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_pruebas_caracterizacion);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_pruebas_caracterizacion" name="fi_pruebas_caracterizacion" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<?php
											}
											?>	
										</div>										
									</div>
									<?php
									if($rayosxEquipo[$i]->categoria2_1 == 16){
										$vertubo2 = "block";
									}else{
										$vertubo2 = "none";
									}
									?>	
									<div class="row" id="div_info_tubo2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" style="display:<?php echo $vertubo2?>">	
										<div class="subtitle text-left">
											<h3><b>Descripción tubo Rx 2:</b></h3>
										</div>
										<div class="row">
											<div class="col">
											   <label for="marca_tubo_rx2">Marca tubo RX</label>
											   <input id="marca_tubo_rx2" name="marca_tubo_rx2" placeholder="Ingresar Marca tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
											</div>

											<div class="col">
											   <label for="modelo_tubo_rx2">Modelo tubo RX</label>
											   <input id="modelo_tubo_rx2" name="modelo_tubo_rx2" placeholder="Ingresar Modelo tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"    type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
											</div>

											<div class="col">
											   <label for="serie_tubo_rx2">Serie tubo RX</label>
											   <input id="serie_tubo_rx2" name="serie_tubo_rx2" placeholder="Ingresar Serie tubo RX" class="form-control input-md validate[minSize[4], maxSize[30]]"  type="text" onkeyup="javascript:this.value=this.value.toUpperCase();"   minlength="4" maxlength="30" title="Ingresar un Tamaño mínimo: 4 carácteres a un Tamaño máximo: 30 carácteres">
											</div>
										</div>
										<div class="row">
											<div class="col">
											   <label for="tension_tubo_rx2">Tensión máxima tubo RX [kV]</label>
											   <input id="tension_tubo_rx2" name="tension_tubo_rx2" placeholder="Ingresar Tensión máxima tubo RX [kV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[4]]"   step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;" >
											</div>

											<div class="col">
											   <label for="contiene_tubo_rx2">Corriente Max del tubo RX [mA]</label>
											   <input id="contiene_tubo_rx2" name="contiene_tubo_rx2" placeholder="Ingresar corriente máxima del tubo RX [mA]" class="form-control input-md validate[custom[number], minSize[1], maxSize[4]]"  step="any" min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
											</div>

											<div class="col">
											   <label for="energia_fotones2">Energ&iacute;a de fotones [MeV]</label>
											   <input id="energia_fotones2" name="energia_fotones2" placeholder="Ingresar Energ&iacute;a de fotones [MeV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[3]]"  step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
											</div>
										</div>
										<div class="row">
											<div class="col">
											   <label for="energia_electrones2">Energ&iacute;a de electrones [MeV]</label>
											   <input id="energia_electrones2" name="energia_electrones2" placeholder="Ingresar Energ&iacute;a de electrones [MeV]" class="form-control input-md validate[custom[number], minSize[1], maxSize[3]]"  step="any"    min="0" max="1000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
											</div>

											<div class="col">
											   <label for="carga_trabajo2">Carga de trabajo [mA.min/semana]</label>
											   <input id="carga_trabajo2" name="carga_trabajo2" placeholder="Ingresar Carga de trabajo [mA.min/semana]" class="form-control input-md validate[custom[number],  minSize[1], maxSize[4]]"  step="any"    min="0" max="10000" step="0.01" title="Ingresar un máximo 4 decimales, separador decimal punto ó coma" onKeyPress="if(this.value.length==6) return false;">
											</div>
											<div class="col">
											   <label for="anio_fabricacion_tubo2">A&ntilde;o de fabricación del tubo</label>
											   <input id="anio_fabricacion_tubo2" name="anio_fabricacion_tubo2" placeholder="Ingresar A&ntilde;o de fabricación del tubo" class="form-control input-md validate[custom[number], minSize[4], maxSize[4], min[1900], max[<?php echo date('Y')?>]]" onKeyPress="if(this.value.length==4) return false;">
											</div>
										</div>
									</div>
									<div class="row" id="div_doc_tubo2_eq<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" style="display:<?php echo $vertubo2?>">
										<div class="row justify-content-md-center">
										<div class="col-md-10">
											<div class="row bg-light text-dark">
												<div class="col-md-4">
													Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_blindajes2 != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_blindajes2);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_blindajes2" name="fi_blindajes2" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													Informe sobre los resultados del control de calidad
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_control_calidad2 != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_control_calidad2);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_control_calidad2" name="fi_control_calidad2" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<?php
											if($tramite_info->categoria == 1){
											?>
											<div class="row">
												<div class="col-md-4">
													Pruebas iniciales de caracterización de los equipos o licencia anterior
												</div>
												<div class="col">
												<?php
													if($rayosxEquipo[$i]->fi_pruebas_caracterizacion2 != ''){
														$resultado_archivo = $this->validacion_model->consultar_archivo($rayosxEquipo[$i]->fi_pruebas_caracterizacion2);
														?>
														<a href="<?php echo base_url('uploads/rayosx/'.$resultado_archivo->nombre)?>" target="_blank">
															<img src="<?php echo base_url('assets/imgs/pdf.png')?>" width="40px">
														</a>
														<?php
													}else{
														echo "Sin archivo";
													}
												?>
												</div>
												<div class="col">
													<input id="fi_pruebas_caracterizacion2" name="fi_pruebas_caracterizacion2" type="file" class="form-control-file archivopdf validate[required]">
												</div>
											</div>
											<?php
											}
											?>	
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Actualizar</button>
							</div>
						</form>							
						</div>
					</div>
				</div>
				
				<div class="modal fade" id="verEquipo<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">	
							<form id="formActualizar<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" name="formActualizar<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" action="<?php echo base_url('usuario/actualizarEquipoRX')?>" method="post" class="text-left border border-light formActEquipo" onsubmit="validarFormEquipoAct(<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>)" enctype="multipart/form-data">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Ver equipo <?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p><h2><b>Equipo ID:<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?></b></h2></p>
									<ul>
										<?php
													if($rayosxEquipo[$i]->categoria1 != 0 || $rayosxEquipo[$i]->categoria1 != NULL){
														if($rayosxEquipo[$i]->categoria1 == 1){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiolog&iacute;a odontol&oacute;gica periapical</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria1 == 2){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Equipo de RX</li>
															<?php
														}
													}
											   
													if($rayosxEquipo[$i]->categoria2 != 0 || $rayosxEquipo[$i]->categoria2 != NULL){
														if($rayosxEquipo[$i]->categoria2 == 1){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radioterapia</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2 == 2){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de alta complejidad</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2 == 3){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de media complejidad</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2 == 4){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radio diagn&oacute;stico de baja complejidad</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2 == 5){
															?>
															<li><b>Equipos generadores de radicaci&oacute;n ionizante: </b>Radiografias odontol&oacute;gicas pan&oacute;ramicas y tomografias orales</li>
															<?php
														}
													}
										
													if($rayosxEquipo[$i]->categoria1_1 != 0 || $rayosxEquipo[$i]->categoria1_1 != NULL){
														if($rayosxEquipo[$i]->categoria1_1 == 1){
															?>
															<li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria1_1 == 2){
															?>
															<li><b>Radiolog&iacute;a odontol&oacute;gica periapical: </b>Equipo de RX odontol&oacute;gico periapical portat&iacute;l</li>
															<?php
														}
													}
										
													if($rayosxEquipo[$i]->categoria1_2 != 0 || $rayosxEquipo[$i]->categoria1_2 != NULL){
														if($rayosxEquipo[$i]->categoria1_2 == 1){
															?>
															<li><b>Equipo de RX: </b>Densit&oacute;metro &oacute;seo</li>
															<?php
														}
													}
													
													if($rayosxEquipo[$i]->categoria2_1 != 0 || $rayosxEquipo[$i]->categoria2_1 != NULL){
														if($rayosxEquipo[$i]->categoria2_1 == 1){
															?>
															<li><b>Equipo de RX: </b>Equipo de RX convencional</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 2){
															?>
															<li><b>Equipo de RX: </b>Tomógrafo Odontológico</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 3){
															?>
															<li><b>Equipo de RX: </b>Tomógrafo</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 4){
															?>
															<li><b>Equipo de RX: </b>Equipo de RX Portátil</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 5){
															?>
															<li><b>Equipo de RX: </b>Equipo de RX Odontológico</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 6){
															?>
															<li><b>Equipo de RX: </b>Panorámico Cefálico</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 7){
															?>
															<li><b>Equipo de RX: </b>Fluoroscopio</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 8){
															?>
															<li><b>Equipo de RX: </b>SPECT-CT</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 9){
															?>
															<li><b>Equipo de RX: </b>Arco en C</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 10){
															?>
															<li><b>Equipo de RX: </b>Mamógrafo</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 11){
															?>
															<li><b>Equipo de RX: </b>Litotriptor</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 12){
															?>
															<li><b>Equipo de RX: </b>Angiógrafo</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 13){
															?>
															<li><b>Equipo de RX: </b>PET-CT</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 14){
															?>
															<li><b>Equipo de RX: </b>Acelerador lineal</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 15){
															?>
															<li><b>Equipo de RX: </b>Sistema de radiocirugia robótica</li>
															<?php
														}else if($rayosxEquipo[$i]->categoria2_1 == 16){
															?>
															<li><b>Equipo de RX: </b><?php echo $rayosxEquipo[$i]->otro_equipo?></li>
															<?php
														}
													}					
										
													if($rayosxEquipo[$i]->tipo_visualizacion != 0 || $rayosxEquipo[$i]->tipo_visualizacion != NULL){
														if($rayosxEquipo[$i]->tipo_visualizacion == 1){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Digital</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 2){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Digitalizado</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 3){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Análogo</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 4){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Revelado Automático</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 5){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Revelado Manual</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 6){
															?>
															<li><b>Tipo de visualización de la imagen: </b>Monitor Análogo</li>
															<?php
														}else if($rayosxEquipo[$i]->tipo_visualizacion == 7){
															?>
															<li><b>Tipo de visualización de la imagen: </b>No Aplica</li>
															<?php
														}
													}
												?>
										<li><b>Marca Equipo: </b><?php echo $rayosxEquipo[$i]->marca_equipo?></li>
										<li><b>Modelo Equipo: </b><?php echo $rayosxEquipo[$i]->modelo_equipo?></li>
										<li><b>Serie Equipo: </b><?php echo $rayosxEquipo[$i]->serie_equipo?></li>
										<li><b>Marca Tubo RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx?></li>
										<li><b>Modelo Tubo RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx?></li>
										<li><b>Serie Tubo RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx?></li>
										<li><b>Tensión máxima tubo RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx?></li>
										<li><b>Cont. Max del tubo RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx?></li>
										<li><b>Energía de fotones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones?></li>
										<li><b>Energía de electrones [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones?></li>
										<li><b>Carga de trabajo [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo?></li>
										<li><b>Ubicación del equipo de la instalación: </b><?php echo $rayosxEquipo[$i]->ubicacion_equipo?></li>
										<li><b>Año de fabricación del equipo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion?></li>
										<li><b>Año de fabricación del tubo: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo?></li>
										
										<?php
											
											if($rayosxEquipo[$i]->fi_blindajes != ''){
													$fi_blindajes = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes);
													
													if($fi_blindajes){
														?>
														<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes->nombre);?>" target="_blank">Ver archivo</a></li>		
														<?php
													}else{
														?>
														<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
														<?php
													}		
												}else{
													?>
													<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
													<?php
												}
												
												
												
												if($rayosxEquipo[$i]->fi_control_calidad != ''){
													$fi_control_calidad = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad);
													
													if($fi_control_calidad){
														?>
														<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad->nombre);?>" target="_blank">Ver archivo</a></li>		
														<?php
													}else{
														?>
														<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
														<?php
													}
												}else{
													?>
													<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
													<?php
												}	

												if($rayosxEquipo[$i]->fi_plano != ''){
													$fi_plano = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_plano);
													
													if($fi_plano){
														?>
														<li><b>Plano general de las instalaciones: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_plano->nombre);?>" target="_blank">Ver archivo</a></li>		
														<?php
													}else{
														?>
														<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
														<?php
													}
												}else{
													?>
													<li><b>Plano general de las instalaciones: </b> Sin archivo disponible</li>		
													<?php
												}		
												
												if($rayosxEquipo[$i]->fi_pruebas_caracterizacion != ''){
													$fi_pruebas_caracterizacion = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion);
													
													if($fi_pruebas_caracterizacion){
														?>
														<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion->nombre);?>" target="_blank">Ver archivo</a></li>		
														<?php
													}else{
														?>
														<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
														<?php
													}
												}else{
													?>
													<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
													<?php
												}		
										
											if($rayosxEquipo[$i]->categoria2_1 == 16){
												
												?>
												<h3>Información Tubo RX 2</h3>
												
												<li><b>Marca Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->marca_tubo_rx2?></li>
												<li><b>Modelo Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->modelo_tubo_rx2?></li>
												<li><b>Serie Tubo 2 RX: </b><?php echo $rayosxEquipo[$i]->serie_tubo_rx2?></li>
												<li><b>Tensión máxima tubo 2 RX [kV]: </b><?php echo $rayosxEquipo[$i]->tension_tubo_rx2?></li>
												<li><b>Cont. Max del tubo 2 RX [mA]: </b><?php echo $rayosxEquipo[$i]->contiene_tubo_rx2?></li>
												<li><b>Energía de fotones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_fotones2?></li>
												<li><b>Energía de electrones 2 [MeV]: </b><?php echo $rayosxEquipo[$i]->energia_electrones2?></li>
												<li><b>Carga de trabajo 2 [mA.min/semana]: </b><?php echo $rayosxEquipo[$i]->carga_trabajo2?></li>
												<li><b>Año de fabricación del tubo2: </b><?php echo $rayosxEquipo[$i]->anio_fabricacion_tubo2?></li>
												<?php
																				
												if($rayosxEquipo[$i]->fi_blindajes2 != ''){
														$fi_blindajes2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_blindajes2);
														
														if($fi_blindajes2){
															?>
															<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_blindajes2->nombre);?>" target="_blank">Ver archivo</a></li>		
															<?php
														}else{
															?>
															<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
															<?php
														}		
													}else{
														?>
														<li><b>Descripción de los blindajes estructurales o portátiles y el cálculo del blindaje: </b> Sin archivo disponible</li>		
														<?php
													}
													
													if($rayosxEquipo[$i]->fi_control_calidad2 != ''){
														$fi_control_calidad2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_control_calidad2);
														
														if($fi_control_calidad2){
															?>
															<li><b>Informe sobre los resultados del control de calidad: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_control_calidad2->nombre);?>" target="_blank">Ver archivo</a></li>		
															<?php
														}else{
															?>
															<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
															<?php
														}
													}else{
														?>
														<li><b>Informe sobre los resultados del control de calidad:</b> Sin archivo disponible</li>		
														<?php
													}	

													
													if($rayosxEquipo[$i]->fi_pruebas_caracterizacion2 != ''){
														$fi_pruebas_caracterizacion2 = $this->rx_model->consultar_archivo_equipo($rayosxEquipo[$i]->fi_pruebas_caracterizacion2);
														
														if($fi_pruebas_caracterizacion2){
															?>
															<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b><a href="<?php echo base_url('uploads/rayosx/'.$fi_pruebas_caracterizacion2->nombre);?>" target="_blank">Ver archivo</a></li>		
															<?php
														}else{
															?>
															<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior:  </b> Sin archivo disponible</li>		
															<?php
														}
													}else{
														?>
														<li><b>Pruebas iniciales de caracterización de los equipos o licencia anterior: </b> Sin archivo disponible</li>		
														<?php
													}
											
												
											}
										?>
									</ul>					  
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							</div>
						</form>							
						</div>
					</div>
				</div>
												
				<div id="ex<?php echo $rayosxEquipo[$i]->id_equipo_rayosx?>" class="modal">
				  
				</div>
				<?php
			}
			?>								
			</tbody>
		</table>
			<?php
		}else{
			echo "<p>Sin datos</p>";
		}	
	}
	
	public function eliminarEquipo(){
		
		if(isset($_REQUEST['id_tramite_rayosx']) && isset($_REQUEST['id_equipo_rayosx'])){
			
			$rayosxEquipo = $this->rx_model->rayosxEquipo($_REQUEST['id_tramite_rayosx']);
			
			if(count($rayosxEquipo)>1){
				$dataAct['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
				$dataAct['id_equipo_rayosx'] = $_REQUEST['id_equipo_rayosx'];
				
				$resultRayosxEquipo = $this->rx_model->eliminarEquipo($dataAct);
				
				$rayosxCategoria = $this->rx_model->rayosxCategoria($_REQUEST['id_tramite_rayosx']);
				
				$rayosxEquipo = $this->rx_model->rayosxEquipo($_REQUEST['id_tramite_rayosx']);

				if(($rayosxCategoria->categoria == 1 || $rayosxCategoria->categoria == 2) && count($rayosxEquipo) > 0){

					$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
					$dataAct['modulo'] = "modulo2";
					$dataAct['estado'] = "1";
					$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);

					$respuestaServicio['btn_menu'] = 1;
				}else{
					$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
					$dataAct['modulo'] = "modulo2";
					$dataAct['estado'] = "0";
					$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
					$respuestaServicio['btn_menu'] = 0;
				}
				
				if($resultRayosxEquipo){
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Se elimino el equipo seleccionado";
				}else{
					$respuestaServicio['estado'] = "ERROR";
					$respuestaServicio['mensaje'] = "No fue posible ejecutar la operación";
				}
			}else{
				$respuestaServicio['estado'] = "ERROR";
				$respuestaServicio['mensaje'] = "No fue posible eliminar el equipo, debe existir al menos un equipo para continuar el tramite";
			}
			
			echo json_encode($respuestaServicio);
		}
	}
	
	public function editarOficialToeRX() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
				$respuestaServicio['estado'] = "ERROR";
				$respuestaServicio['mensaje'] = "Error al guardar la información";
				$respuestaServicio['btn_menu'] = 0;
         }else {
			 			 
			 $rayosxEncargado['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
			 $rayosxEncargado['encargado_pnombre'] = $_REQUEST['encargado_pnombre'];
			 $rayosxEncargado['encargado_snombre'] = $_REQUEST['encargado_snombre'];
			 $rayosxEncargado['encargado_papellido'] = $_REQUEST['encargado_papellido'];
			 $rayosxEncargado['encargado_sapellido'] = $_REQUEST['encargado_sapellido'];
			 $rayosxEncargado['encargado_tdocumento'] = $_REQUEST['encargado_tdocumento'];
			 $rayosxEncargado['encargado_ndocumento'] = $_REQUEST['encargado_ndocumento'];
			 $rayosxEncargado['encargado_lexpedicion'] = $_REQUEST['encargado_lexpedicion'];
			 $rayosxEncargado['encargado_correo'] = $_REQUEST['encargado_correo'];
			 $rayosxEncargado['encargado_nivel'] = $_REQUEST['encargado_nivel'];
			 $rayosxEncargado['encargado_profesion'] = $_REQUEST['encargado_profesion'];
			 //$rayosxEncargado['estado'] = 1;
			 
			 if($_REQUEST['id_encargado_rayosx'] != ''){
				 
				 $rayosxEncargado['id_encargado_rayosx'] = $_REQUEST['id_encargado_rayosx'];
				 //$rayosxEncargado['updated_at'] = date("Y-m-d H:i:s");
				 $resultRayosxEncargado = $this->rx_model->updateRayosxEncargado($rayosxEncargado);
			 }else{
				 //$rayosxEncargado['created_at'] = date("Y-m-d H:i:s");
				 $resultRayosxEncargado = $this->rx_model->insertRayosxEncargado($rayosxEncargado);
			 }

				if($resultRayosxEncargado){
					
					$rayosxOficialToe = $this->rx_model->rayosxOficialToe($_REQUEST['id_tramite_rayosx']);
					$rayosxTemporalToe = $this->rx_model->rayosxTemporalToe($_REQUEST['id_tramite_rayosx']);
					
					if(count($rayosxOficialToe) > 0 && count($rayosxTemporalToe) > 0){
						
						$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
						$dataAct['modulo'] = "modulo3";
						$dataAct['estado'] = "1";
						$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
						
						$respuestaServicio['btn_menu'] = 1;
					}else{
						$respuestaServicio['btn_menu'] = 0;
					}
					
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Actualización exitosa";
										
				}else{
					$respuestaServicio['estado'] = "ERROR";
					$respuestaServicio['mensaje'] = "Error al actualizar el registro";					
				}					 
	
		 }
		 echo json_encode($respuestaServicio);
	}
    
	public function editarTrabajadorToeRX() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
				$respuestaServicio['estado'] = "ERROR";
				$respuestaServicio['mensaje'] = "Error al guardar la información";
				$respuestaServicio['btn_menu'] = 0;
         }else {
			 
			 $rayosxEquipo['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
			 $rayosxEquipo['toe_pnombre'] = $_REQUEST['toe_pnombre'];
			 $rayosxEquipo['toe_snombre'] = $_REQUEST['toe_snombre'];
			 $rayosxEquipo['toe_papellido'] = $_REQUEST['toe_papellido'];
			 $rayosxEquipo['toe_sapellido'] = $_REQUEST['toe_sapellido'];
			 $rayosxEquipo['toe_correo'] = $_REQUEST['toe_correo'];
			 $rayosxEquipo['toe_tdocumento'] = $_REQUEST['toe_tdocumento'];
			 $rayosxEquipo['toe_ndocumento'] = $_REQUEST['toe_ndocumento'];
			 $rayosxEquipo['toe_lexpedicion'] = $_REQUEST['toe_lexpedicion'];
			 $rayosxEquipo['toe_nivel'] = $_REQUEST['toe_nivel'];
			 $rayosxEquipo['toe_profesion'] = $_REQUEST['toe_profesion'];
			 $rayosxEquipo['toe_ult_entrenamiento'] = $_REQUEST['toe_ult_entrenamiento'];
			 $rayosxEquipo['toe_pro_entrenamiento'] = $_REQUEST['toe_pro_entrenamiento'];
			 $rayosxEquipo['toe_registro'] = $_REQUEST['toe_registro'];
			 
			 /*if($_REQUEST['id_categoria_rayosx'] != ''){
				 
				 $rayosxEquipo['id_categoria_rayosx'] = $_REQUEST['id_categoria_rayosx'];
				 $rayosxEquipo['updated_at'] = date("Y-m-d H:i:s");
				 $resultRayosxCategoria = $this->rx_model->updateRayosxTemporalToe($rayosxEquipo);
			 }else{*/
				 $rayosxEquipo['created_at'] = date("Y-m-d H:i:s");
				 $resultRayosxTemporalToe = $this->rx_model->insertRayosxTemporalToe($rayosxEquipo);
			 //}

				if($resultRayosxTemporalToe){
					
					$rayosxOficialToe = $this->rx_model->rayosxOficialToe($_REQUEST['id_tramite_rayosx']);
					$rayosxTemporalToe = $this->rx_model->rayosxTemporalToe($_REQUEST['id_tramite_rayosx']);
					
					if(count($rayosxOficialToe) > 0 && count($rayosxTemporalToe) > 0){
						
						$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
						$dataAct['modulo'] = "modulo3";
						$dataAct['estado'] = "1";
						$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
						
						$respuestaServicio['btn_menu'] = 1;
					}else{
						$respuestaServicio['btn_menu'] = 0;
					}
					
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Trabajador agregado con exito";
					
				}else{
					$respuestaServicio['estado'] = "ERROR";
					$respuestaServicio['mensaje'] = "Error al actualizar el registro";						
				}					 
	
		 }
		 echo json_encode($respuestaServicio);
	}
	
	public function listarTrabajadores(){
		
		$rayosxTemporalToe = $this->rx_model->rayosxTemporalToe($_REQUEST['id_tramite_rayosx']);
					
		if($rayosxTemporalToe){
			?>
			<table class="display nowrap table table-hover">
			   <thead>
				  <tr>
					 <th>ID</th>
					 <th>Número Identificación</th>
					 <th>Nombres y Apellidos </th>
					 <th>Ver Más</th>
					 <th>Eliminar</th>
				  </tr>
			   </thead>

			<tbody>
			<?php 
			if(isset($rayosxTemporalToe)){
				for($i=0;$i<count($rayosxTemporalToe);$i++){
					?>
					<tr>
						<td><?php echo $rayosxTemporalToe[$i]->id_toe_rayosx;?></td>
						<td><?php echo $rayosxTemporalToe[$i]->toe_ndocumento;?></td>
						<td><?php echo $rayosxTemporalToe[$i]->toe_pnombre;?> <?php echo $rayosxTemporalToe[$i]->toe_snombre;?> <?php echo $rayosxTemporalToe[$i]->toe_papellido;?> <?php echo $rayosxTemporalToe[$i]->toe_sapellido;?></td>
						<td>
							<a class="btn green" href="#modalToe<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>" rel="modal:open">Ver mas...</a>
						</td>
						<td>
							<a class="btn red" href="#" onClick="eliminarTOE(<?php echo $rayosxTemporalToe[$i]->id_tramite_rayosx?>,<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>)">Eliminar</a>
						</td>
					</tr>
					<div id="modalToe<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?>" class="modal">
					  <p><b>TOE ID:<?php echo $rayosxTemporalToe[$i]->id_toe_rayosx?></b></p>
						<ul>							
							<li><b>Primer Nombre: </b><?php echo $rayosxTemporalToe[$i]->toe_pnombre?></li>
							<li><b>Segundo Nombre: </b><?php echo $rayosxTemporalToe[$i]->toe_snombre?></li>
							<li><b>Primer Apellido: </b><?php echo $rayosxTemporalToe[$i]->toe_papellido?></li>
							<li><b>Segundo Apellido: </b><?php echo $rayosxTemporalToe[$i]->toe_sapellido?></li>
							<li><b>Número de identificación: </b><?php echo $rayosxTemporalToe[$i]->toe_ndocumento?></li>
							<li><b>Lugar Expedición: </b><?php echo $rayosxTemporalToe[$i]->toe_lexpedicion?></li>
							<li><b>Correo: </b><?php echo $rayosxTemporalToe[$i]->toe_correo?></li>
							<li><b>Profesión: </b><?php echo $rayosxTemporalToe[$i]->toe_profesion?></li>
							<li><b>Nivel Académico: </b><?php echo $rayosxTemporalToe[$i]->toe_nivel?></li>
							<li><b>Fecha del último entrenamiento en protección radiológica: </b><?php echo $rayosxTemporalToe[$i]->toe_ult_entrenamiento?></li>
							<li><b>Fecha del próximo entrenamiento en protección radiológica: </b><?php echo $rayosxTemporalToe[$i]->toe_pro_entrenamiento?></li>
							<li><b>Número del registro profesional de salud: </b><?php echo $rayosxTemporalToe[$i]->toe_registro?></li>
						</ul>					  
					  <a href="#" class="btn yellow" rel="modal:close">Cerrar</a>
					</div>
					<?php	
				}	
			}else{
				?>
				<tr>
					<td colspan="6" scope="col">No Existen TOE Registrados</td>
				</tr>	
				<?php
			}
			?>            
			</tbody>
			</table>
			<?php
		}
		
	}
		
	public function eliminarTOE(){
		
		
		if(isset($_REQUEST['id_tramite_rayosx']) && isset($_REQUEST['id_toe_rayosx'])){
			
			$rayosxTOE = $this->rx_model->rayosxTemporalToe($_REQUEST['id_tramite_rayosx']);
			
			if(count($rayosxTOE)>1){
				$dataAct['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
				$dataAct['id_toe_rayosx'] = $_REQUEST['id_toe_rayosx'];
				
				$resultRayosxTOE = $this->rx_model->eliminarTOE($dataAct);
				
				if($resultRayosxTOE){
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Se elimino el trabajador seleccionado";
				}else{
					$respuestaServicio['estado'] = "ERROR";
					$respuestaServicio['mensaje'] = "No fue posible ejecutar la operación";
				}
			}else{
				$respuestaServicio['estado'] = "ERROR";
				$respuestaServicio['mensaje'] = "No fue posible eliminar el trabajador, debe existir al menos un trabajador para continuar el tramite";
			}
			
			echo json_encode($respuestaServicio);
		}
	}
		
	public function editarDirector() {
		
         if($_REQUEST['id_tramite_rayosx'] == NULL) {
				$this->session->set_flashdata('error', 'Error al actualizar el registro</b>');
                redirect(base_url('usuario/tramite_rayosx'), 'refresh');
                exit;
         }else {			
			 if($_REQUEST['visita_previa'] == 1){
				 
				$rayosxVisita['id'] = $_REQUEST['id_tramite_rayosx'];
				$rayosxVisita['visita_previa'] = $_REQUEST['visita_previa'];
				$resultRayosxVisita = $this->rx_model->updateVisita($rayosxVisita); 
				 
				$rayosxDirector['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];				
				$rayosxDirector['talento_pnombre'] = $_REQUEST['talento_pnombre'];
				$rayosxDirector['talento_snombre'] = $_REQUEST['talento_snombre'];
				$rayosxDirector['talento_papellido'] = $_REQUEST['talento_papellido'];
				$rayosxDirector['talento_sapellido'] = $_REQUEST['talento_sapellido'];
				$rayosxDirector['talento_tdocumento'] = $_REQUEST['talento_tdocumento'];
				$rayosxDirector['talento_ndocumento'] = $_REQUEST['talento_ndocumento'];
				$rayosxDirector['talento_lexpedicion'] = $_REQUEST['talento_lexpedicion'];
				$rayosxDirector['talento_correo'] = $_REQUEST['talento_correo'];
				$rayosxDirector['talento_titulo'] = $_REQUEST['talento_titulo'];
				$rayosxDirector['talento_universidad'] = $_REQUEST['talento_universidad'];
				$rayosxDirector['talento_libro'] = $_REQUEST['talento_libro'];
				$rayosxDirector['talento_registro'] = $_REQUEST['talento_registro'];
				$rayosxDirector['talento_fecha_diploma'] = $_REQUEST['talento_fecha_diploma'];
				$rayosxDirector['talento_resolucion'] = $_REQUEST['talento_resolucion'];
				$rayosxDirector['talento_fecha_convalida'] = $_REQUEST['talento_fecha_convalida'];
				$rayosxDirector['talento_nivel'] = $_REQUEST['talento_nivel'];
				$rayosxDirector['talento_titulo_pos'] = $_REQUEST['talento_titulo_pos'];
				$rayosxDirector['talento_universidad_pos'] = $_REQUEST['talento_universidad_pos'];
				$rayosxDirector['talento_libro_pos'] = $_REQUEST['talento_libro_pos'];
				$rayosxDirector['talento_registro_pos'] = $_REQUEST['talento_registro_pos'];
				$rayosxDirector['talento_fecha_diploma_pos'] = $_REQUEST['talento_fecha_diploma_pos'];
				$rayosxDirector['talento_resolucion_pos'] = $_REQUEST['talento_resolucion_pos'];
				$rayosxDirector['talento_fecha_convalida_pos'] = $_REQUEST['talento_fecha_convalida_pos'];
				
				if(isset($_REQUEST['id_director_rayosx']) && $_REQUEST['id_director_rayosx'] != ''){
				 
					 $rayosxDireccion['id_director_rayosx'] = $_REQUEST['id_director_rayosx'];
					 $rayosxDireccion['updated_at'] = date("Y-m-d H:i:s");
					 $resultRayosxDirector = $this->rx_model->updateRayosxDirector($rayosxDirector);
				 }else{
					 $rayosxDireccion['created_at'] = date("Y-m-d H:i:s");
					 $resultRayosxDirector = $this->rx_model->insertRayosxDirector($rayosxDirector);
				 }
				 
				$rayosxTalento = $this->rx_model->rayosxTalento($_REQUEST['id_tramite_rayosx']);
				$rayosxObjprueba = $this->rx_model->rayosxObjprueba($_REQUEST['id_tramite_rayosx']);
				
				if(count($rayosxTalento) > 0 && count($rayosxObjprueba) > 0){
					$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
					$dataAct['modulo'] = "modulo4";
					$dataAct['estado'] = "1";
					$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
					
					$respuestaServicio['btn_menu'] = 1;
				}else{
					$respuestaServicio['btn_menu'] = 0;
				}
				
				$respuestaServicio['estado'] = "OK";
				$respuestaServicio['mensaje'] = "Actualización exitosa";
				 
				 
			 }else{
				$rayosxVisita['id'] = $_REQUEST['id_tramite_rayosx'];
				$rayosxVisita['visita_previa'] = $_REQUEST['visita_previa'];
				$resultRayosxVisita = $this->rx_model->updateVisita($rayosxVisita);  
				
				$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
				$dataAct['modulo'] = "modulo4";
				$dataAct['estado'] = "1";
				$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
				
				$respuestaServicio['btn_menu'] = 1;
				$respuestaServicio['estado'] = "OK";
				$respuestaServicio['mensaje'] = "Actualización exitosa";
			 }	
		 }		 
		 echo json_encode($respuestaServicio);
	}

	public function editarObjetosPrueba(){
		
		if(isset($_REQUEST['id_tramite_rayosx']) && $_REQUEST['id_tramite_rayosx'] == '') {
				$respuestaServicio['estado'] = "ERROR";
				$respuestaServicio['mensaje'] = "Error al agregar el equipo";
				$respuestaServicio['btn_menu'] = 0;
         }else {
			 
			 $rayosxObjeto['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
			 $rayosxObjeto['obj_nombre'] = $_REQUEST['obj_nombre'];
			 $rayosxObjeto['obj_marca'] = $_REQUEST['obj_marca'];
			 $rayosxObjeto['obj_modelo'] = $_REQUEST['obj_modelo'];
			 $rayosxObjeto['obj_serie'] = $_REQUEST['obj_serie'];
			 $rayosxObjeto['obj_calibracion'] = $_REQUEST['obj_calibracion'];
			 $rayosxObjeto['obj_vigencia'] = $_REQUEST['obj_vigencia'];
			 $rayosxObjeto['obj_fecha'] = $_REQUEST['obj_fecha'];
			 $rayosxObjeto['obj_uso'] = $_REQUEST['obj_uso'];
			 $rayosxObjeto['estado'] = 1;
			 
			 $rayosxObjeto['created_at'] = date("Y-m-d H:i:s");
			 $resultRayosxObjeto = $this->rx_model->insertRayosxObjeto($rayosxObjeto);
			 

				if($resultRayosxObjeto){
					
				$rayosxTalento = $this->rx_model->rayosxTalento($_REQUEST['id_tramite_rayosx']);
				$rayosxObjprueba = $this->rx_model->rayosxObjprueba($_REQUEST['id_tramite_rayosx']);
				
					if(count($rayosxTalento) > 0 && count($rayosxObjprueba) > 0){
						$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
						$dataAct['modulo'] = "modulo4";
						$dataAct['estado'] = "1";
						$resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);
						
						$respuestaServicio['btn_menu'] = 1;
					}else{
						$respuestaServicio['btn_menu'] = 0;
					}
				
					$respuestaServicio['estado'] = "OK";
					$respuestaServicio['mensaje'] = "Objeto de prueba agregado con exito";					
					
				}else{
					$respuestaServicio['estado'] = "ERROR2";
					$respuestaServicio['mensaje'] = "Error al agregar el objeto de prueba";
					$respuestaServicio['btn_menu'] = 0;
				}									
	
		 }
		 echo json_encode($respuestaServicio);
	}
	
	public function listarObjetos(){
		
		$rayosxObjprueba = $this->rx_model->rayosxObjprueba($_REQUEST['id_tramite_rayosx']);
					
		if($rayosxObjprueba){
			?>
			<table class="display nowrap table table-hover">
			   <thead>
				  <tr>
					  <th>ID</th>
					  <th>Nombre del equipo</th>
					  <th>Marca del equipo</th>
					  <th>Modelo del equipo</th>
					  <th>Ver Más</th>
					  <th>Eliminar</th>
				  </tr>
			   </thead>

			<tbody>
			<?php 
			if(isset($rayosxObjprueba)){
				for($i=0;$i<count($rayosxObjprueba);$i++){
					?>
					<tr>
						<td><?php echo $rayosxObjprueba[$i]->id_obj_rayosx;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_nombre;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_marca;?></td>
						<td><?php echo $rayosxObjprueba[$i]->obj_modelo;?></td>
						<td>
							<a class="btn green" href="#modalObj<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>" rel="modal:open">Ver mas...</a>
						</td>
						<td>
							<a class="btn red" href="#" onClick="eliminarObj(<?php echo $rayosxObjprueba[$i]->id_tramite_rayosx?>,<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>)">Eliminar</a>
						</td>
					</tr>
					<div id="modalObj<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?>" class="modal">
					  <p><b>Equipo Objeto de prueba ID:<?php echo $rayosxObjprueba[$i]->id_obj_rayosx?></b></p>
						<ul>							
							<li><b>Nombre del Equipo: </b><?php echo $rayosxObjprueba[$i]->obj_nombre?></li>
							<li><b>Marca del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_marca?></li>
							<li><b>Modelo del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_modelo?></li>
							<li><b>Serie del equipo: </b><?php echo $rayosxObjprueba[$i]->obj_serie?></li>
							<li><b>Calibración: </b><?php echo $rayosxObjprueba[$i]->obj_calibracion?></li>
							<li><b>Vigencia de calibración: </b><?php echo $rayosxObjprueba[$i]->obj_vigencia?></li>
							<li><b>Fecha de calibración: </b><?php echo $rayosxObjprueba[$i]->obj_fecha?></li>
							<li><b>Manual Técnico y ficha Técnica: </b><?php echo $rayosxObjprueba[$i]->obj_manual?></li>
							<li><b>Usos: </b><?php echo $rayosxObjprueba[$i]->obj_uso?></li>							
						</ul>					  
					  <a href="#" class="btn yellow" rel="modal:close">Cerrar</a>
					</div>
					<?php	
				}	
			}else{
				?>
				<tr>
					<td colspan="6" scope="col">No Existen Objetos de prueba Registrados</td>
				</tr>	
				<?php
			}
			?>            
			</tbody>
			</table>
			<?php
		}
		
	}
	
	public function editarDocumentos1(){
        
        $bandera = 1;
        $documentos = array("pn_doc","pj_doc","pj_cyc","fi_doc_encargado","fi_diploma_encargado",
                           "fi_registro_dosimetrico","fi_registro_niveles","fi_certificado_capacitacion","fi_programa_capacitacion",
                           "fi_procedimiento_mantenimiento","fi_pruebas_caracterizacion","fi_programa_tecno","fi_programa_proteccion","fi_soporte_talento",
                           "fi_diploma_director","fi_res_convalida_director","fi_diploma_pos_profe","fi_res_convalida_profe","fi_cert_calibracion",
                           "fi_declaraciones");
        
        for($i=0;$i<count($documentos);$i++){
            if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {

                $nombre_archivo = $documentos[$i]."-".$_REQUEST['id_tramite_rayosx']."-".date('YmdHis');
                $config['upload_path'] = "uploads/rayosx/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '30000';
                $config['file_name'] = $nombre_archivo;
                /* Fin Configuracion parametros para carga de archivos */

                // Cargue libreria
                $this->load->library('upload', $config);
				$this->upload->initialize($config);
				
                if ($this->upload->do_upload($documentos[$i])) {
                    $upload_data = $this->upload->data();
                    $rutaFinal = array('rutaFinal' => $this->upload->data());
                } else {
                    $bandera = 0;
                }

                $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                $datosAr['fecha'] = date('Y-m-d');
                $datosAr['tags'] = "";
                $datosAr['es_publico'] = 1;
                $datosAr['estado'] = 'AC';

                $resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);

                $datosDoc['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
                $datosDoc['documento'] = $documentos[$i];
                $datosDoc['path'] = $rutaFinal['rutaFinal']['full_path'];
                $datosDoc['categoria'] = $_REQUEST['categoria_docs'];
                $datosDoc['id_archivo'] = $resultadoIDDocumento;
                $datosDoc['estado'] = 1;
                $datosDoc['created_at'] = date("Y-m-d H:i:s");
				$resultRayosxArc = $this->rx_model->inactivarDocumentoPrevios($datosDoc);
                $resultRayosxObjeto = $this->rx_model->insertDocumento($datosDoc);

            }
        }
        
        if($bandera = 1){
            
            $dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
            $dataAct['modulo'] = "modulo5";
            $dataAct['estado'] = "1";
            $resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);

            if($resultRayosxDireccion){
                $this->session->set_flashdata('retorno_exito', 'Documentos cargados con exito, por favor finalice el tr&aacute;mite para enviar a la Secretaria de Salud.</b>');
            }else{
                $this->session->set_flashdata('retorno_error', 'Error al enviar los documentos</b>');	
            }
        }else{
            $this->session->set_flashdata('retorno_error', 'Favor verificar, no fue posible cargar todos los documentos.</b>');
        }
		
		
		redirect(base_url('usuario/rx_editarForm/'.$dataAct['id_tramite']), 'refresh');
		exit;
	}
	
	public function editarDocumentos2(){
		
        $bandera = 1;
        $documentos = array("pn_doc","pj_doc","pj_cyc","fi_doc_oficial","fi_diploma_oficial",
                           "fi_registro_dosimetrico","fi_soporte_talento","fi_diploma_director","fi_res_convalida_director","fi_diploma_pos_profe","fi_res_convalida_profe","fi_cert_calibracion",
                           "fi_declaraciones","visita_certificado","visita_prote_radio","visita_niveles","visita_elementos","visita_procedimientos",
									   "visita_resultados","visita_vigilancia_radio","visita_tecnovigilancia","visita_docu_prote");

        for($i=0;$i<count($documentos);$i++){
            if (isset($_FILES[$documentos[$i]]) && $_FILES[$documentos[$i]]['size'] > 0) {

                $nombre_archivo = $documentos[$i]."-".$_REQUEST['id_tramite_rayosx']."-".date('YmdHis');
                $config['upload_path'] = "uploads/rayosx/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '30000';
                $config['file_name'] = $nombre_archivo;
                /* Fin Configuracion parametros para carga de archivos */
                // Cargue libreria
                $this->load->library('upload', $config);
				$this->upload->initialize($config);
				
                if ($this->upload->do_upload($documentos[$i])) {
                    $upload_data = $this->upload->data();
                    $rutaFinal = array('rutaFinal' => $this->upload->data());					
                } else {
                    $bandera = 0;
                }

                $datosAr['ruta'] = $rutaFinal['rutaFinal']['full_path'];
                $datosAr['nombre'] = $rutaFinal['rutaFinal']['file_name'];
                $datosAr['fecha'] = date('Y-m-d');
                $datosAr['tags'] = "";
                $datosAr['es_publico'] = 1;
                $datosAr['estado'] = 'AC';

                $resultadoIDDocumento = $this->usuarios_model->insertarArchivo($datosAr);

                $datosDoc['id_tramite_rayosx'] = $_REQUEST['id_tramite_rayosx'];
                $datosDoc['documento'] = $documentos[$i];
                $datosDoc['path'] = $rutaFinal['rutaFinal']['full_path'];
                $datosDoc['categoria'] = $_REQUEST['categoria_docs'];
                $datosDoc['id_archivo'] = $resultadoIDDocumento;
                $datosDoc['estado'] = 1;
                $datosDoc['created_at'] = date("Y-m-d H:i:s");
				$resultRayosxArc = $this->rx_model->inactivarDocumentoPrevios($datosDoc);
                $resultRayosxObjeto = $this->rx_model->insertDocumento($datosDoc);

            }
        }

        if($bandera = 1){
            
            $dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
            $dataAct['modulo'] = "modulo5";
            $dataAct['estado'] = "1";
            $resultRayosxDireccion = $this->rx_model->actualizarModulo($dataAct);

            if($resultRayosxDireccion){
                $this->session->set_flashdata('retorno_exito', 'Documentos cargados con exito, por favor finalice el tr&aacute;mite para enviar a la Secretaria de Salud.</b>');
            }else{
                $this->session->set_flashdata('retorno_error', 'Error al enviar los documentos</b>');	
            }
        }else{
            $this->session->set_flashdata('retorno_error', 'Favor verificar, no fue posible cargar todos los documentos.</b>');
        }
		
		
		redirect(base_url('usuario/rx_editarForm/'.$dataAct['id_tramite']), 'refresh');
		exit;
	}

	public function completarTramiteRayosx(){
		
		$tramite_info = $this->rx_model->tramite_info($_REQUEST['id_tramite_rayosx']);
		
		if($tramite_info->estado == 1){
			
			$data['id'] = $_REQUEST['id_tramite_rayosx'];
			$data['estado'] = 2;
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "2";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "fecha_envio";
			$dataAct['estado'] = date('Y-m-d H:i:s');
			$resultRayosxFechaEnvio = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "notificacion";
			$dataAct['estado'] = $_REQUEST['notificacion'];
			$resultRayosxNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "correo_notificacion";
			$dataAct['estado'] = $_REQUEST['correo_notificacion'];
			$resultRayosxCorreoNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataflujo['tramite_id'] = $_REQUEST['id_tramite_rayosx'];				
			$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
			$dataflujo['id_estado'] = 2;
			$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
			$dataflujo['observaciones'] = "Finalización del trámite RAYOS X";
			
			$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);
			
		}else if($tramite_info->estado == 13){
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "14";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "fecha_envio_subsanacion1";
			$dataAct['estado'] = date('Y-m-d H:i:s');
			$resultRayosxFechaEnvio = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "notificacion";
			$dataAct['estado'] = $_REQUEST['notificacion'];
			$resultRayosxNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "correo_notificacion";
			$dataAct['estado'] = $_REQUEST['correo_notificacion'];
			$resultRayosxCorreoNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataflujo['tramite_id'] = $_REQUEST['id_tramite_rayosx'];				
			$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
			$dataflujo['id_estado'] = 14;
			$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
			$dataflujo['observaciones'] = "Subsanación primera intancia completo";
			
			$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);
		}else if($tramite_info->estado == 22){
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = "23";
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "fecha_envio_subsanacion2";
			$dataAct['estado'] = date('Y-m-d H:i:s');
			$resultRayosxFechaEnvio = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "notificacion";
			$dataAct['estado'] = $_REQUEST['notificacion'];
			$resultRayosxNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataAct['id_tramite'] = $_REQUEST['id_tramite_rayosx'];
			$dataAct['modulo'] = "correo_notificacion";
			$dataAct['estado'] = $_REQUEST['correo_notificacion'];
			$resultRayosxCorreoNotificacion = $this->rx_model->actualizarModulo($dataAct);
			
			$dataflujo['tramite_id'] = $_REQUEST['id_tramite_rayosx'];				
			$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
			$dataflujo['id_estado'] = 23;
			$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
			$dataflujo['observaciones'] = "Subsanación segunda intancia completo";
			
			$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);
		}
		
		
		
		if($resultRayosxEstado){
			if($_REQUEST['notificacion'] == 1){
				$mensajeAdi = "Recuerde que, al seleccionar la opción de notificación electrónica, puede ser notificado y/o comunicado a través de su correo electrónico, recomendamos revisar periódicamente las carpetas spam o correo no deseado de su email.";
			}else{
				$mensajeAdi = "";
			}
			$this->session->set_flashdata('retorno_exito', 'Se envio el formulario con exito. Recuerde que su número de radicación es el '.$_REQUEST['id_tramite_rayosx'].'. Puede hacer seguimiento de su tramite a traves de la opción "Mis Trámites" del menú principal. '.$mensajeAdi.'</b>');
		}else{
			$this->session->set_flashdata('retorno_error', 'Error al enviar el formulario</b>');	
		}
		
		redirect(base_url('usuario/'), 'refresh');
		exit;
		
	}
	
	public function limpiarCategoria($id_tramite){
		
		$dataAct['id_tramite'] = $id_tramite;
		$dataAct['modulo'] = "categoria";
		$dataAct['estado'] = "0";
		$resultRayosxCategoria = $this->rx_model->actualizarModulo($dataAct);
		
		if($resultRayosxCategoria){
			
			$dataEli['id_tramite_rayosx'] = $id_tramite;
			
			$resultRayosxEquipos = $this->rx_model->eliminarEquiposTodos($dataEli);
			
			if($resultRayosxEquipos){
				$this->session->set_flashdata('retorno_exito', 'Se realizo la actualización de la categoría con éxito y se inactivaron los equipos previamente registrados');	
			}else{
				$this->session->set_flashdata('retorno_exito', 'Se realizo la actualización de la categoría con éxito, pero no fue posible inactivar los equipos');
			}
			
		}else{
			$this->session->set_flashdata('retorno_error', 'Error al realizar la actualización</b>');	
		}
		
		redirect(base_url('usuario/rx_editarForm/'.$id_tramite), 'refresh');
		exit;
		
	}
	
	public function solicitarProrroga($id_tramite){
		
		
		if(isset($id_tramite)){
			
			$dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "solicita_prorroga";
			$dataAct['estado'] = 1;
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			if($resultRayosxEstado){
				$this->session->set_flashdata('retorno_exito', 'Se realizo el registro de la solicitud de prorroga, recuerde que cuenta con 20 días adicionales para subsanar el trámite');					
			}else{
				$this->session->set_flashdata('retorno_error', 'No fue posible ejecutar la operación');					
			}
			
			redirect(base_url('usuario/'), 'refresh');
			exit;
		}
	}
	
	public function anularTramite($id_tramite){
		
		
		if(isset($id_tramite)){
			
			$dataAct['id_tramite'] = $id_tramite;
			$dataAct['modulo'] = "estado";
			$dataAct['estado'] = 8;
			$resultRayosxEstado = $this->rx_model->actualizarModulo($dataAct);
			
			$dataflujo['tramite_id'] = $id_tramite;				
			$dataflujo['id_usuario'] = $this->session->userdata('id_usuario');
			$dataflujo['id_estado'] = 8;
			$dataflujo['fecha_estado'] = date('Y-m-d H:i:s');				
			$dataflujo['observaciones'] = "Usuario solicita anular trámite";
			
			$resultadoCrearTramiteFlujo = $this->rx_model->crear_tramite_flujo($dataflujo);
			
			if($resultRayosxEstado){
				$this->session->set_flashdata('retorno_exito', 'Se realizo el registro de la anulación del trámite');					
			}else{
				$this->session->set_flashdata('retorno_error', 'No fue posible ejecutar la operación');					
			}
			
			redirect(base_url('usuario/'), 'refresh');
			exit;
		}
	}
	
}