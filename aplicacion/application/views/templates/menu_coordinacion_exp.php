<nav class="navbar navbar-expand-md mt-2">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
				<div class="navbar-collapse" id="navbarCollapse">
                    <div class="mr-2">
                        <div class="locator mb-2">
                            <div class="point"><span></span></div>
                        </div>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('usuario/')?>">Mis Trámites</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('usuario/nuevo_tramite')?>">Registrar Trámite</a>
                            </li>
							
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('registro/cambiar_clave_usuario')?>">Cambiar Contrase&ntilde;a</a>
							</li>
							
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('login/logout_ci')?>">Cerrar sesi&oacute;n</a>
							</li>
						</ul>
                    </div>
                </div>	
</nav>